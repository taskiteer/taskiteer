-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2016 at 01:06 PM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nail`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stylist_id` int(11) NOT NULL DEFAULT '0',
  `service_areaid` int(11) NOT NULL,
  `service_id` int(11) NOT NULL DEFAULT '0',
  `service_locationid` int(11) NOT NULL,
  `time` varchar(50) NOT NULL,
  `booking_date` date NOT NULL,
  `create_date` datetime NOT NULL,
  `status` enum('P','A','R','C') NOT NULL DEFAULT 'P' COMMENT 'p=pending,A=accept,R=reject,C=cancel',
  `payment_status` enum('pay','pending') NOT NULL DEFAULT 'pending',
  `amount` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `stylist_id`, `service_areaid`, `service_id`, `service_locationid`, `time`, `booking_date`, `create_date`, `status`, `payment_status`, `amount`) VALUES
(1, 42, 46, 1, 3, 1, '3', '0000-00-00', '0000-00-00 00:00:00', 'A', 'pay', 0.00),
(2, 42, 0, 1, 3, 2, '5', '0000-00-00', '0000-00-00 00:00:00', 'P', 'pay', 0.00),
(3, 42, 0, 1, 3, 2, '5', '2016-07-28', '0000-00-00 00:00:00', 'P', 'pay', 0.00),
(4, 42, 0, 1, 3, 2, '1', '2016-07-29', '0000-00-00 00:00:00', 'P', 'pending', 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `booking_requests`
--

CREATE TABLE IF NOT EXISTS `booking_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `stylist_id` int(11) NOT NULL,
  `request_date` datetime NOT NULL,
  `accept_date` datetime NOT NULL,
  `status` enum('A','R','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `booking_requests`
--

INSERT INTO `booking_requests` (`id`, `booking_id`, `stylist_id`, `request_date`, `accept_date`, `status`) VALUES
(1, 1, 46, '2016-07-27 00:00:00', '0000-00-00 00:00:00', 'N'),
(2, 2, 46, '2016-07-28 00:00:00', '0000-00-00 00:00:00', 'N'),
(3, 3, 46, '2016-07-28 00:00:00', '0000-00-00 00:00:00', 'N'),
(4, 4, 46, '2016-07-28 00:00:00', '0000-00-00 00:00:00', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE IF NOT EXISTS `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) NOT NULL,
  `page_heading` varchar(255) NOT NULL,
  `page_description` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `page_title`, `page_heading`, `page_description`, `status`) VALUES
(1, 'About Us', 'About Us', '<p>\r\n	Test about us</p>\r\n', 1),
(2, 'How it Works', 'Terms of Use', 'Terms of Use\r\nLAST UPDATED: June 2, 2016\r\nWELCOME TO NIWI, LLC.  Our team is here to help you along with any questions you may have at info@niwinow.com.  Please review our Terms and Conditions prior to any use of NIWI, LLC.  By accessing our servers, websites, content and mobile applications therefrom (together, "NIWI"), you agree to these Terms and Conditions ("T&Câ€™s").\r\nLICENSE. If you are 18 or older, we grant you a limited, revocable, nonexclusive, nonassignable, nonsublicensable license to access NIWI in compliance with the T&Câ€™s; unlicensed access is unauthorized. You agree not to license, distribute, make derivative works, display, sell, or "frame" content from NIWI, excluding content you create and sharing with friends/family. You grant us a perpetual, irrevocable, unlimited, worldwide, fully paid/sublicensable license to use, copy, perform, display, distribute, and make derivative works from content you post.\r\nWAIVER OF RESPONSIBILITY\r\ntranslations conflict with the English version, English controls. \r\n\r\n', 1),
(3, 'Privacy Policy', 'Privacy Policy', 'Privacy Policy', 1),
(4, 'Review Guidelines', 'Review Guidelines', 'Review Guidelines', 1),
(5, 'view palans', 'view palans', 'view palans', 1),
(6, 'How it works', 'How it works', 'How it worksHow it worksHow it worksHow it works', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `post_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `subject`, `message`, `email_address`, `phone_number`, `post_date`) VALUES
(3, 'soutik', 'Contact Us', 'sdgdf dfgdf', 'nits.soutik@gmail.com', '234234234', '2016-05-03 08:05:36'),
(4, 'Santanu', 'Contact Us', 'Tss vfvb', 'nits.santanu@gmail.com', '234234234', '2016-05-03 08:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code_2` varchar(2) NOT NULL,
  `code_3` varchar(3) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=252 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `code_2`, `code_3`, `published`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 1),
(2, 'Albania', 'AL', 'ALB', 1),
(3, 'Algeria', 'DZ', 'DZA', 1),
(4, 'American Samoa', 'AS', 'ASM', 1),
(5, 'Andorra', 'AD', 'AND', 1),
(6, 'Angola', 'AO', 'AGO', 1),
(7, 'Anguilla', 'AI', 'AIA', 1),
(8, 'Antarctica', 'AQ', 'ATA', 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 1),
(10, 'Argentina', 'AR', 'ARG', 1),
(11, 'Armenia', 'AM', 'ARM', 1),
(12, 'Aruba', 'AW', 'ABW', 1),
(13, 'Australia', 'AU', 'AUS', 1),
(14, 'Austria', 'AT', 'AUT', 1),
(15, 'Azerbaijan', 'AZ', 'AZE', 1),
(16, 'Bahamas', 'BS', 'BHS', 1),
(17, 'Bahrain', 'BH', 'BHR', 1),
(18, 'Bangladesh', 'BD', 'BGD', 1),
(19, 'Barbados', 'BB', 'BRB', 1),
(20, 'Belarus', 'BY', 'BLR', 1),
(21, 'Belgium', 'BE', 'BEL', 1),
(22, 'Belize', 'BZ', 'BLZ', 1),
(23, 'Benin', 'BJ', 'BEN', 1),
(24, 'Bermuda', 'BM', 'BMU', 1),
(25, 'Bhutan', 'BT', 'BTN', 1),
(26, 'Bolivia', 'BO', 'BOL', 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', 1),
(28, 'Botswana', 'BW', 'BWA', 1),
(29, 'Bouvet Island', 'BV', 'BVT', 1),
(30, 'Brazil', 'BR', 'BRA', 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', 1),
(33, 'Bulgaria', 'BG', 'BGR', 1),
(34, 'Burkina Faso', 'BF', 'BFA', 1),
(35, 'Burundi', 'BI', 'BDI', 1),
(36, 'Cambodia', 'KH', 'KHM', 1),
(37, 'Cameroon', 'CM', 'CMR', 1),
(38, 'Canada', 'CA', 'CAN', 1),
(39, 'Cape Verde', 'CV', 'CPV', 1),
(40, 'Cayman Islands', 'KY', 'CYM', 1),
(41, 'Central African Republic', 'CF', 'CAF', 1),
(42, 'Chad', 'TD', 'TCD', 1),
(43, 'Chile', 'CL', 'CHL', 1),
(44, 'China', 'CN', 'CHN', 1),
(45, 'Christmas Island', 'CX', 'CXR', 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1),
(47, 'Colombia', 'CO', 'COL', 1),
(48, 'Comoros', 'KM', 'COM', 1),
(49, 'Congo', 'CG', 'COG', 1),
(50, 'Cook Islands', 'CK', 'COK', 1),
(51, 'Costa Rica', 'CR', 'CRI', 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', 1),
(53, 'Croatia', 'HR', 'HRV', 1),
(54, 'Cuba', 'CU', 'CUB', 1),
(55, 'Cyprus', 'CY', 'CYP', 1),
(56, 'Czech Republic', 'CZ', 'CZE', 1),
(57, 'Denmark', 'DK', 'DNK', 1),
(58, 'Djibouti', 'DJ', 'DJI', 1),
(59, 'Dominica', 'DM', 'DMA', 1),
(60, 'Dominican Republic', 'DO', 'DOM', 1),
(61, 'East Timor', 'TL', 'TLS', 1),
(62, 'Ecuador', 'EC', 'ECU', 1),
(63, 'Egypt', 'EG', 'EGY', 1),
(64, 'El Salvador', 'SV', 'SLV', 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1),
(66, 'Eritrea', 'ER', 'ERI', 1),
(67, 'Estonia', 'EE', 'EST', 1),
(68, 'Ethiopia', 'ET', 'ETH', 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1),
(70, 'Faroe Islands', 'FO', 'FRO', 1),
(71, 'Fiji', 'FJ', 'FJI', 1),
(72, 'Finland', 'FI', 'FIN', 1),
(74, 'France, Metropolitan', 'FR', 'FRA', 1),
(75, 'French Guiana', 'GF', 'GUF', 1),
(76, 'French Polynesia', 'PF', 'PYF', 1),
(77, 'French Southern Territories', 'TF', 'ATF', 1),
(78, 'Gabon', 'GA', 'GAB', 1),
(79, 'Gambia', 'GM', 'GMB', 1),
(80, 'Georgia', 'GE', 'GEO', 1),
(81, 'Germany', 'DE', 'DEU', 1),
(82, 'Ghana', 'GH', 'GHA', 1),
(83, 'Gibraltar', 'GI', 'GIB', 1),
(84, 'Greece', 'GR', 'GRC', 1),
(85, 'Greenland', 'GL', 'GRL', 1),
(86, 'Grenada', 'GD', 'GRD', 1),
(87, 'Guadeloupe', 'GP', 'GLP', 1),
(88, 'Guam', 'GU', 'GUM', 1),
(89, 'Guatemala', 'GT', 'GTM', 1),
(90, 'Guinea', 'GN', 'GIN', 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', 1),
(92, 'Guyana', 'GY', 'GUY', 1),
(93, 'Haiti', 'HT', 'HTI', 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1),
(95, 'Honduras', 'HN', 'HND', 1),
(96, 'Hong Kong', 'HK', 'HKG', 1),
(97, 'Hungary', 'HU', 'HUN', 1),
(98, 'Iceland', 'IS', 'ISL', 1),
(99, 'India', 'IN', 'IND', 1),
(100, 'Indonesia', 'ID', 'IDN', 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1),
(102, 'Iraq', 'IQ', 'IRQ', 1),
(103, 'Ireland', 'IE', 'IRL', 1),
(104, 'Israel', 'IL', 'ISR', 1),
(105, 'Italy', 'IT', 'ITA', 1),
(106, 'Jamaica', 'JM', 'JAM', 1),
(107, 'Japan', 'JP', 'JPN', 1),
(108, 'Jordan', 'JO', 'JOR', 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', 1),
(110, 'Kenya', 'KE', 'KEN', 1),
(111, 'Kiribati', 'KI', 'KIR', 1),
(112, 'North Korea', 'KP', 'PRK', 1),
(113, 'Korea, Republic of', 'KR', 'KOR', 1),
(114, 'Kuwait', 'KW', 'KWT', 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', 1),
(117, 'Latvia', 'LV', 'LVA', 1),
(118, 'Lebanon', 'LB', 'LBN', 1),
(119, 'Lesotho', 'LS', 'LSO', 1),
(120, 'Liberia', 'LR', 'LBR', 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1),
(122, 'Liechtenstein', 'LI', 'LIE', 1),
(123, 'Lithuania', 'LT', 'LTU', 1),
(124, 'Luxembourg', 'LU', 'LUX', 1),
(125, 'Macau', 'MO', 'MAC', 1),
(126, 'FYROM', 'MK', 'MKD', 1),
(127, 'Madagascar', 'MG', 'MDG', 1),
(128, 'Malawi', 'MW', 'MWI', 1),
(129, 'Malaysia', 'MY', 'MYS', 1),
(130, 'Maldives', 'MV', 'MDV', 1),
(131, 'Mali', 'ML', 'MLI', 1),
(132, 'Malta', 'MT', 'MLT', 1),
(133, 'Marshall Islands', 'MH', 'MHL', 1),
(134, 'Martinique', 'MQ', 'MTQ', 1),
(135, 'Mauritania', 'MR', 'MRT', 1),
(136, 'Mauritius', 'MU', 'MUS', 1),
(137, 'Mayotte', 'YT', 'MYT', 1),
(138, 'Mexico', 'MX', 'MEX', 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', 1),
(141, 'Monaco', 'MC', 'MCO', 1),
(142, 'Mongolia', 'MN', 'MNG', 1),
(143, 'Montserrat', 'MS', 'MSR', 1),
(144, 'Morocco', 'MA', 'MAR', 1),
(145, 'Mozambique', 'MZ', 'MOZ', 1),
(146, 'Myanmar', 'MM', 'MMR', 1),
(147, 'Namibia', 'NA', 'NAM', 1),
(148, 'Nauru', 'NR', 'NRU', 1),
(149, 'Nepal', 'NP', 'NPL', 1),
(150, 'Netherlands', 'NL', 'NLD', 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', 1),
(152, 'New Caledonia', 'NC', 'NCL', 1),
(153, 'New Zealand', 'NZ', 'NZL', 1),
(154, 'Nicaragua', 'NI', 'NIC', 1),
(155, 'Niger', 'NE', 'NER', 1),
(156, 'Nigeria', 'NG', 'NGA', 1),
(157, 'Niue', 'NU', 'NIU', 1),
(158, 'Norfolk Island', 'NF', 'NFK', 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', 1),
(160, 'Norway', 'NO', 'NOR', 1),
(161, 'Oman', 'OM', 'OMN', 1),
(162, 'Pakistan', 'PK', 'PAK', 1),
(163, 'Palau', 'PW', 'PLW', 1),
(164, 'Panama', 'PA', 'PAN', 1),
(165, 'Papua New Guinea', 'PG', 'PNG', 1),
(166, 'Paraguay', 'PY', 'PRY', 1),
(167, 'Peru', 'PE', 'PER', 1),
(168, 'Philippines', 'PH', 'PHL', 1),
(169, 'Pitcairn', 'PN', 'PCN', 1),
(170, 'Poland', 'PL', 'POL', 1),
(171, 'Portugal', 'PT', 'PRT', 1),
(172, 'Puerto Rico', 'PR', 'PRI', 1),
(173, 'Qatar', 'QA', 'QAT', 1),
(174, 'Reunion', 'RE', 'REU', 1),
(175, 'Romania', 'RO', 'ROM', 1),
(176, 'Russian Federation', 'RU', 'RUS', 1),
(177, 'Rwanda', 'RW', 'RWA', 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1),
(179, 'Saint Lucia', 'LC', 'LCA', 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1),
(181, 'Samoa', 'WS', 'WSM', 1),
(182, 'San Marino', 'SM', 'SMR', 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', 1),
(184, 'Saudi Arabia', 'SA', 'SAU', 1),
(185, 'Senegal', 'SN', 'SEN', 1),
(186, 'Seychelles', 'SC', 'SYC', 1),
(187, 'Sierra Leone', 'SL', 'SLE', 1),
(188, 'Singapore', 'SG', 'SGP', 1),
(189, 'Slovak Republic', 'SK', 'SVK', 1),
(190, 'Slovenia', 'SI', 'SVN', 1),
(191, 'Solomon Islands', 'SB', 'SLB', 1),
(192, 'Somalia', 'SO', 'SOM', 1),
(193, 'South Africa', 'ZA', 'ZAF', 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', 1),
(195, 'Spain', 'ES', 'ESP', 1),
(196, 'Sri Lanka', 'LK', 'LKA', 1),
(197, 'St. Helena', 'SH', 'SHN', 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1),
(199, 'Sudan', 'SD', 'SDN', 1),
(200, 'Suriname', 'SR', 'SUR', 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1),
(202, 'Swaziland', 'SZ', 'SWZ', 1),
(203, 'Sweden', 'SE', 'SWE', 1),
(204, 'Switzerland', 'CH', 'CHE', 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 1),
(206, 'Taiwan', 'TW', 'TWN', 1),
(207, 'Tajikistan', 'TJ', 'TJK', 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1),
(209, 'Thailand', 'TH', 'THA', 1),
(210, 'Togo', 'TG', 'TGO', 1),
(211, 'Tokelau', 'TK', 'TKL', 1),
(212, 'Tonga', 'TO', 'TON', 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 1),
(214, 'Tunisia', 'TN', 'TUN', 1),
(215, 'Turkey', 'TR', 'TUR', 1),
(216, 'Turkmenistan', 'TM', 'TKM', 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1),
(218, 'Tuvalu', 'TV', 'TUV', 1),
(219, 'Uganda', 'UG', 'UGA', 1),
(220, 'Ukraine', 'UA', 'UKR', 1),
(221, 'United Arab Emirates', 'AE', 'ARE', 1),
(222, 'United Kingdom', 'GB', 'GBR', 1),
(223, 'United States', 'US', 'USA', 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1),
(225, 'Uruguay', 'UY', 'URY', 1),
(226, 'Uzbekistan', 'UZ', 'UZB', 1),
(227, 'Vanuatu', 'VU', 'VUT', 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1),
(229, 'Venezuela', 'VE', 'VEN', 1),
(230, 'Viet Nam', 'VN', 'VNM', 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1),
(234, 'Western Sahara', 'EH', 'ESH', 1),
(235, 'Yemen', 'YE', 'YEM', 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', 1),
(238, 'Zambia', 'ZM', 'ZMB', 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', 1),
(240, 'Jersey', 'JE', 'JEY', 1),
(241, 'Guernsey', 'GG', 'GGY', 1),
(242, 'Montenegro', 'ME', 'MNE', 1),
(243, 'Serbia', 'RS', 'SRB', 1),
(244, 'Aaland Islands', 'AX', 'ALA', 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', 1),
(246, 'Curacao', 'CW', 'CUW', 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', 1),
(248, 'South Sudan', 'SS', 'SSD', 1),
(249, 'St. Barthelemy', 'BL', 'BLM', 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', 1),
(251, 'Canary Islands', 'IC', 'ICA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `subject`, `content`) VALUES
(1, 'Admin Forgot Password', '<header> <section class="header_top" style="padding: 15px; width:100%; background: #000000;">\r\n<div class="container" style="width:100%;">\r\n	\r\n</div>\r\n</section> <section class="middle_content" style="width: 100%; padding: 50px 0;">\r\n<div class="container" style="width:100%">\r\n	<div class="row" style="width: 100%; padding:0 15px; margin: 0 auto">\r\n		<div class="col-md-12" style="width: 100%; padding: 0 15px;">\r\n			<p>\r\n				Hi there,</p>\r\n			<p>\r\n				Your Password has been changed on&nbsp;NAIL.</p>\r\n			<p>\r\n				Your new login details:</p>\r\n			<p>\r\n				Email: [EMAIL]</p>\r\n			<p>\r\n				LINK: [LINK]</p>\r\n			<p>\r\n				You can now login to your account and can change your password.</p>\r\n			<p>\r\n				Thanks for loving NAIL,</p>\r\n			<p>\r\n				<br />\r\n				<a href="http://104.131.83.218/nail/">Team NAIL</a></p>\r\n			<p>\r\n				&nbsp;</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section> </header>'),
(2, 'Registration', '<header> <section class="middle_content" style="width: 100%; padding: 50px 0;">\r\n<div class="container" style="width:100%">\r\n	<div class="row" style="width: 100%; padding:0 15px; margin: 0 auto">\r\n		<div class="col-md-12" style="width: 100%; padding: 0 15px;">\r\n			<p>\r\n				Hi [POST_OWNER],</p>\r\n			One user has reported on your post. Here is the post details:-<br />\r\n			<p>\r\n				Post: [POST_TITLE]</p>\r\n			<p>\r\n				LINK: [LINK]</p>\r\n			<p>\r\n				You can now login to your account and can change your password.</p>\r\n			<p>\r\n				Thanks for loving NIWI,</p>\r\n			<p>\r\n				<br />\r\n				<a href="http://107.170.152.166/team4/nimi/">Team NIWI</a></p>\r\n			<p>\r\n				&nbsp;</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section></header>'),
(3, 'Booking Request', '<header> <section class="header_top" style="padding: 15px; width:100%; background: #000000;">\r\n<div class="container" style="width:100%;">\r\n	\r\n</div>\r\n</section> <section class="middle_content" style="width: 100%; padding: 50px 0;">\r\n<div class="container" style="width:100%">\r\n	<div class="row" style="width: 100%; padding:0 15px; margin: 0 auto">\r\n		<div class="col-md-12" style="width: 100%; padding: 0 15px;">\r\n			<p>\r\n				Hi [USERNAME],</p>\r\n			<p>\r\n				You have get one request please go to your account and check it.</p>\r\n			<p>\r\n				Thanks for loving NAIL,</p>\r\n			<p>\r\n				<br />\r\n				<a href="http://104.131.83.218/nail/">Team NAIL</a></p>\r\n			<p>\r\n				&nbsp;</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section> </header>'),
(4, 'Booking Acceptation', '<header> <section class="header_top" style="padding: 15px; width:100%; background: #000000;">\r\n<div class="container" style="width:100%;">\r\n	\r\n</div>\r\n</section> <section class="middle_content" style="width: 100%; padding: 50px 0;">\r\n<div class="container" style="width:100%">\r\n	<div class="row" style="width: 100%; padding:0 15px; margin: 0 auto">\r\n		<div class="col-md-12" style="width: 100%; padding: 0 15px;">\r\n			<p>\r\n				Hi [USERNAME],</p>\r\n			<p>\r\n				Your request hasbeen accepted please check you account.</p>\r\n			<p>\r\n				Thanks for loving NAIL,</p>\r\n			<p>\r\n				<br />\r\n				<a href="http://104.131.83.218/nail/">Team NAIL</a></p>\r\n			<p>\r\n				&nbsp;</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n</section> </header>');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) NOT NULL,
  `questions` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `is_active` enum('Y','N') NOT NULL,
  `post_date` date NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `faq_category_id`, `questions`, `answer`, `is_active`, `post_date`, `order`) VALUES
(1, 1, 'dsadsa', 'sadsadsa', 'Y', '0000-00-00', 0),
(2, 2, 'xcvcxv', 'cvxcvcx', 'Y', '2016-06-29', 0),
(3, 1, 'xzc', 'xcxzc', 'Y', '2016-06-29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE IF NOT EXISTS `faq_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `parent_id`, `name`, `active`) VALUES
(1, 0, 'sdsads', 1),
(2, 0, 'sdsadsa sdfdsf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `subscribe_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `newsletters`
--


-- --------------------------------------------------------

--
-- Table structure for table `service_areas`
--

CREATE TABLE IF NOT EXISTS `service_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `lat` varchar(200) NOT NULL,
  `long` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `service_areas`
--

INSERT INTO `service_areas` (`id`, `name`, `address`, `city`, `state`, `zip`, `lat`, `long`, `active`) VALUES
(1, 'Service area 2', '35 Prantick Sarani', 'Kolkara', 'westbengal', '700065', '', '', 1),
(2, 'Service area 1', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_offdates`
--

CREATE TABLE IF NOT EXISTS `service_offdates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `service_offdates`
--

INSERT INTO `service_offdates` (`id`, `title`, `date`, `time`, `active`) VALUES
(1, 'Holly day', '2016-07-05', '2,3', 1),
(2, 'Off day', '2016-06-05', '1,2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_times`
--

CREATE TABLE IF NOT EXISTS `service_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ava_time` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `service_times`
--

INSERT INTO `service_times` (`id`, `ava_time`, `status`) VALUES
(1, '12:30pm', 1),
(2, '01:30pm', 1),
(3, '02:30pm', 1),
(4, '03:30pm', 1),
(5, '05:30pm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paypal_email` varchar(255) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `bank_account` varchar(255) NOT NULL,
  `start_service_time` time NOT NULL,
  `end_service_time` time NOT NULL,
  `stylist_percentage` float NOT NULL,
  `admin_percentage` float NOT NULL,
  `pedicure price` float NOT NULL,
  `manicure_price` float NOT NULL,
  `logo` varchar(255) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `bannertext` text NOT NULL,
  `bannerimage` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `paypal_email`, `site_name`, `site_email`, `bank_account`, `start_service_time`, `end_service_time`, `stylist_percentage`, `admin_percentage`, `pedicure price`, `manicure_price`, `logo`, `site_url`, `bannertext`, `bannerimage`) VALUES
(1, 'nits.aprita@gmail.com', 'Niwi', 'nits.santanu@gmail.com', '', '19:09:00', '19:25:00', 46.25, 15.6, 762.26, 860.96, '14780_logo.png', 'http://107.170.152.166/team4/nimi/', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `social_icons`
--

CREATE TABLE IF NOT EXISTS `social_icons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `social_icons`
--

INSERT INTO `social_icons` (`id`, `name`, `image`, `link`, `active`) VALUES
(1, 'twitter', '577b7d937b2eb.jpg', 'www.twitter.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `style_categories`
--

CREATE TABLE IF NOT EXISTS `style_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `style_categories`
--

INSERT INTO `style_categories` (`id`, `name`, `description`, `active`) VALUES
(1, 'GIRLS NIGHT', 'GIRLS NIGHT', 1),
(2, 'ROMANCE', 'ROMANCE', 1),
(3, 'OFFICE', 'OFFICE', 1),
(4, 'PLAIN', 'PLAIN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `style_services`
--

CREATE TABLE IF NOT EXISTS `style_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `nail_paint_code` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `style_services`
--

INSERT INTO `style_services` (`id`, `category_id`, `name`, `description`, `image`, `price`, `duration`, `nail_paint_code`, `active`) VALUES
(3, 1, 'dsads', 'sadsads', '57907da94acab.jpg', 12.00, '12', 0, 1),
(5, 3, 'dsads', 'sadsads', '5795bb972873d.jpg', 12.00, '12', 0, 1),
(6, 2, 'santanu', 'ewrewr', '57907dc17c724.jpg', 10.00, 'dfsd', 0, 1),
(7, 2, 'ferew', 'dfdfd', '57907e2b83c80.jpg', 200.00, '10', 4, 1),
(8, 1, 'fdgfd', 'fdgfd', '57907e674d0c5.jpg', 10.00, '10', 10, 1),
(9, 3, 'sadsad', 'sdsad', '5792157a55bf9.jpg', 10.00, '10', 3, 1),
(10, 4, 'dfgfdg', 'fdgfdgfd', '57921598de1c7.jpg', 20.00, '10', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `treatments`
--

CREATE TABLE IF NOT EXISTS `treatments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `treatments`
--

INSERT INTO `treatments` (`id`, `name`, `description`, `image`, `price`, `duration`, `active`, `order`) VALUES
(4, 'purple magic', '<p>\r\n	good</p>\r\n', '579b25165f7cd.jpg', 96.00, 34, 1, 0),
(3, 'black polish', '<p>\r\n	beautiful</p>\r\n', '579b24c27e9a4.jpeg', 123.00, 30, 1, 0),
(5, 'black ', '<p>\r\n	good</p>\r\n', '579aff1ed310e.jpg', 89.00, 35, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(111) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `country_id` int(11) DEFAULT NULL COMMENT 'foreign key of country table',
  `state` varchar(255) NOT NULL COMMENT 'We can use it as county in case of US',
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL COMMENT 'registration date and time',
  `member_since` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'phone number',
  `Phone_number` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `last_login` datetime DEFAULT NULL COMMENT 'Update at the time of user login',
  `email_address` varchar(150) NOT NULL,
  `user_pass` varchar(255) NOT NULL COMMENT 'hashing will be done using MD5',
  `facebook_id` int(11) DEFAULT NULL,
  `user_type` enum('C','S','A') NOT NULL,
  `is_admin` tinyint(1) DEFAULT '0' COMMENT '1=Yes/0=No',
  `is_sociallogin` tinyint(1) DEFAULT '0' COMMENT 'Updated at the time of facebooklogin',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=active/0=deactive',
  `deviceid` varchar(255) DEFAULT NULL,
  `devicetype` varchar(50) DEFAULT NULL,
  `is_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `location` text NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) NOT NULL,
  `service_area_id` varchar(255) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_users` (`id`,`first_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `country_id`, `state`, `city`, `zip`, `member_since`, `Phone_number`, `address`, `image`, `last_login`, `email_address`, `user_pass`, `facebook_id`, `user_type`, `is_admin`, `is_sociallogin`, `status`, `deviceid`, `devicetype`, `is_newsletter`, `location`, `country`, `zip_code`, `bank_account_no`, `service_area_id`, `service_id`) VALUES
(12, 'Santanu', 'Patra', 223, 'West Bengal', 'Kolkata', '7114177777777777', '2016-06-29 12:26:41', '9874207080', 'test', '', '2016-06-24 18:41:16', 'nits.santanu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'C', 1, 0, 1, '2342535', 'iphone', 1, 'Duluth, GA, United States', '', '', '', '0', ''),
(42, 'Santanu Patra', '', NULL, 'westbengal', 'Kolkara', '700065', '2016-07-27 15:47:54', '9874207080', '35 Prantick Sarani', '5790781c18592.jpg', NULL, 'nits.santanupatra@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'C', 0, 0, 1, NULL, NULL, 0, '', NULL, NULL, '', '1', ''),
(46, 'Hari sankar', '', NULL, 'westbengal', 'Kolkara', '700065', '2016-07-26 17:45:54', '56656565', '35 Prantick Sarani', '57909de0d7fd1.png', NULL, 'nits.harisankar@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'S', 0, 0, 1, NULL, NULL, 0, '', NULL, NULL, '', '1', '3'),
(47, 'fdg', 'fdg', 17, 'fdg', 'fdgfd', '3221', '2016-07-27 08:07:20', '44987987', '', 'C', NULL, 'fdg@gmail.com', '1233123', NULL, '', 0, 0, 1, NULL, NULL, 0, '', NULL, NULL, '', '', ''),
(48, 'dsfds', 'dsfsf', 17, 'kljlkjlk', 'kljkljkl', '70030', '2016-07-27 08:07:07', '4565465456', '', 'C', NULL, 'dsfsfs@gmail.com', '123456', NULL, '', 0, 0, 1, NULL, NULL, 0, '', NULL, NULL, '', '', ''),
(49, 'dss', 'sds', 1, 'kljklj', 'kljklj', '722225', '2016-07-27 08:07:20', '98798797', '', '57987590e4373.png', NULL, 'sdd@gmail.com', '123456', NULL, 'C', 0, 0, 1, NULL, NULL, 0, '', NULL, NULL, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE IF NOT EXISTS `user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `long` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`id`, `user_id`, `address`, `city`, `state`, `zip`, `lat`, `long`, `active`) VALUES
(1, 42, '35 prantick sarani', 'Kolkata', 'West Bengal', '700030', '', '', 0),
(2, 42, 'dsfdsd', 'dsfdsfds', 'dsfds', 'dsf', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_creditcards`
--

CREATE TABLE IF NOT EXISTS `user_creditcards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `card_no` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_creditcards`
--

INSERT INTO `user_creditcards` (`id`, `user_id`, `card_no`, `is_active`) VALUES
(1, 42, '1321313131132132', 1),
(2, 42, '564654654654', 1);
