<div class="categories form">
<?php echo $this->Form->create('Post',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Post'); ?></legend>
                <?php
                echo $this->Form->input('id');
                 echo $this->Form->input('cat_id',array('empty' => '(choose any category)','options' => $categories,'class'=>'selectbox2','label'=>'Category'));
               echo $this->Form->input('title',array('required' => 'required'));
                echo $this->Form->input('image',array('type'=>'file'));
                ?>
                 <div>
                    <?php
                        if(isset( $posts['Post']['image']) and !empty( $posts['Post']['image']))
                    {
                    ?>
                    <img alt="" src="<?php echo $this->webroot;?>postimg/<?php echo $posts['Post']['image'];?>" style=" height:80px; width:80px;">
                    <?php
                    }
                    else{
                    ?>
                   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                    <?php } ?>
                </div>
                <?php 
                echo $this->Form->input('icon',array('type'=>'file'));
                ?>
                 <div>
                    <?php
                        if(isset( $posts['Post']['icon']) and !empty( $posts['Post']['icon']))
                    {
                    ?>
                    <img alt="" src="<?php echo $this->webroot;?>posticon/<?php echo $posts['Post']['icon'];?>" style=" height:80px; width:80px;">
                    <?php
                    }
                    else{
                    ?>
                   <img alt="" src="<?php echo $this->webroot;?>noimage.png" style=" height:80px; width:80px;">

                    <?php } ?>
                </div>
               <?php
               
                //echo $this->Form->input('user_id',array('empty' => '(choose any category)','options' => $users,'class'=>'selectbox2'));
                echo $this->Form->input('post_order', array('default' => 0));
                 echo $this->Form->label('Feature Service');
                
                echo $this->Form->checkbox('is_feature');
                 echo '<br>';
                echo $this->Form->label('Popular Service');
                 
                echo $this->Form->checkbox('is_popular');
                echo $this->Form->input('old_image', array('type' => 'hidden','default' =>$posts['Post']['image']));
	        echo $this->Form->input('old_icon', array('type' => 'hidden','default' =>$posts['Post']['icon']));

	        ?>

                


	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

