<div class="categories index">
	<h2><?php echo __(' List SubCategory - '); ?><?php echo $parentCat;?></h2>
          <div>
       
        <form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">   
            <table style=" border:none;">
                <tr>
<!--                    <td>Keyword</td>
                    <td><input type="text" name="keyword" value="<?php echo isset($keywords)?$keywords:'';?>" placeholder="Search by Keyword."></td>
                    <td>Activity Status</td>
                    <td><select name="search_is_active" id="search_is_active" style="padding-top: 3px; padding-bottom: 3px;">
                            <option value="" >Select Option</option>
                            <option value="1" <?php echo (isset($Newsearch_is_active) && $Newsearch_is_active=='1')?'selected':'';?>>Active</option>
                            <option value="0" <?php echo (isset($Newsearch_is_active) && $Newsearch_is_active=='0')?'selected':'';?>>Inactive</option>
                        </select></td>
		    <?php 
		    //pr($countries );
		    ?>
                    <td>Country</td>
                    <td><select name="Country" id="Country" style="padding-top: 3px; padding-bottom: 3px;">
                            <option value="" >Select Option</option>
			<?php
			foreach($countries as $key=>$country)
			{
			?>
                            <option value="<?php echo $key ?>" <?php echo (isset($Country) && $Country==$key)?'selected':'';?>><?php echo $country; ?></option>
			<?php
			}
			?>
                        </select></td>
                    <td><input type="submit" name="search" value="Search"></td>
                </tr> -->
                 <tr><a href="<?php echo($this->webroot);?>admin/categories/addsubcat/<?php echo $parent_id;?>" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add Sub Category</a>
            </tr>      
            </table>
        </form>
        <?php //echo $this->Form->end();?>
    </div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
<!--                        <th><?php echo $this->Paginator->sort('images'); ?></th>-->
                       <th>Status</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
        $d=1;
        foreach ($carcategory as $car):
        ?>
	<tr>
		<td><?php echo $d; ?>&nbsp;</td>
		<td><?php echo h($car['Category']['name']);?></td>
<!--                <td><img src="<?php echo $this->webroot;?>category_images/<?php echo $car['Category']['image'];?>" style="width:80px; height:80px;"></td>-->
                <td><?php echo $car['Category']['is_active']==1?'Active':'Inactive'; ?>&nbsp;</td>
		<td>
                  <a href="<?php echo $this->webroot?>admin/questions/index/<?php echo $car['Category']['id']?>" class="btn btn-primary">Question</a>&nbsp;&nbsp;
                  <a href="<?php echo $this->webroot?>admin/categories/editsubcat/<?php echo $car['Category']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;
 <a href="<?php echo $this->webroot?>admin/categories/delete/<?php echo $car['Category']['id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this category?');">Delete</a>&nbsp;&nbsp;
		</td>
	</tr>
<?php
$d++;
endforeach;

?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>