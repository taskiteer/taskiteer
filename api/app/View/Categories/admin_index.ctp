<div class="categories index">
	<h2><?php echo __(' List Category'); ?></h2>
	<table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
<!--                        <th><?php echo 'image'; ?></th>-->
                        <th ><?php echo $this->Paginator->sort('Subcategories'); ?></th>
                        <th>Status</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
            </thead> 
            <tbody id="sortable">
               <?php
                $d=1;
                foreach ($carcategory as $car):
                ?>
                <tr id="<?php echo $car['Category']['id']?>">
                        <td><?php echo $d; ?>&nbsp;</td>
                        <td><?php echo h($car['Category']['name']);?></td>
        <!--                <td><img src="<?php echo $this->webroot;?>category_images/<?php echo $car['Category']['image'];?>" style="width:80px; height:80px;"></td>-->
                        <td><a href="<?php echo $this->webroot?>admin/categories/indexsubcat/<?php echo $car['Category']['id']?>" class="btn btn-success"><?php echo count($car['Children']); ?>&nbsp;</a></td>
                        <td><?php echo $car['Category']['is_active']==1?'Active':'Inactive'; ?>&nbsp;</td>
                        <td>
        <a href="<?php echo $this->webroot?>admin/questions/index/<?php echo $car['Category']['id']?>" class="btn btn-primary">Question</a>&nbsp;&nbsp;
                          <a href="<?php echo $this->webroot?>admin/categories/edit/<?php echo $car['Category']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;
                          <a href="<?php echo $this->webroot?>admin/categories/delete/<?php echo $car['Category']['id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this category?');">Delete</a>&nbsp;&nbsp;
                        </td>
                </tr>
                <?php
                $d++;
                endforeach;

                ?> 
            </tbody>
	
	
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
<script>
    $(function () {
        $("#sortable").sortable({
            update: function (event, ui){
                info = [];
                $("#sortable tr").each(function () {
                    info.push($(this).attr("id"));
                });
                $.post("<?php echo $this->webroot; ?>admin/categories/saveorder", {category: info}, function (data) {
                if(data.trim()==1)
                {
                //location.reload();
                }
                });
            }
        });
        //$("#sortable").disableSelection();
    });
</script>