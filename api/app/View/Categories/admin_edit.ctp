<div class="categories form">
<?php echo $this->Form->create('Category',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Category'); ?></legend>
                <?php

                echo $this->Form->input('name',array('value'=>$car['Category']['name'],'style'=>'width:500px;'));
                echo $this->Form->input('project_name',array('style'=>'width:500px;'));
                echo $this->Form->input('price',array('required' => 'required','style'=>'width:500px;','value'=>$car['Category']['price']));
                echo $this->Form->input('img', array('type' => 'hidden','value' =>$car['Category']['image']));
                echo $this->Form->input('hide_image', array('type' => 'hidden','value' =>$car['Category']['app_image']));
                echo $this->Form->input('hide_bigimage', array('type' => 'hidden','value' =>$car['Category']['bigimage']));
	        echo $this->Form->input('image',array('type'=>'file'));
	        echo $this->Form->input('app_image',array('type'=>'file'));
                echo $this->Form->input('bigimage',array('type'=>'file'));
                echo $this->Form->input('is_active');
	        ?>

                <div style="background:#000; height:120px; width:120px;">
                    <?php
                        if(isset( $car['Category']['image']) and !empty( $car['Category']['image']))
                    {
                    ?>
                    <img alt="" src="<?php echo $this->webroot;?>category_images/<?php echo $car['Category']['image'];?>" style=" height:80px; width:80px;">
                    <?php
                    }
                    else{
                    ?>
                   <img alt="" src="<?php echo $this->webroot;?>category_images/default.png" style=" height:80px; width:80px;">

                    <?php } ?>
                </div>
                <div style=" background:#000; height:120px; width:120px;">
                    <?php
                        if(isset( $car['Category']['app_image']) and !empty( $car['Category']['app_image']))
                    {
                    ?>
                    <img alt="" src="<?php echo $this->webroot;?>category_images/<?php echo $car['Category']['app_image'];?>" style=" height:80px; width:80px;">
                    <?php
                    }
                    else{
                    ?>
                   <img alt="" src="<?php echo $this->webroot;?>category_images/default.png" style=" height:80px; width:80px;">

                    <?php } ?>
                </div>
                <div style=" background:#000; height:120px; width:120px;">
                    <?php
                        if(isset( $car['Category']['bigimage']) and !empty( $car['Category']['bigimage']))
                    {
                    ?>
                    <img alt="" src="<?php echo $this->webroot;?>category_images/bigimage/<?php echo $car['Category']['bigimage'];?>" style=" height:80px; width:80px;">
                    <?php
                    }
                    else{
                    ?>
                   <img alt="" src="<?php echo $this->webroot;?>category_images/default.png" style=" height:80px; width:80px;">

                    <?php } ?>
                </div>



	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

