<?php
//pr($bookings);
//pr($countries);
	
?>
<section class="category-body-area">
    	<div class="container">
        	<div class="row">            	
				<?php
					echo $this->element('user_sidebar');
				?>				
                 
				<div class="col-md-9">
                	<div class="right-dashboard">
                		<h3>Booking List</h3>
                		<div class="table-responsive">
                			<table class="table  table-striped book-table">
                				<thead>
                					<tr>
                						<th width="15%">Order No.</th>                						
										<th width="25%">Treatment</th>
                						<th width="15%">Date & time</th>
                						<th width="10%">Status</th>
                						<th width="10%">Action</th>
                					</tr>
                				</thead>
                				<tbody>
									<?php										
										
										foreach($bookings as $booking):
										//pr($booking);
									?>
                					<tr>
                						<td><?php echo $booking['Booking']['id'];?></td>
                						
										<td>
                							<div class="image-box"><img src="<?php echo $this->webroot.'treatment/'.$booking['Treatment']['image'];?>" alt=""></div>
											                							
                						</td>
                						<td>
											<?php echo $booking['Booking']['booking_date'];?><br>
											<?php echo date('g:i A', strtotime($booking['Booking']['booking_time']));?>
										</td>
                						<td>
											<?php 
												if($booking['Booking']['status'] == 'A')
													echo 'Accept';
												elseif($booking['Booking']['status'] == 'R')
													echo 'Reject';
												else
													echo 'Pending';
											?>
										</td>
                						<td>
											<?php
											if($booking['Booking']['status'] == 'A'){
													echo '<a href="#" class="btn btn-primary">Pay</a>';
											}
											?>
											<a href="<?php echo $this->webroot?>bookings/booking_details/<?php echo $booking['Booking']['id'];?>" class="btn btn-primary">View</a>
										</td>
                					</tr>
									<?php
										endforeach;
									?>               					
                					
                				</tbody>
                			</table>
							
                		</div>
						<div style="clear:both"></div>
						<p>
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
						));
						?>	</p>
						<div class="paging">
						<?php
							echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
							echo $this->Paginator->numbers(array('separator' => ''));
							echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
						?>
						</div>
                	</div>
                </div>
            </div>
        </div>
    </section>