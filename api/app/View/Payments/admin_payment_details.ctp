<?php
	//pr($details);
?>
<div class="users view">
<h2><?php echo __('Booking Details'); ?></h2>
	<dl>
		<?php
			if(count($manicure)>0){				
		?>
		<dt><?php echo __('Manicure Name'); ?></dt>
		<dd>
			<?php echo h($manicure['StyleService']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Manicure Image'); ?></dt>
		<dd>
        	<img src="<?php echo $this->webroot.'style_images/'.$manicure['StyleService']['image'];?>" alt="" width="150">			
			&nbsp;
		</dd>              
		<?php
			}			
			if(count($pedicure)>0){				
		?>
		<dt><?php echo __('Pedicure Name'); ?></dt>
		<dd>
			<?php echo h($pedicure['StyleService']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pedicure Image'); ?></dt>
		<dd>
        	<img src="<?php echo $this->webroot.'style_images/'.$pedicure['StyleService']['image'];?>" alt="" width="150">			
			&nbsp;
		</dd>   
		<?php
			}
		?>
		<dt><?php echo __('Treatment Name'); ?></dt>
		<dd>
			<?php echo h($details['Treatment']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Treatment Image'); ?></dt>
		<dd>
			<img src="<?php echo $this->webroot.'treatment/'.$details['Treatment']['image'];?>" alt="" width="150">
			&nbsp;
		</dd>
		<dt><?php echo __('Booking Price'); ?></dt>
		<dd>
			$ <?php echo h($details['Booking']['booking_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Booking Date'); ?></dt>
		<dd>
			<?php echo h($details['Booking']['booking_date']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Booking Time'); ?></dt>
		<dd>
			<?php echo date('g:i A', strtotime($details['Booking']['booking_time'])); ?>
			&nbsp;
		</dd>
        
        <dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($details['User']['name']); ?>
			&nbsp;
		</dd>
        
        <dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($details['User']['Phone_number']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($details['UserAddresse']['address']); ?>, <?php echo h($details['UserAddresse']['city']); ?>, <?php echo h($details['UserAddresse']['state']); ?>, <?php echo h($details['UserAddresse']['zip']); ?>
			&nbsp;
		</dd>		
		
	</dl>
</div>
<div style="clear:both">&nbsp;</div>
<?php
	if($details['Booking']['status'] != 'A'){
?>
<div style="border:1px solid #999999" class="users view">
	<form class="form-horizontal custom_horizontal" action="<?php echo $this->webroot; ?>admin/bookings/accept" method="post">
		<input type="hidden" name="data[User][booking_id]" value="<?php echo $details['Booking']['id'];?>">
		<dt><?php echo __('Stylist'); ?></dt>
    	<dd>
        	<select name="data[User][stylist]">            	
            	<?php				
					foreach($stylists as $k=>$v):
						echo '<option value="'.$k.'">'.$v.'</option>';					
					endforeach;
				?>
            </select>
        </dd>
        <div style="float:left; width:50px"> <input type="submit" name="submit" value="accept" /></div>
    </form>
</div>
<?php
	}
?>
<div style="clear:both; height:10px;"></div>

<?php //echo $this->element('admin_sidebar'); ?>
