<section class="admin-qus-ans-page">
    <div class="ad-q-a-top">
        <h2><?php echo $posts['Category']['name']; ?></h2>
        <?php  if(!empty($posts)&&!empty($posts['User'])) { ?>
        <div class="qs-user-box">
            <p class="ttle">Posted By</p>
            <div class="user-img">
                <?php
                $SITE_URL=Configure::read("SITE_URL");
                $img=$SITE_URL.'user_images/'.$posts['User']['image'];
                ?>
                <img src="<?php echo $img; ?>" alt="">
            </div>
            <h4><?php echo $posts['User']['name']; ?></h4>
            
            <p>Email Id : <span><?php echo $posts['User']['email']; ?></span></p>
            <p>Phone No : <span><?php echo $posts['User']['mobile_no']; ?></span></p>
             <p>Address : <span><?php echo $posts['Lead']['address']; ?></span></p>
<!--            <p>State : <span><?php echo $posts['Lead']['state']; ?></span></p>
             <p>City : <span><?php echo $posts['Lead']['city']; ?></span></p>
            <p>Zip : <span><?php echo $posts['Lead']['zip']; ?></span></p>-->
            
        </div>
        <?php } ?>
    </div>
<div id='ans_status' class="success" style="display: none;"></div>
<div class="categories form">
    
<?php echo $this->Form->create('Lead',array('enctype'=>'multipart/form-data')); ?>
    <?php //print_r($posts);?>
    <input type="hidden" name="data[Lead][category_id]" value="<?php echo $posts['Lead']['category_id']; ?>" />
	<fieldset>
		<legend><?php echo __('Edit Status'); ?></legend>
                <?php
	
          $sizes = array('5' => 'Approve','1' => 'Ongoing', '2' => 'Reject', '3' => 'Cancel','4' => 'Complete','0'=>'Pending');
        echo $this->Form->input('status', array(
           'options' => $sizes
       ));
        echo $this->Form->input('is_view', array('label'=>"Is Show View","type"=>"checkbox"
       ));
	?>
    
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

</section>