<?php

$uploadLogoFolder = "site_logo";
$uploadLogoPath = WWW_ROOT . $uploadLogoFolder;
$LogoName = $sitesetting['Setting']['logo'];
if(file_exists($uploadLogoPath . '/' . $LogoName) && $LogoName!=''){
    $LogoLink=$this->webroot.'site_logo/'.$LogoName;        
}else{
    $LogoLink=$this->webroot.'adminFiles/images/logo.png';  
}

if(!empty($role_restrictions)) {
    foreach ($role_restrictions as $key => $value) {
        $saved_accesibility[] = $value['Adminrolemeta']['meta_key']; 
    }
} else {
    $saved_accesibility = array();
}

//pr($userdetails);

?><!-- left side start-->
<div class="left-side sticky-left-side admin_access_sidebar">

    <!--logo and iconic logo start-->
    <div class="logo">
         <a href="javascript:void(0);"><img src="<?php echo $LogoLink;?>" alt="" style="height: 41px;"></a>
    </div>

    <div class="logo-icon text-center">
        <a href="javascript:void(0);"><img src="<?php //echo $LogoLink;       ?>" style="height: 60px;" alt=""></a>
    </div>
    <!--logo and iconic logo end-->

    <div class="left-side-inner">

        <!-- visible to small devices only -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media logged-user">		            
                <div class="media-body">
                    <h4>
                        <a href="<?php echo $this->webroot; ?>admin/users/adminprofile/<?php echo $current_user['id']; ?>"><?php echo $current_user['fname'] . ' ' . $current_user['lname']; ?></a></h4>
                </div>
            </div>

            <h5 class="left-nav-title">Account Information</h5>

            <ul class="nav nav-pills nav-stacked custom-nav">
                <li>
                    <a href="<?php echo $this->webroot; ?>admin/users/adminprofile/<?php echo $current_user['id']; ?>"><i class="fa fa-user"></i> <span>Profile</span></a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>admin/users/changepassword"><i class="fa fa-user"></i> <span>Manage Password</span></a>
                </li>
                <li>
                    <a href="<?php echo $this->webroot; ?>admin/users/logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a>
                </li>
            </ul>
        </div>

        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">

            
           <!--  <?php
            if (in_array('setting', $roleAccess)) {
            ?>
            <li>
                <a href="<?php echo $this->webroot?>admin/settings/edit/1"><i class="fa fa-cog"></i> <span>Manage Settings</span></a>
            </li>
            <?php } ?>

            <?php
            if (in_array('setting', $roleAccess)) {
            ?>
            <li class="<?php echo ($this->params['controller'] == 'roles_accesses') ? 'active' : ''; ?>">
                <a href="<?php echo $this->webroot; ?>admin/roles_accesses/"><i class="fa fa-cog"></i> <span>Member Access</span></a>
            </li>
            <?php } ?> -->
            <li>
                <a href="<?php echo $this->webroot?>admin/users/logout"><i class="fa fa-sign-out"></i> <span>Log Out</span></a>
            </li>
             <li>
                <a href="<?php echo $this->webroot?>admin/dashboards/index"><i class="fa fa-home"></i> <span>Dashboard</span></a>
            </li>

            <li>
                <a href="<?php echo $this->webroot?>admin/users/changepwd"><i class="fa fa-home"></i> <span>Change Password</span></a>
            </li>

            <?php
            if (in_array('cms', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'wp_pages') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>CMS Pages</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'wp_pages' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/wp_pages/index"> List Pages</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'wp_pages' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/wp_pages/add"> Add Pages</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            //if (in_array('content', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'cms_page') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Contents</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'cms_page' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/cms_page/index"> List Contents</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'cms_page' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/cms_page/add"> Add Contents</a>
                    </li>
                </ul>
            </li>
            <?php //} ?>

            <?php
            if (in_array('faq', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'faqs') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>FAQ</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'faqs' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/index"> List FAQ</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'faqs' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/add"> Add FAQ</a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_categories') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/categories"> Categories</a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_addcategory') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/faqs/addcategory"> Add Category</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('banner', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'banners') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Banner</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'banners' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/banners/index"> List Banner</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'banners' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/banners/add"> Add Banner</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('home_slider', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'homesliders') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Home Slider</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'homesliders' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/homesliders/index"> List Slider</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'homesliders' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/homesliders/add"> Add Slider</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('email_template', $roleAccess)) {
            ?>
            <li><a href="<?php echo $this->webroot?>admin/email_templates/index"><i class="fa fa-envelope"></i> <span>Email Templates</span></a></li>
            <?php } ?>

            <?php
            if (in_array('normal_users', $roleAccess)) {
            ?>
            <li class="menu-list  <?php echo ($this->params['controller'] == 'normal_users') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span> Users </span></a>
                <ul class="sub-menu-list">

                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/normal_users/index"> Users Listing </a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/normal_users/add"> Add User</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('user_location', $roleAccess)) {
            ?>
            <li><a href="<?php echo $this->webroot?>admin/locations/add"><i class="fa fa-envelope"></i> <span>Add User Location</span></a></li>
            <?php } ?>

            <?php
            //if (in_array('student', $roleAccess)) {
            ?>
         <!--    <li class="menu-list  <?php echo ($this->params['controller'] == 'students') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span> Student </span></a>
                <ul class="sub-menu-list">

                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/students/index"> Student Listing </a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/students/add"> Add Student</a>
                    </li>
                </ul>
            </li> -->
            <?php //} ?>

            <?php
            if (in_array('sub_admins', $roleAccess)) {
            ?>
            <li class="menu-list  <?php echo ($this->params['controller'] == 'sub_admins') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span> Sub Admin </span></a>
                <ul class="sub-menu-list">

                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/sub_admins/index"> Sub Admin Listing </a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/sub_admins/add">Add Sub Admin </a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('categories', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'categories') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Course Categories</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'categories' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/categories/index"> List Categories</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'categories' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/categories/add"> Add Category</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('course', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'posts') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-file-text"></i> <span>Courses</span></a>
                <ul class="sub-menu-list">
                    <!-- <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/index"> List Courses</a>
                    </li> -->
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/add"> Add Course</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/coursestatus/1"> - Approved Courses</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/coursestatus/0"> - In Review Courses</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/coursestatus/3"> - Disapproved Courses</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/coursestatus/4"> - On Hold Courses</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_import_csv') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/import_csv"> Upload From CSV</a>
                    </li>
                   <!--  <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'admin_featured_course') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/posts/featured_course"> Featured Course</a>
                    </li> -->
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('course_tags', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'tags') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-camera"></i> <span>Course Tags</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'tags' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/tags/index"> List Tags</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'tags' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/tags/add"> Add Tag</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('contact_us', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'users' && ($this->params['action'] == 'admin_contact_us' || $this->params['action'] == 'admin_contact_us_add')) ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Contact Us</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_contact_us') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/users/contact_us"> List Contact</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_contact_us_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/users/contact_us_add"> Add Contact</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('seo_keyword', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'Seos') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-camera"></i> <span>SEO Keywords</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Seos' && $this->params['action'] == 'admin_listing') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seos/listing"> List SEO Keywords</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Languages' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seos/add"> Add SEO Keywords</a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            
            <?php
            if (in_array('language_management', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'Languages') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-camera"></i> <span>Language Management</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Languages' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Languages/listing"> List Languages</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Languages' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Languages/add"> Add Languages</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('language_resourse', $roleAccess)) {
            ?>
            <li class="">
                <a href="<?php echo $this->webroot; ?>admin/lang_resources"><i class="fa fa-cog"></i> Language Resources</a>
            </li>
            <?php } ?>

            <?php
            if (in_array('sitemap', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'Sitemaps') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-ticket"></i> <span>Sitemaps</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Sitemaps' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Sitemaps/listing"> List Sitemap</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Sitemaps' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Sitemaps/add"> Add Sitemap</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('analytics', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'Analytics') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Analytics</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Analytics' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Analytics/listing"> List Analytics</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Analytics' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Analytics/add"> Add Analytics</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('seo_url', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'Seourls') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Seo URL</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'Seourls' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seourls/index"> List Seo URL</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'Seourls' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/Seourls/add"> Add Seo URL</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('newsletter', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'newsletters') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Newsletter</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/newsletters/index"> List Newsletter</a>
                    </li>
                    <li class="<?php echo ($this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/newsletters/add"> Add Newsletter</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('newsletter', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'SocialMedias') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Social Medias</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'SocialMedias' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/SocialMedias/listing"> List Social Media</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'SocialMedias' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/SocialMedias/add"> Add Social Media</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            <?php
            if (in_array('testimonials', $roleAccess)) {
            ?>
            <li class="menu-list <?php echo ($this->params['controller'] == 'testimonials') ? 'nav-active': ''; ?>">
                <a href=""><i class="fa fa-mobile"></i> <span>Testimonials</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'testimonials' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/testimonials/index"> List Testimonials</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'testimonials' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot; ?>admin/testimonials/add"> Add Testimonials</a>
                    </li>
                </ul>
            </li>
            <?php } ?>

        </ul>
        <!--sidebar nav end-->

    </div>
</div>
<!-- left side end-->
