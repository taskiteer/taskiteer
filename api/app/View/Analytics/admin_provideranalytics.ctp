<?php ?>
<!--body wrapper start-->
<style>
    #content {
        overflow: inherit;
    }
        #content {
        overflow: inherit;
    }
.star-ratings-css {unicode-bidi: bidi-override;color: #fff; font-size: 25px;height: 25px;width: 100px; position: relative;padding: 0;text-shadow: 0px 1px 0 #a2a2a2;}
.star-ratings-css-top {color: #fc8675;padding: 0;position: absolute;z-index: 1;display: block;top: 0;left: 0;overflow: hidden;}
.star-ratings-css-bottom {padding: 0;display: block;z-index: 0;}
.star-ratings-sprite { background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x;
  font-size: 0;height: 21px;line-height: 0;overflow: hidden;width: 110px;}
.star-ratings-sprite-rating {background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x;background-position: 0 100%;float: left;height: 21px;display: block; }
#chart { width: 100%; height: 350px; margin: 30px auto 0; display: block;}
#chart #numbers { height: 100%; width: 50px; margin: 0; padding: 0;display: inline-block; float: left;}
#chart #numbers li { text-align: right; padding-right: 1em; list-style: none; height: 29px; border-bottom: 1px solid #444;
  position: relative; bottom: 30px;}
#chart #numbers li:last-child { height: 30px; }
#chart #numbers li span { color: #eee; position: absolute; bottom: 0; right: 10px;}
#chart #bars { display: inline-block; background: rgba(0, 0, 0, 0.2); width: calc(100% - 160px); height: 300px; padding: 0;
  margin: 0; box-shadow: 0 0 0 1px #444;}
#chart #bars li { display: table-cell; width: 130px; height: 300px; margin: 0; text-align: center; position: relative;}
#chart #bars li .bar { display: block; width: 110px; margin-left: 15px; background: #49E; position: absolute; bottom: 0;}
#chart #bars li .bar:hover { background: #5AE; cursor: pointer; transform: scale(1.1) }
#chart #bars li .bar:hover:before { color: white; content: data-name; position: relative;  bottom: 20px;}
#chart #bars li span { color: #eee; width: 100%; position: absolute; bottom: -44px; left: 0; text-align: center;}
.deep-purple-box
{
    background: #49586e none repeat scroll 0 0;
    box-shadow: 0 5px 0 #424f63;
    color: #fff;
}
</style>
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--statistics start-->
			
				<div class="row state-overview" style="margin-top:20px;">
                        <div class="col-md-6 col-xs-12 col-sm-6"  style="cursor: pointer">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-gavel"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo !empty($total_user)?$total_user:0 ?></div>
                                    <div class="title">Total Service Provider</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6"  style="cursor: pointer">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-gavel"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo !empty($active_user)?$active_user:0 ?></div>
                                    <div class="title">Active Service Provider</div>
                                </div>
                            </div>
                        </div>            
                                    
                        <div class="col-md-6 col-xs-12 col-sm-6"  style="cursor: pointer">
                            <div class="panel red">
                                <div class="symbol">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo !empty($inactive_user)?$inactive_user:0 ?></div>
                                    <div class="title">Inactive Service Provider</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--statistics end-->
                </div>
                
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="panel">
                        <header class="panel-heading">
                            Service Provider Analytics
                            
                        </header>
                        <div class="panel-body">
                            <ul class="goal-progress">
                                <li>

                                    <div class="details">
                                        <div class="title" id="myDiv" style=" height:100%; width:100%; margin-left:4px;">

                                            





                                        </div>

                                    </div>

                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>




        </div>
<?php echo $this->Html->script('plotly-latest.min.js')?>

<script>
function generate_chart()
{
    $.post("<?php echo $this->webroot; ?>admin/analytics/providergraph",function(data){
    var trace1 = {
        name: 'Rest of world', 
        marker: {color: 'rgb(55, 83, 109)'}, 
        type: 'bar',
        offset:0
      };
      trace1.x=data.x;
      trace1.y=data.y;
      var flag=data.flag;
      

    var data = [trace1];

            var layout = {
          title: 'Service Provider Joining Daily Report',
          xaxis: {
              tickfont: {
              size: 11, 
              color: '#38a1d8',
              
            },
            //tickFormat:function(d){ console.log(d);}
        
            }, 
          yaxis: {
            title: 'Service Provider',
            titlefont: {
              size: 16, 
              color: 'rgb(107, 107, 107)'
            },
            
            tickfont: {
              size: 14, 
              color: "rgb(107, 107, 107)"
            },
            
          }, 
          legend: {
            x: 0, 
            y: 1.0, 
            bgcolor: 'rgba(255, 255, 255, 0)',
            bordercolor: 'rgba(255, 255, 255, 0)'
          }, 
          barmode: 'group', 
          bargap: 0.15, 
          bargroupgap: 0,
        };
        if(flag==0)
        {
         layout.yaxis.range=range; 
        }
        Plotly.newPlot('myDiv', data, layout,{displayModeBar: false});    
    },"json");
}

$(document).ready(function(){
generate_chart();    
})
</script>    