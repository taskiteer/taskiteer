<?php
?>
<section class="container">
        
<div class="login">
  <h1>Reset Password</h1>
  <form id="UserAdminLoginForm" name="UserAdminLoginForm" action="<?php echo $this->webroot; ?>admin/users/reset_password" class="contact_form" method="post">
   <input type="hidden" name="data[User][id]" id"user_id" value="<?php echo $userid;?>" >
	<p>
	Password: <br>
	<input type="password" name="data[User][new_pass]" maxlength="100" id="new_pass" class="contact_text_box" placeholder="Enter new password" required="required" value=""/></p>
	<p>
	Confirm Password: <br>
	<input type="password" name="data[User][con_pass]" maxlength="100" id="con_pass" class="contact_text_box" placeholder="Enter confirm password" required="required" value=""/></p>
	<p class="submit"><input type="submit" name="commit" value="Submit" style="float:none;width:87%;border-radius:0px;margin-bottom:20px;background:#FF0085;color:#ffffff;border-color:#FF0085;box-shadow:none !important;"></p>
  </form>
  	 <div style="margin:0px;auto;width:100%;display:block;height:50px;border-top:1px solid #DEDEDE;">
	     <div style="width: 41%;float:left;border-right:1px solid #DEDEDE;padding-top:10px;padding-bottom:10px; padding-left: 32px;"><a href="<?php echo $this->webroot; ?>admin/" style="color:#FF0085;text-decoration:none;">Go Back</a></div>
	      <div style="float:left;padding-top:10px;padding-bottom:10px; padding-left: 25px;"><a href="<?php echo $this->webroot; ?>admin/users/fotgot_password" style="color:#FF0085;text-decoration:none;">Need Help?</a></div>
	</div>
</div>
</section>
