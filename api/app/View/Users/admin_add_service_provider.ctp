<div class="users form">
<?php

echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Service Provider'); ?></legend>
	<?php
	//pr($service_areas);

		echo $this->Form->input('name',array('required' => 'required','style'=>'width:500px;'));
		echo $this->Form->input('email', array('type' => 'email','required' => 'required','style'=>'width:500px;'));
		echo $this->Form->input('password', array('type' => 'password','required' => 'required','style'=>'width:500px;'));
        //echo $this->Form->input('address', array('required' => 'required','style'=>'width:500px;'));
        //echo $this->Form->input('pin', array('required' => 'required','style'=>'width:500px;'));
        echo $this->Form->input('mobile_no', array('required' => 'required','style'=>'width:500px;'));

        echo $this->Form->input('image', array('type' => 'file','required' => 'required','style'=>'width:500px;'));

        echo $this->Form->input('service_id',array('type'=>'select','options'=>$pst,'style'=>'width:500px;','class'=>'form-control'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
