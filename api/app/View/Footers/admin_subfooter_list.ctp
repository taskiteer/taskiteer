<div class="faqs index">
	<h2><?php echo __('Sub Footer link'); ?></h2>
	<table style="width:100%;border:0px solid red;">
	<tr>
		<td style="width:70%;border:0px solid red;">&nbsp;</td>
		<td style="width:30%;border:1px dashed #ccc;text-align:center"><a href="<?php echo($this->webroot);?>admin/footers/subfooter_add/<?php echo($this->params['pass'][0])?>">Add New sub footerlink</a></td>
	</tr>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th width="10%"><?php echo $this->Paginator->sort('id'); ?></th>
			<th width="70%"><?php echo $this->Paginator->sort('name'); ?></th>
			<th width="30%"><?php echo $this->Paginator->sort('link'); ?></th>
			<th class="actions" width="30%"><?php echo __('Actions'); ?></th>
	</tr>
	<?php 
        $ServiceCnt=0;
        foreach ($Footers as $Footer): 
		//pr($User);
		//exit;
            $ServiceCnt++;?>
	<tr>
		<td width="10%" style="text-align:left"><?php echo $ServiceCnt;?>&nbsp;</td>
		<td width="70%" style="text-align:left"><?php echo h($Footer['Footer']['name']); ?>&nbsp;</td>
		<td width="30%" style="text-align:left"><?php echo h($Footer['Footer']['link']); ?>&nbsp;</td>
		<td class="actions" width="30%" style="text-align:left">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'editsubfooter', $Footer['Footer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'subfootdel', $Footer['Footer']['id'], $Footer['Footer']['parent_id']), null, __('Are you sure you want to delete %s?', $Footer['Footer']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo($this->element('admin_sidebar'));?>