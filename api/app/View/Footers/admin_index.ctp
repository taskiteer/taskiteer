<div class="faqs index">
	<h2><?php echo __('Footer Management'); ?></h2>
	<table style="width:100%;border:0px solid red;">
	<tr>
		<td style="width:70%;border:0px solid red;">&nbsp;</td>
		<td style="width:30%;border:1px dashed #ccc;text-align:center"><a href="<?php echo($this->webroot);?>admin/footers/add">Add New Footer Catagory</a></td>
	</tr>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th width="10%"><?php echo $this->Paginator->sort('id'); ?></th>
			<th width="70%"><?php echo $this->Paginator->sort('name'); ?></th>
			<!--<th width="70%"><?php echo $this->Paginator->sort('link'); ?></th>-->
			<th class="actions" width="20%"><?php echo __('Actions'); ?></th>
	</tr>
	<?php 
        $ServiceCnt=0;
        foreach ($footers as $footer): 
            $ServiceCnt++;?>
	<tr>
		<td width="10%" style="text-align:left"><?php echo $ServiceCnt;?>&nbsp;</td>
		<td width="70%" style="text-align:left"><?php echo h($footer['Footer']['name']); ?>&nbsp;</td>
		<!--<td width="70%" style="text-align:left"><?php echo h($footer['Footer']['link']); ?>&nbsp;</td>-->
               
		<td class="actions" width="20%" style="text-align:left">
		    <?php echo $this->Html->link(__('Sublink'), array('action' => 'subfooter_list', $footer['Footer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $footer['Footer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $footer['Footer']['id']), null, __('Are you sure you want to delete # %s?', $footer['Footer']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo($this->element('admin_sidebar'));?>