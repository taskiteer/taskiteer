<div class="users form">
   <form action="" enctype="multipart/form-data" method="post" accept-charset="utf-8">
      <fieldset>
         <legend>View Applicant</legend>
         <div class="input text">
         <label for="UserName">Name</label>
         <?php echo $applicant["Provider"]["first_name"].' '.$applicant["Provider"]["last_name"];  ?>
         
         </div>
         <div class="input text">
         <label for="UserName">Email</label>
         <?php echo $applicant["Provider"]["email"];  ?>
         
         </div>
         <div class="input text">
         <label for="UserName">Phone</label>
         <?php echo $applicant["Provider"]["mobile_no"];  ?>
         
         </div>
         
         <div class="input text">
         <label for="UserName">Residecial Location</label>
         <?php echo $applicant["Provider"]["address"];  ?>
         
         </div>
         
         <div class="input text">
         <label for="UserName">Business Name</label>
         <?php echo $applicant["Provider"]["business_name"];  ?>
         
         </div>
         <div class="input text">
         <label for="UserName">About</label>
         <?php echo $applicant["Provider"]["about_me"];  ?>
         
         </div>
         <div class="input text">
         <label for="UserName">Profile Image</label>
         <img style=" height:200px; width:200px;" src="<?php echo $this->webroot; ?>user_images/<?php echo $applicant["Provider"]["image"]; ?>" class="img-responsive">
         
         </div>
         
        <div class="input text">
         <label for="UserName">Service Provide</label>
         <?php echo $serviceprovide;  ?>
         
         </div>
         
         <div class="input text">
         <label for="UserName">Service Location</label>
         <?php echo $applicant["Provider"]["service_location"];  ?>
         
         </div>
         
         <div class="input text">
         <label for="UserName">Service Radius(Miles)</label>
         <?php echo $applicant["Provider"]["service_redious"];  ?>
         
         </div>
         
         <div class="input text">
         <label for="UserName">Phone Verified</label>
         <?php  echo $applicant["Provider"]["is_verify"]==1?"Yes":"No";  ?>
         
         </div>
         <div class="input text">
             <button type="button" onclick="location.href='<?php echo $this->webroot; ?>admin/providers/applicant/'">BACK</button>    
         </div>
         
         
      </fieldset>
   </form>
</div>

