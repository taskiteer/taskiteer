<?php ?>  
<div class="providers index">
            <h2 style="width:400px;float:left;"><?php echo __('List of Applicants'); ?></h2>
            <div>
           <?php //echo $this->Form->create("Provider");?>
        <?php echo $this->Form->create("Filter",array('class' => 'filter'));?>
            <table style=" border:none;">
                <tr>

                    <td>Email</td>
                    <td><?php echo $this->Form->input("email",array('type'=>"email",'class'=>'form-control','placeholder'=>'Search by Email...','label'=>false,'div'=>false))?></td>
                    <td>Name</td>
                    <td><?php echo $this->Form->input("name",array('type'=>"text",'class'=>'form-control','placeholder'=>'Search by Name...','label'=>false,'div'=>false))?></td>
                    <td>
                        <input type="submit" name="search" value="Search">
                        <input type="button" name="clear" value="Clear" onclick="location.href='<?php echo $this->webroot; ?>admin/providers/applicant'" style=" float:right; width:80px;">
                    
                    </td>
                </tr>
            </table>
                </form>
            <?php //echo $this->Form->end();?>
            </div>

            <table cellpadding="0" cellspacing="0">
            <tr>
                <th>SN<?php //echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('name'); ?></th>
                <th><?php echo $this->Paginator->sort('Image'); ?></th>
                <th><?php echo $this->Paginator->sort('email','Email'); ?></th>
                <th><?php echo $this->Paginator->sort('mobile_no','Mobile'); ?></th>
                <th><?php echo $this->Paginator->sort('date','Date'); ?></th>
                <th><?php echo $this->Paginator->sort('is_approve'); ?></th>

                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php
            $ProviderCnt=0;

            foreach ($providers as $user):
                //pr($user);
                $ProviderCnt++;
                $uploadImgPath = WWW_ROOT.'user_images';
            ?>
            <tr>
              <td><?php echo $ProviderCnt;//echo h($user['Provider']['id']); ?>&nbsp;</td>
              <td><?php echo h($user['Provider']['name']); ?>&nbsp;</td>
              <td><?php
            $per_profile_img=isset($user['Provider']['image'])?$user['Provider']['image']:'';
            if($per_profile_img!='' && file_exists($uploadImgPath . '/' . $per_profile_img)){
                $ImgLink=$this->webroot.'user_images/'.$per_profile_img;
            }else{
                $ImgLink=$this->webroot.'user_images/default.png';
            }
            echo '<img src="'.$ImgLink.'" alt="" height="42px" width="42px"/>';
            ?>&nbsp;
          </td>
              <td><?php echo h($user['Provider']['email']); ?>&nbsp;</td>
              <td><?php echo h($user['Provider']['mobile_no']); ?>&nbsp;</td>
              <td><?php echo date('d M Y', strtotime($user['Provider']['registration_date'])); ?>&nbsp;</td>
              <td><?php echo $user['Provider']['type']=="SP"?"Yes":"No"; ?>&nbsp;</td>
            <td>

               <!--<a href="<?php //echo $this->webroot?>admin/providers/reset_pass/<?php echo $user['Provider']['id']?>" class="btn btn-primary">Reset Password</a>&nbsp;&nbsp;!-->

               <a href="<?php echo $this->webroot?>admin/providers/view_applicant/<?php echo $user['Provider']['id']?>" class="btn btn-success">View</a>&nbsp;&nbsp;

               <?php
               if($user['Provider']['type']=="U")
               {
               ?>
               
              <a href="<?php echo $this->webroot?>admin/providers/approveal/<?php echo $user['Provider']['id']?>" class="btn btn-danger">Approveal</a>&nbsp;&nbsp;
               <?php }?>
            </td>
            </tr>
    <?php endforeach; ?>
            </table>
            <p>
            <?php
            echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
            ?>	</p>
            <div class="paging">
            <?php
                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
            ?>
            </div>
    </div>
    <?php //echo $this->element('admin_sidebar'); ?>


    <script type="text/javascript">
        $(document).ready(function(){
            $('.ProviderStatus').click(function(){
                var ProviderStatus=$(this).attr('id');
                $('#search_is_active').val(ProviderStatus);
                $('#Searchuserfrm').submit();
                //document.Searchuserfrm.submit();
                //$('#ProviderAdminListForm')[0].submit();
            });
        });
    </script>
    <style>
    .title a{
        color: #fff !important;
    }
    </style>