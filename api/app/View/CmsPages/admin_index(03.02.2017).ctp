<div class="contents index">
	<h2 style="width:400px;float:left;"><?php echo __('Contents'); ?></h2>
<!--	<a href="<?php echo $this->webroot.'admin/cms_page/add'; ?>" style="float:right">Content Add</a>-->
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('page_name'); ?></th>
			<th><?php echo $this->Paginator->sort('Description'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php

        $d=1;
        foreach ($contents as $content): ?>
	<tr>
		<td><?php echo $d; ?>&nbsp;</td>
		<td style="text-align:left"><?php echo h($content['CmsPage']['page_title']);?></td>
		<td style="text-align:left"><?php echo $content['CmsPage']['page_description']?></td>
		<td>
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $content['CmsPage']['id'])); ?>
            <a href="<?php echo $this->webroot?>admin/cms_page/view/<?php echo $content['CmsPage']['id']?>" style="width:50px;"><img src="<?php echo $this->webroot.'images/view.png';?>" style="height:20px; width:20px;" alt="view" title="View"></a>&nbsp;&nbsp;
			<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $content['CmsPage']['id'])); ?>
            <a href="<?php echo $this->webroot?>admin/cms_page/edit/<?php echo $content['CmsPage']['id']?>" style="width:50px;"><img src="<?php echo $this->webroot.'images/edit.png';?>" style="height:20px; width:20px;" alt="edit" title="Edit"></a>&nbsp;&nbsp;

		    <?php //echo $this->Html->link(__('Delete'), array('action' => 'delete', $content['CmsPage']['id']), null, __('Are you sure you want to delete %s?')); ?>

            <a href="<?php echo $this->webroot?>admin/cms_page/delete/<?php echo $content['CmsPage']['id']?>" style="width:50px;"><img src="<?php echo $this->webroot.'images/delete.png';?>" style="height:20px; width:20px;" alt="delete" title="Delete" onclick="return confirm('are you sure to delete this content?');"></a>&nbsp;&nbsp;
		</td>
	</tr>
<?php
$d++;
endforeach;

?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>