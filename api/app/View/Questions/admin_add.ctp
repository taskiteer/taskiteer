<div class="emailTemplates form">
<?php echo $this->Form->create('Question',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Question'); ?></legend>
	<?php
	
        echo $this->Form->input('name',array('label' => 'Question','required' => 'required'));
        echo $this->Form->input('q_order',array('label' => 'Question No'));
        $sizes = array('1' => 'Radio', '2' => 'Checkbox', '3' => 'Dropdown','4' => 'Textbox');
        echo $this->Form->input('type', array(
           'options' => $sizes,
           'empty' => '(choose one)'
       ));


	?>
                <br>
                <button type="button" class="btn btn-primary" onclick="add_option();">Add option</button>
                <p>( To add more options click to this button )</p>
                <div id="answer_part">
                  
                </div>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<script type="text/javascript">
   function add_option()
   {
      $('#answer_part').append('<p><input type="text" name="answers[]"/></p>'); 
   }
    </script>