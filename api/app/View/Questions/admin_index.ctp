<div class="categories index">
	<h2><?php echo __(' List Service Question'); ?></h2>
	<table cellpadding="0" cellspacing="0">
            
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><a href="<?php echo $this->webroot?>admin/questions/add/<?php echo $cat_id;?>"><button class="btn btn-success">ADD</button></a></td>
	</tr>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Category'); ?></th>
                        
                        <th><?php echo $this->Paginator->sort('Question'); ?></th>
                        
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
        $d=1;
        foreach ($posts as $post):
        ?>
	<tr>
		<td><?php echo $d; ?>&nbsp;</td>
		<td><?php echo h($post['Category']['name']);?></td>
                <td><?php echo h($post['Question']['name']);?></td>
                
		<td>

	          <?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $content['EmailTemplate']['id'])); ?>
                  <a href="<?php echo $this->webroot?>admin/questions/edit/<?php echo $post['Question']['id']?>/<?php echo $post['Question']['service_id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;
                  <a href="<?php echo $this->webroot?>admin/questions/delete/<?php echo $post['Question']['id']?>/<?php echo $post['Question']['service_id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this?');">Delete</a>&nbsp;&nbsp;
		</td>
	</tr>
<?php
$d++;
endforeach;

?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>