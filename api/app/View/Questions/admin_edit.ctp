
<div id='ans_status' class="success" style="display: none;"></div>
<div class="categories form">
<?php echo $this->Form->create('Question',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Question'); ?></legend>
                <?php
	
        echo $this->Form->input('name',array('label' => 'Question','required' => 'required'));
         echo $this->Form->input('q_order',array('label' => 'Question No'));
          $sizes = array('1' => 'Radio', '2' => 'Checkbox', '3' => 'Dropdown','4' => 'Textbox');
        echo $this->Form->input('type', array(
           'options' => $sizes,
           'empty' => '(choose one)'
       ));
         //print_r($answers);
	?>
                 <div id="answer_part">
                  <?php
                  foreach($answers as $key=>$val)
                  {
                      //echo $key;
                      //echo $val;
                   ?>
                     <p><input type="text" id="<?php echo $key; ?>" value="<?php echo $val; ?>"/>
                         &nbsp; &nbsp;<button type="button" class="btn btn-success" onclick="update_ans(<?php echo $key; ?>);">Update</button>
                     </p>  
                  <?php
                  }
                  ?>
                </div>

                


	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>

<script type="text/javascript">
    function update_ans(id)
    {
        var ans = $("#"+id).val();
      $.ajax({
            url: "<?php echo $this->webroot; ?>questions/updateAnswer",
            type: 'post',
            dataType: 'json',
            data: {
                id:id,
                ans:ans
            },
            success: function(result){
                //alert(result.ack);
                if(result.ack == '1') {
              
                    $('#ans_status').html('Answer updated successfully');
                    $('#ans_status').show(); 
                    setTimeout(function(){ $('#ans_status').hide(); }, 3000);
                }
                else{
                    $('#ans_status').html('Answer not updated');
                    $('#ans_status').removeClass('success');
                    $('#ans_status').addClass('error');
                    $('#ans_status').show(); 
                    setTimeout(function(){ $('#ans_status').hide(); }, 3000);
                } 
                   
            }
        });
    }
    </script>
