<div class="categories index">
	<h2><?php echo __('Faq'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('question'); ?></th>
                        <th><?php echo $this->Paginator->sort('answer'); ?></th>

			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php

        $d=1;

        foreach ($allfaq as $faq):


            ?>
	<tr>
		<td><?php echo $d; ?>&nbsp;</td>
		<td><?php echo h($faq['Faq']['question']);?></td>
                <td><?php echo h($faq['Faq']['answar']);?></td>

		<td>

	          <?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $content['EmailTemplate']['id'])); ?>
                  <a href="<?php echo $this->webroot?>admin/faqs/edit/<?php echo $faq['Faq']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;
                  <a href="<?php echo $this->webroot?>admin/faqs/delete/<?php echo $faq['Faq']['id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this faq?');">Delete</a>&nbsp;&nbsp;
		</td>
	</tr>
<?php
$d++;
endforeach;

?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>