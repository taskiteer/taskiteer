<?php //echo '<pre>'; print_r($bookings); echo '</pre>'; ?>
<div class="categories index">
	<h2><?php echo __('Booking'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('User'); ?></th>
			<th><?php echo $this->Paginator->sort('Service Provider'); ?></th>
			<th><?php echo $this->Paginator->sort('Service Name'); ?></th>
            <th><?php echo $this->Paginator->sort('Address'); ?></th>
            <th><?php echo $this->Paginator->sort('State'); ?></th>
            <th><?php echo $this->Paginator->sort('Start Date'); ?></th>
            <th><?php echo $this->Paginator->sort('End Date'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($bookings as $book): ?>
	<tr>
		<td><?php echo h($book['Booking']['id']); ?>&nbsp;</td>
		<td><?php echo h($book['User']['name']);?></td>
		<td><?php echo ($book['User1']['name']);?></td>
		<td><?php echo ($book['Post']['title']);?></td>
        <td><?php echo ($book['Booking']['address']);?></td>
        <td><?php echo ($book['Booking']['state']);?></td>
        <td><?php echo date('d M Y', strtotime($book['Booking']['start_date']));?></td>
        <td><?php echo date('d M Y', strtotime($book['Booking']['end_date']));?></td>
		<td>
                <a href="<?php echo $this->webroot?>admin/bookings/view/<?php echo $book['Booking']['id']?>" class="btn btn-primary">View</a>&nbsp;&nbsp;
		</td>
	</tr>
       <?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>