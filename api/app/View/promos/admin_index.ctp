<div class="users index">
	<h2 style="width:400px;float:left;"><?php echo __('Promo Listing'); ?></h2>
	<div>
		<!--<form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">
		<table style=" border:none;">
			<tr>

				<td>Email</td>
				<td><input type="email" name="email" value="<?php //echo isset($email)?$email:'';?>" placeholder="Email"></td>
				<td>Name</td>
				<td><input type="text" name="name" value="<?php //echo isset($name)?$name:'';?>" placeholder="Name"></td>
				<td><input type="submit" name="search" value="Search"></td>
			</tr>
		</table>
		</form>-->
    </div>

            <table cellpadding="0" cellspacing="0">
            <tr>
                <th>SN</th>
                <th><?php echo $this->Paginator->sort('Promo Code'); ?></th>
				<th><?php echo $this->Paginator->sort('Use Times'); ?></th>
                <th><?php echo $this->Paginator->sort('date','Date'); ?></th>
                <th><?php echo $this->Paginator->sort('status','Status'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php
            $PromoCnt=0;

            foreach ($promos as $promo):
                $PromoCnt++;
                $uploadImgPath = WWW_ROOT.'user_images';
            ?>
            <tr>
              <td><?php echo $PromoCnt;?>&nbsp;</td>
              <td><?php echo h($promo['Promo']['promo_code']); ?>&nbsp;</td>
			  <td><?php echo h($promo['Promo']['used_times']); ?>&nbsp;</td>
              <td><?php echo date('d M Y', strtotime($promo['Promo']['created_at'])); ?>&nbsp;</td>
              <td>
              <?php
              if($promo['Promo']['status']==0){
              ?>
			  <a href="<?php echo $this->webroot?>admin/promos/activate/<?php echo $promo['Promo']['id']?>" onclick="return confirm('are you sure to Activate this promo?');" class="btn btn-danger">Inactive</a>&nbsp;&nbsp;
				<?php
              }else{?>
				<a href="<?php echo $this->webroot?>admin/promos/deactivate/<?php echo $promo['Promo']['id']?>" class="btn btn-success" onclick="return confirm('are you sure to Inactivate this promo?');">Active</a>
              <?php
              }
             ?>
            </td>
            <td>
               <a href="<?php echo $this->webroot?>admin/promos/edit/<?php echo $promo['Promo']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;


               <a href="<?php echo $this->webroot?>admin/promos/delete/<?php echo $promo['Promo']['id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this promo?');">Delete</a>&nbsp;&nbsp;
            </td>
            </tr>
    <?php endforeach; ?>
            </table>
            <p>
            <?php
            echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
            ?>	</p>
            <div class="paging">
            <?php
                    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
            ?>
            </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function(){
            $('.UserStatus').click(function(){
                var UserStatus=$(this).attr('id');
                $('#search_is_active').val(UserStatus);
                $('#Searchuserfrm').submit();
            });
        });
    </script>
    <style>
    .title a{
        color: #fff !important;
    }
    </style>