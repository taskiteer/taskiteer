<div class="users form">
	<?php echo $this->Form->create('Promo'); ?>
	<fieldset>
		<legend><?php echo __('Edit Promo'); ?></legend><?php
		echo $this->Form->input('id');
		echo $this->Form->input('promo_code',array('style'=>'width:500px;','required'=>'required'));
		echo $this->Form->text('used_times', array('type' => 'number','style'=>'width:500px;','required' => 'required'));
        ?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>