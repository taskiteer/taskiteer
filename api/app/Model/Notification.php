<?php

App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Notification extends AppModel {
 public $belongsTo = array(
        
       'Lead'=>array(
       'className'=>'Lead',
       'foreignKey'=>'lead_id'
       ),
      'Message'=>array(
       'className'=>'Message',
       'foreignKey'=>'message_id'
       ),
       'RequestQuote'=>array(
       'className'=>'RequestQuote',
       'foreignKey'=>'quote_id'
       ),
     'User'=>array(
       'className'=>'User',
       'foreignKey'=>'sent_by'
       )
        
    );
    
}
