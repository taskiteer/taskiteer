<?php
App::uses('AppModel', 'Model');
#App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 * @property EmailNotification $EmailNotification
 * @property FavoriteList $FavoriteList
 * @property FavoriteShop $FavoriteShop
 * @property FavoriteTreasury $FavoriteTreasury
 * @property InboxMessage $InboxMessage
 * @property Preference $Preference
 * @property Privacy $Privacy
 * @property Security $Security
 * @property SentMessage $SentMessage
 * @property ShippingAddress $ShippingAddress
 * @property Shop $Shop
 */
class Category extends AppModel {



/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(


	);

	/*public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = md5(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}*/

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
         //public $hasOne = 'UserImage';
	public $hasMany = array(
            'Children' => array(
            'className' => 'Category',
            'foreignKey' => 'parent_id'
        )

	);
	public $belongsTo = array(

        


  );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['name'])) {
            $this->data[$this->alias]['slug'] = $this->createSlug($this->data[$this->alias]['name']);
        }
        return true;
    }


}