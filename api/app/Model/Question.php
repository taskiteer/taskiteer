<?php

App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Question extends AppModel {

    public $belongsTo = array(

       'Post'=>array(
       'className'=>'Post',
       'foreignKey'=>'service_id'
       ),
        'Category'=>array(
       'className'=>'Category',
       'foreignKey'=>'cat_id'
       )


  );
    
    public $hasMany = array(
         'Answer' => array(
            'className' => 'Answer',
            'foreignKey' => 'q_id'
        )
	);
}
