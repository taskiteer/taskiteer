<?php
App::uses('AppModel', 'Model');
/**
 * State Model
 *
 * @property Preference $Preference
 */
class RequestQuote extends AppModel {

    public $belongsTo = array(
        
         'User'=>array(
       'className'=>'User',
       'foreignKey'=>'posted_by'
       ),
        'Lead'=>array(
       'className'=>'Lead',
       'foreignKey'=>'lead_id'
       ),
        'Projectowner'=>array(
       'className'=>'User',
       'foreignKey'=>'posted_to'
       )
        
    );

}
