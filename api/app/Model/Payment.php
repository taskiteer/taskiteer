<?php

App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Payment extends AppModel {

    public $belongsTo = array(

       'User'=>array(
       'className'=>'User',
       'foreignKey'=>'user_id'
       ),
        
        'PayTo'=>array(
       'className'=>'User',
       'foreignKey'=>'pay_to'
       ),
       'Lead'=>array(
       'className'=>'Lead',
       'foreignKey'=>'lead_id'
       ),
       'RequestQuote'=>array(
       'className'=>'RequestQuote',
       'foreignKey'=>'quote_id'
       ) 
        


  );
}
