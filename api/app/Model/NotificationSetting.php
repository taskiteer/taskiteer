<?php

App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class NotificationSetting extends AppModel {
 public $belongsTo = array(
     'User'=>array(
       'className'=>'User',
       'foreignKey'=>'user_id'
       )
        
    );
    
}
