<?php
App::uses('Controller', 'Controller');
APP::uses('Controller/Component/Auth','Auth');
class ApiController extends Controller {
    //...

    public $components = array(
        'Flash',
        'Auth' => array(
			'authError'=>'You are not authorised to visit this api',
            'authenticate'=>array(
				'Basic'=>array(
					'fields'=>array(
						'username'=>'username',
						'password'=>'password'
					)
				)
			)
        )
    );

    public function beforeFilter() {
        $this->Auth->allow('index', 'token');
    }

	public function index(){
		echo "Working";
		exit();
	}

	public function token(){
		if ($this->request->is('post')) {
	        if ($this->Auth->login()) {

	        }
	    }
	}

	public function prot(){
		$res = array(
			'success'=>true,
			'message'=>"You are authorised!!"
		);
		echo json_encode($res);
		exit();
	}
}
