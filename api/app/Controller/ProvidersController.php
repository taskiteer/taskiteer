<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class ProvidersController extends AppController {

/**
 * Components
 *
 * @var array
 */
 public $components = array('Paginator');
 public function saveabout() 
 {
    $jsonData = $this->request->input('json_decode');
    foreach ($jsonData as $key =>$val)
    {
        $this->request->data["Provider"][$key]=$val;
    }
    
    try {
    $this->Provider->save($this->request->data);
    $data=array("Ack"=>1,"msg"=>"Saved successfully");    
    } catch (Exception $ex) 
    {
     $data=array("Ack"=>0);        
    }
    echo json_encode($data);exit;

     
 }
 
 function  details($id=null)
 {
      
     $SITE_URL=Configure::read("SITE_URL");
     $this->loadModel("Lead");
     if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }
            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
            if($data->status == 'success')
            {
                date_default_timezone_set($data->timezone);
                $timezone = $data->timezone;
            }
            else
            {
                date_default_timezone_set('Asia/Kolkata');
                $timezone = 'Asia/Kolkata';
            }
            $tz_from = "UTC";
            $tz_to = $timezone;
            $format = 'M d,Y';
      $this->Provider->bindModel(
               array(
                 'hasMany'=>array(
                     'Rating' =>array(
                      'className' => 'Rating',
                      'foreignKey' => 'to_id',
                      'fields' => array("Rating.id","Rating.review_from_name","Rating.avg_score","Rating.comment","Rating.ratting_date"),
                  ),
                    'Portfolio' =>array(
                      'className' => 'Portfolio',
                      'foreignKey' => 'user_id',
                      'fields' => array("Portfolio.id","Portfolio.image"),
                  )   
                     
               )
            )
        ); 
      $options=array(
      "fields"=>array("Provider.id","Provider.name","Provider.image","Provider.avg_score","Provider.address","Provider.service_standout","Provider.enjoy"),
      "conditions"=>array("Provider.id"=>$id)    
      );
      $provider=$this->Provider->find("first",$options);
      $provider['Provider']["image"]=!empty($provider['Provider']["image"])?$SITE_URL.'user_images/'.$provider['Provider']["image"]:$SITE_URL.'nouser.png';
      $provider["Provider"]["avg_score"]= round($provider["Provider"]["avg_score"]);
      $total_review=count($provider["Rating"]);
      $ratings=array();
      foreach ($provider["Rating"] as $rating)
      {
            $dt = new DateTime($rating["ratting_date"], new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $start_time=$dt->format($format) ; 
            $rating["ratting_date"]=$start_time;
            $rating["avg_score"]=round($rating["avg_score"]);
            $ratings[]=$rating;
      }
      $hired=$this->Lead->find("count",array("conditions"=>array("Lead.assigned_to"=>$id)));
       $provider['Provider']["hired"]=$hired;
      $provider['Provider']["reviews"]=$ratings;
      unset($provider['Rating']);
      
      //for portfolios.................................
      $portfolios=array();
      foreach ($provider["Portfolio"] as $portfolio)
      {
          
            $portfolio["image"]=!empty($portfolio["image"])?$SITE_URL.'portfolioimg/'.$portfolio["image"]:$SITE_URL.'nouser.png';
            $portfolios[]=$portfolio;
      }
      $provider['Provider']["portfolio"]=$portfolios;
      unset($provider['Portfolio']);
     
      $data=array("Ack"=>1,"response"=>$provider);
      echo json_encode($data);exit;
      
 }
 
//search pro .............................................................................
       public function list_provider() {
            $SITE_URL=Configure::read("SITE_URL");
            $this->loadModel("Lead");
            $options='';
           
             $jsonData = $this->request->input('json_decode');
           // print_r($jsonData);
            if(isset($jsonData->subcat_id))
            {
             $subcat_id=$jsonData->subcat_id;
            }
            if(isset($jsonData->lat))
            {
             $lat=$jsonData->lat;
            }
            if(isset($jsonData->lng))
            {
             $lng=$jsonData->lng;
            }
           //$subcat_id=47;
           //$lat=22;
           //$lng=88;
          
              
            if(!empty($lat) && !empty($lng)&&!empty($subcat_id))
            {
               
                 $this->Provider->virtualFields = array(
                    'distance' => "(3959 * acos(cos(radians('".$lat."')) * cos(radians(Provider.service_lat)) * cos( radians(Provider.service_lang) - radians('".$lng."')) + sin(radians('".$lat."')) * sin(radians(Provider.service_lat))))"
                );
               $options = array('conditions' => array("Provider.type"=>"SP",'FIND_IN_SET("'.$subcat_id.'",`Provider.cat_id`)','Provider.distance <  ' => '100'), 'order' => array('Provider.id' => 'desc'));  
            
               
            }
          elseif(!empty($subcat_id))
           {
             
              $options = array('conditions' => array("Provider.type"=>"SP",'FIND_IN_SET("'.$subcat_id.'",`Provider.cat_id`)'), 'order' => array('Provider.id' => 'desc')); 
           } 
          
           elseif(!empty($lat) && !empty($lng)) {
               
               $this->Provider->virtualFields = array(
                    'distance' => "(3959 * acos(cos(radians('".$lat."')) * cos(radians(Provider.service_lat)) * cos( radians(Provider.service_lang) - radians('".$lng."')) + sin(radians('".$lat."')) * sin(radians(Provider.service_lat))))"
                );
                $options = array('conditions' => array("Provider.type"=>"SP",'Provider.distance <  ' => '100'), 'order' => array('Provider.id' => 'desc')); 
               
            }
            else{
                $options=array("conditions"=>array("Provider.type"=>"SP"));
            }
            
        $lists=$this->Provider->find('all',$options);
        //print_r($lists);die;
        
                foreach ($lists as $key =>$value)
                {
                  $value['Provider']['image']=!empty($value['Provider']['image'])?$SITE_URL.'user_images/'.$value['Provider']['image']:$SITE_URL.'nouser.png';  
                  $List[]= $value['Provider'];
                  // print_r($value['Provider']);
                  
                }
                //die;
                if(!empty($lists))
                {
                    $data=array('Ack'=>1,'List'=>$List);
                }
                else
                {
                    $data=array('Ack'=>0,'List'=>"");
                }
                echo json_encode($data);exit;
        
        }

 /**
 * index method
 *
 * @return void
 */
public function admin_applicant()
{
    
    
    
            $conditions["Provider.is_applicant"]=1;
            if(($this->request->is('post') || $this->request->is('put')) && isset($this->data['Filter'])){
            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            $filter_url['page'] = 1;
            foreach($this->data['Filter'] as $name => $value){
                if($value){
                    $filter_url[$name] = urlencode($value);
                }
            }	
            return $this->redirect($filter_url);
        } else {
            $limit = 20;
            foreach($this->params['named'] as $param_name => $value){
                if(!in_array($param_name, array('page','sort','direction','limit'))){
                    
                     $conditions['Provider.'.$param_name ." LIKE "] = '%'.urldecode($value).'%';
                     $this->request->data['Filter'][$param_name] = urldecode($value);
                }
            }
        }

    $this->Paginator->settings=array("conditions"=>$conditions,"order"=>"Provider.id desc");    
    $providers=$this->Paginator->paginate("Provider");
    $this->set(compact("providers"));
    
}

public function admin_view_applicant($id=null)
{
    $applicant=$this->Provider->find("first",array("conditions"=>array("Provider.id"=>$id)));
    $this->loadModel("Category");
    $categories=$this->Category->find("all",array("fields"=>array("Category.name"),
                "conditions"=>array("Category.id"=>explode(",",$applicant["Provider"]["cat_id"]))));
    foreach ($categories as $cat)
    {
    $service[]=$cat["Category"]["name"];            
    }
    $serviceprovide=implode(",",$service);
    $this->set(compact("applicant","serviceprovide"));
    
    
}

public function admin_approveal($id=null) 
{
    
    $this->Provider->id = $id;
    try {
        
    $this->Provider->saveField('type', "SP");
    $this->Session->setFlash('Approved as a Professional Successfully.', 'default', array(
                    'class' => 'success'
                ));
    $this->redirect(array("action"=>"applicant"));

        
    } catch (Exception $ex) {
        print_r($ex);    
    }
    
    
}

	

}
