<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class PaymentsController extends AppController
{
    
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    
    function savecreditcard()
    {
        $this->loadModel("User");
        $this->loadModel("Billing");
        require_once(ROOT . '/app/Vendor' . DS  . 'stripe'.DS.'autoload.php');
	$this->loadModel("Setting");
        $setting=$this->Setting->find("first");
        if ($this->request->is(array('post','put'))) 
        {     
          $jsonData = $this->request->input('json_decode');
          $user_id=$jsonData->user_id;
          $token=$jsonData->token;
          $stripe = array(
                        "secret_key"      => $setting["Setting"]["stripe_scret_key"],
                        "publishable_key" => $setting["Setting"]["stripe_public_key"]
                      );
                     
          \Stripe\Stripe::setApiKey($stripe['secret_key']);
          $user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id)));
          $billing=$this->Billing->find("first",array("conditions"=>array("Billing.user_id"=>$user_id)));

          try {
              
              $customer = \Stripe\Customer::create(array(
                                'email' => $user["User"]["email"],
                                'source'  => $token
                                        
                                        
                                        
                                        
                            ));
            $customerId = $customer->id;
            $this->request->data["Billing"]["firstname"]=$user["User"]["name"];
            $this->request->data["Billing"]["user_id"]=$user_id;
            $this->request->data["Billing"]["token"]=$token;
            $this->request->data["Billing"]["customer_id"]=$customerId; 
            if(!empty($billing))
            {
               $this->request->data["Billing"]["id"]=$billing["Billing"]["id"];

            }
            else
            {
               $this->request->data["Billing"]["createtime"]= gmdate("Y-m-d H:i:s");
            }
            if($this->Billing->save($this->request->data))
            {
//                    $this->User->id = $user_id;
//                    $this->User->saveField('braintree_token', $credit["token"]); 
               $data=array("Ack"=>1);  
            } 
              
          } catch (Exception $ex) {
            $data=array("Ack"=>0,"msg"=>$ex->getMessage(),"err"=>$ex);   
          }            
            
            
                echo json_encode($data);exit;
        }   
      
        
        
        
        
   }
   
   function viewcard($user_id=null)
   {
      
       $this->loadModel("Billing");
       try {
        require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS . 'lib' . DS . 'Braintree.php');
       require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS .'config.php');    
       $options=array("fields"=>array("Billing.token","Billing.firstname","Billing.lastname"),"conditions"=>array("Billing.user_id"=>$user_id));
       $payinfo=$this->Billing->find("first",$options);
       if(!empty($payinfo))
       {
            $token=$payinfo["Billing"]["token"];
            try{

                $result = Braintree_CreditCard::find($token);
                $response=array(
                "customerId"=> $result->_attributes['customerId'],"expirationMonth"=>$result->_attributes['expirationMonth'],
                "expirationYear"=>$result->_attributes['expirationYear'],"cardType"=>$result->_attributes['cardType'],
                "CardNumber"=>$result->_attributes['maskedNumber'],"name"=>$payinfo["Billing"]["firstname"].' '.$payinfo["Billing"]["lastname"]    
                );
                $data=array("Ack"=>1,"response"=>$response);
                
               }  
                 catch (Exception $e){
                 $data=array("Ack"=>0);    
                } 
       }
       else
       {
           $data=array("Ack"=>0);
       }    
       } catch (Exception $ex) {
           print_r($ex);
           
       }
       
       
       echo json_encode($data);exit;
   }
   function pay()
   {
       $this->loadModel("RequestQuote");
       $this->loadModel("Billing");
       $this->loadModel("Lead");
       require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS . 'lib' . DS . 'Braintree.php');
       require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS .'config.php');
       if ($this->request->is(array('post','put'))) 
        {     
          $jsonData = $this->request->input('json_decode');
          $amount=$jsonData->amount;
          $user_id=$jsonData->user_id;
          $assigned_to=$jsonData->assigned_to;
          $lead_id=$jsonData->lead_id;
          $quote_id=$jsonData->quote_id;
          //$this->RequestQuote->recursive=-1;
          $this->RequestQuote->unBindModel(array(
          'belongsTo'=>array('User','Lead','Projectowner')    
                ));
          
          $requestquote=$this->RequestQuote->find('first',array("conditions"=>array("RequestQuote.id"=>$quote_id)));
          $billing=$this->Billing->find("first",array("conditions"=>array("Billing.user_id"=>$user_id)));
          
          foreach ($jsonData as $key =>$json)
          {
              $this->request->data["Payment"][$key]=$json;
          }
          $customerId=$billing["Billing"]["customer_id"];
          try {
              
              
              $result = Braintree_Transaction::sale(
            array(

                     'customerId' => $customerId,
                     'amount' => $amount,
                     'options' => [
                     'submitForSettlement' => true
                       ]

               )

             );
              
              if($result->success==1)
                {
                    $this->request->data["Payment"]["transaction_id"]=$result->transaction->_attributes["id"];
                    $this->request->data["Payment"]["pay_to"]=$requestquote["RequestQuote"]["posted_by"];
                    $this->request->data["Payment"]["paymenttime"]= gmdate('Y-m-d H:i:s');
                    if($this->Payment->save($this->request->data))
                    {
                        $this->Lead->id = $lead_id;
                        $this->Lead->saveField('is_paid', 1);
                        $data=array("Ack"=>1);
                    }


                }
            else
            {
                $data=array("Ack"=>0);
            }
              
          } catch (Exception $ex) {
              
              
              print_r($ex);
              
          }
          
          
          
                
            echo json_encode($data);exit;    
        }  
        exit;
   }
   function refund()
        {
            require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS . 'lib' . DS . 'Braintree.php');
            require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS .'config.php');
            $this->loadModel("Lead");
            $this->loadModel("Refund");
            if ($this->request->is(array('post','put'))) 
            {  
               $jsonData = $this->request->input('json_decode');
               $lead_id=$jsonData->lead_id;
               $reason=$jsonData->reason;
               $amount=$jsonData->amount;
               $user_id=$jsonData->user_id;
               $billing=$this->Payment->find("first",array("conditions"=>array("Payment.lead_id"=>$lead_id)));
               $transaction_id=$billing["Payment"]["transaction_id"];
               $transaction = Braintree_Transaction::refund($transaction_id);
               if($transaction->success)
               {
                   $this->request->data["Refund"]["user_id"]=$billing["Payment"]["user_id"];
                   $this->request->data["Refund"]["lead_id"]=$lead_id;
                   $this->request->data["Refund"]["reason"]=$reason;
                   $this->request->data["Refund"]["refund_time"]= gmdate('Y-m-d H:i:s');
                   if($this->Refund->save($this->request->data))
                   {
                       $this->Lead->id = $lead_id;
                       $this->Lead->saveField('is_refund', 1);
                       $data=array("Ack"=>1);
                   }
                   
                   
               }
               else
               {
                       $data=array("Ack"=>0);
               }
               
               
               
            }
            echo json_encode($data);exit;
            
        }
        
        public function showpaystatus() 
        {
            require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS . 'lib' . DS . 'Braintree.php');
            require_once(ROOT . '/app/Vendor' . DS . 'braintree' . DS .'config.php');
            $transaction = Braintree_Transaction::find('ktnpvzs2');
            pr($transaction);exit;
            
        }
        
        public function credithistory($user_id) 
        {
            $SITE_URL=Configure::read("SITE_URL");
            $this->loadModel("Category");
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }
            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
            if($data->status == 'success')
            {
                date_default_timezone_set($data->timezone);
                $timezone = $data->timezone;
            }
            else
            {
                date_default_timezone_set('Asia/Kolkata');
                $timezone = 'Asia/Kolkata';
            }
            $tz_from = "UTC";
            $tz_to = $timezone;
            $format = 'Y-m-d h:i a';
             $this->Payment->bindModel(
               array(
                 'belongsTo'=>array(
                     'User' =>array(
                      'className' => 'User',
                      'foreignKey' => 'user_id',
                      'fields' => array('User.id','User.name','User.image'),
                  ),
                  'Lead' =>array(
                      'className' => 'Lead',
                      'foreignKey' => 'lead_id',
                      'fields' => array("Lead.id","Lead.user_id","Lead.address","Lead.category_id"),
                  ),
                      
                      
                     
               )
            )
        ); 
             
             
             $payments=$this->Payment->find("all",array("conditions"=>array("Payment.pay_to"=>$user_id)));
             if(!empty($payments))
             {
             foreach ($payments as $payment)
             {
                 $dt = new DateTime($payment["Payment"]["paymenttime"], new DateTimeZone($tz_from));
                 $dt->setTimeZone(new DateTimeZone($tz_to));
                 $paymenttime=$dt->format($format) ; 
                 $category=$this->Category->find("first",array("fields"=>"Category.name","conditions"=>array("Category.id"=>$payment["Lead"]["category_id"])));
                 $response[]=array(
                 "lead_id"=> $payment["Lead"]["id"],'user_id'=>$payment["User"]["id"],"name"=>$payment["User"]["name"],
                 "image"=>!empty($payment["User"]["image"])?$SITE_URL.'user_images/'.$payment["User"]["image"]:$SITE_URL.'nouser.png',
                 "price"=>  $payment["Payment"]["amount"],"paymenttime"=>$paymenttime,'category'=>$category["Category"]['name']  
                     
                 );
             }
             $data=array('Ack'=>1,'response'=>$response);
             }
             else
             {
             $data=array('Ack'=>0);
             }
             echo json_encode($data);exit;
             
             
             
            
        }
        
        function transactions($id=null)
        {
            $this->loadModel("Lead");
            $SITE_URL=Configure::read("SITE_URL");
            $options=array("conditions"=>array("Lead.is_paid"=>1,"Lead.is_refund"=>0,"Lead.user_id"=>$id));
             $this->Lead->unBindModel(array('hasMany'=>array('Providers','Userresponse'),
                                            'belongsTo'=>array('User','AssignedPro')
                
                ));
 
            $transactions=$this->Lead->find("all",$options);
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }
            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
            if($data->status == 'success')
            {
                date_default_timezone_set($data->timezone);
                $timezone = $data->timezone;
            }
            else
            {
                date_default_timezone_set('Asia/Kolkata');
                $timezone = 'Asia/Kolkata';
            }
            $tz_from = "UTC";
            $tz_to = $timezone;
            $format = 'm/d/Y h:i a';
            foreach ($transactions as $transaction)
            {
              
                $payment=$this->Payment->find("first",array("conditions"=>array("Payment.lead_id"=>$transaction["Lead"]["id"])));
                $dt = new DateTime($payment["Payment"]["paymenttime"], new DateTimeZone($tz_from));
                $dt->setTimeZone(new DateTimeZone($tz_to));
                $paymenttime=$dt->format($format) ;                 
                $pays[]=array("title"=>$transaction["Lead"]["title"],"payto"=>$payment["PayTo"]["name"],"amount"=>$payment["RequestQuote"]["price"]
                              ,"image"=>!empty($payment['PayTo']['image'])?$SITE_URL.'user_images/'.$payment['PayTo']['image']:$SITE_URL.'nouser.png',
                                "service_name"=>$transaction["Category"]["name"],"payment_time"=>$paymenttime,
                                "address"=>$transaction["Lead"]["address"]
                          );

                
            }
            
            if(!empty($transactions))
            {
                $data=array("Ack"=>1,"response"=>$pays);
            }
            else
            {
                $data=array("Ack"=>0);
                
            }
            
            echo json_encode($data);
            exit;
            
        }




















   /**
     * index method
     *
     * @return void
     */
    
    
    
    
    
    
    
    
    
    
    
    
}