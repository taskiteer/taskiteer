<?php
App::uses('AppController', 'Controller');
/**
 * CarCategories Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class QuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $name = 'Questions';

    public $data2='' ;
    public $components = array('Paginator', 'Session');
    var $uses = array('User',  'Post', 'Question','Answer');


/**
 * index method
 *
 * @return void
 */
    
    public function admin_add($cat_id) {
           
         if ($this->request->is('post')) {
             
             

            $this->request->data['Question']['cat_id']=$cat_id;
            $textTypeCount = $this->Question->find('count',array('conditions'=>array('Question.type'=>4,'Question.cat_id'=>$cat_id)));
            
            if($textTypeCount == 0 || $this->request->data['Question']['type'] != 4){
                $this->Question->create();

                if ($this->Question->save($this->request->data)) {
                        $this->Session->setFlash('The question has been saved.','default', array('class' => 'success'));
                        $q_id=$this->Question->getLastInsertID();
                        if(isset($this->request->data['answers'])&&!empty($this->request->data['answers']))
                        {
                          foreach($this->request->data['answers'] as $answers)
                          {
                             $this->request->data['Answer']['cat_id']=$cat_id;
                             $this->request->data['Answer']['q_id']=$q_id;
                             $this->request->data['Answer']['name']=$answers;

                            $this->Answer->create();
                            $this->Answer->save($this->request->data);
                          }
                        }
                        
                        return $this->redirect(array('action' => 'index',$cat_id));
                } else {
                        $this->Session->setFlash(__('The service could not be saved. Please, try again.'));
                }
            }else{
                $this->Session->setFlash(__('You can add Text type question only once. You have already added question for Text type'));
            }

                
	   }
           
          // $this->set(compact('categories','users'));
	}
        
        public function admin_index($cat_id)
        {
            $conditions = array('Question.cat_id' => $cat_id);
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
           
        $options = array('conditions' => $conditions, 'order' => array('Question.id' => 'ASC'), 'group' => 'Question.id');
        $this->Paginator->settings = $options;
        $title_for_layout = 'Question List';
        //$this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Question'));
        $this->set('cat_id', $cat_id);
        }
        
        public function admin_delete($id = null,$cat_id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid Question'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->Question->delete()) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The Question has been deleted.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The Question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index',$cat_id));
	}
        
        public function admin_edit($id = null,$cat_id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }
           
		if ($this->request->is(array('post', 'put'))) {

                        $this->request->data['Question']['id']=$id;

			if ($this->Question->save($this->request->data)) {

                         $this->Session->setFlash('The Question has been saved.','default', array('class' => 'success'));
                         return $this->redirect(array('action' => 'index',$cat_id));

			} else {
				$this->Session->setFlash(__('The service could not be saved. Please, try again.'));
			}
		} else {
                    
			$posts = $this->Question->find('first', array('conditions' => array('Question.id' => $id)));
                        
                        $this->request->data=$posts;
                        
                        $answers = $this->Answer->find('list', array('conditions' => array('Answer.q_id' => $id)));
                        //print_r($posts);
                        //exit;
                        
		}
                $this->set(compact('posts','answers'));

        }
        
        public function updateAnswer()
        {
            $this->autoRender = false;
                    $this->layout = false;
                    
                    $userid = $this->Session->read('userid');
                    
                    $ret = array();
                    $html = '';
                     if ($this->request->is('post')) {
                         
                         $this->request->data['Answer']['id'] = $this->request->data['id'];
                            $this->request->data['Answer']['name'] = $this->request->data['ans'];
                           
                            if($this->Answer->save($this->request->data)) 
                            {
                                $ret['ack'] = 1;
                            }
                            else
                            {
                                $ret['ack'] = 0;
                            }
                                               
                     }
                     else
                    {
                        
                        $ret['ack'] = 0;
                    } 
                        
                         echo json_encode($ret);
                        exit;
        }
        
        //for frontend..................................
          public function questionlist($cat_id=NULL)
        {
            
              //$cat_id=46;
             $categories=$this->Question->find("all",array('conditions'=>array("Question.cat_id"=>$cat_id),'order' => array('Question.id' => 'ASC')));
             //pr($categories);exit;
                $SITE_URL=Configure::read("SITE_URL");  
               /* foreach ($categories as  $category)
                {
                    $list_categories[]=array("id"=>$category['Question']['id'],"name"=>$category['Question']['name'],"type"=>$category['Question']['type'],"answer"=>$category['Question']['Answer']);
                }*/
                
                
                
                
             foreach ($categories as $key =>$category)
             {
                 if($category["Question"]["type"]==1)
                 {
                    $categories[$key]["Question"]["type"]="radio";
                 }
                 
                 else if($category["Question"]["type"]==2)
                 {
                    $categories[$key]["Question"]["type"]="checkbox";
                 }
                 
                 else if($category["Question"]["type"]==3)
                 {
                    $categories[$key]["Question"]["type"]="select";
                 }
                 
//                 else if($category["Question"]["type"]==4)
//                 {
//                    $categories[$key]["Question"]["type"]="text";
//                 }
                 if($category["Question"]["type"]!=4){
                     $categories[$key]["Question"]["answer"]=!empty($category['Answer'])?$category['Answer']:array();
                    $categories[$key]["Question"]["q_id"]=$category["Question"]["id"];
                    $questionList[]= $categories[$key]["Question"];
                 }                                 
                 
                 
             }
             
             foreach ($categories as $key =>$category)
             {                
                 
                 if($category["Question"]["type"]==4)
                 {
                    $categories[$key]["Question"]["type"]="text";
                    $categories[$key]["Question"]["answer"]=!empty($category['Answer'])?$category['Answer']:array();
                    $categories[$key]["Question"]["q_id"]=$category["Question"]["id"];
                    $questionList[]= $categories[$key]["Question"];  
                 }
                 
                               
                 
                 
             }
             
            
                
                
                
                if(!empty($categories))
                {
                    $data=array('Ack'=>1,'questionList'=>$questionList,"total_question"=>count($questionList));
                }
                else
                {
                    $data=array('Ack'=>0,'questionList'=>"");
                }
                echo json_encode($data);exit;
               
        }
        
        public function questionlistApp($cat_id=NULL)
        {
            
              //$cat_id=46;
             $categories=$this->Question->find("all",array('conditions'=>array("Question.cat_id"=>$cat_id),'order' => array('Question.id' => 'ASC')));
             //pr($categories);exit;
                $SITE_URL=Configure::read("SITE_URL");  
               /* foreach ($categories as  $category)
                {
                    $list_categories[]=array("id"=>$category['Question']['id'],"name"=>$category['Question']['name'],"type"=>$category['Question']['type'],"answer"=>$category['Question']['Answer']);
                }*/
                
                
                
            $questionList[0]['id'] = $cat_id; 
            $questionList[0]['q_id'] = $cat_id;
            $questionList[0]['name'] = 'Please Choose Question';
            $questionList[0]['ans1'] = '';
            $questionList[0]['ans2'] = ''; 
            $questionList[0]['ans3'] = '';
            $questionList[0]['ans4'] = '';
            $questionList[0]['cat_id'] = $cat_id;
            $questionList[0]['q_order'] = 1; 
            $questionList[0]['service_id'] = 0;
            $questionList[0]['status'] = 0;
            $questionList[0]['subcat_id'] = 0;
            $questionList[0]['type'] = "radio";
            $questionList[0]['answer'] = array();
            
             foreach ($categories as $key =>$category)
             {
                 if($category["Question"]["type"]==1)
                 {
                    $categories[$key]["Question"]["type"]="radio";
                 }
                 
                 else if($category["Question"]["type"]==2)
                 {
                    $categories[$key]["Question"]["type"]="checkbox";
                 }
                 
                 else if($category["Question"]["type"]==3)
                 {
                    $categories[$key]["Question"]["type"]="select";
                 }
                 
                 else if($category["Question"]["type"]==4)
                 {
                    $categories[$key]["Question"]["type"]="text";
                 }
                 
                 $categories[$key]["Question"]["answer"]=!empty($category['Answer'])?$category['Answer']:array();
                 $categories[$key]["Question"]["q_id"]=$category["Question"]["id"];
                 $questionList[0]['answer'][]= $categories[$key]["Question"];
                 
                 
                 
             }
             
             $questionList[1]['id'] = 0; 
            $questionList[1]['q_id'] = 0;
            $questionList[1]['name'] = 'Job Description';
            $questionList[1]['ans1'] = '';
            $questionList[1]['ans2'] = ''; 
            $questionList[1]['ans3'] = '';
            $questionList[1]['ans4'] = '';
            $questionList[1]['cat_id'] = $cat_id;
            $questionList[1]['q_order'] = 2; 
            $questionList[1]['service_id'] = 0;
            $questionList[1]['status'] = 0;
            $questionList[1]['subcat_id'] = 0;
            $questionList[1]['type'] = "text";
            $questionList[1]['answer'] = array();
             
            
                
                
                
                if(!empty($categories))
                {
                    $data=array('Ack'=>1,'questionList'=>$questionList,"total_question"=>count($questionList));
                }
                else
                {
                    $data=array('Ack'=>0,'questionList'=>"");
                }
                echo json_encode($data);exit;
               
        }
        
}