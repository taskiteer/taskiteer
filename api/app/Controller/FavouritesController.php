<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class FavouritesController extends AppController
{
    
    /*function beforeFilter() {
    parent::beforeFilter();
    }*/
    /**
     * Components
     *
     * @var array
     */
    public $name = 'Favourites';
    public $components = array('Session', 'RequestHandler', 'Paginator');
    var $uses = array('Favourite', 'User', 'EmailTemplate', 'Setting');
    
    //for get list of myjobs....................................
    public function favpros($user_id=null)
    {
        $SITE_URL = Configure::read("SITE_URL");
        
            $this->loadModel("Category");
            $cat_id=array();
            $options = array(
                'conditions' => array(
                    'Favourite.user_id' => $user_id
                ),
                'fields'=>array("User.id","User.name","User.image","User.cat_id","User.address","User.service_location","Favourite.id"),
                'order' => array(
                    'Favourite.id' => 'DESC'
                )
            );
            
            $lists = $this->Favourite->find("all", $options);
            foreach ($lists as $key => $value) {
                if(!empty($value['User']['cat_id']))
                {
                    $category_id=explode(",",$value['User']['cat_id']);
                    $categories=$this->Category->find("all",
                    array("fields"=>array("Category.name"),"conditions"=>array("Category.id"=>$category_id)));
                    foreach ($categories as $category)
                    {
                        $cat_id[]=$category["Category"]["name"];
                    }
                    
                    
                }
                
               
                $value['User']['image'] = !empty($value['User']['image']) ? $SITE_URL . 'user_images/' . $value['User']['image'] : $SITE_URL . 'nouser.png';
                $value['User']['cat_id']= implode(",", $cat_id);

                $value['User']['fav_id']= $value['Favourite']['id'];
                $List[]                 = $value['User'];
                
            }
            
            if (!empty($lists)) {
                $data = array(
                    'Ack' => 1,
                    'List' => $List
                );
            } else {
                $data = array(
                    'Ack' => 0,
                    'List' => ""
                );
        
            
        }
        echo json_encode($data);
            exit;
    }
    
    //for delete job.............................................
    public function removefav($id=null)
    {
        
            $this->Favourite->id = $id;
            if ($this->Favourite->delete()) 
            {
                $data=array("Ack"=>1);
            }
            else
            {
                $data=array("Ack"=>0);
            }
           
            echo json_encode($data);
            exit;
       
    }
    
    
    //for accept and reject provider....................................
    
    public function addfav()
    {
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData                                    = $this->request->input('json_decode');
            $pro_id                                      = $jsonData->pro_id;
            $user_id                                     = $jsonData->user_id;
            $this->request->data['Favourite']['pro_id']  = $pro_id;
            $this->request->data['Favourite']['user_id'] = $user_id;
            $is_fav                                      = $this->Favourite->find("count", array(
                "conditions" => array(
                    "Favourite.user_id" => $user_id,
                    "Favourite.pro_id" => $pro_id
                )
            ));
            if (!$is_fav) {
                $this->Favourite->create();
                if ($this->Favourite->save($this->request->data)) {
                    
                    $data = array(
                        'Ack' => 1
                    );
                } else {
                    $data = array(
                        'Ack' => 0,
                        'jobList' => ""
                    );
                }
            } else {
                $data = array(
                    'Ack' => 0,
                    'msg' => "You have already favourite"
                );
                
            }
            
            echo json_encode($data);
            exit;
        }
    }
    
    
    
}