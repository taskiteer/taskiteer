<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class RatingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        
        function professional_service()
        {
            $SITE_URL=Configure::read("SITE_URL");
            $professionals=$this->Rating->find('all');
            foreach ($professionals as $key=>$profession)
            {
                $professionals[$key]['User']['image']=!empty($professionals[$key]['User']['image'])?$SITE_URL.'user_images/'.$professionals[$key]['User']['image']:$SITE_URL.'nouser.png';
                    
            }
            
            if(!empty($professionals))
            {
                $data=array('Ack'=>1,"response"=>$professionals);
            }
            else
            {
                $data=array('Ack'=>0,"response"=>$professionals);
            }
            
            echo json_encode($data);exit;
           
            
        }
        
        
        //save review................................
        public function save_review()
        {
            $this->loadModel("Lead");
            if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $lead_id=$jsonData->lead_id;
            // print_r($jsonData);
                $is_exist=$this->Rating->find('count',array("conditions"=>array("Rating.from_id"=>$jsonData->from_id,"Rating.to_id"=>$jsonData->to_id,"Rating.lead_id"=>$lead_id)));
                if($is_exist)
                {
                    $data=array('Ack'=>0,"msg"=>"You have already posted.");
                }
                else
                {
                    
                if(isset($jsonData->lead_id)&&$jsonData->lead_id!='')  {
                      $lead_data=$this->Lead->find('first',array('fields'=>array('category_id'),"conditions"=>array("Lead.id"=>$jsonData->lead_id)));
                
                       $this->request->data['Rating']['category_id']=$lead_data['Lead']['category_id'];
                } 
            else {
                $this->request->data['Rating']['category_id']="";
            }
                 $this->request->data['Rating']['from_id']=$jsonData->from_id;
                $this->request->data['Rating']['to_id']=$jsonData->to_id;
                $this->request->data['Rating']['review_from_name']=!empty($jsonData->review_from_name)?$jsonData->review_from_name:'';
                $this->request->data['Rating']['review_to_name']=!empty($jsonData->review_to_name)?$jsonData->review_to_name:'';
                $this->request->data['Rating']['work_quality']=!empty($jsonData->work_quality)?$jsonData->work_quality:'';
                $this->request->data['Rating']['resposiveness']=!empty($jsonData->resposiveness)?$jsonData->resposiveness:0;
                $this->request->data['Rating']['value_money']=!empty($jsonData->value_money)?$jsonData->value_money:0;
                $this->request->data['Rating']['timely_response']=!empty($jsonData->timely_response)?$jsonData->timely_response:0;
                $this->request->data['Rating']['avg_score']=!empty($jsonData->avg_score)?$jsonData->avg_score:0;
                $this->request->data['Rating']['comment']=!empty($jsonData->comment)?$jsonData->comment:'';
                $this->request->data['Rating']['lead_id']=!empty($lead_id)?$lead_id:'';
                $this->request->data['Rating']['ratting_date']= gmdate('Y-m-d H:i:s');
                $this->Rating->create();
                $this->Rating->save($this->request->data);
                
                //for add avg score in user table........................
                $total = $this->Rating->find('first', array('fields' => array('sum(Rating.avg_score)  AS reviewtotal'), 'conditions'=>array('Rating.to_id'=>$jsonData->to_id)));
                //print_r($total);
                //echo $total[0]['reviewtotal'];
               $count = $this->Rating->find('count', array('conditions'=>array('Rating.to_id'=>$jsonData->to_id)));
               $avg_review=$total[0]['reviewtotal']/$count;
               $this->loadModel('User');
               $arr=array();
               $arr['User']['avg_score']=$avg_review;
               $this->User->id = $jsonData->to_id;
                $this->User->save($arr);
               
             $data=array('Ack'=>1,"msg"=>'posted successfully');
                    
                }
                
             echo json_encode($data);exit;




            }
        }
        
        public function provider_list($lead_id=null,$user_id=null)
        {
            $SITE_URL=Configure::read("SITE_URL");
            $this->loadModel("RequestQuote");
            $this->loadModel("Lead");
            $this->loadModel("Rating");
            $providers=array();
            $this->RequestQuote->unBindModel(array(
            'belongsTo'=>array('Projectowner')    
                ));
            $this->RequestQuote->bindModel(
               array(
                 'belongsTo'=>array(
                     'User' =>array(
                      'className' => 'User',
                      'foreignKey' => 'posted_by',
                      'conditions' => array(),
                  ),
                      
                     
               )
            )
        ); 
            $quotes=$this->RequestQuote->find("all",array("conditions"=>array("RequestQuote.lead_id"=>$lead_id)));
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }
            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
            if($data->status == 'success')
            {
                date_default_timezone_set($data->timezone);
                $timezone = $data->timezone;
            }
            else
            {
                date_default_timezone_set('Asia/Kolkata');
                $timezone = 'Asia/Kolkata';
            }
            $tz_from = "UTC";
            $tz_to = $timezone;
            $format = 'Y-m-d';
            if(!empty($quotes))
            {
                 foreach ($quotes as $quote )
                {
                    $no_of_review=$this->Rating->find("count",array("conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_by"])));
                    $net_ratings=$this->Rating->find("first",array("fields"=>array("SUM(Rating.avg_score) as score"),"conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_by"])));
                    $is_rating=$this->Rating->find("count",array("conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_by"],"Rating.from_id"=>$user_id,"Rating.lead_id"=>$lead_id)));
                    if($no_of_review>0)
                    {
                        $avg_rateing= round($net_ratings[0]["score"]/$no_of_review);
                    }
                    else
                    {
                        $avg_rateing=0;

                    }
                     $dt = new DateTime($quote["RequestQuote"]["posttime"], new DateTimeZone($tz_from));
                     $dt->setTimeZone(new DateTimeZone($tz_to));
                     $start_time=$dt->format($format) ; 
                     $hired=$this->Lead->find("count",array("conditions"=>array("Lead.assigned_to"=>$quote["RequestQuote"]["posted_by"])));
                     
                     $providers[]=array(
                    "id"=>$quote["User"]["id"],     
                    "name"=> $quote["User"]["name"],"image"=>!empty($quote["User"]["image"])?$SITE_URL.'user_images/'.$quote["User"]["image"]:$SITE_URL.'nouser.png',
                    "total_review"=>$no_of_review,"rateing"=>$quote['User']['avg_score'] ,"hired"=>$hired,"last_login"=>$this->how_log_ago($quote["User"]["logout_time"]),   
                    "price"=>$quote["RequestQuote"]["price"],"posttime"=>$start_time,
                    "is_hire"=>$quote["Lead"]["assigned_to"]==$quote["RequestQuote"]["posted_by"]?'Yes':'No',"comment"=>$quote["RequestQuote"]["comment"],"is_rating"=>$is_rating     
                    ,"channelUrl"=> $quote["Lead"]["chanel_id"]
                             );
                }
                
                
               $data=array("Ack"=>1,"response"=>$providers);

            }
            else
            {
                $data=array("Ack"=>0);
                
            }
            echo json_encode($data);
            //pr($quotes);
            exit;

            
        }
        



/**
 * index method
 *
 * @return void
 */
	

	


	

}
