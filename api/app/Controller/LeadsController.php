<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class LeadsController extends AppController {

	/*function beforeFilter() {
		parent::beforeFilter();
	}*/
/**
 * Components
 *
 * @var array
 */
	public $name = 'Leads';
	public $components = array('Session','RequestHandler','Paginator');
	var $uses = array('Lead','User','EmailTemplate','Setting');


	public function admin_index($status=NULL) {

    $this->Lead->recursive = 2;
    if(isset($status)&&$status!='NULL')
    {
       $options = array('conditions' => array('Lead.status' => $status),'order' => array('Lead.id' =>'DESC')); 
    }
    else
    {
    $options = array('order' => array('Lead.id' =>'DESC'));
    }
    //$activeatt = $this->Booking->find('all', $options);

     $this->Paginator->settings = $options;
     $this->set('leads', $this->Paginator->paginate('Lead'));
     //print_r($activeatt);
    // exit;
	}


  public function admin_view($id = null) {

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }
  	$this->Booking->recursive = 2;
    $options = array('conditions' => array('Booking.' . $this->Booking->primaryKey => $id));
    $this->set('books', $this->Booking->find('first', $options));
	}

  public function admin_message($id=null){

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }

    $this->loadModel('Message');
    $this->Message->recursive = 2;
    $options = array('conditions' => array('Message.lead_id' => $id));
    $this->set('messages', $this->Message->find('all', $options));
    
  }

  public function admin_response($id=null){

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }

    $this->loadModel('Userresponse');
    $this->Userresponse->recursive = 2;
     $options = array('conditions' => array('Userresponse.lead_id' => $id),'order'=>array('Userresponse.id'=>'asc'));
     $this->Paginator->settings = $options;
     $this->set('responses', $this->Paginator->paginate('Userresponse'));
     
     //accepted provider details..........................
      $this->loadModel('Appliedprovider');
     $options_provider = array('conditions' => array('Appliedprovider.lead_id' => $id,'Appliedprovider.status' => 1));
     $assigned_pro=$this->Appliedprovider->find('first', $options_provider);
     //print_r($assigned_pro);die;
    $this->set('provider', $assigned_pro);
  }
  
   public function admin_edit($id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }
           
		if ($this->request->is(array('post', 'put'))) {

                        $this->request->data['Lead']['id']=$id;

			if ($this->Lead->save($this->request->data)) {
                            
                            //for insert into applied pro lists table..................................
                           
                             $this->loadModel("Appliedprovider"); 
                             $this->loadModel("User"); 
                             //lead data
                             $this->Lead->recursive = -1; 
                             $posts = $this->Lead->find('first', array('conditions' => array('Lead.id' => $id)));
                             //user data....
                             $this->User->recursive = -1; 
                             $pros = $this->User->find('all', array('conditions' => array('User.cat_id' => $posts['Lead']['category_id'])));
                             //insert data
                              $arr=array();
                            $arr['Appliedprovider']['lead_id'] = $id;
                            $arr['Appliedprovider']['cat_id'] = $posts['Lead']['category_id'];
                            
                            $cat_id=$posts['Lead']['category_id'];
                             
                             foreach($pros as $provider)
                             {
                                  $arr['Appliedprovider']['user_id'] = $provider['User']['id'];
                             $this->Appliedprovider->create();
                                if ($this->Appliedprovider->save($arr)) {

                                }
                             }
                             
                         $this->Session->setFlash('The Status has been updated.','default', array('class' => 'success'));
                         return $this->redirect(array('action' => 'index'));

			} else {
				$this->Session->setFlash(__('The status could not be saved. Please, try again.'));
			}
		} else {
                    
			$posts = $this->Lead->find('first', array('conditions' => array('Lead.id' => $id)));
                        
                        $this->request->data=$posts;
                       
                        //print_r($posts);
                        //exit;
                        
		}
                $this->set(compact('posts'));

        }
        public function post_service() 
        {
          
                        $apiKey = "AAAANIxLqnk:APA91bEQNk1a2rMaKiSg2uwX_lCvLpnFREe8NUriXpWng2GKECuMt_OK0anplLNnEQIHDE0vaiLIqFkHXrFK-f9BCPnBCyHr0R--kjhpMXOFCdv-KSHqL9XseGC0TEzOEpaLpnTTW2c4";       
                       $this->loadModel("Userresponse");
                       $this->loadModel("Category");
                     if ($this->request->is(array('post', 'put'))) 
                     {

                   
                        $jsonData = $this->request->input('json_decode');

                        $location = $jsonData->lead->address;

                        foreach ($jsonData->lead as $key =>$val)
                        {
                            if($key=='address')
                            {
                                    if(!empty($location)){
                                        $prepAddr = str_replace(' ','+',$location);
                                        $url=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=true');
                                        $output= json_decode($url);
                                        $lat = $output->results[0]->geometry->location->lat;
                                        $lang= $output->results[0]->geometry->location->lng;
                                        $this->request->data['Lead']['lat']=$lat;
                                        $this->request->data['Lead']['lang']=$lang;
                                    }
                                    
                                    $this->request->data['Lead']['posttime']=gmdate('Y-m-d H:i:s');
                            }
                            if($key=='date_required'){
                                $this->request->data['Lead']['date_required']=gmdate('Y-m-d',strtotime($this->request->data['Lead']['date_required']));
                            }
                            
                            $this->request->data['Lead'][$key]=$val;
                             
                        }
                        
                        $user_id=$this->request->data['Lead']['user_id'];
                        if(!empty($user_id))
                        {
                            $cat_id=$this->request->data['Lead']['category_id'];
                            $this->User->recursive=-1;
                            $optionsPro = array('fields'=>array("User.id","User.id","User.token"),'conditions' => array('type'=>"SP",'FIND_IN_SET("'.$cat_id.'",`User.cat_id`)')); 
                            $listsPro=$this->User->find('all',$optionsPro);
                            $this->Lead->create();

                            if($this->Lead->save($this->request->data))
                            {

                                $lead_id=$this->Lead->getLastInsertID();

                                foreach($jsonData->questions as $question)
                                {
                                    $this->request->data['Userresponse']['lead_id']=$lead_id;
                                    $this->request->data['Userresponse']['user_id']=$user_id;
                                    $this->request->data['Userresponse']['cat_id']=$cat_id;
                                    $this->request->data['Userresponse']['q_id']=$question->q_id;
                                    $this->request->data['Userresponse']['answer']=$question->answer;
                                    $this->Userresponse->create();
                                    $this->Userresponse->save($this->request->data);


                                }
                                $data=array("Ack"=>1);
                               //for getting pro of that cat..........................
                                
                                if(count($listsPro)>0)
                                {
                                    foreach ($listsPro as $key =>$value)
                                     {
                                             //added for push part................................
                                             $user_id=$value['User']['id'];
                                            //for insert into notification table........................
                                            $notiArr=array();
                                            $this->loadModel('Notification');
                                            $notiArr['Notification']['post_time']=gmdate('Y-m-d H:i:s');                        
                                            $notiArr['Notification']['lead_id']=$lead_id;
                                            $notiArr['Notification']['sent_to']=$user_id;
                                            $notiArr['Notification']['sent_by']=$this->request->data['Lead']['user_id'];
                                            $notiArr['Notification']['type']=2;
                                            $this->Notification->create();
                                            $this->Notification->save($notiArr);                                            
                                            $userDetail=$this->User->find("first",array('conditions'=>array("User.id"=>$user_id,"User.token !="=>"")));
                                            $this->Category->recursive=-1;
                                            $category=$this->Category->find("first",array("conditions"=>array("Category.id"=>$cat_id)));
                                            $price=$category["Category"]["price"];
                                            $device_id=  $userDetail["User"]["token"];   
                                            $device_type=strtolower($userDetail['User']['device_type']);
                            
                                  
                                            $body="New job posted";
                                            if(!empty($device_id))
                                            {

                                                if($device_type=='android')
                                                {
                                             
                                             
                                                     $registatoin_ids=array($device_id);
                                                     $totalmsgarray = array("message" => $body,"title"=>"Post Job","vibrate"=>1,"sound"=>1,"lead_id"=>$lead_id,
                                                                            "price"=>$price
                                                                            );
                                                     $this->android_push($registatoin_ids,$totalmsgarray);

                                                }  
                                               if($device_type=='ios')
                                               {
                                                  //ios_push($device_id, $message); 

                                                     $totalmsgarray = array("message" => $body,"type"=>"Post Job","device_token_id"=>$device_id,
                                                                            "lead_id"=>$lead_id,"price"=>$price);                                                 
                                                    #$this->iphone_push($device_id, $body, 'default', json_encode($totalmsgarray));
                                               }
                                            } 

                                        } 
                                    }
                                    $job_name="job_".$lead_id;
                                    $post_fields="{\n  \"name\": \"".$job_name."\",\n  \"cover_url\": \"\",\n  \"channel_url\": \"open_channel_".$lead_id."\",\n  \"data\": \"\",\n}";
                                    //comment jas code
                                    /*$curl = curl_init();
                                    curl_setopt_array($curl, array(
                                      CURLOPT_URL => "https://api.sendbird.com/v3/open_channels",
                                      CURLOPT_RETURNTRANSFER => true,
                                      CURLOPT_ENCODING => "",
                                      CURLOPT_MAXREDIRS => 10,
                                      CURLOPT_TIMEOUT => 30,
                                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                      CURLOPT_CUSTOMREQUEST => "POST",
                                      CURLOPT_POSTFIELDS => $post_fields,
                                      CURLOPT_HTTPHEADER => array(
                                        "api-token: 53588a8e95639d1686b799e051c2472dcaaaaff3",
                                      ),
                                    ));

                                    $response = curl_exec($curl);
                                    $err = curl_error($curl);*/

                                    curl_close($curl);
                                    //echo "<pre>";print_r($err);die;
                                    if ($err) {
                                        $data=array('Ack'=>0,'msg'=>'sendbird api error');
                                        exit;
                                    } 
                                    else {
                                        $this->Lead->id = $lead_id;
                                        $this->Lead->saveField('chanel_id', "open_channel_".$lead_id);   
                                    }
                           }
                        }else{
                            $data=array('Ack'=>0,'msg'=>'user_id can not be blank');
                            
                        }
                        
                        
                        
                   }

                   echo json_encode($data);exit;
        }
        
        
        //for get list of myjobs....................................
         public function myjobs() 
        {
             $this->loadModel("RequestQuote");
             $this->loadModel("Billing");
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $user_id=$jsonData->user_id;
             $status=$jsonData->status;
             //$user_id=11;
             //$this->Lead->recursive = 2;
             $billing=$this->Billing->find("first",array("conditions"=>array("Billing.user_id"=>$user_id)));   
             
             if(!empty($billing))
             {
                 $is_savecard=1;
             }
             else
             {
                 $is_savecard=0;

             }
             
             
                $quote=array();
              
                if(isset($status)&&$status!='all')
                {
                   $options = array('conditions' => array('Lead.user_id' => $user_id,'Lead.status' => $status),'order' => array('Lead.id' =>'DESC')); 
                }
                else
                {
                $options = array('conditions' => array('Lead.user_id' => $user_id),'order' => array('Lead.id' =>'DESC'));
                }
                
                $categories=$this->Lead->find("all",$options);
                foreach ($categories as $key=> $category)
                {
                    
                        $requestQuote=$this->RequestQuote->find("first",array("conditions"=>array("RequestQuote.lead_id"=>$category["Lead"]["id"],
                        "RequestQuote.posted_by"=>$category["Lead"]["assigned_to"])));
                        
                        if(!empty($requestQuote))
                        {
                        
                        $categories[$key]["Lead"]['quote']=array(
                        "lead_id"=>$category["Lead"]["id"],"quote_id"=>$requestQuote["RequestQuote"]["id"],"user_id"=>$requestQuote["RequestQuote"]["posted_to"],
                        "amount"=>$requestQuote["RequestQuote"]["price"],"assigned_to"=>$category["Lead"]["assigned_to"]    
                        );
                        }
                        else{
                            
                             $categories[$key]["Lead"]['quote']=array();
                            
                        }
                        
                    
                }
                
                
                
                
                if(!empty($categories))
                {
                    $data=array('Ack'=>1,'jobList'=>$categories,"is_savecard"=>$is_savecard);
                }
                else
                {
                    $data=array('Ack'=>0,'jobList'=>"");
                }
                echo json_encode($data);exit;
             }
        }
        
        //for delete job.............................................
         public function removejob() 
        {
              
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $id=$jsonData->id;
             $this->Lead->id = $id;
		
		if ($this->Lead->delete()) {
            
                    $data=array('Ack'=>1);
                }
                else
                {
                    $data=array('Ack'=>0,'jobList'=>"");
                }
                echo json_encode($data);exit;
             }
        }
        
         public function appliedproviders() 
        {
             $this->loadModel("Appliedprovider");
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $lead_id=$jsonData->lead_id;
             //echo $lead_id;die;
               $options = array('conditions' => array('Appliedprovider.lead_id' => $lead_id),'order' => array('Appliedprovider.id' =>'DESC'));
               $categories=$this->Appliedprovider->find("all",$options);
               $SITE_URL=Configure::read("SITE_URL"); 
               foreach ($categories as $key =>$category)
                {
                    
                   $category['User']['image']=!empty($category['User']['image'])?$SITE_URL.'user_images/'.$category['User']['image']:$SITE_URL.'nouser.png';
                   $categories[$key]["listdata"]["User"]=!empty($category['User'])?$category['User']:'';
                   $categories[$key]["listdata"]["Category"]=!empty($category['Category'])?$category['Category']:'';
                   $categories[$key]["listdata"]["Appliedprovider"]=!empty($category['Appliedprovider'])?$category['Appliedprovider']:'';
                                
                   $List[]= $categories[$key]["listdata"];
                   
                }
                
               
                if(!empty($categories))
                {
                    $data=array('Ack'=>1,'providerlist'=>$List);
                }
                else
                {
                    $data=array('Ack'=>0,'providerlist'=>"");
                }
                echo json_encode($data);exit;
             }
        }
        
        //for accept and reject provider....................................
        
         public function acceptProvider() 
        {
             
             $this->loadModel("Appliedprovider"); 
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $id=$jsonData->id;
             
              $this->request->data['Appliedprovider']['id']=$id;
              $this->request->data['Appliedprovider']['status']=1;

                if ($this->Appliedprovider->save($this->request->data)) 
                {
            
                    $data=array('Ack'=>1);
                }
                else
                {
                    $data=array('Ack'=>0,'jobList'=>"");
                }
                echo json_encode($data);exit;
             }
        }
        
     
         public function rejectProvider() 
        {
              
            $this->loadModel("Appliedprovider"); 
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $id=$jsonData->id;
             
              $this->request->data['Appliedprovider']['id']=$id;
              $this->request->data['Appliedprovider']['status']=2;

                if ($this->Appliedprovider->save($this->request->data)) 
                {
            
                    $data=array('Ack'=>1);
                }
                else
                {
                    $data=array('Ack'=>0,'jobList'=>"");
                }
                echo json_encode($data);exit;
             }
        }
        
        public function categorywisejob() 
        {
            $this->loadModel("Category");
            $this->loadModel("Userresponse");
            $this->loadModel("Question");
            $SITE_URL=Configure::read("SITE_URL");

            $options=array();
            $posts=array();
            if ($this->request->is(array('post', 'put'))) 
            {
                 $jsonData = $this->request->input('json_decode');
                 $name=$jsonData->name;
                 if(!empty($name))
                {
                    $conditions["Category.name LIKE"] ='%'.$name.'%';
                    $options=array('conditions'=>$conditions);
                }   
                $categories=$this->Category->find("list",$options);
                if(!empty($categories)) 
                {
                   
                    foreach ($categories as $cat_id => $category)
                    {
                        $leads=$this->Lead->find("all",array("conditions"=>array("Lead.category_id"=>$cat_id)));                        
                        foreach ($leads as $lead)
                        {
                            
                            
                            $lead['User']['image']=!empty($lead['User']['image'])?$SITE_URL.'user_images/'.$lead['User']['image']:$SITE_URL.'nouser.png';
                            $posts[$category][]=array("lead"=>$lead['Lead'],"users"=>$lead['User']);
                            
                        }
                          
                    }
                    
                    if(!empty($posts))
                    {

                        $data=array("Ack"=>1,"response"=>$posts);
                    }
                    else
                    {
                        $data=array("Ack"=>0,"msg"=>"No Jobs Found");
                        
                    }  
                    
                    
                }
                else
                {
                        $data=array("Ack"=>0,"msg"=>"No Jobs Found");
                }
                
                    
                
                
            echo json_encode($data);exit;

            }
           
            
            
            
            
            
        }
        
        
        
        //for get list of myjobs....................................
         public function myjobspro() 
        {
                    if ($this->request->is(array('post', 'put'))) {
                    $jsonData = $this->request->input('json_decode');
                    $user_id=$jsonData->user_id;
                    $status=$jsonData->status;
                    $this->User->recursive=-1;
                    $conditions=array();
                     if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
                        {
                            $ip = $_SERVER['HTTP_CLIENT_IP'];
                        } 
                        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
                        {
                          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                        } 
                        else 
                        {
                          $ip = $_SERVER['REMOTE_ADDR'];
                        }

                        $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));  
                        if($data->status == 'success')
                        {
                            date_default_timezone_set($data->timezone);
                            $timezone = $data->timezone;
                        }
                        else
                        {
                            date_default_timezone_set('Asia/Kolkata');
                            $timezone = 'Asia/Kolkata';
                        }
                        $tz_from = "UTC";
                        $tz_to = $timezone;
                        $format = 'Y-m-d h:i a';
                        if($status!="all")
                        {
                            $conditions["Lead.status"]=$status;
                        }
                        $user_service=$this->User->find("first",array("fields"=>array("User.cat_id"),"conditions"=>array("User.id"=>$user_id)));
                        if(!empty($user_service))
                        {
                            $service= explode(",", $user_service["User"]["cat_id"]);
                            $conditions["Lead.category_id"]=$service;
                        }
                        $options=array("fields"=>array("User.image","User.name","Category.name","Category.price","Lead.*"),"conditions"=>$conditions,"order"=>array("Lead.id"=>"desc"));
                        $this->Lead->unBindModel(array('hasMany'=>array('Providers','Userresponse'),
                                                'belongsTo'=>array('AssignedPro')
                
                ));
                    
                    $this->Lead->bindModel(array('hasMany' => array(
                    'RequestQuote' => array('className' => 'RequestQuote','fields'=>array("RequestQuote.id"),'conditions' => array('RequestQuote.posted_by = "'.$user_id.'"  ')),
                    )));
                    
                    $leads=$this->Lead->find("all",$options);
                   $SITE_URL=Configure::read("SITE_URL");   

                    if(!empty($leads))
                    {
                        foreach ($leads as $lead)
                        {
                        $lead["Lead"]["category_id"]=$lead["Category"]["name"];
                        $lead["Lead"]["profile_image"]=!empty($lead["User"]["image"])?$SITE_URL."user_images/".$lead["User"]["image"]:$SITE_URL.'nouser.png';
                        $dt = new DateTime($lead["Lead"]["posttime"], new DateTimeZone($tz_from));
                        $dt->setTimeZone(new DateTimeZone($tz_to));
                        $start_time=$dt->format($format) ;
                        $lead["Lead"]["posttime"]=$start_time;
                        $lead["Lead"]["assigned_to"]=(int)$lead["Lead"]["assigned_to"];
                        $lead["Lead"]["is_bid"]=!empty($lead["RequestQuote"])?1:0;
                        $lead["Lead"]["name"]=$lead["User"]["name"];
                        $lead["Lead"]["price"]=$lead["Category"]["price"];
                        $jobList[]=$lead["Lead"];
                        
                        }
                        
                        $data=array("Ack"=>1,"jobList"=>$jobList);
                    }
                    
                    else
                    {
                        $data=array("Ack"=>0,"jobList"=>array());
                    }
                    
                    
                    
                    echo json_encode($data);exit;
             }
        }
        
        //for delete job.............................................
         public function removejobpro() 
        {
              $this->loadModel("Appliedprovider");  
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $id=$jsonData->id;
             $this->Appliedprovider->id = $id;
		
		if ($this->Appliedprovider->delete()) {
            
                    $data=array('Ack'=>1);
                }
                else
                {
                    $data=array('Ack'=>0,'jobList'=>"");
                }
                echo json_encode($data);exit;
             }
        }
        
        //all project list...................................................
         public function projects($user_id=null,$searchval=null) 
        {
             //echo $searchval;
             
             $this->loadModel("Category"); 
             
             $this->loadModel("User");  
             $SITE_URL=Configure::read("SITE_URL");
             $category_idd=array();
             if(isset($user_id) && !empty($user_id)){
                 $user_details = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
                 $category_id = $user_details['User']['cat_id'];
                 if(!empty($category_id)){
                     $category_idd = explode(',',$category_id);
                        $category_idd = array_filter($category_id);
                 }
             }
             //print_r($category_idd);
             //exit;
             $status=5; //for admin approve................................
              $joins = array(
           
          array('table'=>'categories', 
                 'alias' => 'Category',
                 'type'=>'left',
              'conditions'=> array(
                 'Category.id = Lead.category_id'
          )
                 )
        );
              if($searchval!=''&&$searchval!='null'&&$searchval!='all')
              {
                  
                  
                      $categoryName = '';
                  
                  
                  if(!empty($category_idd)){
                      $lead = $this->Lead->find('all',array('conditions'=>array('AND' => array(
      'Lead.status' => $status,
      'Lead.title LIKE' => '%'.$searchval.'%',
      'Lead.category_id'=>$category_idd
      ))));
                  }else{
                      $lead = $this->Lead->find('all',array('conditions'=>array('AND' => array(
      'Lead.status' => $status,
        //'Lead.category_id'=>$searchval
      'Lead.title LIKE' => '%'.$searchval.'%'
      ))));
                  }
                 
                }  else {
                   $categoryName = '';
                   if(!empty($category_idd)){
                       $options = array('conditions' => array('Lead.status' => $status,'Lead.category_id'=>$category_idd),'order' => array('Lead.id' =>'DESC')); 
                   }else{
                       $options = array('conditions' => array('Lead.status' => $status),'order' => array('Lead.id' =>'DESC')); 
                   }
               
               
                
                $lead=$this->Lead->find("all",$options);
               }
                
            
                //print_r($lead);die;
                
                $Arr=[];
                
                
                if(!empty($lead))
                {
                    foreach($lead as $key =>$leadDetail)
                    {
                    $Arr[$key]=array("id"=>$leadDetail['Lead']['id'],"category_id"=>$leadDetail['Lead']['category_id'],"title"=>$leadDetail["Lead"]["title"],"address"=>$leadDetail['Lead']['address'],"description"=>$leadDetail['Lead']['description'],"posttime"=>$leadDetail['Lead']['posttime'],"userid"=>$leadDetail['User']['id'],"username"=>$leadDetail['User']['name'],
                        "userimage"=>!empty($leadDetail['User']['image'])?$SITE_URL.'user_images/'.$leadDetail['User']['image']:$SITE_URL.'nouser.png',"project_name"=>$leadDetail['Category']['project_name'],'price'=>$leadDetail['Category']['price']);
                    }
                    $data=array('Ack'=>1,'List'=>$Arr,'categoryName'=>$categoryName);
                }
                else
                {
                    $data=array('Ack'=>0,'List'=>"",'categoryName'=>$categoryName);
                }
                echo json_encode($data);exit;
            
        }
        
        public function projectsapp() 
        {
               $this->loadModel("Category");  
               $this->loadModel("RequestQuote");
             $SITE_URL=Configure::read("SITE_URL");   
             $status=5; //for admin approve................................
              $joins = array(
           
          array('table'=>'categories', 
                 'alias' => 'Category',
                 'type'=>'left',
              'conditions'=> array(
                 'Category.id = Lead.category_id'
          )
                 )
        );
              $jsonData = $this->request->input('json_decode');
              $searchval=$jsonData->searchval;
              $user_id=$jsonData->user_id;
              if($searchval!=''&&$searchval!='NULL'&&$searchval!='all')
              {
                 $lead = $this->Lead->find('all',array('conditions'=>array('AND' => array(
      'Lead.status' => $status,
      'Lead.description LIKE' => '%'.$searchval.'%'
      ))));
                } 
             
               else {
               $options = array('conditions' => array('Lead.status' => $status),'order' => array('Lead.id' =>'DESC')); 
               
                
                $lead=$this->Lead->find("all",$options);
               }
                
//                echo "<pre>";
//                print_r($lead);die;
                
                $Arr=[];
                
                
                if(!empty($lead))
                {
                    foreach($lead as $key =>$leadDetail)
                    {
                    $is_quote_post=$this->RequestQuote->find("count",array("conditions"=>array("RequestQuote.posted_by"=>$user_id,"RequestQuote.lead_id"=>$leadDetail['Lead']['id'])));    
                        
                    $Arr[$key]=array("id"=>$leadDetail['Lead']['id'],"address"=>$leadDetail['Lead']['address'],"description"=>$leadDetail['Lead']['description'],"posttime"=>$leadDetail['Lead']['posttime'],"userid"=>$leadDetail['User']['id'],"username"=>$leadDetail['User']['name'],
                        "userimage"=>!empty($leadDetail['User']['image'])?$SITE_URL.'user_images/'.$leadDetail['User']['image']:$SITE_URL.'nouser.png',"project_name"=>$leadDetail['Category']['project_name'],"is_quote_post"=>$is_quote_post
                         ,"price"=>$leadDetail["Category"]["price"],"title"=>$leadDetail["Lead"]["title"]
                            );
                    }
                    $data=array('Ack'=>1,'List'=>$Arr);
                }
                else
                {
                    $data=array('Ack'=>0,'List'=>"");
                }
                echo json_encode($data);exit;
            
        }
        
        
        
        public function hireProvider() 
        {
             $this->request->data1=array();
             $this->loadModel("Appliedprovider"); 
             $this->loadModel("Notification");
             $this->loadModel("User");
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             
             $lead_id=$jsonData->lead_id;
             $provider_id=$jsonData->provider_id;
             $login_id=$jsonData->user_id;
             
             //lead update...........................................
             $this->request->data['Lead']['id']=$lead_id;
             $this->request->data['Lead']['status']=1;
             $this->request->data['Lead']['assigned_to']=$provider_id;
             
             
                if ($this->Lead->save($this->request->data)) 
                {
                    $this->Appliedprovider->updateAll(
                        array('Appliedprovider.status' => 1,'Appliedprovider.project_status' => 1), 
                        array('Appliedprovider.user_id' => $provider_id,'Appliedprovider.lead_id' => $lead_id)
                    );
                    
                    $this->loadModel("RequestQuote"); 
                    $this->RequestQuote->updateAll(
                        array('RequestQuote.status' => 1), 
                        array('RequestQuote.posted_by' => $provider_id,'RequestQuote.lead_id' => $lead_id)
                    );
                    
                    $quote=$this->RequestQuote->find("first",array("conditions"=>array("RequestQuote.posted_by"=>$provider_id,"RequestQuote.lead_id"=>$lead_id)));
                    $notify["Notification"]["sent_by"]=$quote["RequestQuote"]["posted_to"];
                    $notify["Notification"]["sent_to"]=$provider_id;
                    $notify["Notification"]["lead_id"]=$lead_id;
                    $notify["Notification"]["quote_id"]=$quote["RequestQuote"]["id"];
                    $notify["Notification"]["type"]=4;
                    $notify["Notification"]["post_time"]= gmdate("Y-m-d H:i:s");
                    if($this->Notification->save($notify))
                    {
                        $data=array('Ack'=>1);
                        //added for push part................................
                        //$user_id=$jsonData->posted_to;
                        $userDetail=$this->User->find("first",array('conditions'=>array("User.id"=>$provider_id)));
                        $loginDetail=$this->User->find("first",array('conditions'=>array("User.id"=>$login_id)));
                        $login_name = $loginDetail['User']['name'];
                        $device_id=$userDetail['User']['token'];
                        $device_type=strtolower($userDetail['User']['device_type']);
                        $body="You are hired by ".$login_name;
                       if(!empty($device_id))
                       {

                         if($device_type=='android')
                           {

                              $registatoin_ids=array($device_id);
                              $totalmsgarray = array("message" => $body,"title"=>"hired","vibrate"=>1,"sound"=>1,"lead_id"=>$lead_id);
                               $this->android_push($registatoin_ids,$totalmsgarray);

                           }  
                           elseif($device_type=='ios')
                           {
                              //ios_push($device_id, $message); 

                                   $totalmsgarray = array("message" => $body,"type"=>"hired","lead_id"=>$lead_id);                                      
                                   $this->iphone_push($device_id, $body, 'default', json_encode($totalmsgarray));
                           }
                       }
                    
                    }
                }
                else
                {
                    $data=array('Ack'=>0,'jobList'=>"");
                }
                echo json_encode($data);
             }
             exit;
        }
        
        public function showbids($user_id=null) 
        {
            $SITE_URL=Configure::read("SITE_URL");
            $this->loadModel("User");
            $this->loadModel("Rating");
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }

            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
            if($data->status == 'success')
            {
                date_default_timezone_set($data->timezone);
                $timezone = $data->timezone;
            }
            else
            {
                date_default_timezone_set('Asia/Kolkata');
                $timezone = 'Asia/Kolkata';
            }
            $tz_from = "UTC";
            $tz_to = $timezone;
            $format = 'Y-m-d';
            $post=array();
            $this->Lead->unBindModel(array('hasMany'=>array('Providers','Userresponse'),
                                            'belongsTo'=>array('User','AssignedPro')
                
                ));
            $this->Lead->bindModel(
               array(
                 'hasMany'=>array(
                     'RequestQuote' =>array(
                      'className' => 'RequestQuote',
                      'foreignKey' => 'lead_id',
                      'conditions' => array(),
                  ),
                     
               )
            )
        ); 
            $leads=$this->Lead->find("all",array("conditions"=>array("Lead.user_id"=>$user_id)));
            if(!empty($leads))
            {
                foreach ($leads as $lead)
                {
                   $bids=array(); 
                   $dt = new DateTime($lead["Lead"]["posttime"], new DateTimeZone($tz_from));
                   $dt->setTimeZone(new DateTimeZone($tz_to));
                   $start_time=$dt->format($format) ; 
                   if(!empty($lead["RequestQuote"]))
                   {
                       foreach ($lead["RequestQuote"] as $quote)
                       {
                           $is_rating=$this->Rating->find("count",array("conditions"=>array("Rating.from_id"=>$user_id,'to_id'=>$quote["posted_by"])));
                           $this->User->recursive=-1;
                           $user=$this->User->find("first",array("fields"=>array("User.id","User.name","User.image"),"conditions"=>array("User.id"=>$quote["posted_by"])));
                           $user["User"]["image"]=!empty( $user["User"]["image"])?$SITE_URL.'user_images/'.$user["User"]["image"]:$SITE_URL.'nouser.png';
                           $user["User"]["is_hire"]=$lead["Lead"]["assigned_to"]==$quote["posted_by"]?'Yes':'No';
                           $user["User"]["is_rating"]=$is_rating;
                           $bids[]=$user["User"];
                       }
                   }
                   $total_bid=count($lead["RequestQuote"]);
                   $post[]=array('category_id'=>$lead["Category"]["id"],"category_name"=>$lead["Category"]["name"],"posttime"=>$start_time,
                                  "bids"=>$bids,"description"=>$lead["Lead"]["description"],"activity"=>!empty($lead["Lead"]["assigned_to"])?"Hired":"Ready to hire","lead_id"=>$lead["Lead"]["id"],
                       'total_quote'=>"".$total_bid.""
                       );
                   
                }
                $data=array("Ack"=>1,"response"=>$post);
            }
            else
            {
                $data=array("Ack"=>0);
            }
            echo json_encode($data);exit;
           
        }
        
        function job_list_category($cat_id=null)
        {
            $SITE_URL=Configure::read("SITE_URL");
            $response=array();
             if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
              $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
              $ip = $_SERVER['REMOTE_ADDR'];
            }
            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
            if($data->status == 'success')
            {
                date_default_timezone_set($data->timezone);
                $timezone = $data->timezone;
            }
            else
            {
                date_default_timezone_set('Asia/Kolkata');
                $timezone = 'Asia/Kolkata';
            }
            $tz_from = "UTC";
            $tz_to = $timezone;
            $format = 'd-m-Y h:i a';
            $this->Lead->unBindModel(array(
            'belongsTo'=>array('AssignedPro'),
            'hasMany'=>array('Providers','Userresponse'),   
                ));
            $options=array(
            "conditions"=> array("Lead.category_id"=>$cat_id),
            "fields"=>array("User.id","User.name","User.image","Category.id","Category.name","Lead.id","Lead.description","Lead.address",
                            "Lead.posttime"
                ),    
                
            );
            $leads=$this->Lead->find("all",$options);
            if(!empty($leads))
            {
                foreach ($leads as $lead)
                {
                    $lead["User"]["image"]=!empty($lead["User"]["image"])?$SITE_URL.'user_images/'. $lead["User"]["image"]:$SITE_URL.'nouser.png';
                    $dt = new DateTime($lead["Lead"]["posttime"], new DateTimeZone($tz_from));
                    $dt->setTimeZone(new DateTimeZone($tz_to));
                    $start_time=$dt->format($format) ; 
                    $lead["Lead"]["posttime"]=$start_time;
                    $response[]=$lead; 
                }
                $data=array("Ack"=>1,"response"=>$response,'cat_name'=>$leads[0]["Category"]["name"]);
            }
            else
            {
                $data=array("Ack"=>0);
            }
            
            echo json_encode($data);exit;
            
            
        }
        
        function set_complete($id)
        {
            $this->Lead->id = $id;
            if($this->Lead->saveField('status', 4))
            {
                $data=array('Ack'=>1);
            }
            else
            {
                $data=array('Ack'=>0);
            }
            echo json_encode($data);exit;
            
        }
        
        
        //for android push............................................
       
        
        //for ios push..............................................................
        
        function test_push()
        {
            $device_id="BB5047CD247AAB2899F7FE5C4A36B34DDBD7FB5B963F5B38F1CFD0DE0A5FFF6F";
            $totalmsgarray = array("message" =>"This is test push");
            $this->iphone_push($device_id, json_encode($totalmsgarray));
        }

         
}
