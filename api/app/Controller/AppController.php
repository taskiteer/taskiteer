 <?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
use Twilio\Rest\Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */

class AppController extends Controller
{
    
    public $components = array('Session');
    var $uses = array('SocialIcon', 'Footer', 'BookingRequest');
    
    public function beforeFilter()
    {
        $adminRoute = Configure::read('Routing.prefixes');
        #pr($adminRoute);
        if (isset($this->params['prefix']) && in_array($this->params['prefix'], $adminRoute)) {
            $this->layout = 'admin_default';
        } else {
            $this->layout = 'default';
        }
        
    }
    
    
    
    public function sendsms_mms($body = null, $phone_no = null)
    {
        require_once(ROOT . '/app/Vendor' . DS . 'twillo' . DS . 'autoload.php');
        // Your Account Sid and Auth Token from twilio.com/user/account
        /**** LIVE ***/
        $sid   = "ACf8a5d366a6da1f411935254025b07e06";
        $token = "1a1197879b5b281bcb418cd88c6b9cfe";
        
        // pr($sendpeople);exit;
        $client = new Client($sid, $token);
        
        // Use the client to do fun stuff like send text messages!
        try {
            $client->messages->create(
            // the number you'd like to send the message to
                $phone_no, array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+14694164496',
                // the body of the text message you'd like to send
                'body' => $body
            ));
            
        }
        catch (Exception $ex) {
            
            
        }
        
    }
    
    
    public function php_mail($to, $from, $subject, $message)
    {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        //$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        //$headers .= 'To: '.$to_name.' <'.$to.'>' . "\r\n";
        $headers .= 'From: ' . $from . "\r\n";
        mail($to, $subject, $message, $headers);
    }
    
    function create_slug($string, $ext = '')
    {
        $replace = '-';
        $string  = strtolower($string);
        
        //replace / and . with white space
        $string = preg_replace("/[\/\.]/", " ", $string);
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        
        //remove multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        
        //convert whitespaces and underscore to $replace
        $string = preg_replace("/[\s_]/", $replace, $string);
        
        //limit the slug size
        $string = substr($string, 0, 200);
        
        //slug is generated
        return ($ext) ? $string . $ext : $string;
    }
    
    /*function _setErrorLayout() {
    if ($this->name == 'CakeError') {
    $this->layout = 'error404';
    }
    }*/
    
    
    public function beforeRender()
    {
        
        $this->loadModel('User');
        $this->loadModel('Setting');
        $this->loadModel('Blockip');
        $conditions     = array(
            'Conditions' => array(
                '`Blockip`.`ip_address`' => $_SERVER['REMOTE_ADDR']
            )
        );
        $blockipaddress = $this->Blockip->find('first', $conditions);
        $this->set('blockipaddress', $blockipaddress);
        $adminuserid = $this->Session->read('adminuserid');
        
        $options     = array(
            'conditions' => array(
                'Setting.' . $this->Setting->primaryKey => 1
            )
        );
        $sitesetting = $this->Setting->find('first', $options);
        $this->set(compact('sitesetting'));
        
        //set sitelogo for the site
        $setting_array = array(
            'conditions' => array(
                'Setting.id' => '1'
            )
        );
        $Content       = $this->Setting->find('first', $setting_array);
        //pr($Content);
        if ($Content['Setting']['logo'] != '') {
            $logo_name = $Content['Setting']['logo'];
        } else {
            $logo_name = 'logo.png';
        }
        if ($Content['Setting']['fav_icon'] != '') {
            $fav_icon = $Content['Setting']['fav_icon'];
        } else {
            $fav_icon = '104573631_favicon.ico';
        }
        
        $this->set('logo', $this->webroot . 'site_logo/' . $logo_name);
        $this->set('fav_icon', $this->webroot . 'fav_icon/' . $fav_icon);
        
        if (isset($userid) && $userid != '') {
            //echo $userid;
            $options     = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $userid
                )
            );
            $userdetails = $this->User->find('first', $options);
            $this->set(compact('userdetails'));
            
        } else if (isset($adminuserid) && $adminuserid != '') {
            $options     = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $adminuserid
                )
            );
            $userdetails = $this->User->find('first', $options);
            $this->set(compact('userdetails'));
        }
        
        if (isset($adminuserid) && $adminuserid != '') {
            $sidebar  = array(
                'conditions' => array(
                    'User.id' => $adminuserid
                )
            );
            $sidebars = $this->User->find('first', $sidebar);
            $this->set(compact('sidebars'));
            
        }
        if (isset($adminuserid) && $adminuserid != '') {
            $roleAccess = $this->getRolesAccess($userdetails['User']['role']);
            if (!empty($roleAccess)) {
                $this->set('roleAccess', unserialize($roleAccess['RolesAccess']['accessibility']));
            }
        }
        $adminUrl = Configure::read('SITE_URL').'admin/';
        $this->set(compact('adminUrl'));
        
    }
    public function getRolesAccess($role = NULL)
    {
        $this->loadModel('RolesAccess');
        return $this->RolesAccess->find('first', array(
            'conditions' => array(
                'RolesAccess.role_id' => $role
            )
        ));
    }
    
    public function send_email($from = null, $to = null, $subject = null, $message = null)
    {
        $Email = new CakeEmail('gmail');
        $Email->emailFormat('both');
        $Email->from(array(
            "taskiteer@gmail.com" => "Taskiteer"
        ));
        $Email->to($to);
        $Email->subject($subject);
        $Email->send($message);
    }
    
    function how_log_ago($timestamp)
    {
        if (!isset($timestamp) || empty($timestamp))
            return false;
        $current    = gmdate("H:i:s");
        $difference = strtotime($current) - strtotime($timestamp);
        if ($difference < 60)
            return $difference . " seconds ago";
        else {
            $difference = round($difference / 60);
            if ($difference < 60)
                return $difference . " minute(s) ago";
            else {
                $difference = round($difference / 60);
                if ($difference < 24)
                    return $difference . " hour(s) ago";
                else {
                    $difference = round($difference / 24);
                    if ($difference < 7)
                        return $difference . " day(s) ago";
                    else {
                        $difference = round($difference / 7);
                        return $difference . " week(s) ago";
                    }
                }
            }
        }
    }
    
    public function iphone_push($device_token_id, $message, $notificationsound = 'default', $totalarray)
    {
        
        $data = array();
        
        
        
        $firsttoken = $device_token_id;
        
        // Put your device token here (without spaces):
        
        
        
        $deviceToken = $firsttoken;
        
        // Put your private key's passphrase here:
        
        
        
        $passphrase = '1234567890';
        
        // Put your alert message here:////////////////////////////////////////////////////////////////////////////////
        
        $ctx = stream_context_create();
        
        //require_once(WWW_ROOT.'RealEstatePushDev.pem');
        
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'TaskiteerProPushDev.pem'); //for Development
        
        //stream_context_set_option($ctx, 'ssl', 'local_cert', 'TaskiteerProPushDist.pem'); //for Distribution
        
        
        
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        
        // Open a connection to the APNS server
        
        
        
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);//for Development
        
        //$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); //for Distribution
        
        
        //print_r($fp);
        
        
        
        
        if (!$fp){
            
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }
            
        
        'Connected to APNS' . PHP_EOL;
        
        // Create the payload body
        
        
        
        $body['aps'] = array(
            
            'alert' => $message,
            
            'sound' => 'default',
            
            'data' => $totalarray
            
        );
        
        // Encode the payload as JSON
        
        
        
        $payload = json_encode($body);
        
        
        
        // Build the binary notification
        
        
        
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        
        
        
        // Send it to the server
        
        $result = fwrite($fp, $msg, strlen($msg));
        
        
        //print_r($result);
        //if (!$result)
        
        //echo 'Message not delivered' . PHP_EOL;
        
        
        
        //else
        
        //echo 'Message successfully delivered' . PHP_EOL;
        
        
        
        // Close the connection to the server
        
        fclose($fp);
        
        
    }
    
    
    public function iphone_push_user($device_token_id, $message, $notificationsound = 'default', $totalarray)
    {
        
        $data = array();
        
        
        
        $firsttoken = $device_token_id;
        
        // Put your device token here (without spaces):
        
        
        
        $deviceToken = $firsttoken;
        
        // Put your private key's passphrase here:
        
        
        
        $passphrase = '1234567890';
        
        // Put your alert message here:////////////////////////////////////////////////////////////////////////////////
        
        $ctx = stream_context_create();
        
        //require_once(WWW_ROOT.'RealEstatePushDev.pem');
        
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'TaskiteerPushDev.pem'); //for Development
        
        //stream_context_set_option($ctx, 'ssl', 'local_cert', 'TaskiteerPushDist.pem'); //for Distribution
        
        
        
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        
        // Open a connection to the APNS server
        
        
        
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);//for Development
        
        //$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx); //for Distribution
        
        
        
        
        
        
        
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        
        'Connected to APNS' . PHP_EOL;
        
        // Create the payload body
        
        
        
        $body['aps'] = array(
            
            'alert' => $message,
            
            'sound' => 'default',
            
            'data' => $totalarray
            
        );
        
        // Encode the payload as JSON
        
        
        
        $payload = json_encode($body);
        
        
        
        // Build the binary notification
        
        
        
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        
        
        
        // Send it to the server
        
        $result = fwrite($fp, $msg, strlen($msg));
        
        
        
        //if (!$result)
        
        //echo 'Message not delivered' . PHP_EOL;
        
        
        
        //else
        
        //echo 'Message successfully delivered' . PHP_EOL;
        
        
        
        // Close the connection to the server
        
        fclose($fp);
        
        
    }
    
    function android_push($registatoin_ids,$message) {

        //echo "subhradip";
        //$url = 'https://android.googleapis.com/qcm/send';
        $url = 'https://fcm.googleapis.com/fcm/send';
        //print_r($registatoin_ids);
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
        //print_r($fields);
        //exit;
        $headers = array(
            //'Authorization: key=AIzaSyAPM9-XCGSQMBHWkeRd-c-hkgVAqyaOS80',
            'Authorization: key=AAAAsXY5RAM:APA91bEZFU6076tCmxJyUW1bK7SGynJbtuECGTq3dacLeD3XxG3yTdhsqauaBXcl3-fAWbghN01itwRm_7_6_vkoBlCDVdLCC-SXoaQqNxUmie3OdDI7GOFf5Au3Uu8HZHc64B8A-XIC',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
       //print_r($result);
//      exit;
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo $result;
    }
    
    

}
