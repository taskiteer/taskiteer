<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class MessagesdealsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');



/**
 * index method
 *
 * @return void
 */
function sent_message()
{
   $this->loadModel("Deal");
    $this->loadModel("Messagesdeal");
    $this->loadModel("Lead");
    $this->loadModel("Notification");
    $this->RequestQuote->recursive=-1;
    if ($this->request->is(array('post', 'put'))) 
    {
        $jsonData = $this->request->input('json_decode');
        foreach ($jsonData as $key=>$json)
        {
            $this->request->data["Messagesdeal"][$key]=$json;
        }
        $quote=$this->Deal->find("first",array("conditions"=>array("Deal.id"=>$jsonData->quote_id)));
        if($jsonData->sent_by==$quote["Deal"]["user_id"])
        {
            $this->request->data["Messagesdeal"]["sent_to"]=$quote["Deal"]["user_id"];
            $send_to=$quote["RequestQuote"]["posted_to"];
        }
        else
        {
            $this->request->data["Messagesdeal"]["sent_to"]=$quote["Deal"]["user_id"];
            $send_to=$quote["Deal"]["user_id"];
        }
        $this->request->data["Messagesdeal"]["deal_id"]=$jsonData->quote_id;
        $this->request->data["Messagesdeal"]["dt_pub_date"]= gmdate("Y-m-d H:i:s");
         
         
         
         try
         {
             $this->Messagesdeal->save($this->request->data);
             $message_id=$this->Messagesdeal->getLastInsertID();
             $this->request->data["Notification"]["sent_by"]=$jsonData->sent_by;
             $this->request->data["Notification"]["sent_to"]=$send_to;
             $this->request->data["Notification"]["message_id"]=$message_id;
             $this->request->data["Notification"]["post_time"]=$this->request->data["Messagesdeal"]["dt_pub_date"];
             $this->request->data["Notification"]["type"]=1;             
             $this->Notification->save($this->request->data);
             $data=array("Ack"=>1);
         } catch (Exception $ex) 
         {
             pr($ex);
             $data=array("Ack"=>0);
             

         }
         
         echo json_encode($data);exit;
         
        

    } 
}

function  conversations($sent_by=null,$quote_id=null,$logged_user=null)
{
    $this->loadModel("Deal");
    $this->loadModel("Messagesdeal");
    $this->loadModel("Lead");
    $this->loadModel("Notification");
    $conditions["Messagesdeal.deal_id"]=$quote_id;
    $conditions["OR"][]=array("Messagesdeal.sent_by"=>$sent_by);
    $conditions["OR"][]=array("Messagesdeal.sent_to"=>$sent_by);
    $results=$this->Messagesdeal->find("all",array("conditions"=>$conditions,"order"=>array("Messagesdeal.id asc")));
    $chats=array();
    $SITE_URL=Configure::read("SITE_URL");
    $this->loadModel("User");
    $this->User->recursive=-1;
    $chats["conversation"]=array();
    
    foreach ($results as $result)
    {
       $sent_by=$this->User->find("first",array('fields'=>array("User.id","User.name","User.image"),"conditions"=>array("User.id"=>$result["Messagesdeal"]["sent_by"]))); 
       $sent_by["User"]["image"]=!empty($sent_by["User"]["image"])?$SITE_URL.'user_images/'.$sent_by["User"]["image"]:$SITE_URL.'nouser.png';
       $result["Messagesdeal"]["sent_by"]=$sent_by["User"];
       
       $sent_to=$this->User->find("first",array('fields'=>array("User.id","User.name","User.image"),"conditions"=>array("User.id"=>$result["Messagesdeal"]["sent_to"]))); 
       $sent_to["User"]["image"]=!empty($sent_to["User"]["image"])?$SITE_URL.'user_images/'.$sent_to["User"]["image"]:$SITE_URL.'nouser.png';
       if($result["Messagesdeal"]["sent_to"]==$logged_user)
       {
         $this->Messagesdeal->id = $result["Messagesdeal"]["id"];
         $this->Messagesdeal->saveField('is_read', 1);
         $this->Notification->query("update notifications set is_read=1 where message_id='".$result["Messagesdeal"]["id"]."'");
       }
       
       $result["Messagesdeal"]["sent_to"]=$sent_to["User"];
       
       
       
       $result["Messagesdeal"]["howlongago"]=$this->how_log_ago($result["Messagesdeal"]["dt_pub_date"]);
       $chats['conversation'][]=$result["Messagesdeal"];
       
       
    }
    
    //chat with user details...................................
    $QuoteArr=$this->Deal->find("first",array("conditions"=>array("Deal.id"=>$quote_id))); 
    $UserArr=$this->User->find("first",array("conditions"=>array("User.id"=>$QuoteArr["Deal"]["user_id"]))); 
    $this->loadModel("Lead");
    $this->loadModel("Rating");
    $avg_score=round($UserArr['User']['avg_score']);
    $hired=0;
    $hired=$this->Lead->find("count",array("conditions"=>array("Lead.assigned_to"=>$QuoteArr["Deal"]["user_id"])));
   $chats['Userdetail']=array("id"=>$UserArr["User"]["id"],"address"=>$UserArr["User"]["address"],"name"=>$UserArr["User"]["name"],"email"=>$UserArr["User"]["email"],"mobile_no"=>$UserArr["User"]["mobile_no"],
                        "userimage"=>!empty($UserArr['User']['image'])?$SITE_URL.'user_images/'.$UserArr['User']['image']:$SITE_URL.'nouser.png',
                        "rateing"=>$avg_score,"hired_no"=>$hired
       
       );
   
   $chats['Dealdetail']=array("id"=>$QuoteArr["Deal"]["id"],"name"=>$QuoteArr["Deal"]["name"],"description"=>$QuoteArr["Deal"]["description"],
                        "dealimage"=>!empty($QuoteArr['Deal']['image'])?$SITE_URL.'dealimg/'.$QuoteArr['Deal']['image']:$SITE_URL.'nouser.png',
                        "rateing"=>$avg_score
       
       );
   
  
   if(!empty($chats))
    {
        $data=array("Ack"=>1,'chats'=>$chats);
    }
    else
    {
        $data=array("Ack"=>0,'chats'=>$chats);
    }
    
    echo json_encode($data);exit;
    
    
    
    
    
}

function count_notification($user_id=null)
{
    $this->loadModel("Notification");
    $count=$this->Notification->find('count',array('conditions'=>array("Notification.sent_to"=>$user_id,"Notification.is_read"=>0)));
    $data=array('Ack'=>1,'notification'=>$count);
    echo json_encode($data);exit;
}

	
function notification($user_id=null)
{
   $SITE_URL=Configure::read("SITE_URL");  
   $this->loadModel("Category");
    $this->loadModel("Notification");
    $notification_count=$this->Notification->find('count',array('conditions'=>array("Notification.sent_to"=>$user_id,"Notification.is_read"=>0)));    
    $Arr=[];
    if($notification_count>0)
    {
    $notification=$this->Notification->find('all',array('conditions'=>array("Notification.sent_to"=>$user_id,"Notification.is_read"=>0)));
     foreach($notification as $key =>$notiDetail)
                    {
                       
                        if($notiDetail['Notification']['type']==1)
                        {
                            $msg="sent you a Messagesdeal";
                        }
                        elseif($notiDetail['Notification']['type']==2)
                        {
                            $msg="posted a Job";
                        }
                         elseif($notiDetail['Notification']['type']==3)
                        {
                            $msg="posted a job Request"; 
                        }
                        
                        $time = strtotime($notiDetail['Notification']['post_time']);
                        $timeDiff = time() - $time;
                        $unit='86400';
                        $numberOfUnits = floor($timeDiff / $unit);

                        if($numberOfUnits>30)
                        {
                           $notification_time=date('M d H:i A', $time); 
                        }
                        else if($numberOfUnits==1)
                        {
                            $notification_time=$numberOfUnits." Day ago";
                        }
                         else if($numberOfUnits==0||$numberOfUnits<1)
                          {
                             $notification_time=date('H:i A', $time);
                          } 
                          else 
                          {
                             $notification_time=$numberOfUnits." Days ago";
                          } 
                      
                    //$cat=$this->Category->find('first',array('conditions'=>array("Category.id"=>$notiDetail['Lead']['category_id']))); 
                    //print_r($cat);
                    $Arr[$key]=array("id"=>$notiDetail['Notification']['id'],"msg"=>$msg,"username"=>$notiDetail['User']['name'],"posttime"=>$notification_time,
                        "userimage"=>!empty($notiDetail['User']['image'])?$SITE_URL.'user_images/'.$notiDetail['User']['image']:$SITE_URL.'nouser.png');
                    }
                   
    }
    $data=array('Ack'=>1,'notification'=>$notification_count,'notificationArr'=>$Arr);
    echo json_encode($data);exit;
}

function notificationdetail($user_id=null)
{
   $SITE_URL=Configure::read("SITE_URL");  
   $this->loadModel("Category");
   $this->loadModel("Lead");
   $this->loadModel("RequestQuote");
   $this->loadModel("Messagesdeal");
    $this->loadModel("Notification");
    $notification_count=$this->Notification->find('count',array('conditions'=>array("Notification.sent_to"=>$user_id)));    
    $Arr=[];
    if($notification_count>0)
    {
    $notification=$this->Notification->find('all',array('conditions'=>array("Notification.sent_to"=>$user_id)));
     foreach($notification as $key =>$notiDetail)
                    {
                       $msg='';
                       $msgContent='';
                        if($notiDetail['Notification']['type']==1)
                        {
                           $contentMsg=$this->Messagesdeal->find('first',array('conditions'=>array("Messagesdeal.id"=>$notiDetail['Notification']['message_id'])));
                            $msg="sent you a Messagesdeal";
                            $msgContent=$contentMsg['Messagesdeal']['msg'];
                        }
                        elseif($notiDetail['Notification']['type']==2)
                        {
                            $msg="posted a Job";
                             $contentMsg=$this->Lead->find('first',array('conditions'=>array("Lead.id"=>$notiDetail['Notification']['lead_id'])));
                             $msgContent="Job location - ".$contentMsg['Lead']['address']." ".$contentMsg['Lead']['description'];
                             
                        }
                         elseif($notiDetail['Notification']['type']==3)
                        {
                            $msg="posted a job Request"; 
                             $contentMsg=$this->RequestQuote->find('first',array('conditions'=>array("RequestQuote.id"=>$notiDetail['Notification']['quote_id'])));
                             $msgContent=$contentMsg['RequestQuote']['comment'];
                        }
                        
                        $time = strtotime($notiDetail['Notification']['post_time']);
                        $timeDiff = time() - $time;
                        $unit='86400';
                        $numberOfUnits = floor($timeDiff / $unit);

                        if($numberOfUnits>30)
                        {
                           $notification_time=date('M d H:i A', $time); 
                        }
                        else if($numberOfUnits==1)
                        {
                            $notification_time=$numberOfUnits." Day ago";
                        }
                         else if($numberOfUnits==0||$numberOfUnits<1)
                          {
                             $notification_time=date('H:i A', $time);
                          } 
                          else 
                          {
                             $notification_time=$numberOfUnits." Days ago";
                          } 
                      
                    //$cat=$this->Category->find('first',array('conditions'=>array("Category.id"=>$notiDetail['Lead']['category_id']))); 
                    //print_r($cat);
                    $Arr[$key]=array("id"=>$notiDetail['Notification']['id'],"msg"=>$msg,"username"=>$notiDetail['User']['name'],"posttime"=>$notification_time,"msgContent"=>$msgContent,
                        "userimage"=>!empty($notiDetail['User']['image'])?$SITE_URL.'user_images/'.$notiDetail['User']['image']:$SITE_URL.'nouser.png');
                    }
        $data=array('Ack'=>1,'notification'=>$notification_count,'notificationArr'=>$Arr);            
    }
    else
    {
    $data=array('Ack'=>0,'notification'=>0,'notificationArr'=>'');
    }
    echo json_encode($data);exit;
}
}
