<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class PortfoliosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public function uploadportfolio() {
            if ($_POST) {
                                                                //pr($this->request->data);exit;
                
                                                                
                    $user_id=$this->request->data["user_id"];                                            
                    if(!empty($_FILES['image']['name']))
                    {

                        $pathpart=pathinfo($_FILES['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "portfolioimg/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($_FILES['image']['tmp_name'],$full_flg_path);
                        $this->request->data['Portfolio']["image"]=$filename;
                        $this->request->data['Portfolio']["user_id"]=$this->request->data["user_id"];
                        $this->Portfolio->create();
                        if ($this->Portfolio->save($this->request->data)) 
                        {
                           $data=array("Ack"=>1,"msg"=>"Portfolio uploaded successfully");  
                           
                        }

                        }
                        else
                        {
                           $data=array("Ack"=>0,"msg"=>"Upload Failure");  
                        }
                        
                        
                        
                    }
                     
            }
            
            
            echo json_encode($data);
            
            exit;
	}
        public function index($user_id=null) 
        {
            $portfolios=$this->Portfolio->find("all",array("conditions"=>array("Portfolio.user_id"=>$user_id)));
            $SITE_URL=Configure::read("SITE_URL");
            $response=array();
            if(!empty($portfolios))
            {
                foreach ($portfolios as $portfolio)
                {
                    $response[]=array("id"=>$portfolio["Portfolio"]["id"],"image"=>$SITE_URL.'portfolioimg/'.$portfolio["Portfolio"]["image"]);
                }
                $data=array("Ack"=>1,"response"=>$response);
                
            }
            else
            {
                $data=array("Ack"=>0);
            }
            
         echo json_encode($data);exit;   
            
        }
        public function delete($id=null) 
        {
            $portfolio=$this->Portfolio->find("first",array("conditions"=>array("Portfolio.id"=>$id)));
            unlink(WWW_ROOT.'portfolioimg/'.$portfolio["Portfolio"]["image"]);
            $this->Portfolio->id = $id;
            if ($this->Portfolio->delete()) 
            {
                $data=array("Ack"=>1);
            }
            
            echo json_encode($data);exit;


            
        }
        
        
        



}
