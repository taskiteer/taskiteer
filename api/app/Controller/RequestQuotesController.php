<?php

App::uses('AppController', 'Controller');

/**
 * Seos Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class RequestQuotesController extends AppController {

	var $uses = array('User','RequestQuote','Lead');
	public $components = array('Paginator', 'Session');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'RequestQuote.id' => 'desc'
        )
    );
    public function admin_index() {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        $this->RequestQuote->recursive = 0;
        $this->Paginator->settings = $this->paginate;
        $this->set('RequestQuotes', $this->Paginator->paginate('RequestQuote'));
    }

	public function admin_add() {
		$is_admin = $this->Session->read('is_admin');
		if (!isset($is_admin) && $is_admin == '') {
			$this->redirect('/admin');
		}



		if($this->request->is('post')) {

			if(!empty($this->request->data['RequestQuote']['quotes']['tmp_name'])) {
				$path = $this->request->data['RequestQuote']['quotes']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if(in_array($ext, array('png','pdf','quotes','jpeg','jpg'))) {
                	$uploadPath = Configure::read('UPLOAD_QUOTE_PATH');
                	$imageName = rand() . '_' . (strtolower(trim($this->request->data['RequestQuote']['quotes']['name'])));
                    $full_image_path = $uploadPath . '/' . $imageName;
                    move_uploaded_file($this->request->data['RequestQuote']['quotes']['tmp_name'], $full_image_path);
                    $this->request->data['RequestQuote']['quotes'] = $imageName;

                    $this->RequestQuote->create();
                    if($this->RequestQuote->save($this->request->data)) {
                    	$this->Session->setFlash('save.', 'default', array('class'=>'success'));
                        return $this->redirect(array('action' => 'index'));
                    } else {
                    	$this->Session->setFlash('DB error.', 'default', array('class'=>'error'));
                        return $this->redirect(array('action' => 'index'));
                    }

                }
                else{
                   $this->Session->setFlash('file extention is not valid.', 'default', array('class'=>'error'));
                   return $this->redirect(array('action' => 'admin_add')); 
                }
			} else {
				$this->Session->setFlash('include quotes before submit.');
			}
			// $this->RequestQuote->create();
			// if($this->RequestQuote->save($this->request->data)) {

			// }
		}

		$users  = $this->User->find('list', array('fields' => array('User.id', 'User.first_name')));
        $posts  = $this->Post->find('list', array('fields' => array('Post.id', 'Post.post_title')));
		$this->set(compact('users','posts'));
	}

    public function admin_edit($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }

        if ($this->request->is(array('post', 'put'))) {
            if(!empty($this->request->data['RequestQuote']['quotes']['tmp_name'])) {
                $path = $this->request->data['RequestQuote']['quotes']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                if ($ext) {
                    $uploadPath = Configure::read('UPLOAD_QUOTE_PATH');
                    $extensionValid = array('png','pdf','quotes');
                    if (in_array($ext, $extensionValid)) {
                        $OldImg = $this->request->data['saved_image'];
                        $imageName = rand() . '_' . (strtolower(trim($this->request->data['RequestQuote']['quotes']['name'])));
                        $full_image_path = $uploadPath . '/' . $imageName;
                        move_uploaded_file($this->request->data['RequestQuote']['quotes']['tmp_name'], $full_image_path);
                        $this->request->data['RequestQuote']['quotes'] = $imageName;
                        if ($OldImg != '') {
                            unlink($uploadPath . '/' . $OldImg);
                        }
                    }
                    else
                    {
                    $this->Session->setFlash(__('Invalid Image Type.'));
                    return $this->redirect(array('action' => 'edit', $id));
                    }
                }
            }
            else
            {
                $this->request->data['RequestQuote']['quotes'] = $this->request->data['saved_image'];
                unset($this->request->data['RequestQuote']['saved_image']);
            }

                $id = $this->request->data['RequestQuote']['id'];
                $status = $this->request->data['RequestQuote']['status'];
                $data = array('id' => $id,'status' => $status);
                $this->User->save($data);


            if ($this->RequestQuote->save($this->request->data)) {

                $this->Session->setFlash('The Quote has been saved.','default', array('class'=>'success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('The Quote could not be saved. Please, try again.','default', array('class'=>'error'));
            }
        } else {

            $options = array('conditions' => array('RequestQuote.id' => $id));
            $this->request->data = $this->RequestQuote->find('first', $options);
        }
        $users  = $this->User->find('list', array('fields' => array('User.id', 'User.first_name')));
        $posts  = $this->Post->find('list', array('fields' => array('Post.id', 'Post.post_title')));
        $this->set(compact('users','posts'));

    }

    public function admin_view($id = null) {
        if (!$this->RequestQuote->exists($id)) {
            throw new NotFoundException(__('Invalid order'));
        }
        $options = array('conditions' => array('RequestQuote.' . $this->RequestQuote->primaryKey => $id));
        $this->set('RequestQuote', $this->RequestQuote->find('first', $options));
    }

    public function admin_delete($id = null) {
        $this->RequestQuote->id = $id;
        if (!$this->RequestQuote->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->RequestQuote->delete()) {
            $this->Session->setFlash('The order has been deleted.','default', array('class'=>'success'));
        } else {
            $this->Session->setFlash('The order could not be deleted. Please, try again.','default', array('class'=>'error'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function add(){

        $userid = $this->Session->read('userid');
        if (empty($userid)) {
            $this->redirect('/users/login');
        }

        // $userdetails = $this->Post->User->find('all', array('fields' => array('User.id', 'User.admin_type'), 'conditions' => array('User.id' => $userid)));

        // $admin_type=$userdetails[0]['User']['admin_type'];

        // if ( $admin_type==1 || $admin_type==2 && $admin_type==1)
        // {
        //     throw new NotFoundException(__('Invalid user'));
        // }


        if($this->request->is('post')) {
            
            if(!empty($this->request->data['RequestQuote']['quotes']['tmp_name'])) {
                $path = $this->request->data['RequestQuote']['quotes']['name'];
                $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                
                if(in_array($ext, array('png','pdf','quotes','jpeg','jpg'))) {
                    $uploadPath = Configure::read('UPLOAD_QUOTE_PATH');
                    $imageName = rand() . '_' . (strtolower(trim($this->request->data['RequestQuote']['quotes']['name'])));
                    $full_image_path = $uploadPath . '/' . $imageName;
                    move_uploaded_file($this->request->data['RequestQuote']['quotes']['tmp_name'], $full_image_path);
                    $this->request->data['RequestQuote']['quotes'] = $imageName;
                    
                    
                    $this->RequestQuote->create();
                    
                    if($this->RequestQuote->save($this->request->data)) {
                        $this->Session->setFlash('Request For Quote Saved.', 'default', array('class'=>'success'));
                        return $this->redirect(array('controller' => 'users','action' => 'update_profile'));
                    } else {
                        $this->Session->setFlash('DB error.', 'default', array('class'=>'error'));
                    }
                
                }
                else{
                    $this->Session->setFlash('file extention is not valid.', 'default', array('class'=>'error'));
                    return $this->redirect(array('action' => 'add'));
                }
            } else {
                $this->Session->setFlash('include quotes before submit.');
            }


        }

        $users  = $this->User->find('list', array('fields' => array('User.id', 'User.first_name')));
        $posts  = $this->Post->find('list', array('fields' => array('Post.id', 'Post.post_title')));
        $this->set(compact('users','posts'));

    }
	public function doBraintreePayment(){
		$jsonData = $this->request->input('json_decode');
		if($jsonData->payment_method_nonce){
			require_once("/../../vendor/autoload.php");
			$gateway = new Braintree_Gateway([
				'environment' => 'sandbox',
				'merchantId' => 'wnpyj8fwfdqgh9hq',
				'publicKey' => 'tzx7phj4tmrzy655',
				'privateKey' => 'cede53256038b9d6dc43d67ba74f1fbe'
			]);
			$result = $gateway->transaction()->sale([
				'amount' => $jsonData->price,
				'paymentMethodNonce' => $jsonData->payment_method_nonce,
				'options' => [
				'submitForSettlement' => true
				]
			]);
			if ($result->success || !is_null($result->transaction)) {
				return $result;
			}else{
				$data=array("Ack"=>0);
				echo json_encode($data);exit;
			}
			
		}
	}
	//payment using braintree
	public function post_quote_braintree(){
		$this->loadModel('ProfBid');       
        $apiKey = "AAAANIxLqnk:APA91bEQNk1a2rMaKiSg2uwX_lCvLpnFREe8NUriXpWng2GKECuMt_OK0anplLNnEQIHDE0vaiLIqFkHXrFK-f9BCPnBCyHr0R--kjhpMXOFCdv-KSHqL9XseGC0TEzOEpaLpnTTW2c4";
		if ($this->request->is(array('post', 'put'))){
			$jsonData = $this->request->input('json_decode');
			if($jsonData->payment_method_nonce){
			  if($paymentData=$this->doBraintreePayment($jsonData)){
				  $this->loadModel('ProfPayment');
				  $data['ProfPayment']['lead_id']=$jsonData->lead_id;
				  $data['ProfPayment']['user_id']=$jsonData->posted_by;
				  $data['ProfPayment']['amount']=1;
				  $data['ProfPayment']['paid_date']=date('Y-m-d h:i:s');
				  $data['ProfPayment']['transaction_id']=$paymentData->transaction->id;
				  if($this->ProfPayment->save($data)){
                    $payment_id=$this->ProfPayment->getLastInsertID();
					if($payment_id){
						$this->post_quote_after_payment($jsonData);
					}
				  }else{
					$data=array("Ack"=>0);
					echo json_encode($data);exit;
				  }
				  
			  }
			}
		}
	}
	

	public function post_quote_after_payment($QuoteData){
		$this->loadModel('ProfBid');       
        $apiKey = "AAAANIxLqnk:APA91bEQNk1a2rMaKiSg2uwX_lCvLpnFREe8NUriXpWng2GKECuMt_OK0anplLNnEQIHDE0vaiLIqFkHXrFK-f9BCPnBCyHr0R--kjhpMXOFCdv-KSHqL9XseGC0TEzOEpaLpnTTW2c4";       
        if ($this->request->is(array('post', 'put'))) {
			$jsonData = $QuoteData;
			$this->request->data['RequestQuote']['posttime']=gmdate('Y-m-d H:i:s');                        
			$this->request->data['RequestQuote']['lead_id']=$jsonData->lead_id;
			$this->request->data['RequestQuote']['posted_to']=$jsonData->posted_to;
			$this->request->data['RequestQuote']['posted_by']=$jsonData->posted_by;
			$this->request->data['RequestQuote']['comment']=$jsonData->comment;
			$this->request->data['RequestQuote']['price']=$jsonData->price;
			 $quoteDetail=$this->RequestQuote->find("first",array('conditions'=>array("RequestQuote.lead_id"=>$jsonData->lead_id,"RequestQuote.posted_by"=>$jsonData->posted_by)));
            if(empty($quoteDetail)){
                $bid_count = $this->ProfBid->find("count", array('conditions'=>array('ProfBid.user_id'=>$jsonData->posted_by,'ProfBid.lead_id'=>$jsonData->lead_id)));
				if($bid_count == 0){
					$bidSave = array();
					$bidSave['ProfBid']['user_id'] = $jsonData->posted_by;
					$bidSave['ProfBid']['lead_id'] = $jsonData->lead_id;
					$bidSave['ProfBid']['bid_date'] = gmdate('Y-m-d H:i:s');
					$this->ProfBid->create();
					$this->ProfBid->save($bidSave);
				}
				$this->RequestQuote->create();
                if($this->RequestQuote->save($this->request->data)){
				   $quote_id=$this->RequestQuote->getLastInsertID();
					
					//for insert into notification table........................
					$notiArr=array();
					$this->loadModel('Notification');
					$notiArr['Notification']['post_time']=gmdate('Y-m-d H:i:s');                        
					$notiArr['Notification']['quote_id']=$quote_id;
					$notiArr['Notification']['sent_to']=$jsonData->posted_to;
					$notiArr['Notification']['sent_by']=$jsonData->posted_by;
					$notiArr['Notification']['type']=3;
					$this->Notification->create();
					$this->Notification->save($notiArr);
					
					//for insert into message table...................................
					$this->loadModel("Message");
					$msg=$jsonData->comment." My price is ".$jsonData->price;
					$msgArr=array();
					$msgArr['Message']["dt_pub_date"]= gmdate("Y-m-d H:i:s");                   
					$msgArr['Message']['quote_id']=$quote_id;
					$msgArr['Message']['lead_id']=$jsonData->lead_id;
					$msgArr['Message']['sent_to']=$jsonData->posted_to;
					$msgArr['Message']['sent_by']=$jsonData->posted_by;
					$msgArr['Message']['msg']=$msg;
					$this->Message->create();
					$this->Message->save($msgArr);
					$data=array("Ack"=>1);
					
					//added for push part................................
					 $user_id=$jsonData->posted_to;
					 $userDetail=$this->User->find("first",array('conditions'=>array("User.id"=>$user_id)));
					 $device_id=$userDetail['User']['token'];
					 $device_type=strtolower($userDetail['User']['device_type']);
					 $body="New quote posted for your job";
					 if(!empty($device_id)){
						if($device_type=='android'){
						   $registatoin_ids=array($device_id);
						   $totalmsgarray = array("message" => $body,"title"=>"Quote posted","vibrate"=>1,"sound"=>1,"quote_id"=>$quote_id);
							$this->android_push($registatoin_ids,$totalmsgarray);
						}elseif($device_type=='ios'){
						   $totalmsgarray = array("message" => $body,"type"=>"Quote posted","quote_id"=>$quote_id);                                      
							$this->iphone_push_user($device_id, $body, 'default', json_encode($totalmsgarray));
						}
                    }    
                }
            }else {
                $data=array("Ack"=>0);
            }
		} 
        echo json_encode($data);exit;
	}
	
    //for frontend add quote by customer...............................................
       public function post_quote() {
            $this->loadModel('ProfBid');       
            $apiKey = "AAAANIxLqnk:APA91bEQNk1a2rMaKiSg2uwX_lCvLpnFREe8NUriXpWng2GKECuMt_OK0anplLNnEQIHDE0vaiLIqFkHXrFK-f9BCPnBCyHr0R--kjhpMXOFCdv-KSHqL9XseGC0TEzOEpaLpnTTW2c4";       
           if ($this->request->is(array('post', 'put'))) {
				$jsonData = $this->request->input('json_decode');
				$this->request->data['RequestQuote']['posttime']=gmdate('Y-m-d H:i:s');                        
				$this->request->data['RequestQuote']['lead_id']=$jsonData->lead_id;
				$this->request->data['RequestQuote']['posted_to']=$jsonData->posted_to;
				$this->request->data['RequestQuote']['posted_by']=$jsonData->posted_by;
				$this->request->data['RequestQuote']['comment']=$jsonData->comment;
				$this->request->data['RequestQuote']['price']=$jsonData->price;
                        //print_r($this->request->data);
                        //$user_id=$this->request->data['RequestQuote']['user_id'];
                        //$pro_id=$this->request->data['RequestQuote']['pro_id'];
                         $quoteDetail=$this->RequestQuote->find("first",array('conditions'=>array("RequestQuote.lead_id"=>$jsonData->lead_id,"RequestQuote.posted_by"=>$jsonData->posted_by)));
                     //print_r($quoteDetail);die;
                         if(empty($quoteDetail))
                     {
                        
                        $bid_count = $this->ProfBid->find("count", array('conditions'=>array('ProfBid.user_id'=>$jsonData->posted_by,'ProfBid.lead_id'=>$jsonData->lead_id)));
                        if($bid_count == 0){
                            $bidSave = array();
                            $bidSave['ProfBid']['user_id'] = $jsonData->posted_by;
                            $bidSave['ProfBid']['lead_id'] = $jsonData->lead_id;
                            $bidSave['ProfBid']['bid_date'] = gmdate('Y-m-d H:i:s');
                            $this->ProfBid->create();
                            $this->ProfBid->save($bidSave);
                        }
                        
                   
                        $this->RequestQuote->create();
                        if($this->RequestQuote->save($this->request->data))
                        {
                           $quote_id=$this->RequestQuote->getLastInsertID();
                            
                            //for insert into notification table........................
                            $notiArr=array();
                            $this->loadModel('Notification');
                            $notiArr['Notification']['post_time']=gmdate('Y-m-d H:i:s');                        
                            $notiArr['Notification']['quote_id']=$quote_id;
                            $notiArr['Notification']['sent_to']=$jsonData->posted_to;
                            $notiArr['Notification']['sent_by']=$jsonData->posted_by;
                            $notiArr['Notification']['type']=3;
                            $this->Notification->create();
                            $this->Notification->save($notiArr);
                            
                            //for insert into message table...................................
                            $this->loadModel("Message");
                            $msg=$jsonData->comment." My price is ".$jsonData->price;
                            $msgArr=array();
                            $msgArr['Message']["dt_pub_date"]= gmdate("Y-m-d H:i:s");                   
                            $msgArr['Message']['quote_id']=$quote_id;
                            $msgArr['Message']['lead_id']=$jsonData->lead_id;
                            $msgArr['Message']['sent_to']=$jsonData->posted_to;
                            $msgArr['Message']['sent_by']=$jsonData->posted_by;
                            $msgArr['Message']['msg']=$msg;
                            $this->Message->create();
                            $this->Message->save($msgArr);
                            $data=array("Ack"=>1);
                            
                            //added for push part................................
                             $user_id=$jsonData->posted_to;
                             $userDetail=$this->User->find("first",array('conditions'=>array("User.id"=>$user_id)));
                             $device_id=$userDetail['User']['token'];
                             $device_type=strtolower($userDetail['User']['device_type']);
                             $body="New quote posted for your job";
                            if(!empty($device_id))
                            {

                              if($device_type=='android')
                                {

                                   $registatoin_ids=array($device_id);
                                   $totalmsgarray = array("message" => $body,"title"=>"Quote posted","vibrate"=>1,"sound"=>1,"quote_id"=>$quote_id);
                                    $this->android_push($registatoin_ids,$totalmsgarray);

                                }  
                                elseif($device_type=='ios')
                                {
                                   //ios_push($device_id, $message); 

                                        $totalmsgarray = array("message" => $body,"type"=>"Quote posted","quote_id"=>$quote_id);                                      
                                        $this->iphone_push_user($device_id, $body, 'default', json_encode($totalmsgarray));
                                }
                            }    
                    
                                                                     
                        }
                     }
                    else {
                        $data=array("Ack"=>0);
                    }
					
                    if($data['Ack']==1){
					   $this->updatePromoTime($jsonData->promo_code);
					}
                        
                   } 
                   
                   
                   
                   echo json_encode($data);exit;
        }
		
		public function updatePromoTime($promo_code){
			if($promo_code){
				$this->loadModel('Promo');
				$promoInfo=$this->Promo->find('first',array('conditions'=>array('promo_code'=>$promo_code)));
				$promoInfo['Promo']['used_times']=($promoInfo['Promo']['used_times'])-1;
				$this->Promo->save($promoInfo);
			} 
		}
        
        
        //quote submit for ios............................................................
          public function post_quote_ios() 
        {
              //echo 1;
              if ($_POST) {
                  //print_r($this->request->data);
                    // print_r($_FILES);
                   $quoteDetail=$this->RequestQuote->find("first",array('conditions'=>array("RequestQuote.lead_id"=>$this->request->data['lead_id'],"RequestQuote.posted_by"=>$this->request->data['posted_by'])));
                   
                   if(empty($quoteDetail))
                     {
                          if(!empty($_FILES['image']['name']))
                    {

                        $pathpart=pathinfo($_FILES['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "quoteimg/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($_FILES['image']['tmp_name'],$full_flg_path);
                        $this->request->data['RequestQuote']["image"]=$filename;
                        }
                        
                        
                    }
                    else{
                        $this->request->data['RequestQuote']["image"]=""; 
                    } 
                        $this->request->data['RequestQuote']['posttime']=gmdate('Y-m-d H:i:s');                        
                        $this->request->data['RequestQuote']['lead_id']=$this->request->data['lead_id'];
                        $this->request->data['RequestQuote']['posted_to']=$this->request->data['posted_to'];
                        $this->request->data['RequestQuote']['posted_by']=$this->request->data['posted_by'];
                        $this->request->data['RequestQuote']['comment']=$this->request->data['comment'];
                        $this->request->data['RequestQuote']['price']=$this->request->data['price'];
                        $this->request->data['RequestQuote']['price_type']=$this->request->data['price_type'];
                        //print_r($this->request->data);
                        //$user_id=$this->request->data['RequestQuote']['user_id'];
                        //$pro_id=$this->request->data['RequestQuote']['pro_id'];
                        $this->RequestQuote->create();
                        if($this->RequestQuote->save($this->request->data))
                        {
                           $quote_id=$this->RequestQuote->getLastInsertID();
                            
                            //for insert into notification table........................
                           $notiArr=array();
                            $this->loadModel('Notification');
                            $notiArr['Notification']['post_time']=gmdate('Y-m-d H:i:s');                        
                            $notiArr['Notification']['quote_id']=$quote_id;
                            $notiArr['Notification']['sent_to']=$this->request->data['posted_to'];
                            $notiArr['Notification']['sent_by']=$this->request->data['posted_by'];
                            $notiArr['Notification']['type']=3;
                            $this->Notification->create();
                            $this->Notification->save($notiArr);
                            
                            //for insert into message table...................................
                            $this->loadModel("Message");
                            $msg=$this->request->data['comment']." My price is ".$this->request->data['price'];
                            $msgArr=array();
                            $msgArr['Message']["dt_pub_date"]= gmdate("Y-m-d H:i:s");                   
                            $msgArr['Message']['quote_id']=$quote_id;
                            $msgArr['Message']['lead_id']=$this->request->data['lead_id'];
                            $msgArr['Message']['sent_to']=$this->request->data['posted_to'];
                            $msgArr['Message']['sent_by']=$this->request->data['posted_by'];
                            $msgArr['Message']['msg']=$msg;
                            $this->Message->create();
                            $this->Message->save($msgArr);
                            $data=array("Ack"=>1);
                            
                                     //added for push part................................
                       $user_id=$this->request->data['posted_to'];
                    $userDetail=$this->User->find("first",array('conditions'=>array("User.id"=>$user_id)));
                    $device_type=strtolower($userDetail['User']['device_type']);
                   $device_id=$userDetail['User']['token'];
                     $body="New quote posted for your job";
                        if(!empty($device_id))
                        {
                            if($device_type=='android')
                            {

                               $registatoin_ids=array($device_id);
                               $totalmsgarray = array("message" => $body,"title"=>"Quote posted","vibrate"=>1,"sound"=>1,"quote_id"=>$quote_id);
                                $this->android_push($registatoin_ids,$totalmsgarray);

                            }  
                            elseif($device_type=='ios')
                            {
                               //ios_push($device_id, $message); 

                                    $totalmsgarray = array("message" => $body,"type"=>"Quote posted","quote_id"=>$quote_id);                                      
                                    $this->iphone_push_user($device_id, $body, 'default', json_encode($totalmsgarray));
                            }
                           
//                          if($userDetail['User']['device_type']=='android')
//                            {
//                            
//                              //android_push($device_id);
//                              $msg = array
//                                        (
//                                              'body' 	=> $body,
//                                              'title'	=> 'Quote posted',
//                                              'icon'	=> 'myicon',/*Default Icon*/
//                                              'sound' => 'mySound'/*Default sound*/
//                                        );
//                                      $fields = array
//                                                      (
//                                                              'to'		=> $device_id,
//                                                              'notification'	=> $msg
//                                                      );
//
//
//                                      $headers = array
//                                                      (
//                                                              'Authorization: key=' . $apiKey,
//                                                              'Content-Type: application/json'
//                                                      );
//                              #Send Reponse To FireBase Server	
//                                              $ch = curl_init();
//                                              curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
//                                              curl_setopt( $ch,CURLOPT_POST, true );
//                                              curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
//                                              curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
//                                              curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
//                                              curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
//                                              $result = curl_exec($ch );
//                                              curl_close( $ch );
//                              #Echo Result Of FireBase Server
//                              //echo $result;
//
//                            }  
//                            elseif($userDetail['User']['device_type']=='ios')
//                            {
//                               //ios_push($device_id, $message); 
//                                  $totalmsgarray = array("message" => $body);
//
//                                iphone_push($device_id, $body, 'default', json_encode($totalmsgarray));
//                            }
                          }
                                                                     
                        }
                        else
                        {
                             $data=array("Ack"=>0);
                        }
                     }
                     else
                        {
                             $data=array("Ack"=>0,'msg'=>"You have already quoted this job");
                        }
                        
                   } 
                   
                   echo json_encode($data);exit;
        }
        
        
               public function addquote($lead_id=NULL,$user_id=NULL) 
               {
                $this->loadModel('Question');
                $this->loadModel("ProfPayment");
                $this->loadModel("ProfBid");
                $this->loadModel("Setting");
                $SITE_URL=Configure::read("SITE_URL");  
                $count=$this->ProfPayment->find("count",array("conditions"=>array(
                "ProfPayment.user_id"=>$user_id,"ProfPayment.lead_id"=>$lead_id    
                )));
                $leadDetail=$this->Lead->find("first",array('conditions'=>array("Lead.id"=>$lead_id)));
                $bid_count = $this->ProfBid->find("count", array('conditions'=>array('ProfBid.user_id'=>$user_id)));
                $bid_setting = $this->Setting->find("first");
                if(($count || $bid_count <= $bid_setting['Setting']['no_of_free_bids']) || ($leadDetail['Category']['price']==0))
                {
                    
                //print_r($leadDetail);die;
                $Arr=[];
               if(!empty($leadDetail)){
                $Arr['job']=array("id"=>$lead_id,"address"=>$leadDetail['Lead']['address'],"channelUrl"=>$leadDetail["Lead"]["chanel_id"],"name"=>$leadDetail['Category']['project_name'],"category"=>$leadDetail['Category']['name'],"description"=>$leadDetail['Lead']['description'],"posttime"=>$leadDetail['Lead']['posttime'],"username"=>$leadDetail['User']['name'],"userid"=>$leadDetail['User']['id'],
                        "userimage"=>!empty($leadDetail['User']['image'])?$SITE_URL.'user_images//'.$leadDetail['User']['image']:$SITE_URL.'nouser.png',"lat"=>$leadDetail['Lead']['lat'],"lang"=>$leadDetail['Lead']['lang']);
              
                    if(!empty($leadDetail['Userresponse']))
                    {
                        foreach($leadDetail['Userresponse'] as $key =>$response)
                        {
                           $this->Question->recursive=-1;
                          $questionArr=$this->Question->find("first",array('conditions'=>array("Question.id"=>$response['q_id'])));

                          $Arr['response'][$key] = array("id"=>$response['id'],"question"=>$questionArr['Question']['name'],"answer"=>$response['answer']);
                        }
                    }
                    
                    //for getting previous quote...................................
                     $quoteDetail=$this->RequestQuote->find("first",array('conditions'=>array("RequestQuote.lead_id"=>$lead_id,"RequestQuote.posted_by"=>$user_id)));
                     if(!empty($quoteDetail))
                     {
                       $Arr['quotePosted'] = array("id"=>$quoteDetail['RequestQuote']['id'],"comment"=>$quoteDetail['RequestQuote']['comment'],"price"=>$quoteDetail['RequestQuote']['price'],"price_type"=>$quoteDetail['RequestQuote']['price_type']);  
                       $Arr['is_quote_post']=1;
                       
                     }
                     else
                     {
                        $Arr['quotePosted'] = ""; 
                        $Arr['is_quote_post']=0;
                     }
                        $data=array('Ack'=>1,'Detail'=>$Arr);
                    }
                        else
                        {
                            $data=array('Ack'=>0,'Detail'=>"");
                        }
                    
                }
                else
                {
                    $data=array("Ack"=>0,'lead_id'=>$lead_id,'amount'=>$leadDetail["Category"]["price"]);
                }
                
                
                
                
                
                echo json_encode($data);exit;
               
            }
            
                public function addquoteapp($lead_id=NULL,$user_id=NULL) 
               {
                $this->loadModel('Question');
                $this->loadModel("ProfPayment");
                $this->loadModel("ProfBid");
                $this->loadModel("Setting");
                $SITE_URL=Configure::read("SITE_URL");  
                $count=$this->ProfPayment->find("count",array("conditions"=>array(
                "ProfPayment.user_id"=>$user_id,"ProfPayment.lead_id"=>$lead_id    
                )));
                $leadDetail=$this->Lead->find("first",array('conditions'=>array("Lead.id"=>$lead_id)));
               
                    
                //print_r($leadDetail);die;
                $Arr=[];
               if(!empty($leadDetail)){
                    $bid_count = $this->ProfBid->find("count", array('conditions'=>array('ProfBid.user_id'=>$user_id)));
                    $bid_setting = $this->Setting->find("first");
                    if($bid_count <= $bid_setting['Setting']['no_of_free_bids']){
                        $amount = '0';
                    }else{
                        $amount = $leadDetail["Category"]["price"];
                    }
                $Arr['job']=array("id"=>$lead_id,"address"=>$leadDetail['Lead']['address'],"channelUrl"=>$leadDetail["Lead"]["chanel_id"],"name"=>$leadDetail['Category']['project_name'],"category"=>$leadDetail['Category']['name'],"description"=>$leadDetail['Lead']['description'],"posttime"=>$leadDetail['Lead']['posttime'],"username"=>$leadDetail['User']['name'],"userid"=>$leadDetail['User']['id'],
                        "userimage"=>!empty($leadDetail['User']['image'])?$SITE_URL.'user_images//'.$leadDetail['User']['image']:$SITE_URL.'nouser.png',"lat"=>$leadDetail['Lead']['lat'],"lang"=>$leadDetail['Lead']['lang'],'amount'=>$amount);
              
                    if(!empty($leadDetail['Userresponse']))
                    {
                        foreach($leadDetail['Userresponse'] as $key =>$response)
                        {
                           $this->Question->recursive=-1;
                          $questionArr=$this->Question->find("first",array('conditions'=>array("Question.id"=>$response['q_id'])));

                          $Arr['response'][$key] = array("id"=>$response['id'],"question"=>$questionArr['Question']['name'],"answer"=>$response['answer']);
                        }
                    }else{
                        $Arr['response']=array();
                    }
                    
                    //for getting previous quote...................................
                     $quoteDetail=$this->RequestQuote->find("first",array('conditions'=>array("RequestQuote.lead_id"=>$lead_id,"RequestQuote.posted_by"=>$user_id)));
                     if(!empty($quoteDetail))
                     {
                       $Arr['quotePosted'] = array("id"=>$quoteDetail['RequestQuote']['id'],"comment"=>$quoteDetail['RequestQuote']['comment'],"price"=>$quoteDetail['RequestQuote']['price'],"price_type"=>$quoteDetail['RequestQuote']['price_type']);  
                       $Arr['is_quote_post']=1;
                       
                     }
                     else
                     {
                        $Arr['quotePosted'] = ""; 
                        $Arr['is_quote_post']=0;
                     }
                        $data=array('Ack'=>1,'Detail'=>$Arr);
                    }
                        else
                        {
                            $data=array('Ack'=>0,'Detail'=>"");
                        }
                    
                        echo json_encode($data);exit;
               
            } 
            
            
            
            
            
            public function quoteslist($project_id=NULL) 
        {
              $this->loadModel("Rating");
              $SITE_URL=Configure::read("SITE_URL");   
              $options = array('conditions' => array('RequestQuote.lead_id' => $project_id),'order' => array('RequestQuote.id' =>'DESC'));   
              $quotes=$this->RequestQuote->find("all",$options);
                //print_r($lead);die;
                
                $Arr=[];
                
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
                {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } 
                elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
                {
                  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } 
                else 
                {
                  $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
                if($data->status == 'success')
                {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                }
                else
                {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
                $tz_from = "UTC";
                $tz_to = $timezone;
                //$format = 'Y-m-d h:i a';
                $format = 'M d,Y';
                 $format_time = 'H:i';
                if(!empty($quotes))
                {
                    foreach($quotes as $key =>$quote)
                    {
                   
                    
                     //to check if it is hired or not.........
                     $optionsHire = array('conditions' => array('Lead.id' => $quote['RequestQuote']['lead_id'],'Lead.assigned_to' => $quote['RequestQuote']['posted_by']));                
                
                    $hiredArr=$this->Lead->find("all",$optionsHire);
                    if(count($hiredArr)>0)
                    {
                        $is_hired=1;
                    }
                    else {
                        $is_hired=0;
                    }
                    $no_of_review=$this->Rating->find("count",array("conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_by"])));
                    $net_ratings=$this->Rating->find("first",array("fields"=>array("SUM(Rating.avg_score) as score"),"conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_by"])));
                    if($no_of_review>0)
                    {
                        $avg_rateing= round($net_ratings[0]["score"]/$no_of_review);
                    }
                    else
                    {
                        $avg_rateing=0;

                    }
                    $dt = new DateTime($quote["RequestQuote"]["posttime"], new DateTimeZone($tz_from));
                    $dt->setTimeZone(new DateTimeZone($tz_to));
                    $posttime=$dt->format($format) ; 
                    $time=$dt->format($format_time) ; 
                    $hired=$this->Lead->find("count",array("conditions"=>array("Lead.assigned_to"=>$quote["RequestQuote"]["posted_by"])));

                    
                    
                    //for hire ends here...............................
                     $Arr[$key]=array("id"=>$quote['RequestQuote']['id'],"comment"=>$quote['RequestQuote']['comment'],"price"=>$quote['RequestQuote']['price'],"posttime"=>$posttime,"time"=>$time,"userid"=>$quote['User']['id'],"username"=>$quote['User']['name'],
                        "userimage"=>!empty($quote['User']['image'])?$SITE_URL.'user_images//'.$quote['User']['image']:$SITE_URL.'nouser.png',"is_hired"=>$is_hired,"lead_id"=>$quote['RequestQuote']['lead_id'],
                         "total_review"=>$no_of_review,"rateing"=>$quote['User']['avg_score'],"last_login"=>$this->how_log_ago($quote["User"]["logout_time"]),"hired"=>$hired);
                    }
                    $data=array('Ack'=>1,'List'=>$Arr);
                }
                else
                {
                    $data=array('Ack'=>0,'List'=>"");
                }
                echo json_encode($data);exit;
            
        }
        
           public function quotessendlist($user_id=NULL) 
        {
               $this->loadModel('Category');
               $this->loadModel('Rating');
             $SITE_URL=Configure::read("SITE_URL");  
              if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
                {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } 
                elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
                {
                  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } 
                else 
                {
                  $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
                if($data->status == 'success')
                {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                }
                else
                {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
                $tz_from = "UTC";
                $tz_to = $timezone;
                $format = 'M d,Y';
                $format_time = 'H:i';
               $options = array('conditions' => array('RequestQuote.posted_by' => $user_id),'order' => array('RequestQuote.id' =>'DESC')); 
               
                
                $quotes=$this->RequestQuote->find("all",$options);
                //print_r($lead);die;
                
                $Arr=[];
                
                
                if(!empty($quotes))
                {
                    foreach($quotes as $key =>$quote)
                    {
                   
                    
                     //to check if it is hired or not.........
                     $optionsHire = array('conditions' => array('Lead.id' => $quote['RequestQuote']['lead_id'],'Lead.assigned_to' => $quote['RequestQuote']['posted_by']));                
                
                    $hiredArr=$this->Lead->find("all",$optionsHire);
                    if(count($hiredArr)>0)
                    {
                        $is_hired=1;
                    }
                    else {
                        $is_hired=0;
                    }
                    
                    //for hire ends here...............................
                    
                    //for category ....................................
                    $optionsCat = array('conditions' => array('Category.id' => $quote['Lead']['category_id']));  
                    
                    $dt = new DateTime($quote["RequestQuote"]["posttime"], new DateTimeZone($tz_from));
                    $dt->setTimeZone(new DateTimeZone($tz_to));
                    $posttime=$dt->format($format) ; 
                    $time=$dt->format($format_time) ; 
                    $category=$this->Category->find("first",$optionsCat);
                    $cat_name=!empty($category["Category"]["name"])?$category["Category"]["name"]:'';
                    $no_review=$this->Rating->find('count',array("conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_to"])));
                    //print_r($quote['Projectowner']);die;
                     $Arr[$key]=array("id"=>$quote['RequestQuote']['id'],"comment"=>$quote['RequestQuote']['comment'],"price"=>$quote['RequestQuote']['price'],"posttime"=>$posttime,"time"=>$time,"userid"=>$quote['Projectowner']['id'],"username"=>$quote['Projectowner']['name'],
                        "userimage"=>!empty($quote['Projectowner']['image'])?$SITE_URL.'user_images//'.$quote['Projectowner']['image']:$SITE_URL.'nouser.png',"rating"=>$quote['Projectowner']['avg_score'],"is_hired"=>$is_hired,"lead_id"=>$quote['RequestQuote']['lead_id'],"address"=>$quote['Lead']['address'],"category"=>$cat_name,
                         'no_review'=>$no_review,"chanel_id"=>$quote['Lead']['chanel_id']);
                    }
                    
                    $data=array('Ack'=>1,'List'=>$Arr);
                }
                else
                {
                    $data=array('Ack'=>0,'List'=>"");
                }
                echo json_encode($data);exit;
            
        }
        
        public function hiredlist_serv($user_id=null) 
        {
            $this->loadModel("Lead");
            $this->loadModel("Rating");
             $this->Lead->unBindModel(array(
            'belongsTo'=>array('User'),
            'hasMany'=>array('Providers','Userresponse'),   
                ));
           $SITE_URL=Configure::read("SITE_URL");  
           if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
                {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } 
                elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
                {
                  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } 
                else 
                {
                  $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
                if($data->status == 'success')
                {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                }
                else
                {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
                $tz_from = "UTC";
                $tz_to = $timezone;
                $format = 'M d,Y';
                $format_time = 'H:i';
             
             
            $leads=$this->Lead->find('all',array('conditions'=>array('Lead.user_id'=>$user_id,'Lead.assigned_to >'=>0)));
            if(!empty($leads))
            {
                foreach ($leads as $lead)
                {
                
                    $this->RequestQuote->recursive=-1;
                    $quote=$this->RequestQuote->find("first",array("conditions"=>array("RequestQuote.lead_id"=>$lead["Lead"]["id"],"RequestQuote.posted_by"=>$lead["Lead"]["assigned_to"])));
                    $dt = new DateTime($quote["RequestQuote"]["posttime"], new DateTimeZone($tz_from));
                    $dt->setTimeZone(new DateTimeZone($tz_to));
                    $posttime=$dt->format($format) ; 
                    $time=$dt->format($format_time) ; 
                    $no_review=$this->Rating->find('count',array("conditions"=>array("Rating.to_id"=>$quote["RequestQuote"]["posted_by"])));
                    $response[]=array(
                    "id"=> $quote["RequestQuote"]["id"],"comment"=>$quote["RequestQuote"]["comment"],"price"=>$quote["RequestQuote"]["price"],
                    "user_id"=> $lead["AssignedPro"]["id"],"userimage"=>!empty($lead["AssignedPro"]["image"])?$SITE_URL.'user_images/'.$lead["AssignedPro"]["image"]:$SITE_URL.'noimage.png',
                    "username"=> $lead["AssignedPro"]["name"],"rating"=>$lead["AssignedPro"]["avg_score"],"lead_id"=>$lead["Lead"]["id"],
                    "address"=>$lead["Lead"]["address"],"category"=>$lead["Category"]["name"],"posttime"=>$posttime,"time"=>$time,"no_review"=>$no_review   

                    ); 
                
                }
                
                $data=array('Ack'=>1,"response"=>$response);
            }
            else
            {
                $data=array('Ack'=>0);
            }
            
            echo json_encode($data);exit;
            
            
            
        }
        
        public function myhiredlist_serv($user_id=null) 
        {
            $this->loadModel("Lead");
            $this->loadModel("Rating");
             $this->Lead->unBindModel(array(
            'belongsTo'=>array('AssignedPro'),
            'hasMany'=>array('Providers','Userresponse'),   
                ));
           $SITE_URL=Configure::read("SITE_URL");  
           if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
                {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } 
                elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
                {
                  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } 
                else 
                {
                  $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));            
                if($data->status == 'success')
                {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                }
                else
                {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
                $tz_from = "UTC";
                $tz_to = $timezone;
                $format = 'M d,Y';
                $format_time = 'H:i';
             
             
            $leads=$this->Lead->find('all',array('conditions'=>array('Lead.assigned_to'=>$user_id)));
            if(!empty($leads))
            {
                foreach ($leads as $lead)
                {
                
                    $this->RequestQuote->recursive=-1;
                    $quote=$this->RequestQuote->find("first",array("conditions"=>array("RequestQuote.lead_id"=>$lead["Lead"]["id"],"RequestQuote.posted_by"=>$lead["Lead"]["assigned_to"])));
                    $dt = new DateTime($quote["RequestQuote"]["posttime"], new DateTimeZone($tz_from));
                    $dt->setTimeZone(new DateTimeZone($tz_to));
                    $posttime=$dt->format($format) ; 
                    $time=$dt->format($format_time) ; 
                    $no_review=$this->Rating->find('count',array("conditions"=>array("Rating.to_id"=>$lead["User"]["id"])));
                    $response[]=array(
                    "id"=> $quote["RequestQuote"]["id"],"comment"=>$quote["RequestQuote"]["comment"],"price"=>$quote["RequestQuote"]["price"],
                    "user_id"=> $lead["User"]["id"],"userimage"=>!empty($lead["User"]["image"])?$SITE_URL.'user_images/'.$lead["User"]["image"]:$SITE_URL.'noimage.png',
                    "username"=> $lead["User"]["name"],"rating"=>$lead["User"]["avg_score"],"lead_id"=>$lead["Lead"]["id"],
                    "address"=>$lead["Lead"]["address"],"category"=>$lead["Category"]["name"],"posttime"=>$posttime,"time"=>$time,"no_review"=>$no_review   

                    ); 
                
                }
                
                $data=array('Ack'=>1,"response"=>$response);
            }
            else
            {
                $data=array('Ack'=>0);
            }
            
            echo json_encode($data);exit;
            
            
            
        }
        
         public function test_push() {
             $this->autoRender = false;
             $device_id = 'dPwfud0B6So:APA91bEB5AMz2gZ-of6JoIjPTdqS8IrB1XqkIwsBYDb3fyxNn6hWy_6W5AizT62A1nzgXbbxKJZ0fugkVEz8tb47Lu6Q_-g5rJm8pTQIli53wPnmZf9bJIzw1QpHVzVoCJvwHwArQlEJ';
//              $type = 'order_status_change';
//            $registration_ids = array();
//            $registration_ids[] = $device_id;
//            $totalmsgarray = array("message" => 'hii',"type"=>$type,"to_id" => 1,"from_id" => 1);
//
//            $push_message = array("message" => 'hii',"encoded"=>json_encode($totalmsgarray));
             $registatoin_ids=array($device_id);
        $totalmsgarray = array("message" => "hi hii","title"=>"hello","vibrate"=>1,"sound"=>1,"quote_id"=>1);
             //$totalmsgarray = array("notification"=>array("title"=>"hello","body"=>"ff"));
         $this->android_push($registatoin_ids,$totalmsgarray);
//        $device_id = '8990F718E17656E3B1D8BCFA86D66EBF13C6D1FCFAD87F5C67C71D3019F08FB0';
//        $totalmsgarray = array("message" => 'hello',"type"=>"Quote posted","quote_id"=>1);                                      
//        $this->iphone_push($device_id, 'hello', 'default', json_encode($totalmsgarray));
         exit;
         }
        
         
        



}