<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class MessagesController extends AppController

	{
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array(

		'Paginator'
	);
	/**
	 * index method
	 *
	 * @return void
	 */
	function sent_message()
		{
		$this->loadModel("RequestQuote");
		$this->loadModel("Notification");
		$this->RequestQuote->recursive = - 1;
		if ($this->request->is(array(
			'post',
			'put'
		)))
			{
			$jsonData = $this->request->input('json_decode');
			foreach($jsonData as $key => $json)
				{
				$this->request->data["Message"][$key] = $json;
				}

			$quote = $this->RequestQuote->find("first", array(
				"conditions" => array(
					"RequestQuote.id" => $jsonData->quote_id
				)
			));
			if ($jsonData->sent_by == $quote["RequestQuote"]["posted_by"])
				{
				$this->request->data["Message"]["sent_to"] = $quote["RequestQuote"]["posted_to"];
				$send_to = $quote["RequestQuote"]["posted_to"];
				}
			  else
				{
				$this->request->data["Message"]["sent_to"] = $quote["RequestQuote"]["posted_by"];
				$send_to = $quote["RequestQuote"]["posted_by"];
				}

			$this->request->data["Message"]["lead_id"] = $quote["RequestQuote"]["lead_id"];
			$this->request->data["Message"]["dt_pub_date"] = gmdate("Y-m-d H:i:s");
			try
				{

				// $this->Message->save($this->request->data);
				// $message_id=$this->Message->getLastInsertID();

				$this->request->data["Notification"]["sent_by"] = $jsonData->sent_by;
				$this->request->data["Notification"]["sent_to"] = $send_to;
				$this->request->data["Notification"]["message_id"] = 0;
				$this->request->data["Notification"]["post_time"] = gmdate("Y-m-d H:i:s");
				$this->request->data["Notification"]["type"] = 1;
				$this->Notification->save($this->request->data);
				$data = array(
					"Ack" => 1
				);
				}

			catch(Exception $ex)
				{
				pr($ex);
				$data = array(
					"Ack" => 0
				);
				}

			echo json_encode($data);
			exit;
			}
		}

	function conversations($sent_by = null, $quote_id = null, $logged_user = null)
		{
		$this->loadModel("RequestQuote");
		$this->loadModel("Lead");
		$this->loadModel("Notification");
		$conditions["Message.quote_id"] = $quote_id;
		$conditions["OR"][] = array(
			"Message.sent_by" => $sent_by
		);
		$conditions["OR"][] = array(
			"Message.sent_to" => $sent_by
		);
		$results = $this->Message->find("all", array(
			"conditions" => $conditions,
			"order" => array(
				"Message.id asc"
			)
		));
		$chats = array();
		$SITE_URL = Configure::read("SITE_URL");
		$this->loadModel("User");
		$this->User->recursive = - 1;
		$chats["conversation"] = array();
		foreach($results as $result)
			{
			$sent_by = $this->User->find("first", array(
				'fields' => array(
					"User.id",
					"User.name",
					"User.image"
				) ,
				"conditions" => array(
					"User.id" => $result["Message"]["sent_by"]
				)
			));
			$sent_by["User"]["image"] = !empty($sent_by["User"]["image"]) ? $SITE_URL . 'user_images/' . $sent_by["User"]["image"] : $SITE_URL . 'nouser.png';
			$result["Message"]["sent_by"] = $sent_by["User"];
			$sent_to = $this->User->find("first", array(
				'fields' => array(
					"User.id",
					"User.name",
					"User.image"
				) ,
				"conditions" => array(
					"User.id" => $result["Message"]["sent_to"]
				)
			));
			$sent_to["User"]["image"] = !empty($sent_to["User"]["image"]) ? $SITE_URL . 'user_images/' . $sent_to["User"]["image"] : $SITE_URL . 'nouser.png';
			if ($result["Message"]["sent_to"] == $logged_user)
				{
				$this->Message->id = $result["Message"]["id"];
				$this->Message->saveField('is_read', 1);
				$this->Notification->query("update notifications set is_read=1 where message_id='" . $result["Message"]["id"] . "'");
				}

			$result["Message"]["sent_to"] = $sent_to["User"];
			$result["Message"]["howlongago"] = $this->how_log_ago($result["Message"]["dt_pub_date"]);
			$chats['conversation'][] = $result["Message"];
			}

		// chat with user details...................................

		$QuoteArr = $this->RequestQuote->find("first", array(
			'fields' => array(
				"RequestQuote.posted_by",
				"RequestQuote.lead_id"
			) ,
			"conditions" => array(
				"RequestQuote.id" => $quote_id
			)
		));
		$UserArr = $this->User->find("first", array(
			"conditions" => array(
				"User.id" => $QuoteArr["RequestQuote"]["posted_by"]
			)
		));
		$this->loadModel("Lead");
		$this->loadModel("Rating");
		$avg_score = round($UserArr['User']['avg_score']);
		$hired = $this->Lead->find("count", array(
			"conditions" => array(
				"Lead.assigned_to" => $UserArr["User"]["id"]
			)
		));
		$chats['Userdetail'] = array(
			"id" => $UserArr["User"]["id"],
			"address" => $UserArr["User"]["address"],
			"name" => $UserArr["User"]["name"],
			"email" => $UserArr["User"]["email"],
			"mobile_no" => $UserArr["User"]["mobile_no"],
			"userimage" => !empty($UserArr['User']['image']) ? $SITE_URL . 'user_images/' . $UserArr['User']['image'] : $SITE_URL . 'nouser.png',
			"rateing" => $avg_score,
			"hired_no" => $hired
		);

		// to check if it is hired or not.........

		$optionsHire = array(
			'conditions' => array(
				'Lead.id' => $QuoteArr['RequestQuote']['lead_id'],
				'Lead.assigned_to' => $QuoteArr['RequestQuote']['posted_by']
			)
		);
		$hiredArr = $this->Lead->find("all", $optionsHire);
		if (count($hiredArr) > 0)
			{
			$is_hired = 1;
			}
		  else
			{
			$is_hired = 0;
			}

		$chats['is_hired'] = $is_hired;
		$chats['lead_id'] = $QuoteArr['RequestQuote']['lead_id'];
		if (!empty($chats))
			{
			$data = array(
				"Ack" => 1,
				'chats' => $chats
			);
			}
		  else
			{
			$data = array(
				"Ack" => 0,
				'chats' => $chats
			);
			}

		echo json_encode($data);
		exit;
		}

	function count_notification($user_id = null)
		{
		$this->loadModel("Notification");
		$count = $this->Notification->find('count', array(
			'conditions' => array(
				"Notification.sent_to" => $user_id,
				"Notification.is_read" => 0
			)
		));
		$data = array(
			'Ack' => 1,
			'notification' => $count
		);
		echo json_encode($data);
		exit;
		}

	function notification($user_id = null)
		{
		$SITE_URL = Configure::read("SITE_URL");
		$this->loadModel("Category");
		$this->loadModel("Notification");
                $this->loadModel("RequestQuote");
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
			{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		  else
			{
			$ip = $_SERVER['REMOTE_ADDR'];
			}

		$data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
		if ($data->status == 'success')
			{
			date_default_timezone_set($data->timezone);
			$timezone = $data->timezone;
			}
		  else
			{
			date_default_timezone_set('Asia/Kolkata');
			$timezone = 'Asia/Kolkata';
			}

		$tz_from = "UTC";
		$tz_to = $timezone;
		$format = 'Y-m-d H:i:s';
		$notification_count = $this->Notification->find('count', array(
			'conditions' => array(
				"Notification.sent_to" => $user_id,
				"Notification.is_read" => 0
			)
		));
		$overallnotification = $this->Notification->find('count', array(
			'conditions' => array(
				"Notification.sent_to" => $user_id
			)
		));
		$Arr = [];
		if ($overallnotification > 0)
			{
			$notification = $this->Notification->find('all', array(
				'conditions' => array(
					"Notification.sent_to" => $user_id
				) ,
				"order" => array(
					"Notification.id desc"
				)
			));
			foreach($notification as $key => $notiDetail)
				{
				if ($notiDetail['Notification']['type'] == 1)
					{
					$msg = "sent you a Message";
					}
				elseif ($notiDetail['Notification']['type'] == 2)
					{
					$msg = "posted a Job";
					}
				elseif ($notiDetail['Notification']['type'] == 3)
					{
					$msg = "posted a job Request";
                                        
                                        
                                        
					}
				elseif ($notiDetail['Notification']['type'] == 4)
					{
					$msg = "hired  you for a job";
					}

				$dt = new DateTime($notiDetail["Notification"]["post_time"], new DateTimeZone($tz_from));
				$dt->setTimeZone(new DateTimeZone($tz_to));
				$start_time = $dt->format($format);
				$time = strtotime($start_time);
				$timeDiff = strtotime(gmdate("Y-m-d")) - $time;
				$unit = '86400';
				$numberOfUnits = floor($timeDiff / $unit);
				if ($numberOfUnits > 30)
					{
					$notification_time = date('M d h:i a', $time);
					}
				  else
				if ($numberOfUnits == 1)
					{
					$notification_time = $numberOfUnits . " Day ago";
					}
				  else
				if ($numberOfUnits == 0 || $numberOfUnits < 1)
					{
					$notification_time = date('h:i a', $time);
					}
				  else
					{
					$notification_time = $numberOfUnits . " Days ago";
					}

				// $cat=$this->Category->find('first',array('conditions'=>array("Category.id"=>$notiDetail['Lead']['category_id'])));
				// print_r($cat);

				$Arr[$key] = array(
					"id" => $notiDetail['Notification']['id'],
					"msg" => $msg,
					"username" => $notiDetail['User']['name'],
					"type" => (int)$notiDetail['Notification']['type'],
					"posttime" => $notification_time,
                                        "quote_id"=>!empty($notiDetail['Notification']['quote_id'])?$notiDetail['Notification']['quote_id']:"",
                                        "lead_id"=>!empty($notiDetail["Notification"]["lead_id"])?$notiDetail["Notification"]["lead_id"]:"",
					"userimage" => !empty($notiDetail['User']['image']) ? $SITE_URL . 'user_images/' . $notiDetail['User']['image'] : $SITE_URL . 'nouser.png'
				);
				}
			}

		$data = array(
			'Ack' => 1,
			'notification' => $notification_count,
			'notificationArr' => $Arr
		);
		echo json_encode($data);
		exit;
		}

	function unreadnotification($user_id = null)
	{
            $this->LoadModel("Notification");
            $this->LoadModel("User");
            $total_unread = $this->Notification->find("count", array(
                    "conditions" => array(
                            "Notification.sent_to" => $user_id,
                            "Notification.is_read" => 0
                    )
            ));
            $userData= array();
            $userData['User']['id']          = $user_id;                
            $userData['User']['logout_time'] = gmdate("Y-m-d H:i:s");
            $this->User->save($userData);


            echo json_encode(array(
                    "Ack" => 1,
                    "total_unread" => $total_unread
            ));
            exit;
	}

	function change_notifystatus($user_id = null)
		{
		$this->loadModel("Notification");
		$this->Notification->updateAll(array(
			'Notification.is_read' => 1
		) , array(
			'Notification.sent_to' => $user_id
		));
		$total_unread = $this->Notification->find("count", array(
			"conditions" => array(
				"Notification.is_read" => 0,
				"Notification.sent_to" => $user_id
			)
		));
		echo json_encode(array(
			"Ack" => 1,
			"total_unread" => $total_unread
		));
		exit;
		}

	function notificationdetail($user_id = null)
		{
		$SITE_URL = Configure::read("SITE_URL");
		$this->loadModel("Category");
		$this->loadModel("Lead");
		$this->loadModel("RequestQuote");
		$this->loadModel("Message");
		$this->loadModel("Notification");
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
			{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		  else
			{
			$ip = $_SERVER['REMOTE_ADDR'];
			}

		$data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
		if ($data->status == 'success')
			{
			date_default_timezone_set($data->timezone);
			$timezone = $data->timezone;
			}
		  else
			{
			date_default_timezone_set('Asia/Kolkata');
			$timezone = 'Asia/Kolkata';
			}

		$tz_from = "UTC";
		$tz_to = $timezone;
		$format = 'Y-m-d H:i:s';
		$notification_count = $this->Notification->find('count', array(
			'conditions' => array(
				"Notification.sent_to" => $user_id
			)
		));
		$Arr = [];
		if ($notification_count > 0)
			{
			$notification = $this->Notification->find('all', array(
				'conditions' => array(
					"Notification.sent_to" => $user_id
				)
			));
			foreach($notification as $key => $notiDetail)
				{
				$msg = '';
				$msgContent = '';
				if ($notiDetail['Notification']['type'] == 1)
					{
					$contentMsg = $this->Message->find('first', array(
						'conditions' => array(
							"Message.id" => $notiDetail['Notification']['message_id']
						)
					));
					$msg = "sent you a Message";
					$msgContent = $contentMsg['Message']['msg'];
					}
				elseif ($notiDetail['Notification']['type'] == 2)
					{
					$msg = "posted a Job";
					$contentMsg = $this->Lead->find('first', array(
						'conditions' => array(
							"Lead.id" => $notiDetail['Notification']['lead_id']
						)
					));
					$msgContent = "Job location - " . $contentMsg['Lead']['address'] . " " . $contentMsg['Lead']['description'];
					}
				elseif ($notiDetail['Notification']['type'] == 3)
					{
					$msg = "posted a job Request";
					$contentMsg = $this->RequestQuote->find('first', array(
						'conditions' => array(
							"RequestQuote.id" => $notiDetail['Notification']['quote_id']
						)
					));
					$msgContent = $contentMsg['RequestQuote']['comment'];
					}

                                        $dt = new DateTime($notiDetail["Notification"]["post_time"], new DateTimeZone($tz_from));
                                        $dt->setTimeZone(new DateTimeZone($tz_to));
                                        $start_time = $dt->format($format);
                                        $time = strtotime($start_time);
                                        $timeDiff = strtotime(gmdate("Y-m-d")) - $time;
                                        $unit = '86400';
                                        $numberOfUnits = floor($timeDiff / $unit);
                                        if ($numberOfUnits > 30)
                                                {
                                                $notification_time = date('M d h:i A', $time);
                                                }
                                          else
                                        if ($numberOfUnits == 1)
                                                {
                                                $notification_time = $numberOfUnits . " Day ago";
                                                }
                                          else
                                        if ($numberOfUnits == 0 || $numberOfUnits < 1)
                                                {
                                                $notification_time = date('h:i A', $time);
                                                }
                                          else
                                                {
                                                $notification_time = $numberOfUnits . " Days ago";
                                                }

				// $cat=$this->Category->find('first',array('conditions'=>array("Category.id"=>$notiDetail['Lead']['category_id'])));
				// print_r($cat);

				$Arr[$key] = array(
					"id" => $notiDetail['Notification']['id'],
					"msg" => $msg,
					"username" => $notiDetail['User']['name'],
					"posttime" => $notification_time,
					"msgContent" => $msgContent,
					"userimage" => !empty($notiDetail['User']['image']) ? $SITE_URL . 'user_images/' . $notiDetail['User']['image'] : $SITE_URL . 'nouser.png'
				);
				}

			// for update read status to 1............................................

			$this->Notification->updateAll(array(
				'Notification.is_read' => 1
			) , array(
				'Notification.sent_to' => $user_id
			));
			$data = array(
				'Ack' => 1,
				'notification' => $notification_count,
				'notificationArr' => $Arr
			);
			}
		  else
			{
			$data = array(
				'Ack' => 0,
				'notification' => 0,
				'notificationArr' => ''
			);
			}

		echo json_encode($data);
		exit;
		}

	function quoteDetail($id = null, $user_id = null)
		{
		$SITE_URL = Configure::read("SITE_URL");
		$this->loadModel("RequestQuote");
		$quoteDetail = $this->RequestQuote->find("first", array(
			'conditions' => array(
				"RequestQuote.id" => $id
			)
		));
		if ($quoteDetail["RequestQuote"]["posted_by"] == $user_id)
			{
			$chatuser = $quoteDetail["RequestQuote"]["posted_to"];
			}
		  else
			{
			$chatuser = $quoteDetail["RequestQuote"]["posted_by"];
			}

		$this->loadModel("User");
		$this->loadModel("Lead");
		$UserArr = $this->User->find("first", array(
			"conditions" => array(
				"User.id" => $quoteDetail["RequestQuote"]["posted_by"]
			)
		));
		$this->loadModel("Rating");
		$avg_score = round($UserArr['User']['avg_score']);
		$hired = $this->Lead->find("count", array(
			"conditions" => array(
				"Lead.assigned_to" => $UserArr["User"]["id"]
			)
		));
		$user_detail = array(
			"id" => $UserArr["User"]["id"],
			"address" => $UserArr["User"]["address"],
			"name" => $UserArr["User"]["name"],
			"email" => $UserArr["User"]["email"],
			"mobile_no" => $UserArr["User"]["mobile_no"],
			"userimage" => !empty($UserArr['User']['image']) ? $SITE_URL . 'user_images/' . $UserArr['User']['image'] : $SITE_URL . 'nouser.png',
			"rateing" => $avg_score,
			"hired_no" => $hired
		);
		$arr = array(
			"chatuser" => $chatuser,
			"channelUrl" => $quoteDetail["Lead"]["chanel_id"],
			"is_hired" => $quoteDetail["Lead"]["assigned_to"] == $quoteDetail["RequestQuote"]["posted_by"] ? 1 : 0,
			"Userdetail" => $user_detail,
			"unique_id" => uniqid()
		);
		$data = array(
			"Ack" => 1,
			"response" => $arr
		);
		echo json_encode($data);
		exit;
		}
	}
