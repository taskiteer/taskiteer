<?php

App::uses('AppController', 'Controller');

/**
 * Faqs Controller
 *
 * @property Faq $Faq
 * @property PaginatorComponent $Paginator
 */
class PlantsController extends AppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Plant.id' => 'desc'
        )
    );
    
    public function admin_index() {
    
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('Plant');
        $this->Plant->recursive = 0;
        $this->Paginator->settings = $this->paginate;
        $Plants = $this->Paginator->paginate();
        //$Plants=$this->Plant->find('all');
        //print_r($Plants);die;
        $this->set('Plants',$Plants);

    }
    
    public function admin_add() {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if ($this->request->is('post')) {
            // echo '<pre>';
            // print_r($this->request->data);
            // exit;
            // //$this->request->data['Plant']['status']=0; 
            // if (isset($this->request->data['Plant']['image']) && $this->request->data['Plant']['image']['name'] != '') {
            //     $path = $this->request->data['Plant']['image']['name'];
            //     $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
            //     if ($ext) {
            //         $uploadPath = Configure::read('UPLOAD_Plant_PATH');
            //         $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
            //         if (in_array($ext, $extensionValid)) {
            //             $imageName = rand() . '_' . (strtolower(trim($this->request->data['Plant']['image']['name'])));
            //             $full_image_path = $uploadPath . '/' . $imageName;
            //             //move_uploaded_file($this->request->data['Plant']['image']['tmp_name'], $full_image_path);
            //             $this->request->data['Plant']['image'] = $imageName;
            //         } else {
            //             $this->Session->setFlash(__('Invalid Image Type.'));
            //             return $this->redirect(array('action' => 'edit', $id));
            //         }
            //     }
            // } else {
            //     $this->request->data['Plant']['image'] = $this->request->data['Plant']['image'];
            // }
            $this->Plant->create();
            //print_r($this->request->data);
            //echo'<br>';
            //echo $this->request->data['Plant'] ['start']; 
            $c=strcmp($this->request->data['Plant']['end'],$this->request->data['Plant'] ['start']);
            // echo $c;
            // exit;
            if($c>0)
           {
            if ($this->Plant->save($this->request->data)) {
                $this->Session->setFlash('The Plant has been saved.','default', array('class' => 'success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                return $this->redirect(array('action' => 'add'));
                $this->Session->setFlash(__('The Plant could not be saved. Please, try again.'));
            }
        }
        else
        {
           $this->Session->setFlash(__('May be you give incorrect data  could not be saved. Please, try again.'));  
        }
        }
    }
    
    public function admin_edit($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        //$this->loadModel('FaqCategory');
        if (!$this->Plant->exists($id)) {
            throw new NotFoundException(__('Invalid Faq'));
        }
        if ($this->request->is(array('post', 'put'))) {
            // if (isset($this->request->data['Plant']['image']) && $this->request->data['Plant']['image']['name'] != '') {
            //     $path = $this->request->data['Plant']['image']['name'];
            //     $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
            //     if ($ext) {
            //         $uploadPath = Configure::read('UPLOAD_Plant_PATH');
            //         $extensionValid = array('jpg', 'jpeg', 'png', 'gif');
            //         if (in_array($ext, $extensionValid)) {
            //             $OldImg = $this->request->data['Plant']['saved_image'];
            //             $imageName = rand() . '_' . (strtolower(trim($this->request->data['Plant']['image']['name'])));
            //             $full_image_path = $uploadPath . '/' . $imageName;
            //             move_uploaded_file($this->request->data['Plant']['image']['tmp_name'], $full_image_path);
            //             $this->request->data['Plant']['image'] = $imageName;
            //             if ($OldImg != '') {
            //                 unlink($uploadPath . '/' . $OldImg);
            //             }
            //         } else {
            //             $this->Session->setFlash(__('Invalid Image Type.'));
            //             return $this->redirect(array('action' => 'edit', $id));
            //         }
            //     }
            // } else {
            //     unset($this->request->data['Plant']['image']);
            // }
            // print_r($this->request->data);
            // exit;
            if ($this->Plant->save($this->request->data)) {
                $this->Session->setFlash('The Plant has been saved.','default', array('class' => 'success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Plant could not be saved. Please, try again.'));
            }
        } else {

            $options = array('conditions' => array('Plant.' . $this->Plant->primaryKey => $id));
            $this->request->data = $this->Plant->find('first', $options);
        }
        //$this->set(compact('Plants'));
        
    }
    
    public function admin_delete($id = null) {
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Plant->id = $id;
        if (!$this->Plant->exists()) {
            throw new NotFoundException(__('Invalid Faq'));
        }

        $this->request->onlyAllow('post', 'delete');
        $Plant_row = $this->Plant->find('first', array('conditions' => array('Plant.id' => $id)));
        $uploadPath = Configure::read('UPLOAD_Plant_PATH');
        $OldImg = $Plant_row['Plant']['image'];
        unlink($uploadPath . '/' . $OldImg);
        if ($this->Plant->delete()) {
            
            $this->Session->setFlash(__('The Plant has been deleted.'));
        } else {
            $this->Session->setFlash(__('The Plant could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function ajaxPlantSelect() {
        
        $data = array();
        if($this->Plant->updateAll(array('status'=>1), array('Plant.id'=>$this->request->data['Plant_id']))){
            $this->Plant->updateAll(array('status'=>0), array('Plant.id !='=>$this->request->data['Plant_id']));
            $data['Ack'] = 1;
            $data['res'] = 'Plant Image is Successfully Set';
        }else{
            $data['Ack'] = 0;
        }

        echo json_encode($data);
        exit;    
    }

}