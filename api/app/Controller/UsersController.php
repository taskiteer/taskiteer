<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
use Twilio\Rest\Client;

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{
    
    
    /*function beforeFilter() {
    parent::beforeFilter();
    }*/
    /**
     * Components
     *
     * @var array
     */
    public $name = 'Users';
    public $components = array('Session', 'RequestHandler', 'Paginator');
    var $uses = array('User', 'EmailTemplate', 'Setting', 'Newsletter', 'Eventnotification', 'Leftsideaction', 'Contact');
    
    
    
    public function admin_index()
    {
        
        $title_for_layout = 'Admin Login';
        $this->set(compact('title_for_layout'));
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (isset($is_admin) && $is_admin != '') {
            $this->redirect('/admin/users/edit/' . $userid);
        }
        #$this->User->recursive = 0;
        #$this->set('users', $this->Paginator->paginate());
    }
    
    public function admin_dashboard()
    {
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        
        if ($userid == '' && $userid == '') {
            $this->redirect('/admin');
        }
        $this->loadModel('User');
        $this->loadModel('Category');
        $this->loadModel('Post');
        $previous_seven_days_date = date('Y-m-d', strtotime('-7 days'));
        $users = $this->User->find('all', array(
            'conditions' => array(
                'User.type' => 'U','User.is_admin'=>0
            )
        ));
        $new_users = $this->User->find('count', array(
            'conditions' => array(
                'User.type' => 'U','User.is_admin'=>0,'DATE(User.registration_date) >='=>$previous_seven_days_date
            )
        ));
        
        
        $spusers = $this->User->find('all', array(
            'conditions' => array(
                'User.type' => 'SP','User.is_admin'=>0
            )
        ));
        $new_spusers = $this->User->find('count', array(
            'conditions' => array(
                'User.type' => 'SP','User.is_admin'=>0,'DATE(User.registration_date) >='=>$previous_seven_days_date
            )
        ));
        
        $cat = $this->Category->find('all');
        $subcat = $this->Category->find('count', array(
            'conditions' => array(
                'Category.parent_id !=' => 0
            )
        ));        
        
        $post = $this->Post->find('all');
        
        $new_post = $this->Post->find('count', array(
            'conditions' => array(
                'DATE(Post.post_date) >='=>$previous_seven_days_date
            )
        ));
        
        $this->set(compact('users', 'spusers', 'cat', 'post','new_users','new_spusers','subcat','new_post'));
        
    }
    
    
    public function admin_fotgot_password()
    {
        $title_for_layout = 'Forgot Password';
        $this->set(compact('title_for_layout'));
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $options = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.is_admin' => 1
                )
            );
            
            $user = $this->User->find('first', $options);
            //pr($user);
            if ($user) {
                $contact_email = $this->Setting->find('first', array(
                    'conditions' => array(
                        'Setting.id' => 1
                    ),
                    'fields' => array(
                        'Setting.site_email',
                        'Setting.site_name'
                    )
                ));
                $site_name     = $contact_email['Setting']['site_name'];
                //$password = $this->User->get_fpassword();
                $siteurl       = "http://104.131.83.218/team6/service_pro/";
                $link          = '<a href="' . $siteurl . 'admin/users/reset_password/' . base64_encode($user['User']['id']) . '">Reset Password</a>';
                //$this->request->data['User']['id'] = $user['User']['id'];
                //$this->request->data['User']['password'] = $password;
                $adminEmail    = 'superadmin@abc.com';
                $key           = Configure::read('SITE_URL');
                $this->loadModel('EmailTemplate');
                $EmailTemplate = $this->EmailTemplate->find('first', array(
                    'conditions' => array(
                        'EmailTemplate.id' => 1
                    )
                ));
                $Subject_mail  = $site_name . ' Forgot Password';
                $from          = 'admin@fuga.in';
                $msg_body      = str_replace(array(
                    '[USER]',
                    '[EMAIL]',
                    '[LINK]'
                ), array(
                    $user['User']['name'],
                    $user['User']['email'],
                    $link
                ), $EmailTemplate['EmailTemplate']['content']);
                
                $this->php_mail($user['User']['email'], $from, $Subject_mail, $msg_body);
                $this->Session->setFlash('A new password has been sent to your mail. Please check mail.', 'default', array(
                    'class' => 'success'
                ));
                
                
            } else {
                $this->Session->setFlash("Invalid email or You are not authorize to access.");
            }
        }
    }
    
    
    public function admin_reset_password($userid = null)
    {
        $title_for_layout = 'Reset Password';
        $this->set(compact('userid', 'title_for_layout'));
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $userid  = base64_decode($this->request->data['User']['id']);
            $options = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $userid
                )
            );
            $user    = $this->User->find('first', $options);
            if (count($user) > 0) {
                if ($this->request->data['User']['new_pass'] == $this->request->data['User']['con_pass']) {
                    $user_data_auth['User']['id']       = $userid;
                    $user_data_auth['User']['password'] = md5($this->request->data['User']['new_pass']);
                    if ($this->User->save($user_data_auth)) {
                        $this->Session->setFlash('Password updated successfully.', 'default', array(
                            'class' => 'success'
                        ));
                        return $this->redirect(array(
                            'action' => 'index'
                        ));
                    }
                } else {
                    $this->Session->setFlash('Wrong user.', 'default');
                    return $this->redirect(array(
                        'action' => 'reset_password'
                    ));
                }
            }
            
            
        } else {
            if (!isset($userid) && $userid == '') {
                $this->Session->setFlash(__('Wrong url for reset.', 'default', array(
                    'class' => 'error'
                )));
                return $this->redirect(array(
                    'action' => 'forgot_password'
                ));
            }
        }
        
    }
    
    
    
    
    
    public function admin_login()
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        
        if (isset($is_admin) && $is_admin != '') {
            $this->redirect('/admin/users/dashboard/');
        }
        if ($this->request->is('post')) {
            $options   = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['usernamel'],
                    'User.password' => md5($this->request->data['User']['passwordl']),
                    'User.status' => 1,
                    'OR' => array(
                        'User.role !=' => '0',
                        'User.is_admin' => 1
                    )
                )
            );
            $loginuser = $this->User->find('first', $options);
            if (!$loginuser) {
                $this->Session->setFlash(__('Invalid email or password, try again', 'default', array(
                    'class' => 'error'
                )));
                return $this->redirect(array(
                    'action' => 'admin_index'
                ));
            } else {
                $this->Session->write('adminuserid', $loginuser['User']['id']);
                $this->Session->write('is_admin', $loginuser['User']['is_admin']);
                $this->Session->setFlash('You have successfully logged in', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'dashboard'
                ));
            }
        }
    }
    
    
    public function admin_logout()
    {
        #return $this->redirect($this->Auth->logout());
        $this->Session->delete('adminuserid');
        $this->Session->delete('is_admin');
        $this->redirect('/admin');
    }
    
    
    public function admin_edit($id = null)
    {
        //$this->loadModel('UserImage');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->request->data1 = array();
        $title_for_layout     = 'User Edit';
        $this->set(compact('title_for_layout'));
        //$service_areas=$this->User->ServiceArea->find('list');
        //$this->set(compact('title_for_layout','service_areas'));
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('The user has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'edit',
                    $id
                ));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            
            $options             = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $id
                )
            );
            $this->request->data = $this->User->find('first', $options);
        }
    }
    
    
    public function admin_edit_customer($id = null)
    {
        //$this->loadModel('UserImage');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->request->data1 = array();
        $title_for_layout     = 'User Edit';
        $this->set(compact('title_for_layout'));
        //$service_areas=$this->User->ServiceArea->find('list');
        //$this->set(compact('title_for_layout','service_areas'));
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            if (!empty($this->request->data['User']['image']['name'])) {
                $pathpart       = pathinfo($this->request->data['User']['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "user_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = $this->request->data['User']['img'];
            }
            $this->request->data['User']['image'] = $filename;
            
            
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('The user has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'list'
                ));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            
            $options             = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $id
                )
            );
            $this->request->data = $this->User->find('first', $options);
        }
    }
    
    
    
    function admin_changepwd()
    {
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $this->User->create();
            $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
            $this->request->data['User']['id']       = $userid;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Your password changed successfully.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'changepwd'
                ));
                
            } else {
                
            }
        }
    }
    
    
    public function admin_add()
    {
        $this->loadModel('UserImage');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        //$countries=$this->User->Country->find('list');
        $this->request->data1 = array();
        $title_for_layout     = 'User Add';
        
        if ($this->request->is('post')) {
            $options     = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email']
                )
            );
            $emailexists = $this->User->find('first', $options);
            if (!$emailexists) {
                
                if (!empty($this->request->data['User']['image']['name'])) {
                    $pathpart       = pathinfo($this->request->data['User']['image']['name']);
                    $ext            = $pathpart['extension'];
                    $extensionValid = array(
                        'jpg',
                        'jpeg',
                        'png',
                        'gif'
                    );
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder  = "user_images/";
                        $uploadPath    = WWW_ROOT . $uploadFolder;
                        $filename      = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['User']['image'] = $filename;
                
                $this->request->data['User']['registration_date'] = date("d-m-Y");
                $this->request->data['User']['password']          = md5($this->request->data['User']['password']);
                $this->request->data['User']['status']            = 1;
                $this->request->data['User']['type']              = "U";
                $this->User->create();
                
                if ($this->User->save($this->request->data)) {
                    
                    $this->Session->setFlash('The user has been saved.', 'default', array(
                        'class' => 'success'
                    ));
                    return $this->redirect(array(
                        'action' => 'list'
                    ));
                } else {
                    $this->Session->setFlash(__('The user could not be saved. Please, try again.', 'default', array(
                        'class' => 'error'
                    )));
                }
                
            } else {
                $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array(
                    'class' => 'error'
                )));
            }
        }
        
        
    }
    
    public function admin_list()
    {
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        // $countries=$this->User->Country->find('list');
        $title_for_layout = 'User List';
        //$active_user=$this->User->find("count",array('conditions'=>array('User.status'=>1,'User.id !='=>2, 'User.is_admin'=>0)));
        //$inactive_user=$this->User->find("count",array('conditions'=>array('User.status'=>0,'User.id !='=>2, 'User.is_admin'=>0)));
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            //pr($this->request->data);exit;
            
            $email    = $this->request->data['email'];
            $name     = $this->request->data['name'];
            $QueryStr = "(User.is_admin ='0' AND User.type ='U')";
            if ($name != '') {
                $QueryStr .= " AND (User.name like '%" . $name . "%')";
            }
            if ($email != '') {
                $QueryStr .= " AND (User.email = '" . $email . "')";
            }
            
            
            $options = array(
                'conditions' => array(
                    $QueryStr
                ),
                'order' => array(
                    'User.id' => 'desc'
                )
            );
            // pr($options);
            //exit;
            //$Newsearch_is_active=$search_is_active;
        } else {
            $options = array(
                'conditions' => array(
                    'User.is_admin' => 0,
                    'User.type' => 'U'
                ),
                'order' => array(
                    'User.id' => 'desc'
                )
            );
            
            $email = '';
            $name  = '';
        }
        
        //$options = array('User.id !=' => 2);
        //$this->set('user', $this->User->find('first', $options));
        $this->Paginator->settings = $options;
        $this->set('users', $this->Paginator->paginate('User'));
        $this->set(compact('title_for_layout', 'country', 'email', 'name', 'active_user', 'inactive_user'));
    }
    
    
    
    
    
    public function admin_delete($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        //$this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The user has been deleted.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        
        return $this->redirect(array(
            'action' => 'admin_list'
        ));
        
    }
    
    
    public function admin_delete_service_provider($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        //$this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The user has been deleted.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
        }
        
        return $this->redirect(array(
            'action' => 'admin_list_service_provider'
        ));
        
    }
    
    
    
    public function admin_block($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->request->data['User']['id']     = $id;
        $this->request->data['User']['status'] = 0;
        
        if ($this->User->save($this->request->data)) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The user has been blocked.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The user could not be blocked. Please, try again.'));
        }
        //return $this->redirect(array('action' => 'admin_list'));
        
        $option1   = array(
            'conditions' => array(
                'User.id' => $id
            )
        );
        $checktype = $this->User->find('first', $option1);
        //print_r($option1);
        //echo $type=$checktype['User']['type'];
        //exit;
        if ($checktype['User']['type'] == "SP") {
            return $this->redirect(array(
                'action' => 'admin_list_service_provider'
            ));
        } else {
            return $this->redirect(array(
                'action' => 'admin_list'
            ));
            
        }
        
        
    }
    
    public function admin_unblock($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->request->data['User']['id']     = $id;
        $this->request->data['User']['status'] = 1;
        
        if ($this->User->save($this->request->data)) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The user has been unblocked.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The user could not be unblocked. Please, try again.'));
        }
        //return $this->redirect(array('action' => 'admin_list'));
        $option1   = array(
            'conditions' => array(
                'User.id' => $id
            )
        );
        $checktype = $this->User->find('first', $option1);
        //print_r($option1);
        //echo $type=$checktype['User']['type'];
        //exit;
        if ($checktype['User']['type'] == "SP") {
            return $this->redirect(array(
                'action' => 'admin_list_service_provider'
            ));
        } else {
            return $this->redirect(array(
                'action' => 'admin_list'
            ));
            
        }
        
    }
    
    
    public function active_account($code)
    {
        
        $userid = base64_decode($code);
        
        $data['User']['id']     = $userid;
        $data['User']['status'] = 1;
        if ($this->User->save($data)) {
            $this->Session->setFlash('Your account hasbeen activated.', 'default', array(
                'class' => 'success'
            ));
            //return $this->redirect(array('action' => 'active_account'));
            echo "Now your account has been actived";
        } else {
            $this->Session->setFlash('Wrong information.', 'default');
            //return $this->redirect(array('action' => 'registartion'));
            echo "Your account is not actived";
        }
        
        exit;
    }
    
    
    public function admin_reset_pass($userid = null)
    {
        $title_for_layout = 'Reset Password';
        $this->set(compact('userid', 'title_for_layout'));
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            if ($this->request->data['User']['new_password'] == $this->request->data['User']['confirm_password']) {
                $user_data_auth['User']['id']       = $userid;
                $user_data_auth['User']['password'] = md5($this->request->data['User']['new_password']);
                if ($this->User->save($user_data_auth)) {
                    $this->Session->setFlash('Password updated successfully.', 'default', array(
                        'class' => 'success'
                    ));
                    return $this->redirect(array(
                        'action' => 'reset_pass'
                    ));
                }
            } else {
                $this->Session->setFlash('Password doesnot match.', 'default');
                return $this->redirect(array(
                    'action' => 'reset_pass'
                ));
            }
            
            
        }
        
    }
    
    
    
    public function admin_add_subadmin()
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if ($this->request->is('post')) {
            $options     = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email']
                )
            );
            $emailexists = $this->User->find('first', $options);
            if (!$emailexists) {
                $this->request->data['User']['left_sidebar']      = implode(",", $this->request->data['User']['left_sidebar']);
                $this->request->data['User']['password']          = md5($this->request->data['User']['password']);
                $this->request->data['User']['status']            = 1;
                $this->request->data['User']['registration_date'] = date('d-m-Y');
                $this->request->data['User']['is_admin']          = 2;
                $this->User->create();
                
                if ($this->User->save($this->request->data)) {
                    
                    $this->Session->setFlash('The subadmin has been saved.', 'default', array(
                        'class' => 'success'
                    ));
                    return $this->redirect(array(
                        'action' => 'admin_list_subadmin'
                    ));
                } else {
                    $this->Session->setFlash(__('The subadmin could not be saved. Please, try again.', 'default', array(
                        'class' => 'error'
                    )));
                }
                
            } else {
                $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array(
                    'class' => 'error'
                )));
            }
        }
        $leftaction  = array(
            'conditions' => array(
                'Leftsideaction.status' => 1
            ),
            'fields' => array(
                'Leftsideaction.leftaction'
            )
        );
        $leftactions = $this->Leftsideaction->find('list', $leftaction);
        $this->set(compact('leftactions'));
    }
    
    
    public function admin_list_subadmin()
    {
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        // $countries=$this->User->Country->find('list');
        $title_for_layout = 'Subadmin List';
        
        $options                   = array(
            'conditions' => array(
                'User.is_admin =' => 2
            ),
            'order' => array(
                'User.id' => 'desc'
            )
        );
        //$options = array('User.id !=' => 2);
        //$this->set('user', $this->User->find('first', $options));
        $this->Paginator->settings = $options;
        $this->set('subadmins', $this->Paginator->paginate('User'));
        //$this->set(compact('title_for_layout','country','email','name','active_user','inactive_user'));
    }
    public function admin_edit_subadmin($id = null)
    {
        //$this->loadModel('UserImage');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->request->data1 = array();
        $title_for_layout     = 'Subadmin Edit';
        $this->set(compact('title_for_layout'));
        //$service_areas=$this->User->ServiceArea->find('list');
        //$this->set(compact('title_for_layout','service_areas'));
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->request->data['User']['left_sidebar'] = implode(",", $this->request->data['User']['left_sidebar']);
            $this->request->data['User']['id']           = $id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('The subadmin has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'list_subadmin'
                ));
            } else {
                $this->Session->setFlash(__('The subadmin could not be saved. Please, try again.'));
            }
        } else {
            
            $options             = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $id
                )
            );
            $this->request->data = $this->User->find('first', $options);
            
            $leftaction  = array(
                'conditions' => array(
                    'Leftsideaction.status' => 1
                ),
                'fields' => array(
                    'Leftsideaction.leftaction'
                )
            );
            $leftactions = $this->Leftsideaction->find('list', $leftaction);
            $this->set(compact('leftactions'));
        }
    }
    
    
    public function admin_delete_subadmin($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        //$this->request->onlyAllow('post', 'delete');
        if ($this->User->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The subadmin has been deleted.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The subadmin could not be deleted. Please, try again.'));
        }
        return $this->redirect(array(
            'action' => 'admin_list_subadmin'
        ));
    }
    
    public function admin_block_subadmin($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->request->data['User']['id']     = $id;
        $this->request->data['User']['status'] = 0;
        
        if ($this->User->save($this->request->data)) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The subadmin has been blocked.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The subadmin could not be blocked. Please, try again.'));
        }
        return $this->redirect(array(
            'action' => 'admin_list_subadmin'
        ));
    }
    
    public function admin_unblock_subadmin($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->request->data['User']['id']     = $id;
        $this->request->data['User']['status'] = 1;
        
        if ($this->User->save($this->request->data)) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The subadmin has been unblocked.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The subadmin could not be unblocked. Please, try again.'));
        }
        return $this->redirect(array(
            'action' => 'admin_list_subadmin'
        ));
    }
    
    
    public function admin_add_service_provider()
    {
        
        $this->loadModel('UserImage');
        $this->loadModel('Post');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        //$countries=$this->User->Country->find('list');
        
        $pst = $this->Post->find('list', array(
            'Post.id',
            'Post.name'
        ));
        
        $this->request->data1 = array();
        $title_for_layout     = 'User Add';
        
        if ($this->request->is('post')) {
            
            $options     = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email']
                )
            );
            $emailexists = $this->User->find('first', $options);
            if (!$emailexists) {
                
                if (!empty($this->request->data['User']['image']['name'])) {
                    $pathpart       = pathinfo($this->request->data['User']['image']['name']);
                    $ext            = $pathpart['extension'];
                    $extensionValid = array(
                        'jpg',
                        'jpeg',
                        'png',
                        'gif'
                    );
                    if (in_array(strtolower($ext), $extensionValid)) {
                        $uploadFolder  = "user_images/";
                        $uploadPath    = WWW_ROOT . $uploadFolder;
                        $filename      = uniqid() . '.' . $ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                    } else {
                        $this->Session->setFlash(__('Invalid image type.'));
                    }
                } else {
                    $filename = '';
                }
                $this->request->data['User']['image']             = $filename;
                $this->request->data['User']['type']              = 'SP';
                $this->request->data['User']['registration_date'] = date("d-m-Y");
                $this->request->data['User']['password']          = md5($this->request->data['User']['password']);
                $this->request->data['User']['status']            = 1;
                $this->User->create();
                
                if ($this->User->save($this->request->data)) {
                    
                    $this->Session->setFlash('The service provider  has been saved.', 'default', array(
                        'class' => 'success'
                    ));
                    return $this->redirect(array(
                        'action' => 'admin_list_service_provider'
                    ));
                } else {
                    $this->Session->setFlash(__('The service provider could not be saved. Please, try again.', 'default', array(
                        'class' => 'error'
                    )));
                }
                
            } else {
                $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array(
                    'class' => 'error'
                )));
            }
        }
        $this->set(compact('pst'));
    }
    
    
    public function admin_list_service_provider()
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Service Provider List';
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $email    = $this->request->data['email'];
            $name     = $this->request->data['name'];
            $QueryStr = "(User.type ='SP'  AND User.is_admin ='0')";
            if ($name != '') {
                $QueryStr .= " AND (User.name like '%" . $name . "%')";
            }
            if ($email != '') {
                $QueryStr .= " AND (User.email = '" . $email . "')";
            }
            
            
            $options = array(
                'conditions' => array(
                    $QueryStr
                ),
                'order' => array(
                    'User.id' => 'desc'
                )
            );
            // pr($options);
            //exit;
            //$Newsearch_is_active=$search_is_active;
        } else {
            $options = array(
                'conditions' => array(
                    'User.type =' => 'SP',
                    'User.is_admin' => 0
                ),
                'order' => array(
                    'User.id' => 'desc'
                )
            );
            
            $email = '';
            $name  = '';
        }
        
        //$options = array('User.id !=' => 2);
        //$this->set('user', $this->User->find('first', $options));
        $this->User->recursive     = 2;
        $this->Paginator->settings = $options;
        $this->set('users', $this->Paginator->paginate('User'));
        
        $this->set(compact('title_for_layout', 'country', 'email', 'name', 'active_user', 'inactive_user'));
    }
    
    public function admin_providers($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $title_for_layout = 'Service Provider List';
        
        $options                   = array(
            'conditions' => array(
                'User.service_id' => $id
            )
        );
        $this->User->recursive     = 2;
        $this->Paginator->settings = $options;
        $this->set('users', $this->Paginator->paginate('User'));
    }
    
    public function admin_edit_service_provider($id = null)
    {
        //$this->loadModel('UserImage');
        $this->loadModel('Post');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $pst                  = $this->Post->find('list', array(
            'Post.id',
            'Post.name'
        ));
        $this->request->data1 = array();
        $title_for_layout     = 'User Edit';
        $this->set(compact('title_for_layout', 'pst'));
        //$service_areas=$this->User->ServiceArea->find('list');
        //$this->set(compact('title_for_layout','service_areas'));
        
        
        
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            if (!empty($this->request->data['User']['image']['name'])) {
                $pathpart       = pathinfo($this->request->data['User']['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "user_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['User']['image']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = $this->request->data['User']['img'];
            }
            $this->request->data['User']['image'] = $filename;
            
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('The user has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'list_service_provider'
                ));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        } else {
            
            $options             = array(
                'conditions' => array(
                    'User.' . $this->User->primaryKey => $id
                )
            );
            $this->request->data = $this->User->find('first', $options);
        }
    }
    
    
    
    public function admin_setting()
    {
        //$this->loadModel('UserImage');
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        
        
        
        
        $options             = array(
            'conditions' => array(
                'Setting.' . $this->User->primaryKey => 1
            )
        );
        $this->request->data = $this->Setting->find('first', $options);
        
    }
    public function signup_service()
    {
        
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->loadModel("NotificationSetting");
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData    = $this->request->input('json_decode');
            $email       = $jsonData->email;
            $options     = array(
                'conditions' => array(
                    "User.email" => $email
                )
            );
            $count_email = $this->User->find('count', $options);
            if (!$count_email) {
                $this->request->data['User']['email']     = $email;
                $this->request->data['User']['name']      = $jsonData->name;
                $this->request->data['User']['password']  = md5($jsonData->password);
                if(isset($jsonData->type) && !empty($jsonData->type)){
                    $this->request->data['User']['type']      = $jsonData->type;
                }else{
                    $this->request->data['User']['type']      = 'U';
                }
                
                $this->request->data['User']['mobile_no'] = $jsonData->mobile_no;
		$this->request->data['User']['isd_code']  = $jsonData->isd;
                $this->request->data['User']['device_type'] = $jsonData->device_type;
                $this->request->data['User']['token'] = $jsonData->token;

                if (isset($jsonData->dob_date) && $jsonData->dob_date != '') {
                    $this->request->data['User']['dob_date'] = $jsonData->dob_date;
                }
                if (isset($jsonData->dob_mon) && $jsonData->dob_mon != '') {
                    $this->request->data['User']['dob_mon'] = $jsonData->dob_mon;
                }
                if (isset($jsonData->dob_year) && $jsonData->dob_year != '') {
                    $this->request->data['User']['dob_year'] = $jsonData->dob_year;
                }
                $this->request->data['User']['registration_date'] = gmdate('Y-m-d H:i:s');
                $ip                                               = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; // the IP address to query
                $query                                            = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
                if ($query && $query['status'] == 'success') {
                    $this->request->data['User']['lat']   = $query['lat'];
                    $this->request->data['User']['lang']  = $query['lon'];
                    $this->request->data['User']['state'] = $query['regionName'];
                    $this->request->data['User']['city']  = $query['city'];
                }
                
                $this->request->data['User']['country_id']  = $jsonData->countries->id;

                try {
                    $this->User->save($this->request->data);
                    $last_id = $this->User->getLastInsertID();
                    $this->loadModel("EmailTemplate");
                    $this->loadModel("Setting");
                    $setting    = $this->Setting->find('first', array(
                        'conditions' => array(
                            'Setting.id' => 1
                        )
                    ));
                    $mail_notification["NotificationSetting"]["user_id"]=$last_id;
                    $mail_notification["NotificationSetting"]["new_customer"]=1;
                    $mail_notification["NotificationSetting"]["customer_activity"]=1;
                    $mail_notification["NotificationSetting"]["request_reminder"]=1;
                    $mail_notification["NotificationSetting"]["viewed_quote"]=1;
                    $mail_notification["NotificationSetting"]["quote_activity"]=1;
                    $mail_notification["NotificationSetting"]["reminder_email"]=1;
                    $mail_notification["NotificationSetting"]["quote_followup"]=1;
                    $mail_notification["NotificationSetting"]["type"]="e";
                    $this->NotificationSetting->save($mail_notification);
                    $sms_notification["NotificationSetting"]["user_id"]=$last_id;
                    $sms_notification["NotificationSetting"]["new_customer"]=1;
                    $sms_notification["NotificationSetting"]["customer_activity"]=1;
                    $sms_notification["NotificationSetting"]["request_reminder"]=1;
                    $sms_notification["NotificationSetting"]["viewed_quote"]=1;
                    $sms_notification["NotificationSetting"]["quote_activity"]=1;
                    $sms_notification["NotificationSetting"]["reminder_email"]=1;
                    $sms_notification["NotificationSetting"]["quote_followup"]=1;
                    $sms_notification["NotificationSetting"]["type"]="m";
                    $this->NotificationSetting->create();
                    $this->NotificationSetting->save($sms_notification);
                    
                    $adminEmail = $setting['Setting']['site_email'];
                    $tpl        = $this->EmailTemplate->find('first', array(
                        'conditions' => array(
                            'EmailTemplate.id' => 2
                        )
                    ));
                    $SITE_URL   = Configure::read("SITE_URL");
                    $link       = $SITE_URL . 'users/active_service/' . base64_encode($last_id);
                    $msg_body   = str_replace(array(
                        '[USERNAME]',
                        '[EMAIL]',
                        '[PASSWORD]',
                        '[PHONE]',
                        '[LINK]'
                    ), array(
                        $jsonData->name,
                        $email,
                        $jsonData->password,
                        $jsonData->mobile_no,
                        $link
                    ), $tpl['EmailTemplate']['content']);
                    $this->send_email($adminEmail, $email, $tpl['EmailTemplate']['subject'], $msg_body);
                    $data = array(
                        'Ack' => 1,
                        "msg" => 'signup successfully'
                    );
                    
                }
                catch (Exception $ex) {
                    print_r($ex);
                    exit;
                    
                }
            } else {
                $data = array(
                    'Ack' => 0,
                    "msg" => 'Duplicate email address'
                );
                
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    public function active_service($id = null)
    {
        $this->User->id = base64_decode($id);
        $this->User->saveField('status', 1);
        $WEB_URL = Configure::read("WEB_URL");
        return $this->redirect($WEB_URL . 'cms/11');
        
        
        
        
        exit;
        
        
    }
    function login_service()
    {
        $this->loadModel("Notification");
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            $this->loadModel("Rating");
            $email       = $jsonData->email;
	    //$type        = $jsonData->type;
            $password    = $jsonData->password;
            $device_type = !empty($jsonData->device_type) ? $jsonData->device_type : '';
            $token       = !empty($jsonData->token) ? $jsonData->token : '';
            $options     = array(
                'conditions' => array(
                    "User.email" => $email,
                    "User.password" => md5($password),
                )
            );
            $user        = $this->User->find('first', $options);
            if (!empty($user)) {
                
                if ($user['User']['status'] == 1) {

		    	/*if($user['User']['type'] == $type){*/
		            $this->request->data['User']['id']          = $user['User']['id'];
		            $this->request->data['User']['is_loggedin'] = 'Y';
		            $this->request->data['User']['device_type'] = $device_type;
		            $this->request->data['User']['token']       = $token;
		            $this->User->save($this->request->data);
		            $user                               = $this->User->find('first', $options);
		            $SITE_URL                           = Configure::read("SITE_URL");
		            $user['User']['hide_image']         = $user['User']['image'];
		            $user['User']['image']              = !empty($user['User']['image']) ? $SITE_URL . 'user_images/' . $user['User']['image'] : $SITE_URL . 'nouser.png';
		            $user['User']['rating']             = $user['User']['avg_score'];
		            $name                               = explode(" ", $user['User']['name']);
		            $user['User']['rating']             = $user['User']['avg_score'];
		            $first_name = '';
		            for($i=0;$i < (count($name)-1);$i++){
		                $first_name = $first_name != ''?$first_name.' '.$name[$i]:$name[$i];
		            }
		            $user['User']['first_name']         = $first_name;
		            $user['User']['last_name']          = !empty($name[count($name)-1]) ? $name[count($name)-1] : '';
		            $no_review                          = $this->Rating->find('count', array(
		                "conditions" => array(
		                    "Rating.to_id" => $user["User"]["id"]
		                )
		            ));
		            $user['User']['no_review']          = "" . $no_review . "";
		            $total_notify                       = $this->Notification->find("count", array(
		                "conditions" => array(
		                    "Notification.sent_to" => $user["User"]["id"],
		                    "Notification.is_read" => 0
		                )
		            ));
		            $user['User']['total_notification'] = $total_notify;
		            $data                               = array(
		                'Ack' => 1,
		                'response' => $user['User']
		            );


			/*} else {
				 $data = array(
				    'Ack' => 0,
				    "msg" => 'Invalid user type'
				);
			}*/
                } else {
                    $data = array(
                        'Ack' => 2,
                        'msg' => "You are not a active user"
                    );
                    
                }
                
                
            } else {
                $data = array(
                    'Ack' => 0,
                    "msg" => 'Invalid log in credential'
                );
                
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    function changepwd_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData         = $this->request->input('json_decode');
            $old_password     = $jsonData->old_password;
            $new_password     = $jsonData->new_password;
            $confirm_password = $jsonData->confirm_password;
            $user_id          = $jsonData->user_id;
            $options          = array(
                'conditions' => array(
                    'User.password' => md5($old_password),
                    'User.id' => $user_id
                )
            );
            $user             = $this->User->find('first', $options);
            
            if (!empty($user)) {
                
                
                if ($new_password != $confirm_password) {
                    $data = array(
                        'Ack' => 0,
                        'msg' => 'Confirm password does not match'
                    );
                } else {
                    $this->request->data['User']['id']       = $user_id;
                    $this->request->data['User']['password'] = md5($new_password);
                    if ($this->User->save($this->request->data)) {
                        $data = array(
                            'Ack' => 1,
                            'msg' => 'Password Changed Successfully'
                        );
                    }
                }
            } else {
                $data = array(
                    'Ack' => 1,
                    'msg' => 'Your current password is not correct'
                );
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    
    function forgotpwd_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            $email    = $jsonData->email;
            $options  = array(
                'conditions' => array(
                    'User.email' => $email
                )
            );
            $user     = $this->User->find('first', $options);
            if (!empty($user)) {
                $this->loadModel("EmailTemplate");
                $this->loadModel("Setting");
                $setting    = $this->Setting->find('first', array(
                    'conditions' => array(
                        'Setting.id' => 1
                    )
                ));
                $adminEmail = $setting['Setting']['site_email'];
                $tpl        = $this->EmailTemplate->find('first', array(
                    'conditions' => array(
                        'EmailTemplate.id' => 10
                    )
                ));
                
                $password                                = rand(100000, 999999);
                $this->request->data['User']['id']       = $user["User"]["id"];
                $this->request->data['User']['password'] = md5($password);
                if ($this->User->save($this->request->data)) {
                    $msg_body = str_replace(array(
                        '[PASSWORD]'
                    ), array(
                        $password
                    ), $tpl['EmailTemplate']['content']);
                    $this->send_email($adminEmail, $user['User']['email'], $tpl['EmailTemplate']['subject'], $msg_body);
                    $data = array(
                        'Ack' => 1,
                        'msg' => "Your new password sent to your email"
                    );
                    
                }
                
            } else {
                $data = array(
                    'Ack' => 0,
                    'msg' => "Invalid Email"
                );
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    
    
    function forgotpwdlink_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            $email    = $jsonData->email;
            $options  = array(
                'conditions' => array(
                    'User.email' => $email
                )
            );
            $user     = $this->User->find('first', $options);
            if (!empty($user)) {
                
                $length = 10;
                $chars  = '0123456789';
                $str    = '';
                $max    = strlen($chars) - 1;
                for ($i = 0; $i < $length; $i++) {
                    $str .= $chars[rand(0, $max)];
                }
                
                $tokenid = str_pad($user['User']['id'], '10', $str, STR_PAD_LEFT);
                
                
                $user['User']['password_token_id'] = $tokenid;
                
                $this->User->id = $user['User']['id'];
                
                $this->User->saveField('password_token_id', $tokenid);
                
                $siteurl = Configure::read('SITE_URL');
                //$link='<a href="'.$siteurl.'users/reset_password/'.base64_encode($tokenid).'">Reset Password</a>';
                $link    = $siteurl . 'users/reset_password/' . base64_encode($tokenid);
                
                
                
                
                
                
                $this->loadModel("EmailTemplate");
                $this->loadModel("Setting");
                $setting    = $this->Setting->find('first', array(
                    'conditions' => array(
                        'Setting.id' => 1
                    )
                ));
                $adminEmail = $setting['Setting']['site_email'];
                $tpl        = $this->EmailTemplate->find('first', array(
                    'conditions' => array(
                        'EmailTemplate.id' => 1
                    )
                ));
                //$password=rand(100000,999999);
                //$this->request->data['User']['id']=$user["User"]["id"];
                //$this->request->data['User']['password']=md5($password);
                //if($this->User->save($this->request->data))
                //{
                $msg_body   = str_replace(array(
                    '[LINK]'
                ), array(
                    $link
                ), $tpl['EmailTemplate']['content']);
                $this->send_email($adminEmail, $user['User']['email'], $tpl['EmailTemplate']['subject'], $msg_body);
                $data = array(
                    'Ack' => 1,
                    'msg' => "Password link has been sent to your email",
                    'token_id' => $tokenid
                );
                
                //}
                
            } else {
                $data = array(
                    'Ack' => 0,
                    'msg' => "Invalid Email"
                );
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    function reset_password()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            $token_id = $jsonData->password_token_id;
            $password = $jsonData->password;
            $options  = array(
                'conditions' => array(
                    'User.password_token_id' => $token_id
                )
            );
            $user     = $this->User->find('first', $options);
            if (!empty($user)) {
                
                $this->request->data['User']['id']       = $user["User"]["id"];
                $this->request->data['User']['password'] = md5($password);
                if ($this->User->save($this->request->data)) {
                    
                    $data = array(
                        'Ack' => 1,
                        'msg' => "New Password Has Been Generated"
                    );
                    
                }
                
            } else {
                $data = array(
                    'Ack' => 0,
                    'msg' => "Invalid Email"
                );
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    
    function ph_verifyApi()
    {
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $jsonData = $this->request->input('json_decode');
            $user_id  = $jsonData->user_id;
            $ph_no    = $jsonData->mobile_no;
            $options  = array(
                'conditions' => array(
                    'User.id' => $user_id,
                    'User.mobile_no' => $ph_no
                )
            );
            $user     = $this->User->find('first', $options);
            $sms_th   = array();
            if (!empty($user)) {
                
                
                
                $tokenid = rand(1000, 9999);
                
                
                $user['User']['ph_otp'] = $tokenid;
                
                $this->User->id = $user['User']['id'];
                
                $this->User->saveField('ph_otp', $tokenid);
                
                $contry_isd = '+91';
                $thnm       = $user['User']['name'];
                
                if (!empty($user['User']['mobile_no']) && $user['User']['mobile_no'] != NULL) {
                    $sms = "Hello," . $thnm;
                    $sms .= " Please verify your phone by " . $tokenid;
                    $this->sendsms_mms($sms);
                    
                }
                
                
                
                
                $data = array(
                    'Ack' => 1,
                    'msg' => "New otp has been send to your mobile",
                    'otp' => $tokenid
                );
                
                
                
            } else {
                $data = array(
                    'Ack' => 0,
                    'msg' => "Invalid Mobile No"
                );
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    function otp_verify()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            $otp      = $jsonData->ph_otp;
            $user_id  = $jsonData->user_id;
            $options  = array(
                'conditions' => array(
                    'User.id' => $user_id,
                    'User.ph_otp' => $otp
                )
            );
            $user     = $this->User->find('first', $options);
            if (!empty($user)) {
                
                $this->request->data['User']['id']        = $user_id;
                $this->request->data['User']['is_verify'] = 1;
                if ($this->User->save($this->request->data)) {
                    
                    $data = array(
                        'Ack' => 1,
                        'msg' => "You have successfully verified your number "
                    );
                    
                }
                
            } else {
                $data = array(
                    'Ack' => 0,
                    'msg' => "Invalid Ph no or Invalid user"
                );
            }
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    
    
    public function fbsignup_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData                                      = $this->request->input('json_decode');
            $email                                         = !empty($jsonData->email) ? $jsonData->email : '';
	    $type                                          = !empty($jsonData->type) ? $jsonData->type : '';
            $social_user_id                                = $jsonData->social_user_id;
            $name                                          = $jsonData->name;
            $social_type                                   = $jsonData->social_type;
            $device_type                                   = !empty($jsonData->device_type) ? $jsonData->device_type : '';
            $token                                         = !empty($jsonData->token) ? $jsonData->token : '';
            $options                                       = array(
                'conditions' => array(
                    "User.email" => $email
                )
            );
            $user_email                                    = $this->User->find('first', $options);
            $this->request->data["User"]["social_user_id"] = $social_user_id;
            $this->request->data["User"]["email"]          = $email;
	    $this->request->data["User"]["type"]           = $type;
            $this->request->data["User"]["name"]           = $name;
            $this->request->data["User"]["device_type"]    = $device_type;
            $this->request->data["User"]["token"]          = $token;
            $this->request->data['User']['is_loggedin']    = 'Y';
            $this->request->data['User']['social_type']    = $social_type;
            $this->request->data['User']['status']         = 1;
            $name_arr                                      = explode(" ", $name);
            $this->request->data["User"]["first_name"]     = !empty($name_arr[0]) ? $name_arr[0] : '';
            $this->request->data["User"]["last_name"]      = !empty($name_arr[1]) ? $name_arr[1] : '';
            $ip                                            = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; // the IP address to query
            $query                                         = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
            if ($query && $query['status'] == 'success') {
                $this->request->data['User']['lat']  = $query['lat'];
                $this->request->data['User']['lang'] = $query['lon'];
            }
            if (!empty($jsonData->file)) {
                
                
                $uploadFolder = "user_images/";
                $uploadPath   = WWW_ROOT . $uploadFolder;
                $filename     = uniqid() . '.jpeg';
                copy($jsonData->file, $uploadPath . $filename);
                $this->request->data['User']['image'] = $filename;
                
            }
            
            if (empty($email)) {
                $data = array(
                    'Ack' => 2
                );
            }
            
            else if (!empty($user_email)) {
                $this->request->data["User"]["id"] = $user_email["User"]["id"];
                
                
                
                $this->User->save($this->request->data);
                $last_id = $user_email["User"]["id"];
                $options = array(
                    'conditions' => array(
                        "User.id" => $last_id
                    )
                );
                $user    = $this->User->find('first', $options);
                if (!empty($user['User']['image'])) {
                    $SITE_URL              = Configure::read("SITE_URL");
                    $user['User']['image'] = $SITE_URL . 'user_images/' . $user['User']['image'];
                    
                }
                $data = array(
                    'Ack' => 1,
                    'response' => $user['User']
                );
                
                
            } else {
                $this->request->data['User']['registration_date'] = gmdate('Y-m-d H:i:s');
                $this->User->save($this->request->data);
                $last_id                    = $this->User->getLastInsertID();
                $options                    = array(
                    'conditions' => array(
                        "User.id" => $last_id
                    )
                );
                $user                       = $this->User->find('first', $options);
                $SITE_URL                   = Configure::read("SITE_URL");
                $user['User']['hide_image'] = $user['User']['image'];
                $user['User']['image']      = !empty($user['User']['image']) ? $SITE_URL . 'user_images/' . $user['User']['image'] : $SITE_URL . 'nouser.png';
                $data                       = array(
                    'Ack' => 1,
                    'response' => $user['User']
                );
            }
            
            
            
            
            echo json_encode($data);
            exit;
            
            
            
            
        }
    }
    
    public function myprofile_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $this->loadModel("Rating");
            $jsonData                      = $this->request->input('json_decode');
            $user_id                       = $jsonData->user_id;
            $options                       = array(
                "conditions" => array(
                    "User.id" => $user_id
                )
            );
            $user                          = $this->User->find("first", $options);
            $SITE_URL                      = Configure::read("SITE_URL");
            $user['User']['hide_image']    = !empty($user['User']['image']) ? $user['User']['image'] : '';
            $user['User']['city']          = !empty($user['User']['city']) ? $user['User']['city'] : '';
            $user['User']['address']       = !empty($user['User']['address']) ? $user['User']['address'] : '';
            $user['User']['state']         = !empty($user['User']['state']) ? $user['User']['state'] : '';
            $user['User']['first_name']    = !empty($user['User']['first_name']) ? $user['User']['first_name'] : '';
            $user['User']['last_name']     = !empty($user['User']['last_name']) ? $user['User']['last_name'] : '';
            $user['User']['business_name'] = !empty($user['User']['business_name']) ? $user['User']['business_name'] : '';
            $user['User']['image']         = !empty($user['User']['image']) ? $SITE_URL . 'user_images/' . $user['User']['image'] : $SITE_URL . 'nouser.png';
            $no_review                     = $this->Rating->find('count', array(
                "conditions" => array(
                    "Rating.to_id" => $user["User"]["id"]
                )
            ));
            $user['User']['no_review']     = "" . $no_review . "";
            $data                          = array(
                'Ack' => 1,
                'response' => $user['User']
            );
            echo json_encode($data);
            exit;
            
        }
        
    }
    
    public function editprofile_service($id = null)
    {
        $SITE_URL = Configure::read("SITE_URL");
        if ($_POST) {
            $email     = $this->request->data['email'];
            $user_id     = $this->request->data['id'];
            foreach ($this->request->data as $key => $json) {
                if ($key != 'image') {
                    $this->request->data['User'][$key] = $json;
                }
            }
            
            if (!empty($_FILES['image']['name'])) {
                $pathpart       = pathinfo($_FILES['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "user_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($_FILES['image']['tmp_name'], $full_flg_path);
                    $this->request->data['User']["image"] = $filename;
                } else {
                    
                }
            } 
            $options     = array(
                'conditions' => array(
                    "User.email" => $email,
                    "User.id !=" => $user_id
                )
            );
            $count_email = $this->User->find('count', $options);
            if ($count_email > 0) {
                $data = array(
                    'Ack' => 0,
                    "msg" => "Duplicate email address"
                );
            } else {
                
                $this->request->data['User']["mobile_no"] = $this->request->data['User']["phone"];
                if ($this->User->save($this->request->data)) {
                    $options                    = array(
                        'conditions' => array(
                            "User.id" => $user_id
                        )
                    );
                    $user                       = $this->User->find('first', $options);
                    $user['User']['hide_image'] = !empty($user['User']['image']) ? $user['User']['image'] : '';
                    $user['User']['image']      = !empty($user['User']['image']) ? $SITE_URL . 'user_images/' . $user['User']['image'] : $SITE_URL . 'nouser.png';
                    
                    $data = array(
                        'Ack' => 1,
                        "msg" => "Profile has been saved successfully",
                        "response" => $user['User']
                    );
                    
                }
                
            }
            
            
            
            
        }
        echo json_encode($data);
        exit;
        
    }
    
    public function editprofileapp()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $SITE_URL   = Configure::read("SITE_URL");
            $jsonData   = $this->request->input('json_decode');
            $first_name = !empty($jsonData->first_name) ? $jsonData->first_name : '';
            $last_name  = !empty($jsonData->last_name) ? $jsonData->last_name : '';
            $id         = !empty($jsonData->id) ? $jsonData->id : '';
            if (!empty($first_name) or !empty($last_name)) {
                $this->request->data["User"]["name"] = $first_name . ' ' . $last_name;
            }
            
            foreach ($jsonData as $key => $json) {
                $this->request->data["User"][$key] = $json;
                if ($key == "email") {
                    $user_id     = $jsonData->id;
                    $email       = $jsonData->email;
                    $count_email = $this->User->find('count', array(
                        "conditions" => array(
                            "User.email" => $email,
                            "User.id !=" => $user_id
                        )
                    ));
                    if ($count_email > 0) {
                        $data = array(
                            "Ack" => 0,
                            "msg" => "Duplicate email address"
                        );
                        echo json_encode($data);
                        exit;
                    }
                    
                }
            }
            if ($this->User->save($this->request->data)) {
                $user                  = $this->User->find("first", array(
                    "conditions" => array(
                        "User.id" => $id
                    )
                ));
                $user["User"]["image"] = !empty($user["User"]["image"]) ? $SITE_URL . 'user_images/' . $user["User"]["image"] : $SITE_URL . 'nouser.png';
                $data                  = array(
                    "Ack" => 1,
                    "msg" => "Profile changed successfully",
                    "response" => $user
                );
            } else {
                $data = array(
                    "Ack" => 0,
                    "msg" => "Profile changed Failure"
                );
            }
            
            
        }
        
        echo json_encode($data);
        exit;
        
        
    }
    
    public function profileimage()
    {
       
        $SITE_URL = Configure::read("SITE_URL");
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $id   = $this->request->data["id"];
            $user = $this->User->find("first", array(
                "fields" => array(
                    "User.image"
                ),
                "conditions" => array(
                    "User.id" => $id
                )
            ));
            
            if (!empty($_FILES['image']['name'])) {
                $this->request->data['User']["id"] = $this->request->data["id"];
                $pathpart                          = pathinfo($_FILES['image']['name']);
                $ext                               = $pathpart['extension'];
                $extensionValid                    = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "user_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    if (!empty($user["User"]["image"])){
                        $previous_image_name = explode('.',$user["User"]["image"]);
                        $previous_image_name = $previous_image_name[0];
                        $filename      = time().$previous_image_name . '.' . $ext;
                        if (file_exists($uploadPath.$user["User"]["image"])) {
                            unlink($uploadPath.$user["User"]["image"]);
                        }
                    }else{
                        $filename      = uniqid() . '.' . $ext;
                    }
                    
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($_FILES['image']['tmp_name'], $full_flg_path);
                    $this->request->data['User']["image"] = $filename;
                    
                    if ($this->User->save($this->request->data)) {
                        $data = array(
                            "Ack" => 1,
                            "msg" => "profile picture changed successfully",
                            "image" => $SITE_URL . 'user_images/' . $filename
                        );
                    } else {
                        $data = array(
                            "Ack" => 0,
                            "msg" => "Internal error"
                        );
                    }
                    
                } else {
                    $data = array(
                        "Ack" => 0,
                        "msg" => "Invalid image type"
                    );
                }
            }
            echo json_encode($data);
           
        }
        
         exit;
    }
    
    public function coverimage()
    {
        $SITE_URL = Configure::read("SITE_URL");
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $id   = $this->request->data["id"];
            $user = $this->User->find("first", array(
                "fields" => array(
                    "User.coverimage"
                ),
                "conditions" => array(
                    "User.id" => $id
                )
            ));
           
            
            
            if (!empty($_FILES['coverimage']['name'])) {
                $this->request->data['User']["id"] = $this->request->data["id"];
                $pathpart                          = pathinfo($_FILES['coverimage']['name']);
                $ext                               = $pathpart['extension'];
                $extensionValid                    = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "coverimage/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($_FILES['coverimage']['tmp_name'], $full_flg_path);
                    $this->request->data['User']["coverimage"] = $filename;
                    if (!empty($user["User"]["coverimage"]) && file_exists($uploadPath.$user["User"]["coverimage"])) {
                        unlink($uploadPath.$user["User"]["coverimage"]);
                    }
                    if ($this->User->save($this->request->data)) {
                        $data = array(
                            "Ack" => 1,
                            "msg" => "profile picture changed successfully",
                            "coverimage" => $SITE_URL . 'coverimage/' . $filename
                        );
                    } else {
                        $data = array(
                            "Ack" => 0,
                            "msg" => "Internal error"
                        );
                    }
                    
                } else {
                    $data = array(
                        "Ack" => 0,
                        "msg" => "Invalid image type"
                    );
                }
            }
            echo json_encode($data);
           
        }
         exit;
    }
    
    public function uploadlisence()
    {
        $SITE_URL = Configure::read("SITE_URL");
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $id   = $this->request->data["id"];
            $user = $this->User->find("first", array(
                "fields" => array(
                    "User.lisence"
                ),
                "conditions" => array(
                    "User.id" => $id
                )
            ));
            if (!empty($_FILES['lisence']['name'])) {
                $this->request->data['User']["id"] = $this->request->data["id"];
                $pathpart                          = pathinfo($_FILES['lisence']['name']);
                $ext                               = $pathpart['extension'];
                $extensionValid                    = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "lisence/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($_FILES['lisence']['tmp_name'], $full_flg_path);
                    $this->request->data['User']["lisence"] = $filename;
                    if (!empty($user["User"]["lisence"]) && file_exists($uploadPath.$user["User"]["lisence"])) {
                        unlink($uploadPath.$user["User"]["lisence"]);
                    }
                    if ($this->User->save($this->request->data)) {
                        $data = array(
                            "Ack" => 1,
                            
                        );
                    } else {
                        $data = array(
                            "Ack" => 0,
                            "msg" => "Internal error"
                        );
                    }
                    
                } else {
                    $data = array(
                        "Ack" => 0,
                        "msg" => "Invalid image type"
                    );
                }
            }
            
            echo json_encode($data);
           
        }
         exit;
    }
    
    
    public function saveprofile()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            unset($jsonData->coverimage);
            unset($jsonData->image);
            unset($jsonData->provide_service);
            foreach ($jsonData as $key => $json)
            {
                if($key=="address")
                {
                $location                                        = $jsonData->address;
                $prepAddr                                        = str_replace(' ', '+', $location);
                $url                                             = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=true');
                $output                                          = json_decode($url);
                $lat                                             = $output->results[0]->geometry->location->lat;
                $lang                                            = $output->results[0]->geometry->location->lng;
                $this->request->data['User']['lat']              = $lat;
                $this->request->data['User']['lang']             = $lang;
                }
                $this->request->data["User"][$key]=$json;
            }
            try {
                
                $this->User->save($this->request->data);
                $data=array("Ack"=>1);
                echo json_encode($data);    
            } catch (Exception $ex) {
                print_r($ex);    
            }
            
            
            
            exit;
            

            
            
        }
        
    }
    
    
    
    
    public function logout_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');
            $user_id  = $jsonData->user_id;
            
            $this->request->data['User']['id']          = $user_id;
            $this->request->data['User']['is_loggedin'] = 'N';
            $this->request->data['User']['logout_time'] = gmdate("Y-m-d H:i:s");
            if ($this->User->save($this->request->data)) {
                $data = array(
                    'Ack' => 1
                );
            }
            
            echo json_encode($data);
            exit;
        }
    }
    public function getassignedprodetails()
    {
        $this->loadModel('Rating');
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData                  = $this->request->input('json_decode');
            $user_id                   = $jsonData->provider_id;
            $from_id                   = $jsonData->user_id;
            $options                   = array(
                "conditions" => array(
                    "User.id" => $user_id
                )
            );
            $user                      = $this->User->find("first", $options);
            $SITE_URL                  = Configure::read("SITE_URL");
            $user['User']['userimage'] = !empty($user['User']['image']) ? $SITE_URL . 'user_images/' . $user['User']['image'] : $SITE_URL . 'nouser.png';
            
            //for getting previous rating...................................
            $this->Rating->recursive = '-1';
            $quoteDetail             = $this->Rating->find("first", array(
                'conditions' => array(
                    "Rating.to_id" => $user_id,
                    "Rating.from_id" => $from_id
                )
            ));
            //print_r($quoteDetail);
            if (!empty($quoteDetail)) {
                $user['User']['quotePosted'] = $quoteDetail['Rating'];
            } else {
                $user['User']['quotePosted'] = "";
            }
            $data = array(
                'Ack' => 1,
                'Detail' => $user['User']
            );
            echo json_encode($data);
            exit;
        }
    }
    
    function geo_ip()
    {
        $ip    = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; // the IP address to query
        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        pr($query);
        exit;
        
        
        
    }
    
    function service_location($id = null)
    {
        $this->User->recursive = -1;
        $options               = array(
            "conditions" => array(
                "User.id" => $id
            ),
            "fields" => array(
                "User.service_location",
                "User.id"
            )
        );
        $user                  = $this->User->find("first", $options);
        $data                  = array(
            "Ack" => 1,
            "response" => $user["User"]
        );
        echo json_encode($data);
        exit;
        
        
        
    }
    
    function save_serviceloc()
    {
        $this->loadModel("Country");
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData                                        = $this->request->input('json_decode');
            $otp                                             = rand(1111,9999); 
            $this->request->data['User']['service_lat']      = $jsonData->service_lat;
            $this->request->data['User']['service_lang']     = $jsonData->service_lang;
            $this->request->data['User']['service_location'] = $jsonData->service_location;
            $this->request->data['User']['ph_otp']           = $otp;
            $this->request->data['User']['service_redious']  = $jsonData->service_redious;
            $this->request->data['User']['id']               = $jsonData->id;
            $countrycode                                     = $jsonData->countrycode;
            try {
                $this->User->save($this->request->data);
                $user=$this->User->find("first",array("conditions"=>array("User.id"=>$jsonData->id),"fields"=>array("User.mobile_no","User.country_id")));
                $country=$this->Country->find("first",array("conditions"=>array("Country.id"=>$user["User"]["country_id"])));
                $phone_no="+".$country["Country"]["isd_code"].$user["User"]["mobile_no"];
                $this->sendsms_mms("Your verification code is ".$otp,$phone_no);
                
                
                $data = array(
                    "Ack" => 1,
                    "ph_otp"=>$otp
                );
                
            }
            catch (Exception $ex) {
                
                $data = array(
                    "Ack" => 0
                );
            }
            
            
            echo json_encode($data);
            exit;
        }
        
        
        
    }
    
    
    function user_profile($id = null)
    {
        $SITE_URL = Configure::read("SITE_URL");
        $this->loadModel("Category");
        $options                    = array(
            "conditions" => array(
                "User.id" => $id
            ),
            "fields" => array(
                "User.id",
                "User.name",
                "User.business_name",
                "User.image",
                "User.coverimage",
                "User.avg_score",
                "User.service_location",
                "User.address",
                "User.about_me",
                "User.mobile_no",
                "User.lisence",
                "User.cat_id",
                "User.service_lat",
                "User.service_lang",
                "User.service_location"

            )
        );
        $user                       = $this->User->find("first", $options);
        $user["User"]["image"]      = !empty($user["User"]["image"]) ? $SITE_URL . 'user_images/' . $user["User"]["image"] : $SITE_URL . 'nouser.png';
        $user["User"]["coverimage"] = !empty($user["User"]["coverimage"]) ? $SITE_URL . 'coverimage/' . $user["User"]["coverimage"] : $SITE_URL . 'default_cover.jpg';
        if (!empty($user["User"]["cat_id"])) {
            $cat_arr    = explode(",", $user["User"]["cat_id"]);
            $categories = $this->Category->find("all", array(
                "fields" => array(
                    "Category.id",
                    "Category.name"
                ),
                "conditions" => array(
                    "Category.id" => $cat_arr
                )
            ));
            foreach ($categories as $key=> $cat) {
                if($key==0)
                {
                    $user["User"]["main_service"]=$cat["Category"]["name"];
                }
                $cat_list[] = $cat['Category'];
            }
        }
        $user["User"]["provide_service"] = !empty($cat_list) ? $cat_list : array();
        $user["User"]["remain_service"]  = (int) count($cat_list) - 4;
        $data                            = array(
            "Ack" => 1,
            "response" => $user["User"]
        );
        
        echo json_encode($data);
        exit;
        
    }
    
    function testsms()
    {
        $this->sendsms_mms('Hi Karun',"+919733459151");
        exit;
    }
    
    function testpush()
    {
        $registatoin_ids=array("dSxDUdnsgPY:APA91bHzcs3lSYS0TDGV21WrP1W-SoGhAy8VeXBgsNHYv4nk5bUpMpYkaRW8pXVBDMLJ8XNqS3CNB9QJ1Li25hmiSCcbiIO4FgICEi-dPvGS9o8t5S7rOKgU62xWTZmw2lNlVFuF5055");
        $totalmsgarray = array("message" => "Test Push","title"=>"Post Job","vibrate"=>1,"sound"=>1
                               );
        $this->android_push($registatoin_ids,$totalmsgarray);
        exit;
    }
    
    public function sendLinkEmail()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode');           
            $email = $jsonData->email;
            $user_id = $jsonData->id;
            $email = explode(',',$email);
            //print_r($email);
            if(!empty($email)){
                $WEB_URL = Configure::read("WEB_URL");
                $link = $WEB_URL.'prodetail/'.$user_id;
                $this->loadModel("EmailTemplate");
                $this->loadModel("Setting");
                $setting    = $this->Setting->find('first', array(
                    'conditions' => array(
                        'Setting.id' => 1
                    )
                ));
                $adminEmail = $setting['Setting']['site_email'];
                $tpl        = $this->EmailTemplate->find('first', array(
                    'conditions' => array(
                        'EmailTemplate.id' => 11
                    )
                ));
                $flag = 1;
                $not_register_email = '';
                foreach($email as $email_val){
                    if(!empty($email_val)){
                        $options  = array(
                            'conditions' => array(
                                'User.email' => $email_val
                            )
                        );
                        $user     = $this->User->find('first', $options);
                        if($user){
                            $msg_body = str_replace(array(
                                '[NAME]','[LINK]'
                            ), array(
                                $user['User']['name'],$link
                            ), $tpl['EmailTemplate']['content']);
                            try {
                                $this->send_email($adminEmail, $user['User']['email'], $tpl['EmailTemplate']['subject'], $msg_body);
                            } catch (Exception $ex) {
                                print_r($ex->getMessage());
                            }
                            
                    
                        }else{
                            $flag = 0;
                            if($not_register_email != ''){
                                $not_register_email = $not_register_email.', '.$email_val;
                            }else{
                                $not_register_email = $email_val;
                            }
                        }
                    }
                }
                if($flag == 1){
                    $data = array(
                        "Ack" => 1,
                        "response" => 'Email successfully sent.'
                    );
                }else{
                    //echo $not_register_email;
                    $data = array(
                        "Ack" => 0,
                        "response" => 'Invalid email : '.$not_register_email
                    );
                }
            }else{
                $data = array(
                        "Ack" => 0,
                        "response" => 'Please enter email'
                    );
            }
            
            echo json_encode($data);
            
            exit;
            

            
            
        }
        
    }
    public function saveReview()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData = $this->request->input('json_decode'); 
            $comment = $jsonData->comment;
            $from_id = $jsonData->from_id;
            $to_id = $jsonData->to_id;
            $from_user = $this->User->find('first',array('conditions'=>array('User.id'=>$from_id),'recursive'=>-1));
            $to_user = $this->User->find('first',array('conditions'=>array('User.id'=>$to_id),'recursive'=>-1));
            $reviewData = array();
            $reviewData['Rating']['from_id'] = $from_id;
            $reviewData['Rating']['to_id'] = $to_id;
            $reviewData['Rating']['review_from_name'] = $from_user['User']['name'];
            $reviewData['Rating']['review_to_name'] = $to_user['User']['name'];
            $reviewData['Rating']['lead_id'] = 0;
            $reviewData['Rating']['category_id'] = 0;
            $reviewData['Rating']['work_quality'] = 0;
            $reviewData['Rating']['resposiveness'] = 0;
            $reviewData['Rating']['value_money'] = 0;
            $reviewData['Rating']['timely_response'] = 0;
            $reviewData['Rating']['avg_score'] = 0;
            $reviewData['Rating']['status'] = 0;
            $reviewData['Rating']['comment'] = $comment;
            $reviewData['Rating']['ratting_date']= gmdate('Y-m-d H:i:s');
            $reviewData['Rating']['is_read']= 0;
            $reviewData['Rating']['is_approved']= 0;
            $this->loadModel('Rating');
            $this->Rating->create();
            if($this->Rating->save($reviewData)){
                $data = array(
                    "Ack" => 1,
                    "response" => 'Review successfully submitted'
                );
            }else{
                $data = array(
                    "Ack" => 0,
                    "response" => 'Error occurred please try again'
                );
            }
            echo json_encode($data);
            
            exit;
        }
    }
        
//    $this->request->data['Rating']['from_id']=$jsonData->from_id;
//                $this->request->data['Rating']['to_id']=$jsonData->to_id;
//                $this->request->data['Rating']['review_from_name']=!empty($jsonData->review_from_name)?$jsonData->review_from_name:'';
//                $this->request->data['Rating']['review_to_name']=!empty($jsonData->review_to_name)?$jsonData->review_to_name:'';
//                $this->request->data['Rating']['work_quality']=!empty($jsonData->work_quality)?$jsonData->work_quality:'';
//                $this->request->data['Rating']['resposiveness']=!empty($jsonData->resposiveness)?$jsonData->resposiveness:0;
//                $this->request->data['Rating']['value_money']=!empty($jsonData->value_money)?$jsonData->value_money:0;
//                $this->request->data['Rating']['timely_response']=!empty($jsonData->timely_response)?$jsonData->timely_response:0;
//                $this->request->data['Rating']['avg_score']=!empty($jsonData->avg_score)?$jsonData->avg_score:0;
//                $this->request->data['Rating']['comment']=!empty($jsonData->comment)?$jsonData->comment:'';
//                $this->request->data['Rating']['lead_id']=!empty($lead_id)?$lead_id:'';
//                $this->request->data['Rating']['ratting_date']= gmdate('Y-m-d H:i:s');
//                $this->Rating->create();
//                $this->Rating->save($this->request->data);
    
}
