<?php
App::uses('AppController', 'Controller');
/**
 * CmsPages Controller
 *
 * @property CmsPage $CmsPage
 * @property PaginatorComponent $Paginator
 */
class BusinessesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	var $uses = array('Business');
/**
 * index method
 *
 * @return void
 */
 
 
  public function BusinessaddUpdateApi(){
		 $this->autoRender = false;
                 $uid=$_REQUEST['user_id'];
		 $this->request->data['Business']['name']=$_REQUEST['name'];
		 $this->request->data['Business']['email']=$_REQUEST['email'];
		 $this->request->data['Business']['ph_no']=$_REQUEST['ph_no'];
		 $this->request->data['Business']['address']=$_REQUEST['address'];
                 $this->request->data['Business']['cat_id']=$_REQUEST['cat_id'];
                 $this->request->data['Business']['doc_1']=$_FILES['doc_1'];
                 $this->request->data['Business']['doc_2']=$_FILES['doc_2'];
                 $this->request->data['Business']['doc_3']=$_FILES['doc_3'];


               // print_r($this->request->data);
               //exit;

		if ($this->request->is('post')) {
                $options=array("conditions"=>array("Business.user_id"=>$uid));
                $busin=$this->Business->find("first",$options);
              
            


                   if(!empty($this->request->data['Business']['doc_1']['name'])){
                    $pathpart=pathinfo($this->request->data['Business']['doc_1']['name']);
                    $ext=$pathpart['extension'];
                    //$extensionValid = array('jpg','jpeg','png','gif');
                    
                    $uploadFolder = "busin_images/";
                    $uploadPath = WWW_ROOT . $uploadFolder;
                    $filename =uniqid().'.'.$ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Business']['doc_1']['tmp_name'],$full_flg_path);
                   
                   }
                   else{
                    $filename=!empty($busin['Business']['doc_1'])?$busin['Business']['doc_1']:'';
                   }
                   $this->request->data['Business']['doc_1'] = $filename;


                   if(!empty($this->request->data['Business']['doc_2']['name'])){
                    $pathpart1=pathinfo($this->request->data['Business']['doc_2']['name']);
                    $ext1=$pathpart1['extension'];
                 
                   
                    $uploadFolder1 = "busin_images/";
                    $uploadPath1 = WWW_ROOT . $uploadFolder1;
                    $filename1 =uniqid().'.'.$ext1;
                    $full_flg_path1 = $uploadPath1 . '/' . $filename1;
                    move_uploaded_file($this->request->data['Business']['doc_2']['tmp_name'],$full_flg_path1);
                   
                   }
                   else{
                    $filename1=!empty($busin['Business']['doc_2'])?$busin['Business']['doc_2']:'';
                   }
                   $this->request->data['Business']['doc_2'] = $filename1;



                    if(!empty($this->request->data['Business']['doc_3']['name'])){
                    $pathpart2=pathinfo($this->request->data['Business']['doc_3']['name']);
                    $ext2=$pathpart2['extension'];
                   
                    $uploadFolder2 = "busin_images/";
                    $uploadPath2 = WWW_ROOT . $uploadFolder2;
                    $filename2 =uniqid().'.'.$ext2;
                    $full_flg_path2 = $uploadPath2 . '/' . $filename2;
                    move_uploaded_file($this->request->data['Business']['doc_3']['tmp_name'],$full_flg_path2);
                    
                   
                    }
                   else{
                    $filename2=!empty($busin['Business']['doc_3'])?$busin['Business']['doc_3']:'';
                   }
                   $this->request->data['Business']['doc_3'] = $filename2;

		   $this->request->data['Business']['status'] = 1;
		   $this->request->data['Business']['user_id'] = $uid;
			//pr($this->request->data);
			//exit;
			
		if(empty($busin)){	
		   $this->Business->create();
		   }else{
		    $this->request->data['Business']['id'] = $busin['Business']['id'];
		   }

		   if ($this->Business->save($this->request->data)) {

		  

                        $ack=array('ack'=>1,'msg'=>'Business Details has been saved');
                        echo json_encode($ack);
                   }
			}
			 else {
                        $ack=array('ack'=>0,'msg'=>'Your registration is not successfull');
                        echo json_encode($ack);
		    }


                

                
		    
		}
		
		
   public function business_det()
        {
         $site_url=Configure::read("SITE_URL");  
         if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $user_id=$jsonData->user_id;
         
          $options=array("conditions"=>array("Business.user_id"=>$user_id));
          $user=$this->Business->find("first",$options);
          
          
         

          $user['Business']['doc_1_url']= !empty($user['Business']['doc_1'])?$site_url."busin_images/".$user['Business']['doc_1']:$site_url.'noimage.png';
          $user['Business']['doc_2_url']= !empty($user['Business']['doc_2'])?$site_url."busin_images/".$user['Business']['doc_2']:$site_url.'noimage.png';
          $user['Business']['doc_3_url']= !empty($user['Business']['doc_3'])?$site_url."busin_images/".$user['Business']['doc_3']:$site_url.'noimage.png';
          
          $data=array('Ack'=>1,'response'=>$user['Business']);
          echo json_encode($data);exit;

         }

        }		
		
		
		
	public function index() {
		$this->CmsPage->recursive = 0;
		$this->set('content', $this->Paginator->paginate());
	}
	
	
	public function admin_index() {	
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $title_for_layout = 'CmsPage List';
            $this->CmsPage->recursive = 0;
            $this->set('contents', $this->Paginator->paginate());
            $this->set(compact('title_for_layout'));
	}
	
	

	public function admin_view($id = null) {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $title_for_layout = 'CmsPage View';
            if (!$this->CmsPage->exists($id)) {
                    throw new NotFoundException(__('Invalid CmsPage'));
            }
            $options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
            $content = $this->CmsPage->find('first', $options);		
            $this->set(compact('title_for_layout','content'));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			
			$this->CmsPage->create();
			
			if ($this->CmsPage->save($this->request->data)) {
				$this->Session->setFlash('The content has been saved.','default', array('class' => 'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The content could not be saved. Please, try again.'));
			}
			
		
	}

	}


	public function admin_edit($id = null) {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            if (!$this->CmsPage->exists($id)) {
                    throw new NotFoundException(__('Invalid content'));
            }
            if ($this->request->is(array('post', 'put'))) {
				
			
			
                    if ($this->CmsPage->save($this->request->data)) {
                            $this->Session->setFlash('The content has been saved.','default', array('class' => 'success'));
						return $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Session->setFlash(__('The content could not be saved. Please, try again.'));
                    }
            
			
				
		}       else 
				{
						$options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
						$this->request->data = $this->CmsPage->find('first', $options);
				}
				
	}



	public function admin_delete($id = null) {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $this->CmsPage->id = $id;
            if (!$this->CmsPage->exists()) {
                    throw new NotFoundException(__('Invalid category'));
            }
            if ($this->CmsPage->delete($id)) {
                    $this->Session->setFlash('The content has been deleted.','default', array('class' => 'success'));
            } else {
                    $this->Session->setFlash(__('The content could not be deleted. Please, try again.'));
            }
            return $this->redirect(array('action' => 'index'));
	}
	
	/*public function about_us(){						
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 1));
		$content = $this->CmsPage->find('first', $option);
		$this->set(compact('title_for_layout', 'About Us'));
		$this->set(compact('content', $content));
	}
	
	public function terms_of_use(){						
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 2));
		$content = $this->CmsPage->find('first', $option);		           
		$this->set(compact('content', $content));
	}
	
	public function privacy_policy(){						
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 3));
		$content = $this->CmsPage->find('first', $option);		           
		$this->set(compact('content', $content));
	}
	
	public function review_guidelines(){						
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 4));
		$content = $this->CmsPage->find('first', $option);		           
		$this->set(compact('content', $content));
	}
      
	public function view_palans(){
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 5));
		$content = $this->CmsPage->find('first', $option);
		$this->set(compact('content', $content));
		
	}
	
	public function how_it_works(){
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 6));
		$content = $this->CmsPage->find('first', $option);
		$this->set(compact('content', $content));
		
	}
	public function contact_us(){
		if ($this->request->is('post')) {
			$name = $this->request->data['Contact']['name'];
			$email = $this->request->data['Contact']['email_address'];			
			$subject = $this->request->data['Contact']['subject'];
			$message = $this->request->data['Contact']['message'];
			
			$this->Contact->save($this->request->data);
			$contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
			if($contact_email){
				$adminEmail = $contact_email['Setting']['site_email'];
				
			} else {
				$adminEmail = 'nits.santanupatra@gmail.com';
			}			
			$content = '<header> <section class="middle_content" style="width: 100%; padding: 50px 0;">
						<div class="container" style="width:100%">
							<div class="row" style="width: 100%; padding:0 15px; margin: 0 auto">
								<div class="col-md-12" style="width: 100%; padding: 0 15px;">
									<p>
										Name:'.$name.'
									</p>
									<p>
										Email:'.$email.'
									</p>
									<p>
										'.$message.'
									</p>
									<p>&nbsp;
										</p>
								</div>
							</div>
						</div>
					</section></header>';			
			$this->php_mail($adminEmail,$email,$subject,$content);			
			
			$this->Session->setFlash('Email send successfully.', 'default', array('class' => 'success'));
		}
		
	}*/
	
	
	/*-------------------------------------------------TravelBuds Api------------------------------------------------------*/
       
	  
	    public function termsofuseApi(){
	    $this->autoRender = false;						
		$option = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 3));
		$content = $this->CmsPage->find('first', $option);
		//$this->set(compact('content', $content));
		if(count($content)>0)
		{
 		  $data=array($content['CmsPage']['page_description']);
		
		 }else{
		  $data=array('Here is no content');
		 
		 }
		 //$data=array('ack'=>0,'details'=>'Here is no content');
		 echo json_encode($data);
	     }
		 
		 public function aboutApi(){
	    $this->autoRender = false;						
		$option1 = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 13));
		$content1 = $this->CmsPage->find('first', $option1);
		//$this->set(compact('content', $content));
		if(count($content1)>0)
		{
 		   $data1=array($content1['CmsPage']['page_description']);
		
		 }else{
		  $data1=array('Here is no content');
		 
		 }
		 //$data=array('ack'=>0,'details'=>'Here is no content');
		 echo json_encode($data1);
	   }
	
	    public function helpApi(){
	    $this->autoRender = false;						
		$option2 = array('conditions' => array('CmsPage.'.$this->CmsPage->primaryKey => 14));
		$content2 = $this->CmsPage->find('first', $option2);
		//$this->set(compact('content', $content));
		if(count($content2)>0)
		{
 		   $data2=array($content2['CmsPage']['page_description']);
		
		 }else{
		  $data2=array('Here is no content');
		 
		 }
		 //$data=array('ack'=>0,'details'=>'Here is no content');
		 echo json_encode($data2);
	}
	
	
	
	
}
