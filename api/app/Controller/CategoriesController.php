<?php
App::uses('AppController', 'Controller');
/**
 * CarCategories Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController
{
    
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    
    
    /**
     * index method
     *
     * @return void
     */
    public function index()
    {
        $this->EmailTemplate->recursive = 0;
        $this->set('emailTemplates', $this->Paginator->paginate());
    }
    
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    
    
    /**
     * add method
     *
     * @return void
     */
    public function admin_add()
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Category']['image']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                    $this->request->data['Category']['image'] = $filename;
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = '';
                $this->request->data['Category']['image'] = $filename;
            }
             if (!empty($this->request->data['Category']['app_image']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['app_image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['app_image']['tmp_name'], $full_flg_path);
                    $this->request->data['Category']['app_image'] = $filename;
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = '';
                $this->request->data['Category']['app_image'] = $filename;
            }
            if (!empty($this->request->data['Category']['bigimage']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['bigimage']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/bigimage/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['bigimage']['tmp_name'], $full_flg_path);
                    $this->request->data['Category']['bigimage'] = $filename;
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = '';
                $this->request->data['Category']['bigimage'] = $filename;
            }
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $last_id=$this->Category->getLastInsertID();
                $this->Category->id = $last_id;
                $this->Category->saveField('sl_no',$last_id);
                $this->Session->setFlash('The category has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
    }
    
    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->EmailTemplate->exists($id)) {
            throw new NotFoundException(__('Invalid email template'));
        }
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            if ($this->EmailTemplate->save($this->request->data)) {
                $this->Session->setFlash(__('The email template has been saved.'));
                return $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
            }
        } else {
            $options             = array(
                'conditions' => array(
                    'EmailTemplate.' . $this->EmailTemplate->primaryKey => $id
                )
            );
            $this->request->data = $this->EmailTemplate->find('first', $options);
        }
    }
    
    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid community'));
        }
        //$this->request->onlyAllow('post', 'delete');
        if ($this->Category->delete()) {
            //$this->UserImage->delete()
            $this->Session->setFlash('The category has been deleted.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
        }
        return $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    
    public function admin_index()
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $title_for_layout          = 'Category List';
        $this->paginate            = array(
            'order' => array(
                'Category.sl_no' => 'asc'
            ),
            "limit" => 70
        );
        //$this->Carcategory->recursive = 0;
        $this->Paginator->settings = $this->paginate;
        $this->set('carcategory', $this->Paginator->paginate());
        $this->set(compact('title_for_layout'));
    }
    public function admin_saveorder() {
    $userid   = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if (!isset($is_admin) && $is_admin == '') {
        $this->redirect('/admin');
    }   
    $categories=$this->request->data["category"];
    $sl_no=1;
    foreach ($categories as $category)
    {
        $this->Category->id = $category;
        $this->Category->saveField('sl_no', $sl_no);
        $sl_no++;
    }
    echo "1";
    exit;
    
        
    }
    
    public function categorylist_service()
    {
            $jsonData = $this->request->input('json_decode');
            $user_id = !empty($jsonData->user_id)?$jsonData->user_id:'';
            $catArr=array();
            if(isset($user_id))
            {
            $this->loadModel('User');
            $this->User->recursive = '-1';
            $user = $this->User->find("first", array(
                'conditions' => array(
                    "User.id" => $user_id
                )
            ));
            if(!empty($user))
            {
                $catArr=explode(",",$user['User']['cat_id']);
            }
            }
        $categories = $this->Category->find("all", array(
            'conditions' => array(
                "Category.parent_id" => 0,"Category.is_active" => 1,
            ),
            'order' => 'Category.sl_no ASC'
        ));
        //pr($categories);exit;
        $SITE_URL   = Configure::read("SITE_URL");
        foreach ($categories as $category) {
            $selected=0;
             if(isset($user_id))
            {
            if (in_array($category['Category']['id'], $catArr))
            {
                $selected=1;
            }
            }
            $list_categories[] = array(
                "id" => $category['Category']['id'],
                "name" => $category['Category']['name'],
                "image" => !empty($category['Category']['image']) ? $SITE_URL . 'category_images/' . $category['Category']['image'] : $SITE_URL . 'noimage.png',
                "status" => $category['Category']['status'],
                "selected" => $selected
            );
        }
        
        
        
        if (!empty($list_categories)) {
            $data = array(
                'Ack' => 1,
                'categoryList' => $list_categories
            );
        } else {
            $data = array(
                'Ack' => 0,
                'categoryList' => ""
            );
        }
        echo json_encode($data);
        exit;
        
        
    }
    
    public function sub_categorylist_service()
    {
            $jsonData = $this->request->input('json_decode');
            $user_id = !empty($jsonData->user_id)?$jsonData->user_id:'';
            $catArr=array();
            if(isset($user_id))
            {
            $this->loadModel('User');
            $this->User->recursive = '-1';
            $user = $this->User->find("first", array(
                'conditions' => array(
                    "User.id" => $user_id
                )
            ));
            if(!empty($user))
            {
                $catArr=explode(",",$user['User']['cat_id']);
            }
            }
        $categories = $this->Category->find("all", array(
            'conditions' => array(
                "Category.parent_id !=" => 0,"Category.is_active" => 1,
            ),
            'order' => 'Category.sl_no ASC'
        ));
        //pr($categories);exit;
        $SITE_URL   = Configure::read("SITE_URL");
        foreach ($categories as $category) {
            $selected=0;
             if(isset($user_id))
            {
            if (in_array($category['Category']['id'], $catArr))
            {
                $selected=1;
            }
            }
            $list_categories[] = array(
                "id" => $category['Category']['id'],
                "name" => $category['Category']['name'],
                "image" => !empty($category['Category']['image']) ? $SITE_URL . 'category_images/' . $category['Category']['image'] : $SITE_URL . 'noimage.png',
                "status" => $category['Category']['status'],
                "selected" => $selected
            );
        }
        
        
        
        if (!empty($list_categories)) {
            $data = array(
                'Ack' => 1,
                'categoryList' => $list_categories
            );
        } else {
            $data = array(
                'Ack' => 0,
                'categoryList' => ""
            );
        }
        echo json_encode($data);
        exit;
        
        
    }
    
    public function userprovideservice($user_id=null)
    {
        $selected=array();
        $this->loadModel('User');
        $this->User->recursive = '-1';
        $user = $this->User->find("first", array(
            'conditions' => array(
             "User.id" => $user_id
            ),
            "fields"=>array("User.id","User.cat_id"),
        ));
        $selected=explode(",",$user['User']['cat_id']);
          
        $categories = $this->Category->find("list", array(
            'conditions' => array(
                "Category.is_active" => 1,
                "Category.parent_id >" => 0
            ),
            'order' => array('Category.name'=>"asc")
        ));
       
        if(!empty($categories))
        {
            $data=array("Ack"=>1,"categories"=>$categories,"service"=>$selected);
        }
        else
        {
            $data=array("Ack"=>0);
        }
        echo json_encode($data);
        exit;
       
       
    }
    
     


    public function maincategory_service()
    {
        
        
        $categories = $this->Category->find("all", array(
            'conditions' => array(
                "Category.is_active" => 1
            ),
            'order' => array(
                'Category.name' => 'ASC'
            )
        ));
        //pr($categories);exit;
        $SITE_URL   = Configure::read("SITE_URL");
        foreach ($categories as $category) {
            $list_categories[] = array(
                "id" => $category['Category']['id'],
                "name" => ucwords($category['Category']['name']),
                "image" => !empty($category['Category']['image']) ? $SITE_URL . 'category_images/' . $category['Category']['image'] : $SITE_URL . 'noimage.png',
                "status" => $category['Category']['status']
            );
        }
        
        
        
        if (!empty($list_categories)) {
            $data = array(
                'Ack' => 1,
                'categoryList' => $list_categories
            );
        } else {
            $data = array(
                'Ack' => 0,
                'categoryList' => ""
            );
        }
        echo json_encode($data);
        exit;
        
        
    }
    
    
    public function subcategory_service($parent_id)
    {
        
        
        $categories = $this->Category->find("all", array(
            'conditions' => array(
                "Category.is_active" => 1,
                "Category.parent_id" => $parent_id
            ),
            'order' => array(
                'Category.name' => 'ASC'
            )
        ));
        //pr($categories);exit;
        $SITE_URL   = Configure::read("SITE_URL");
        foreach ($categories as $category) {
            $list_categories[] = array(
                "id" => $category['Category']['id'],
                "name" => $category['Category']['name'],
                "image" => !empty($category['Category']['image']) ? $SITE_URL . 'category_images/' . $category['Category']['image'] : $SITE_URL . 'noimage.png',
                "status" => $category['Category']['status']
            );
        }
        
        
        
        if (!empty($list_categories)) {
            $data = array(
                'Ack' => 1,
                'categoryList' => $list_categories
            );
        } else {
            $data = array(
                'Ack' => 0,
                'categoryList' => ""
            );
        }
        echo json_encode($data);
        exit;
        
        
    }
    
    
    
    
    public function filter_categoryserv($name = null)
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $jsonData   = $this->request->input('json_decode');
            $name  =$this->request->data('name');

          
            $categories = $this->Category->find("all", array(
                'conditions' => array(
                    "Category.is_active" => 1,
                    "Category.name LIKE" => '%' . $name . '%'
                )
            ));
            $SITE_URL   = Configure::read("SITE_URL");
            foreach ($categories as $category) {
                $list_categories[] = array(
                    "id" => $category['Category']['id'],
                    "name" => $category['Category']['name'],
                    "image" => !empty($category['Category']['image']) ? $SITE_URL . 'category_images/' . $category['Category']['image'] : $SITE_URL . 'noimage.png',
                    "app_image" => !empty($category['Category']['app_image']) ? $SITE_URL . 'category_images/' . $category['Category']['app_image'] : $SITE_URL . 'noimage.png',
                    "status" => $category['Category']['status']
                );
            }
            
            if (!empty($list_categories)) {
                $data = array(
                    'Ack' => 1,
                    'categoryList' => $list_categories
                );
            } else {
                $data = array(
                    'Ack' => 0,
                    'categoryList' => ""
                );
            }
            echo json_encode($data);
            exit;
            
            
            
        }
        
    }
    
    
    
    
    public function admin_edit($id = null)
    {
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $this->request->data['Category']['id'] = $id;
            
            if (!empty($this->request->data['Category']['image']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = $this->request->data['Category']['img'];
            }
            $this->request->data['Category']['image'] = $filename;
            if (!empty($this->request->data['Category']['app_image']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['app_image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['app_image']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = $this->request->data['Category']['hide_image'];
            }
            $this->request->data['Category']['app_image'] = $filename;
            if (!empty($this->request->data['Category']['bigimage']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['bigimage']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/bigimage/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['bigimage']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = $this->request->data['Category']['hide_bigimage'];
            }
            $this->request->data['Category']['bigimage'] = $filename;
            
            if ($this->Category->save($this->request->data)) {
                
                $this->Session->setFlash('The category has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'index'
                ));
                
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array(
                'conditions' => array(
                    'Category.' . $this->Category->primaryKey => $id
                )
            );
            $this->request->data     = $this->Category->find('first', $options);
            $car = $this->request->data;
            //print_r($car);
            //exit;
            $this->set(compact('car'));
        }
        
    }
    
    
    
    //for subcategory part................................................................
    public function admin_addsubcat($parent_id = NULL)
    {
        
        $optionsParent = array(
            'conditions' => array(
                'Category.' . $this->Category->primaryKey => $parent_id
            )
        );
        $parentCatarr  = $this->Category->find('first', $optionsParent);
        //print_r($car);
        //exit;
        $parentCat     = $parentCatarr['Category']['name'];
        $this->set(compact('parentCat'));
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Category']['image']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = '';
            }
            $this->request->data['Category']['image']     = $filename;
            $this->request->data['Category']['parent_id'] = $parent_id;
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash('The category has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
    }
    
    
    public function admin_editsubcat($id = null)
    {
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            
            $this->request->data['Category']['id'] = $id;
            
            if (!empty($this->request->data['Category']['image']['name'])) {
                $pathpart       = pathinfo($this->request->data['Category']['image']['name']);
                $ext            = $pathpart['extension'];
                $extensionValid = array(
                    'jpg',
                    'jpeg',
                    'png',
                    'gif'
                );
                if (in_array(strtolower($ext), $extensionValid)) {
                    $uploadFolder  = "category_images/";
                    $uploadPath    = WWW_ROOT . $uploadFolder;
                    $filename      = uniqid() . '.' . $ext;
                    $full_flg_path = $uploadPath . '/' . $filename;
                    move_uploaded_file($this->request->data['Category']['image']['tmp_name'], $full_flg_path);
                } else {
                    $this->Session->setFlash(__('Invalid image type.'));
                }
            } else {
                $filename = $this->request->data['Category']['img'];
            }
            $this->request->data['Category']['image'] = $filename;
            
            if ($this->Category->save($this->request->data)) {
                
                $this->Session->setFlash('The category has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'index'
                ));
                
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array(
                'conditions' => array(
                    'Category.' . $this->Category->primaryKey => $id
                )
            );
            $this->request->data     = $this->Category->find('first', $options);
            $car = $this->request->data;
            
            $optionsParent = array(
                'conditions' => array(
                    'Category.' . $this->Category->primaryKey => $car['Category']['parent_id']
                )
            );
            $parentCatarr  = $this->Category->find('first', $optionsParent);
            //print_r($car);
            //exit;
            $parentCat     = $parentCatarr['Category']['name'];
            $this->set(compact('parentCat'));
            //print_r($car);
            //exit;
            $this->set(compact('car'));
        }
        
    }
    
    
    public function admin_indexsubcat($parent_id = NULL)
    {
        
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $title_for_layout          = 'Category List';
        $this->paginate            = array(
            'conditions' => array(
                'Category.parent_id' => $parent_id
            ),
            'order' => array(
                'Category.id' => 'desc'
            )
        );
        //$this->Carcategory->recursive = 0;
        $this->Paginator->settings = $this->paginate;
        $this->set('carcategory', $this->Paginator->paginate());
        $this->set(compact('title_for_layout'));
        
        $optionsParent = array(
            'conditions' => array(
                'Category.' . $this->Category->primaryKey => $parent_id
            )
        );
        $parentCatarr  = $this->Category->find('first', $optionsParent);
        //print_r($car);
        //exit;
        $parentCat     = $parentCatarr['Category']['name'];
        $this->set(compact('parentCat', 'parent_id'));
    }
    
    
    function professionals()
    {
        $this->loadModel("Rating");
        $this->loadModel("Lead");
        $this->loadModel("User");
        $SITE_URL      = Configure::read("SITE_URL");
        $providers     = array();
        $providers["maincat"]=array();
        $providers["professionals"]=array();
        $providers["populars"]=array();
        $options       = array(
            'fields' => array(
                "Rating.category_id"
            ),
            "group" => array(
                "Rating.category_id"
            ),
            'order' => array(
                'SUM(Rating.avg_score)' => 'desc'
            )
        );
        $professionals = $this->Rating->find("all", $options);
        foreach ($professionals as $professional) {
            $category = $this->Category->find("first", array(
                "conditions" => array(
                    "Category.id" => $professional["Rating"]["category_id"]
                )
            ));
            $category["Category"]["image"] = !empty($category["Category"]["image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["image"] : $SITE_URL . 'noimage.png';
            $category["Category"]["app_image"] = !empty($category["Category"]["app_image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["app_image"] : $SITE_URL . 'noimage.png';
            $providers['professionals'][]  = $category["Category"];
            
        }
        $options = array(
            'fields' => array(
                "Lead.category_id"
            ),
            "conditions" => array(
                "Lead.status" => 4
            ),
            "group" => array(
                "Lead.category_id"
            ),
            'order' => array(
                'COUNT(Lead.id)' => 'desc'
            )
        );
        
        
        $leads = $this->Lead->find("all", $options);
        foreach ($leads as $lead) {
            $category = $this->Category->find("first", array(
                "conditions" => array(
                    "Category.id" => $lead["Lead"]["category_id"]
                )
            ));
            $category["Category"]["image"] = !empty($category["Category"]["image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["image"] : $SITE_URL . 'noimage.png';
            $category["Category"]["app_image"] = !empty($category["Category"]["app_image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["app_image"] : $SITE_URL . 'noimage.png';
            $providers["populars"][] = $category["Category"];
        }
        
        $options = array(
            "conditions" => array(
                'Category.is_active'=>1
            ),
            "order" => array(
                "Category.name asc"
            )
        );
        
        $categories = $this->Category->find("all", $options);
        
        foreach ($categories as $category) {
            $category["Category"]["image"] = !empty($category["Category"]["image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["image"] : $SITE_URL . 'noimage.png';
            $category["Category"]["app_image"] = !empty($category["Category"]["app_image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["app_image"] : $SITE_URL . 'noimage.png';
            $category["Category"]["bigimage"] = !empty($category["Category"]["bigimage"]) ? $SITE_URL . 'category_images/bigimage/' . $category["Category"]["bigimage"] : $SITE_URL . 'noimage.png';
            $providers["maincat"][]        = $category["Category"];
        }
        
        
        
        
        $data = array(
            "response" => $providers,
            'Ack' => !empty($providers) ? 1 : 0
        );
        echo json_encode($data);
        exit; 
    }


    function catsubcat()
    {
        $this->loadModel("Rating");
        $this->loadModel("Lead");
        $this->loadModel("User");
        $SITE_URL      = Configure::read("SITE_URL");
        $providers     = array();
        $providers["maincat"]=array();
        
        $options = array(
            "conditions" => array(
                "Category.parent_id" => 0,'Category.is_active'=>1
            ),
            "order" => array(
                "Category.name asc"
            )
        );
        
        $categories = $this->Category->find("all", $options);
        
        foreach ($categories as $cat_key=>$category) {
            $image = !empty($category["Category"]["image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["image"] : $SITE_URL . 'noimage.png';
            $appimage = !empty($category["Category"]["app_image"]) ? $SITE_URL . 'category_images/' . $category["Category"]["app_image"] : $SITE_URL . 'noimage.png';
            $bigimage = !empty($category["Category"]["bigimage"]) ? $SITE_URL . 'category_images/bigimage/' . $category["Category"]["bigimage"] : $SITE_URL . 'noimage.png';
            //$providers["maincat"][]        = $category["Category"];

            $suboptions = array(
            "conditions" => array(
                "Category.parent_id" =>$category["Category"]["id"],'Category.is_active'=>1
            ),
            "order" => array(
                "Category.name asc"
            )
            );
            //print_r($suboptions);
            //exit;
            $subcategories=array();
            $subcategories = $this->Category->find("all", $suboptions);
            $subcount=count($subcategories);
            $allsubcat=array();
            foreach ($subcategories as $subcategory) {
                
                $subcategory["Category"]["image"] = !empty($subcategory["Category"]["image"]) ? $SITE_URL . 'category_images/' . $subcategory["Category"]["image"] : $SITE_URL . 'noimage.png';
                $subcategory["Category"]["app_image"] = !empty($subcategory["Category"]["app_image"]) ? $SITE_URL . 'category_images/' . $subcategory["Category"]["app_image"] : $SITE_URL . 'noimage.png';
                $subcategory["Category"]["bigimage"] = !empty($subcategory["Category"]["bigimage"]) ? $SITE_URL . 'category_images/bigimage/' . $subcategory["Category"]["bigimage"] : $SITE_URL . 'noimage.png';
                $allsubcat[] = $subcategory["Category"];
            }
            $providers["maincat"][] = array('name'=>$category["Category"]["name"],'count'=>$subcount,'subcat'=>$allsubcat);
        }
        
        $data = array(
            "response" => $providers,
            'massage' => !empty($providers) ? 'successful' : 'No data found',
            'Ack' => !empty($providers) ? 1 : 0
        );
        echo json_encode($data);
        exit; 
    }
    
    function saveproviderservice()
    {
        $this->loadModel("User");
        $SITE_URL      = Configure::read("SITE_URL");
        if ($this->request->is(array('post', 'put'))) 
        {
           $jsonData = $this->request->input('json_decode');
           $user_id=$jsonData->user_id;
           $cat_id=$jsonData->cat_id;
           $this->request->data["User"]["id"]=$user_id;
           $this->request->data["User"]["cat_id"]=$cat_id;
           if($this->User->save($this->request->data))
           {
               $data=array("Ack"=>1,"msg"=>"Category saved successfully");
           }
           else
           {
               $data=array("Ack"=>0);
           }
           
           echo json_encode($data);exit;
           
           
           

        }
        
        
        
        
    }
    
    public function subcat()
    {
        
        $categories = $this->Category->find("list", array(
            'conditions' => array(
                "Category.is_active" => 1,
                "Category.parent_id >" => 0
            ),
            'order' => 'Category.name ASC'
        ));
       
        if(!empty($categories))
        {
            $data=array("Ack"=>1,"categories"=>$categories);
        }
        else
        {
            $data=array("Ack"=>0);
        }
        echo json_encode($data);
        exit;
       
       
    }
    
    //subcatlist for ios..............................................
      public function subcat_service($user_id=null)
    {
       $this->loadModel("User");
       $user=$this->User->find("first",array("fields"=>array("User.cat_id"),"conditions"=>array("User.id"=>$user_id)));
       $cat_id=explode(",",$user["User"]["cat_id"]);
        $categories = $this->Category->find("all", array(
            'conditions' => array(
                "Category.is_active" => 1,
                "Category.parent_id >" => 0
            ),
            'order' => 'Category.name ASC'
        ));
       
        if(!empty($categories))
        {
            foreach($categories as $key =>$value)
                    {
                    $Arr[]=array("id"=>$value['Category']['id'],"name"=>$value['Category']['name'],"Value"=> in_array($value['Category']['id'], $cat_id)?1:0);
                    }
            $data=array("Ack"=>1,"categories"=>$Arr);
        }
        else
        {
            $data=array("Ack"=>0);
        }
        echo json_encode($data);
        exit;
       
       
    }
    
    
}