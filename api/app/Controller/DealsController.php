<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class DealsController extends AppController {

/**
 * Components
 *
 * @var array
 */
   
	public $components = array('Paginator');
	public function adddeal() {
            //print_r($this->request->data);
            //print_r($_FILES);
            if ($_POST) {
                    
                                                      
                    $user_id=$this->request->data["user_id"];                                            
                    if(!empty($_FILES['image']['name']))
                    {

                        $pathpart=pathinfo($_FILES['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "dealimg/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($_FILES['image']['tmp_name'],$full_flg_path);
                        $this->request->data['Deal']["image"]=$filename;
                        }
                        
                        
                    }
                    else{
                        $this->request->data['Deal']["image"]=""; 
                    }
                    
                    //print_r($this->request->data);
                        $this->request->data['Deal']["user_id"]=$this->request->data["user_id"];
                        $this->request->data['Deal']["name"]=$this->request->data["name"];
                        $this->request->data['Deal']["description"]=$this->request->data["description"];
                        $this->request->data['Deal']["old_price"]=$this->request->data["old_price"];
                        $this->request->data['Deal']["new_price"]=$this->request->data["new_price"];
                        $this->request->data['Deal']["start_date"]=$this->request->data["start_date"];
                        $this->request->data['Deal']["end_date"]=$this->request->data["end_date"];
                        //$this->request->data['Deal']["cat_id"]=$this->request->data["cat_id"];
                        $this->request->data['Deal']["lat"]=$this->request->data["lat"];
                        $this->request->data['Deal']["lang"]=$this->request->data["lng"];
                        $this->request->data['Deal']["address"]=$this->request->data["location"];
                        $this->Deal->create();
                        if ($this->Deal->save($this->request->data)) 
                        {
                           $data=array("Ack"=>1,"msg"=>"Deal added successfully");  
                           
                        }

                        else
                        {
                           $data=array("Ack"=>0,"msg"=>"Please try again");  
                        }
                        
                     
            }
            
            
            echo json_encode($data);
            
            exit;
	}
        public function index() 
        {
            $this->loadModel('Category');
            $this->loadModel('User');
           $today=date("Y-m-d");
            //$deals=$this->Deal->find("all",array("conditions"=>array("Deal.user_id"=>$user_id,"Deal.end_date >="=>$today)));
           $deals=$this->Deal->find("all",array("conditions"=>array("Deal.end_date >="=>$today,"Deal.status"=>1)));
            $SITE_URL=Configure::read("SITE_URL");
            $response=array();
            if(!empty($deals))
            {
                foreach ($deals as $deal)
                {
                    //print_r($deal);
                    $response[]=array("id"=>$deal["Deal"]["id"],"image"=>$SITE_URL.'dealimg/'.$deal["Deal"]["image"],"name"=>$deal["Deal"]["name"],"description"=>$deal["Deal"]["description"],"old_price"=>$deal["Deal"]["old_price"],"new_price"=>$deal["Deal"]["new_price"],"start_date"=>$deal["Deal"]["start_date"],"end_date"=>$deal["Deal"]["end_date"],
                      "username"=>$deal["User"]["name"],"userimage"=>$SITE_URL.'user_images/'.$deal["Deal"]["image"],"category"=>$deal["Category"]["name"]); 
                   
                }
                $data=array("Ack"=>1,"response"=>$response);
                
            }
            else
            {
                $data=array("Ack"=>0);
            }
            
         echo json_encode($data);exit;   
            
        }
        
         public function details($id=NULL) 
        {
            $this->loadModel('Category');
            $this->loadModel('User');
           $today=date("Y-m-d");
            //$deals=$this->Deal->find("all",array("conditions"=>array("Deal.user_id"=>$user_id,"Deal.end_date >="=>$today)));
           $deal=$this->Deal->find("first",array("conditions"=>array("Deal.id"=>$id)));
           //print_r($deal);
            $SITE_URL=Configure::read("SITE_URL");
            $response=array();
            if(!empty($deal))
            {
                    $response=array("id"=>$deal["Deal"]["id"],"image"=>$SITE_URL.'dealimg/'.$deal["Deal"]["image"],"name"=>$deal["Deal"]["name"],"description"=>$deal["Deal"]["description"],"old_price"=>$deal["Deal"]["old_price"],"new_price"=>$deal["Deal"]["new_price"],"start_date"=>$deal["Deal"]["start_date"],"end_date"=>$deal["Deal"]["end_date"],
                      "username"=>$deal["User"]["name"],"userimage"=>$SITE_URL.'user_images/'.$deal["Deal"]["image"],"category"=>$deal["Category"]["name"],"userid"=>$deal["User"]["id"]); 
                                
                $data=array("Ack"=>1,"response"=>$response);
                
            }
            else
            {
                $data=array("Ack"=>0);
            }
            
         echo json_encode($data);exit;   
            
        }
        
        public function mydeals($user_id=NULL) 
        {
            $this->loadModel('Category');
            $this->loadModel('User');
           $today=date("Y-m-d");
           $deals=$this->Deal->find("all",array("conditions"=>array("Deal.user_id"=>$user_id,"Deal.status"=>1)));
          
            $SITE_URL=Configure::read("SITE_URL");
            $response=array();
            if(!empty($deals))
            {
                foreach ($deals as $deal)
                {
                    //print_r($deal);
                    $response[]=array("id"=>$deal["Deal"]["id"],"image"=>$SITE_URL.'dealimg/'.$deal["Deal"]["image"],"name"=>$deal["Deal"]["name"],"description"=>$deal["Deal"]["description"],"old_price"=>$deal["Deal"]["old_price"],"new_price"=>$deal["Deal"]["new_price"],"start_date"=>$deal["Deal"]["start_date"],"end_date"=>$deal["Deal"]["end_date"],
                      "username"=>$deal["User"]["name"],"userimage"=>$SITE_URL.'user_images/'.$deal["Deal"]["image"],"category"=>$deal["Category"]["name"]); 
                   
                }
                $data=array("Ack"=>1,"response"=>$response);
                
            }
            else
            {
                $data=array("Ack"=>0);
            }
            
         echo json_encode($data);exit;   
            
        }
        
        
        
        public function delete($id=null) 
        {
            $deal=$this->Deal->find("first",array("conditions"=>array("Deal.id"=>$id)));
            unlink(WWW_ROOT.'portfolioimg/'.$deal["Deal"]["image"]);
            $this->Deal->id = $id;
            if ($this->Deal->delete()) 
            {
                $data=array("Ack"=>1);
            }
            
            echo json_encode($data);exit;


            
        }
        
        public function inactive($id=null) 
        {
            $this->Deal->id = $id;
            $this->request->data=array();
            
            $this->request->data['Deal']["status"]=0;
           
            if ($this->Deal->save($this->request->data)) 
            {
                $data=array("Ack"=>1);
            }
            else
            {
               $data=array("Ack"=>0); 
            }
            
            echo json_encode($data);exit;


            
        }
        
        public function active($id=null) 
        {
            $deal=$this->Deal->find("first",array("conditions"=>array("Deal.id"=>$id)));
            unlink(WWW_ROOT.'portfolioimg/'.$deal["Deal"]["image"]);
            $this->Deal->id = $id;
            if ($this->Deal->delete()) 
            {
                $data=array("Ack"=>1);
            }
            
            echo json_encode($data);exit;


            
        }
        
        
        //for edit deal details................................................
            public function dealdata($id=NULL) 
        {
            $this->loadModel('Category');
            $this->loadModel('User');
           $today=date("Y-m-d");
            //$deals=$this->Deal->find("all",array("conditions"=>array("Deal.user_id"=>$user_id,"Deal.end_date >="=>$today)));
           
           $deal=$this->Deal->find("first",array("conditions"=>array("Deal.id"=>$id), 'recursive'=>-1));
           //print_r($deal);
            $SITE_URL=Configure::read("SITE_URL");
            $response=array();
            if(!empty($deal))
            {
                  /*  $response=array("id"=>$deal["Deal"]["id"],"image"=>$SITE_URL.'dealimg/'.$deal["Deal"]["image"],"name"=>$deal["Deal"]["name"],"description"=>$deal["Deal"]["description"],"old_price"=>$deal["Deal"]["old_price"],"new_price"=>$deal["Deal"]["new_price"],"start_date"=>$deal["Deal"]["start_date"],"end_date"=>$deal["Deal"]["end_date"],
                      "username"=>$deal["User"]["name"],"userimage"=>$SITE_URL.'user_images/'.$deal["Deal"]["image"],"category"=>$deal["Category"]["name"]); */
                 if($deal["Deal"]["image"]!=''&&$deal["Deal"]["image"]!='NULL')
                 {
                   $deal["Deal"]["image"]=$SITE_URL.'dealimg/'.$deal["Deal"]["image"];  
                 }
                $data=array("Ack"=>1,"response"=>$deal);
                
            }
            else
            {
                $data=array("Ack"=>0,"response"=>"");
            }
            
         echo json_encode($data);exit;   
            
        }

        //for update deal......................................
        public function updatedeal() {
            //print_r($this->request->data);
            //print_r($_FILES);
            if ($_POST) {
                    
                                                      
                    $id=$this->request->data["id"];                                            
                    if(!empty($_FILES['image']['name']))
                    {

                        $pathpart=pathinfo($_FILES['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "dealimg/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($_FILES['image']['tmp_name'],$full_flg_path);
                        $this->request->data['Deal']["image"]=$filename;
                        }
                        
                        
                    }
                   
                        $this->request->data['Deal']["name"]=$this->request->data["name"];
                        $this->request->data['Deal']["description"]=$this->request->data["description"];
                        $this->request->data['Deal']["old_price"]=$this->request->data["old_price"];
                        $this->request->data['Deal']["new_price"]=$this->request->data["new_price"];
                        $this->request->data['Deal']["start_date"]=$this->request->data["start_date"];
                        $this->request->data['Deal']["end_date"]=$this->request->data["end_date"];
                        //$this->request->data['Deal']["cat_id"]=$this->request->data["cat_id"];
                        if(!empty($this->request->data["lat"]))
                        {
                        $this->request->data['Deal']["lat"]=$this->request->data["lat"];
                        }
                        if(!empty($this->request->data["lng"]))
                        {
                        $this->request->data['Deal']["lang"]=$this->request->data["lng"];
                        }
                        if(!empty($this->request->data["location"]))
                        {
                        $this->request->data['Deal']["address"]=$this->request->data["location"];
                        }
                      $this->Deal->id=$id;
                        if ($this->Deal->save($this->request->data)) 
                        {
                           $data=array("Ack"=>1,"msg"=>"Deal updated successfully");  
                           
                        }

                        else
                        {
                           $data=array("Ack"=>0,"msg"=>"Please try again");  
                        }
                        
                     
            }
            
            
            echo json_encode($data);
            
            exit;
	}

}
