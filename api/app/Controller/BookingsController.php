<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BookingsController extends AppController {

	/*function beforeFilter() {
		parent::beforeFilter();
	}*/
/**
 * Components
 *
 * @var array
 */
	public $name = 'Bookings';
	public $components = array('Session','RequestHandler','Paginator');
	var $uses = array('Booking','User','EmailTemplate','Setting');


	public function admin_index() {

          $this->Booking->recursive = 2;
          $options = array('conditions' => array('Booking.status' =>'A'));
          //$activeatt = $this->Booking->find('all', $options);

           $this->Paginator->settings = $options;
           $this->set('bookings', $this->Paginator->paginate('Booking'));
           //print_r($activeatt);
          // exit;

	}


         public function admin_view($id = null) {

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
	$this->Booking->recursive = 2;
        $options = array('conditions' => array('Booking.' . $this->Booking->primaryKey => $id));
        $this->set('books', $this->Booking->find('first', $options));
	}



}
