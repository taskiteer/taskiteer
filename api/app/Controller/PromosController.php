<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
use Twilio\Rest\Client;

/**
 * Promos Controller
 *
 * @property Promo $Promo
 * @property PaginatorComponent $Paginator
 */
class PromosController extends AppController{
    
    /**
     * Components
     *
     * @var array
     */
    public $name = 'Promos';
    public $components = array('Session', 'RequestHandler', 'Paginator');
    var $uses = array('Promo', 'EmailTemplate', 'Setting', 'Newsletter', 'Eventnotification', 'Leftsideaction', 'Contact');
    
    
    
    public function admin_index()
    {
        $title_for_layout = 'Promo Listing';
        $this->set(compact('title_for_layout'));
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
		if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->set('promos', $this->Paginator->paginate('Promo'));
    }
	
	public function admin_deactivate($id = null){
		$userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->request->data['Promo']['id']     = $id;
        $this->request->data['Promo']['status'] = 0;
        
        if ($this->Promo->save($this->request->data)) {
            $this->Session->setFlash('The promo has been deactivate.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The promo could not be deactivate. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }
	
	public function admin_activate($id = null){
		$userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->request->data['Promo']['id']     = $id;
        $this->request->data['Promo']['status'] = 1;
        
        if ($this->Promo->save($this->request->data)) {
            $this->Session->setFlash('The promo has been activate.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The promo could not be activate. Please, try again.'));
        }
        return $this->redirect(array('action' => 'admin_index'));
    }
	
	public function admin_add(){
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $title_for_layout     = 'Promo Add';
        
        if ($this->request->is('post')) {
            $options     = array(
                'conditions' => array(
                    'Promo.promo_code' => $this->request->data['Promo']['promo_code']
                )
            );
            $promoexists = $this->Promo->find('first', $options);
            if (!$promoexists) {
                $this->request->data['Promo']['status'] = 1;
                $this->request->data['Promo']['created_at'] = date('Y-m-d h:i:s');
                $this->Promo->create();
                
                if ($this->Promo->save($this->request->data)) {
                    
                    $this->Session->setFlash('The promo has been saved.', 'default', array(
                        'class' => 'success'
                    ));
                    return $this->redirect(array(
                        'action' => 'index'
                    ));
                } else {
                    $this->Session->setFlash(__('The promo could not be saved. Please, try again.', 'default', array(
                        'class' => 'error'
                    )));
                }
                
            } else {
                $this->Session->setFlash(__('Promo already exists. Please, try another.', 'default', array(
                    'class' => 'error'
                )));
            }
        }
	}
	
	public function admin_delete($id = null){
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        $this->Promo->id = $id;
        if (!$this->Promo->exists()) {
            throw new NotFoundException(__('Invalid promo'));
        }
        
        if ($this->Promo->delete()) {
            $this->Session->setFlash('The promo has been deleted.', 'default', array(
                'class' => 'success'
            ));
        } else {
            $this->Session->setFlash(__('The promo could not be deleted. Please, try again.'));
        }
        
        return $this->redirect(array(
            'action' => 'index'
        ));
        
    }
	
	public function admin_edit($id = null){
        $userid   = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if (!isset($is_admin) && $is_admin == '') {
            $this->redirect('/admin');
        }
        
        $this->request->data1 = array();
        $title_for_layout     = 'Promo Edit';
        $this->set(compact('title_for_layout'));
        if (!$this->Promo->exists($id)) {
            throw new NotFoundException(__('Invalid promo'));
        }
		$this->request->data['Promo']['created_at']=date('Y-m-d h:i:s');
		$this->request->data['Promo']['updated_at']=date('Y-m-d h:i:s');
        if ($this->request->is(array('post','put'))) {
			if ($this->Promo->save($this->request->data)) {
                $this->Session->setFlash('The promo has been saved.', 'default', array(
                    'class' => 'success'
                ));
                return $this->redirect(array(
                    'action' => 'index'
                ));
            } else {
                $this->Session->setFlash(__('The promo could not be saved. Please, try again.'));
            }
        } else {
            
            $options             = array(
                'conditions' => array(
                    'Promo.' . $this->Promo->primaryKey => $id
                )
            );
            $this->request->data = $this->Promo->find('first', $options);
        }
    }
	
	//promo is valid or not
	public function promoValidateCheck(){
		$jsonData = $this->request->input('json_decode');
		$promo=$jsonData->promo;
		if($promo){
			
			$totalPromo=$this->Promo->find('count',array('conditions'=>array('promo_code'=>$promo)));
			$promoInfo=$this->Promo->find('first',array('conditions'=>array('promo_code'=>$promo)));
			
			if($totalPromo>0 && $promoInfo['Promo']['used_times']>0){
				$data=array("Ack"=>1);
			}else{
				$data=array("Ack"=>0);
			}
		}else{
			$data=array("Ack"=>0);
		}
		echo json_encode($data);exit;
	}
}
