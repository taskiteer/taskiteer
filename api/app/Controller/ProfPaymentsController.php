<?php
App::uses('AppController', 'Controller');
/**
 * CmsPages Controller
 *
 * @property CmsPage $CmsPage
 * @property PaginatorComponent $Paginator
 */
class ProfPaymentsController extends AppController

	{
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array(

		'Paginator'
	);
	/**
	 * index method
	 *
	 * @return void
	 */
	public

	function setpay($user_id = null)
		{
		
                require_once(ROOT . '/app/Vendor' . DS  . 'stripe'.DS.'autoload.php');
		$this->loadModel("Billing");
		$this->loadModel("Setting");
                $this->loadModel("ProfBid");
                $this->loadModel("User");
                $setting=$this->Setting->find("first");
		if ($this->request->is(array(
			'post',
			'put'
		)))
			{
			$jsonData = $this->request->input('json_decode');
			$user_id = $jsonData->user_id;
			$lead_id = $jsonData->lead_id;
			$amount = $jsonData->amount;
                        //$amount = 0.5;
			$token = $jsonData->token;
			$stripe = array(
                        "secret_key"      => $setting["Setting"]["stripe_scret_key"],
                        "publishable_key" => $setting["Setting"]["stripe_public_key"]
                      );
                     
                      \Stripe\Stripe::setApiKey($stripe['secret_key']);
                     $user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id)));
                     
                      $options = array(
				"fields" => array(
					"Billing.customer_id"
				) ,
				"conditions" => array(
					"Billing.user_id" => $user_id
				)
			);
			$billing = $this->Billing->find("first", $options);
                        if(!empty($billing))
                        {
				$customerId = $billing["Billing"]["customer_id"];
                        }
                        else
                        {
                              
                              
                              try{
                                  
				$customer = \Stripe\Customer::create(array(
                                'email' => $user["User"]["email"],
                                'source'  => $token
                                        
                                        
                                        
                                        
                            ));
				$customerId = $customer->id;
                                $this->request->data["Billing"]["firstname"] = $user["User"]["name"];
                                $this->request->data["Billing"]["user_id"] = $user_id;
                                $this->request->data["Billing"]["token"] = $token;
                                $this->request->data["Billing"]["customer_id"] = $customerId;
                                $this->request->data["Billing"]["createtime"] = gmdate("Y-m-d H:i:s");  
                                $this->Billing->save($this->request->data);
                                
                              } catch (Exception $ex) {
                                  
                                  $data = array(
							"Ack" => 0,"msg"=>$ex->getMessage(),"err"=>$ex
						); 
                                  
                                  echo json_encode($data);
                                  exit;
                                  

                              }
                              
                        }
                      
                        try {
                            
                            
                            $charge = \Stripe\Charge::create(array(
                                'customer' => $customerId,
                                'amount'   => (100*$amount),
                                'currency' => 'usd'
                            ));
                            
                            $this->request->data["ProfPayment"]["transaction_id"] = $charge->id;
                            $this->request->data["ProfPayment"]["lead_id"] = $lead_id;
                            $this->request->data["ProfPayment"]["user_id"] = $user_id;
                            $this->request->data["ProfPayment"]["amount"] = $amount;
                            $this->request->data["ProfPayment"]["paid_date"] = gmdate('Y-m-d H:i:s');
                            $this->ProfPayment->save($this->request->data);
                            $bidSave = array();
                            $bidSave['ProfBid']['user_id'] = $user_id;
                            $bidSave['ProfBid']['lead_id'] = $lead_id;
                            $bidSave['ProfBid']['bid_date'] = gmdate('Y-m-d H:i:s');
                            $this->ProfBid->create();
                            $this->ProfBid->save($bidSave);
                            $data = array(
                                    "Ack" => 1,
                                    "msg"=>"Payment Successfull"
                            );
                           
                            
                        } catch (Exception $ex) {
                            
                            $data = array(
                                                       "Ack" => 0,"msg"=>$ex->getMessage(),"err"=>$ex
                                               ); 

                                 echo json_encode($data);
                                 exit;
                            
                            
                        }
                      
			
			  
			}

		echo json_encode($data);
		exit;
		}
                
                
                

	public function setpayweb($user_id = null)
		{
		
                require_once(ROOT . '/app/Vendor' . DS  . 'stripe'.DS.'autoload.php');
		$this->loadModel("Billing");
		$this->loadModel("Setting");
                $this->loadModel("ProfBid");
                $this->loadModel("User");
                $setting=$this->Setting->find("first");
		if ($this->request->is(array(
			'post',
			'put'
		)))
			{
			$jsonData = $this->request->input('json_decode');
                        //print_r($jsonData);
                        //exit;
			$user_id = $jsonData->user_id;
			$lead_id = $jsonData->lead_id;
			$amount = $jsonData->amount;
                        //$amount = 0.5;
			//$token = $jsonData->token;
			$stripe = array(
                        "secret_key"      => $setting["Setting"]["stripe_scret_key"],
                        "publishable_key" => $setting["Setting"]["stripe_public_key"]
                      );
                     
                      \Stripe\Stripe::setApiKey($stripe['secret_key']);
                     $user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id)));
                     
                      $options = array(
				"fields" => array(
					"Billing.customer_id"
				) ,
				"conditions" => array(
					"Billing.user_id" => $user_id
				)
			);
			$billing = $this->Billing->find("first", $options);
                        if(!empty($billing))
                        {
				$customerId = $billing["Billing"]["customer_id"];
                        }
                        else
                        {
                            $data = array("Ack" => 2,"msg"=>"Please save credit card details first");
                            echo json_encode($data);
                            exit;
                              
                              
//                              try{
//                                  
//				$customer = \Stripe\Customer::create(array(
//                                'email' => $user["User"]["email"],
//                                'source'  => $token
//                                        
//                                        
//                                        
//                                        
//                            ));
//				$customerId = $customer->id;
//                                $this->request->data["Billing"]["firstname"] = $user["User"]["name"];
//                                $this->request->data["Billing"]["user_id"] = $user_id;
//                                $this->request->data["Billing"]["token"] = $token;
//                                $this->request->data["Billing"]["customer_id"] = $customerId;
//                                $this->request->data["Billing"]["createtime"] = gmdate("Y-m-d H:i:s");  
//                                $this->Billing->save($this->request->data);
//                                
//                              } catch (Exception $ex) {
//                                  
//                                  $data = array(
//							"Ack" => 0,"msg"=>"Stripe Error"
//						); 
//                                  
//                                  echo json_encode($data);
//                                  exit;
//                                  
//
//                              }
                              
                        }
                      
                        try {
                            
                            
                            $charge = \Stripe\Charge::create(array(
                                'customer' => $customerId,
                                'amount'   => (100*$amount),
                                'currency' => 'usd'
                            ));
                            
                            $this->request->data["ProfPayment"]["transaction_id"] = $charge->id;
                            $this->request->data["ProfPayment"]["lead_id"] = $lead_id;
                            $this->request->data["ProfPayment"]["user_id"] = $user_id;
                            $this->request->data["ProfPayment"]["amount"] = $amount;
                            $this->request->data["ProfPayment"]["paid_date"] = gmdate('Y-m-d H:i:s');
                            $this->ProfPayment->save($this->request->data);
                            $bidSave = array();
                            $bidSave['ProfBid']['user_id'] = $user_id;
                            $bidSave['ProfBid']['lead_id'] = $lead_id;
                            $bidSave['ProfBid']['bid_date'] = gmdate('Y-m-d H:i:s');
                            $this->ProfBid->create();
                            $this->ProfBid->save($bidSave);
                            $data = array(
                                    "Ack" => 1,
                                    "msg"=>"Payment Successfull"
                            );
                           
                            
                        } catch (Exception $ex) {
                            
                            $data = array(
                                                       "Ack" => 0,"msg"=>$ex->getMessage(),"err"=>$ex,"errr"=>$ex->getMessage()
                                               ); 

                                 echo json_encode($data);
                                 exit;
                            
                            
                        }
                      
			
			  
			}

		echo json_encode($data);
		exit;
		}

	function checkpay()
		{
                $this->loadModel('Category');
                $this->loadModel('ProfBid');
                $this->loadModel('Setting');
		if ($this->request->is(array(
			'post',
			'put'
		)))
			{
			$jsonData = $this->request->input('json_decode');
			$user_id = $jsonData->user_id;
			$lead_id = $jsonData->lead_id;
                        $category_id = $jsonData->category_id;
			$options = array(
				"conditions" => array(
					"ProfPayment.user_id" => $user_id,
					"ProfPayment.lead_id" => $lead_id
				)
			);
			try
				{
                                $category_price_count = $this->Category->find("count", array("conditions"=>array("Category.id"=>$category_id,"Category.price"=>0)));
                                if($category_price_count == 0){
//                                    $bidSave = array();
//                                    $bidSave['ProfBid']['user_id'] = $user_id;
//                                    $bidSave['ProfBid']['lead_id'] = $lead_id;
//                                    $bidSave['ProfBid']['bid_date'] = gmdate('Y-m-d H:i:s');
                                    $bid_count = $this->ProfBid->find("count", array('conditions'=>array('ProfBid.user_id'=>$user_id)));
                                    $bid_setting = $this->Setting->find("first");
                                    if($bid_count >= $bid_setting['Setting']['no_of_free_bids']){
                                        $count = $this->ProfPayment->find("count", $options);
                                        $data = array(
                                                "Ack" => 1,
                                                "is_paid" => (int)$count
                                        );
                                        
                                    }else{
                                        //$this->ProfBid->create();
                                        //$this->ProfBid->save($bidSave);
                                        $data = array(
                                            "Ack" => 1,
                                            "is_paid" => 1
                                        );
                                    }
                                    
                                }else{
                                    $data = array(
                                            "Ack" => 1,
                                            "is_paid" => 1
                                    );
                                }
				
				}

			catch(Exception $ex)
				{
				$data = array(
					"Ack" => 0
				);
				}
			}

		echo json_encode($data);
		exit;
		}
                
                
        function generatetoken()
        {
            require_once (ROOT . '/app/Vendor' . DS . 'braintree' . DS . 'lib' . DS . 'Braintree.php');
            require_once (ROOT . '/app/Vendor' . DS . 'braintree' . DS . 'config.php');
            try {
                
                $clientToken = Braintree_ClientToken::generate(
                array(
                 "customerId"=>"109764366"   
                )        
            );
            echo "<pre>";    
            print_r($clientToken);
            
                
                
            } catch (Exception $ex) {
                print_r($ex);    
            }
            
            exit;
            
        }        
                
                
	}
        