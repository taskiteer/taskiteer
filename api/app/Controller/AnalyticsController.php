<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class AnalyticsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
        var $uses = array('User','Post');



/**
 * index method
 *
 * @return void
 */
	public function admin_useranalytics() {
        $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
           if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
           }    
        
        $total_user=$this->User->find('count',array('conditions'=>array('User.type'=>'U','User.is_admin'=>0)));
        $active_user=$this->User->find('count',array('conditions'=>array('User.type'=>'U','User.status'=>1,'User.is_admin'=>0)));
        $inactive_user=$this->User->find('count',array('conditions'=>array('User.type'=>'U','User.status'=>0,'User.is_admin'=>0)));
        $this->set(compact('total_user','active_user','inactive_user'));
	}
        
        public function admin_graph() 
        {
            
            $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; 

            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
            $result=array();
            if($data->status == 'success')
            {
                 $timezone = $data->timezone;
            }
            else 
            {
                $timezone="Asia/Kolkata";
            }
            //$timezone="Asia/Kolkata";
            $flag=0;

            date_default_timezone_set($timezone);
            $GMT = new \DateTimeZone('UTC');
            $last_date=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date("Y-m-1"))));
            $start_date=date('Y-m-d 00:00:00',strtotime(date('Y-m-01')));
            $end_date=date("Y-m-d 23:59:59",strtotime($last_date));
            $start=new \DateTime($start_date);
            $start->setTimezone($GMT);
            $start_time=$start->format('Y-m-d H:i:s');
            $end=new \DateTime($end_date);
            $end->setTimezone($GMT);
            $end_time=$end->format('Y-m-d H:i:s');
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
             while($start_time<=$end_time)
        {
           $options=array("conditions"=>array("User.registration_date >="=>$start_time,"User.registration_date <= "=>$initital_end,"User.type"=>'U','User.is_admin'=>0)); 
           $user_joined=$this->User->find('count',$options);
           $userTimezone = new \DateTimeZone($timezone);
            $gmtTimezone = new \DateTimeZone('GMT'); 
            $myDateTime = new \DateTime($start_time, $gmtTimezone);
            $offset = $userTimezone->getOffset($myDateTime);
            $x[]=date('d/m/Y', $myDateTime->format('U') + $offset);
            $y[]=!empty($user_joined)?$user_joined:0;
            if($user_joined>0)
            {
                $flag=1;
            }
                     
            $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  
           
           
           
        }
        
         $result=array('x'=>$x,'y'=>$y,'flag'=>$flag);
         
         echo json_encode($result);exit;
            
            
            
        }
        
        
        public function admin_provideranalytics() {
            
        $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
           if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
           }
        $total_user=$this->User->find('count',array('conditions'=>array('User.type'=>'SP','User.is_admin'=>0)));
        $active_user=$this->User->find('count',array('conditions'=>array('User.type'=>'SP','User.status'=>1,'User.is_admin'=>0)));
        $inactive_user=$this->User->find('count',array('conditions'=>array('User.type'=>'SP','User.status'=>0,'User.is_admin'=>0)));
        $this->set(compact('total_user','active_user','inactive_user'));
	}
        
        public function admin_providergraph() 
        {
            
            $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; 

            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
            $result=array();
            if($data->status == 'success')
            {
                 $timezone = $data->timezone;
            }
            else 
            {
                $timezone="Asia/Kolkata";
            }
            //$timezone="Asia/Kolkata";
            $flag=0;

            date_default_timezone_set($timezone);
            $GMT = new \DateTimeZone('UTC');
            $last_date=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date("Y-m-1"))));
            $start_date=date('Y-m-d 00:00:00',strtotime(date('Y-m-01')));
            $end_date=date("Y-m-d 23:59:59",strtotime($last_date));
            $start=new \DateTime($start_date);
            $start->setTimezone($GMT);
            $start_time=$start->format('Y-m-d H:i:s');
            $end=new \DateTime($end_date);
            $end->setTimezone($GMT);
            $end_time=$end->format('Y-m-d H:i:s');
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
             while($start_time<=$end_time)
        {
           $options=array("conditions"=>array("User.registration_date >="=>$start_time,"User.registration_date <= "=>$initital_end,"User.type"=>'SP','User.is_admin'=>0)); 
           $user_joined=$this->User->find('count',$options);
           $userTimezone = new \DateTimeZone($timezone);
            $gmtTimezone = new \DateTimeZone('GMT'); 
            $myDateTime = new \DateTime($start_time, $gmtTimezone);
            $offset = $userTimezone->getOffset($myDateTime);
            $x[]=date('d/m/Y', $myDateTime->format('U') + $offset);
            $y[]=!empty($user_joined)?$user_joined:0;
            if($user_joined>0)
            {
                $flag=1;
            }
                     
            $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  
           
           
           
        }
        
         $result=array('x'=>$x,'y'=>$y,'flag'=>$flag);
         
         echo json_encode($result);exit;
            
            
            
        }
        
        
        public function admin_postanalytics() {
        
        $total_post=$this->Post->find('count',array());
        $this->set(compact('total_post'));
        
	}
        
        public function admin_postgraph() 
        {
            
            $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; 

            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
            $result=array();
            if($data->status == 'success')
            {
                 $timezone = $data->timezone;
            }
            else 
            {
                $timezone="Asia/Kolkata";
            }
            //$timezone="Asia/Kolkata";
            $flag=0;

            date_default_timezone_set($timezone);
            $GMT = new \DateTimeZone('UTC');
            $last_date=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date("Y-m-1"))));
            $start_date=date('Y-m-d 00:00:00',strtotime(date('Y-m-01')));
            $end_date=date("Y-m-d 23:59:59",strtotime($last_date));
            $start=new \DateTime($start_date);
            $start->setTimezone($GMT);
            $start_time=$start->format('Y-m-d H:i:s');
            $end=new \DateTime($end_date);
            $end->setTimezone($GMT);
            $end_time=$end->format('Y-m-d H:i:s');
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
             while($start_time<=$end_time)
        {
           $options=array("conditions"=>array("Post.post_date >="=>$start_time,"Post.post_date <= "=>$initital_end)); 
           $user_joined=$this->Post->find('count',$options);
           $userTimezone = new \DateTimeZone($timezone);
           $gmtTimezone = new \DateTimeZone('GMT'); 
           $myDateTime = new \DateTime($start_time, $gmtTimezone);
           $offset = $userTimezone->getOffset($myDateTime);
           $x[]=date('d/m/Y', $myDateTime->format('U') + $offset);
           $y[]=!empty($user_joined)?$user_joined:0;
            if($user_joined>0)
            {
                $flag=1;
            }
                     
            $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  
           
           
           
        }
        
         $result=array('x'=>$x,'y'=>$y,'flag'=>$flag);
         
         echo json_encode($result);exit;
            
            
            
        }
        
        
        public function admin_categorygraph() 
        {
            
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
                $this->redirect('/admin');
            }
            $this->loadModel("Category");
            $this->loadModel("Lead");
            $categories=$this->Category->find("list",array('conditions'=>array("Category.parent_id !="=>0)));
            $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']; 

            $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip)); 
            $result=array();
            if($data->status == 'success')
            {
                 $timezone = $data->timezone;
            }
            else 
            {
                $timezone="Asia/Kolkata";
            }
            //$timezone="Asia/Kolkata";
            $flag=0;

            date_default_timezone_set($timezone);
            $GMT = new \DateTimeZone('UTC');
            
            
            
            $last_date=date("Y-m-d",strtotime("+3 month -1 second",strtotime(date("Y-m-1"))));
            $start_date=date('Y-m-d 00:00:00',strtotime(date('Y-m-01')));
            $end_date=date("Y-m-d 23:59:59",strtotime($last_date));
            foreach ($categories as $cat_id => $category)
            {
            $start=new \DateTime($start_date);
            $start->setTimezone($GMT);
            $start_time=$start->format('Y-m-d H:i:s');
            $end=new \DateTime($end_date);
            $end->setTimezone($GMT);
            $end_time=$end->format('Y-m-d H:i:s');
            $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));
            $job_posted=0;
            while($start_time<=$end_time)
           {
               $options=array("conditions"=>array("Lead.posttime >="=>$start_time,"Lead.posttime <= "=>$initital_end,"Lead.category_id"=>$cat_id)); 
               $user_joined=$this->Lead->find('count',$options);
               $job_posted=$job_posted+$user_joined;
               $userTimezone = new \DateTimeZone($timezone);
               $gmtTimezone = new \DateTimeZone('GMT'); 
               $myDateTime = new \DateTime($start_time, $gmtTimezone);
               $offset = $userTimezone->getOffset($myDateTime);
                if($user_joined>0)
                {
                    $flag=1;
                }       
               $start_time=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));;
               $initital_end=date('Y-m-d H:i:s', strtotime("+1 Days", strtotime($start_time)));  

           }
           $x[]=$category;
           $y[]=!empty($job_posted)?$job_posted:0;
           
           
      }
            
            
            
            
        
            $result=array('x'=>$x,'y'=>$y,'flag'=>$flag);
         
            echo json_encode($result);exit;
            
            
            
        }
        
        function admin_categoryanalytic()
        {
            
        }
        
        
        
        


	


	

}
