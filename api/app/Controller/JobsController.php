<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class JobsController extends AppController {

	/*function beforeFilter() {
		parent::beforeFilter();
	}*/
/**
 * Components
 *
 * @var array
 */
	public $name = 'Jobs';
	public $components = array('Session','RequestHandler','Paginator');
	var $uses = array('Lead','User','EmailTemplate','Setting');


	 //for get list of myjobs....................................
       /*  public function jobdetails() 
        {
              
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $id=$jsonData->id;
            $this->loadModel('Userresponse');
            $this->Userresponse->recursive = 2;
             $options = array('conditions' => array('Userresponse.lead_id' => $id),'order'=>array('Userresponse.id'=>'asc'));
             $this->Paginator->settings = $options;
             $responses=$this->Userresponse->find("all",$options);

             //accepted provider details..........................
              $this->loadModel('Appliedprovider');
             $options_provider = array('conditions' => array('Appliedprovider.lead_id' => $id,'Appliedprovider.status' => 1));
             $assigned_pro=$this->Appliedprovider->find('first', $options_provider);
             //print_r($assigned_pro);die;
           
                
                if(!empty($responses))
                {
                    $data=array('Ack'=>1,'jobdetails'=>$responses,'providerdetails'=>$assigned_pro);
                }
                else
                {
                    $data=array('Ack'=>0,'jobdetails'=>"",'providerdetails'=>"");
                }
                echo json_encode($data);exit;
             }
        } */
        
        public function jobdetails() 
        {
              
            $SITE_URL=Configure::read("SITE_URL");   
             $this->loadModel('Question');
             if ($this->request->is(array('post', 'put'))) {
             $jsonData = $this->request->input('json_decode');
             $lead_id=$jsonData->id;
            $this->loadModel('Lead');
            $leadDetail=$this->Lead->find("first",array('conditions'=>array("Lead.id"=>$lead_id)));
             $Arr=[];
               if(!empty($leadDetail)){
                $Arr['job']=array("id"=>$lead_id,"address"=>$leadDetail['Lead']['address'],"name"=>$leadDetail['Category']['project_name'],"category"=>$leadDetail['Category']['name'],"description"=>$leadDetail['Lead']['description'],"posttime"=>$leadDetail['Lead']['posttime'],"username"=>$leadDetail['User']['name'],"userid"=>$leadDetail['User']['id'],
                        "userimage"=>!empty($leadDetail['User']['image'])?$SITE_URL.'user_images/'.$leadDetail['User']['image']:$SITE_URL.'nouser.png',
                    "assignedusername"=>$leadDetail['AssignedPro']['name'],"assigneduserid"=>$leadDetail['AssignedPro']['id'],
                        "assigneduserimage"=>!empty($leadDetail['AssignedPro']['image'])?$SITE_URL.'user_images/'.$leadDetail['AssignedPro']['image']:$SITE_URL.'nouser.png');
               }
                if(!empty($leadDetail['Userresponse']))
                {
                    foreach($leadDetail['Userresponse'] as $key =>$response)
                    {
                       $this->Question->recursive=-1;
                      $questionArr=$this->Question->find("first",array('conditions'=>array("Question.id"=>$response['q_id'])));
                      
                      $Arr['response'][$key] = array("id"=>$response['id'],"question"=>$questionArr['Question']['name'],"answer"=>$response['answer']);
                    }
                }
                if(!empty($leadDetail))
                {
                    $data=array('Ack'=>1,'Detail'=>$Arr);
                }
                else
                {
                    $data=array('Ack'=>0,'Detail'=>"");
                }
                echo json_encode($data);exit;
             }
        }
}
