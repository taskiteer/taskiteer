<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class AvailabilitiesController extends AppController
{
    
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    
    function index_service($user_id = null)
    {
        $events = array();
        
        $availability = $this->Availability->find("first", array(
            "conditions" => array(
                "Availability.user_id" => $user_id
            )
        ));
        if (!empty($availability)) {
            $days = explode(",", $availability["Availability"]["days"]);
            if (!empty($availability["Availability"]["timezone"])) {
                $timezone = $availability["Availability"]["timezone"];
            } else {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
                if ($data->status == 'success') {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                } else {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
            }
            
            $tz_from = "UTC";
            $tz_to   = $timezone;
            $format  = 'H:i';
            $dt      = new DateTime($availability["Availability"]["start_time"], new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $start_time = $dt->format($format);
            
            $dt = new DateTime($availability["Availability"]["end_time"], new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $end_time = $dt->format($format);
            foreach ($days as $day) {
                $date1      = date("Y-m-01");
                $date2      = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date("Y-m-1"))));
                $start_date = strtotime($date1);
                $end_date   = strtotime($date2);
                while (1) {
                    $start_date = strtotime('next ' . $day, $start_date);
                    if ($start_date > $end_date)
                        break;
                    $focusdate = date("Y-m-d", $start_date);
                    $events[]  = array(
                        "title" => "Available",
                        "start" => $focusdate . 'T' . $start_time,
                        "end" => $focusdate . 'T' . $end_time,
                        'allDay' => false
                    );
                    
                    
                    
                }
            }
            
            $data = array(
                "Ack" => 1,
                "events" => $events
            );
            
            
        } else {
            $data = array(
                "Ack" => 0,
                "events" => array()
            );
        }
        
        echo json_encode($data);
        
        exit;
        
    }
    
    
    
    function add_service()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $days       = array();
            $jsonData   = $this->request->input('json_decode');
            $timezone   = !empty($jsonData->timezone) ? $jsonData->timezone : '';
            $start_time = $jsonData->start_time->id;
            $end_time   = $jsonData->end_time->id;
            foreach ($jsonData as $key => $json) {
                if ($key == 'sunday' or $key == 'monday' or $key == 'tuesday' or $key == 'wednesday' or $key == 'thursday' or $key == 'friday' or $key == 'saturday') {
                    $days[] = $key;
                }
                
                else if ($key == 'advance') {
                    $this->request->data["Availability"][$key] = $json->id;
                }
                
                else if ($key == 'notice_period') {
                    $this->request->data["Availability"][$key] = $json->id;
                }
                
                
                
                else {
                    $this->request->data["Availability"][$key] = $json;
                    
                }
                
                
            }
            
            if (empty($jsonData->id)) {
                $this->request->data["Availability"]["post_date"] = gmdate("Y-m-d H:i:s");
            } 
            $this->request->data["Availability"]["days"] = implode(",", $days);
            if (!empty($timezone)) {
                date_default_timezone_set($timezone);
                
            } else {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
                if ($data->status == 'success') {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                } else {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
                
            }
            $tz_from = $timezone;
            $tz_to   = 'UTC';
            $format  = 'H:i';
            
            $dt = new DateTime($start_time, new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $start_time = $dt->format($format);
            
            $dt = new DateTime($end_time, new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $end_time = $dt->format($format);
            
            $this->request->data["Availability"]["start_time"] = $start_time;
            $this->request->data["Availability"]["end_time"]   = $end_time;
            
            
            try {
                $this->Availability->save($this->request->data);
                $data = array(
                    "Ack" => 1
                );
                
            }
            catch (Exception $ex) {
                pr($ex);
                exit;
            }
            
        }
        
        echo json_encode($data);
        
        
        exit;
    }
    
    function add_serviceapp()
    {
        if ($this->request->is(array(
            'post',
            'put'
        ))) {
            $days       = array();
            $jsonData   = $this->request->input('json_decode');
            $timezone   = !empty($jsonData->savedata->timezone) ? $jsonData->savedata->timezone : '';
            $start_time = $jsonData->savedata->start_time->id;
            $end_time   = $jsonData->savedata->end_time->id;
            foreach ($jsonData->savedata as $key => $json) {
                if ($key == 'sunday' or $key == 'monday' or $key == 'tuesday' or $key == 'wednesday' or $key == 'thursday' or $key == 'friday' or $key == 'saturday') {
                    $days[] = $key;
                }
                
                else if ($key == 'advance') {
                    $this->request->data["Availability"][$key] = $json->id;
                }
                
                else if ($key == 'notice_period') {
                    $this->request->data["Availability"][$key] = $json->id;
                }
                
                
                
                else {
                    $this->request->data["Availability"][$key] = $json;
                    
                }
                
                
            }
            
            if (empty($jsonData->savedata->id)) {
                $this->request->data["Availability"]["post_date"] = gmdate("Y-m-d H:i:s");
            } 
            $this->request->data["Availability"]["days"] = implode(",", $days);
            if (!empty($timezone)) {
                date_default_timezone_set($timezone);
                
            } else {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
                if ($data->status == 'success') {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                } else {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
                
            }
            $tz_from = $timezone;
            $tz_to   = 'UTC';
            $format  = 'H:i';
            
            $dt = new DateTime($start_time, new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $start_time = $dt->format($format);
            
            $dt = new DateTime($end_time, new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $end_time = $dt->format($format);
            
            $this->request->data["Availability"]["start_time"] = $start_time;
            $this->request->data["Availability"]["end_time"]   = $end_time;
            
            
            try {
                $this->Availability->save($this->request->data);
                $data = array(
                    "Ack" => 1
                );
                
            }
            catch (Exception $ex) {
                pr($ex);
                exit;
            }
            
        }
        
        echo json_encode($data);
        
        
        exit;
    }
    
    
    
    function viewavailabilty($user_id = null)
    {
        $this->loadModel("Zone");
        $availablehours = array(
            '00:00' => '12:00 a.m.',
            '01:00' => '1:00 a.m.',
            '02:00' => '2:00 a.m.',
            '03:00' => '3:00 a.m.',
            '04:00' => '4:00 a.m.',
            '05:00' => '5:00 a.m.',
            '06:00' => '6:00 a.m.',
            '07:00' => '7:00 a.m.',
            '08:00' => '8:00 a.m.',
            '09:00' => '9:00 a.m.',
            '10:00' => '10:00 a.m.',
            '11:00' => '11:00 a.m.',
            '12:00' => '12:00 p.m.',
            '13:00' => '1:00 p.m.',
            '14:00' => '2:00 p.m.',
            '15:00' => '3:00 p.m.',
            '16:00' => '4:00 p.m.',
            '17:00' => '5:00 p.m.',
            '18:00' => '6:00 p.m.',
            '19:00' => '7:00 p.m.',
            '20:00' => '8:00 p.m.',
            '21:00' => '9:00 p.m.',
            '22:00' => '10:00 p.m.',
            '23:00' => '11:00 p.m.',
            '24:00' => '12:00 midnight'
        );
         $hoursarray=array(
        array('id'=>'00:00','name'=>'12:00 a.m.'),array('id'=>'01:00','name'=>'1:00 a.m.'),array('id'=>'02:00','name'=>'2:00 a.m.'),
        array('id'=>'03:00','name'=>'3:00 a.m.'),array('id'=>'04:00','name'=>'4:00 a.m.'),array('id'=>'05:00','name'=>'5:00 a.m.'),
        array('id'=>'06:00','name'=>'6:00 a.m.'),array('id'=>'07:00','name'=>'7:00 a.m.'),array('id'=>'08:00','name'=>'8:00 a.m.'),
        array('id'=>'09:00','name'=>'9:00 a.m.'),array('id'=>'10:00','name'=>'10:00 a.m.'),array('id'=>'11:00','name'=>'11:00 a.m.'),
        array('id'=>'12:00','name'=>'12:00 p.m.'),array('id'=>'13:00','name'=>'1:00 p.m.'),array('id'=>'14:00','name'=>'2:00 p.m.'),
        array('id'=>'15:00','name'=>'3:00 p.m.'),array('id'=>'16:00','name'=>'4:00 p.m.'),array('id'=>'17:00','name'=>'5:00 p.m.'),
        array('id'=>'18:00','name'=>'6:00 p.m.'),array('id'=>'19:00','name'=>'7:00 p.m.'),array('id'=>'20:00','name'=>'8:00 p.m.'),
        array('id'=>'21:00','name'=>'9:00 p.m.'),array('id'=>'22:00','name'=>'10:00 p.m.'),array('id'=>'23:00','name'=>'11:00 p.m.'),
        array('id'=>'24:00','name'=>'12:00 midnight')
         
        );
         
        $notice=array(
        array('id'=>0,'name'=>'Same day booking'),array('id'=>1,'name'=>'1'),array('id'=>2,'name'=>'2'),
        array('id'=>3,'name'=>'3'),array('id'=>4,'name'=>'4'),array('id'=>5,'name'=>'5'),
        array('id'=>6,'name'=>'6'),array('id'=>7,'name'=>'7'),array('id'=>8,'name'=>'8'),array('id'=>9,'name'=>'9'),
        array('id'=>10,'name'=>'10'),array('id'=>11,'name'=>'11'),array('id'=>12,'name'=>'12'),array('id'=>13,'name'=>'13'),
        array('id'=>14,'name'=>'14'),array('id'=>15,'name'=>'15'),array('id'=>16,'name'=>'16'),array('id'=>17,'name'=>'17'),
        array('id'=>18,'name'=>'18'),array('id'=>19,'name'=>'19'),array('id'=>20,'name'=>'20'),array('id'=>21,'name'=>'21'),
        array('id'=>22,'name'=>'22'),array('id'=>23,'name'=>'23'),array('id'=>24,'name'=>'24'),array('id'=>25,'name'=>'25'),
        array('id'=>26,'name'=>'26'),array('id'=>27,'name'=>'27'),array('id'=>28,'name'=>'28'),array('id'=>29,'name'=>'29'),
        array('id'=>30,'name'=>'30')    
         
        );
        $advance=array(
        array('id'=>0,'name'=>'No limit'),array('id'=>1,'name'=>'1'),array('id'=>2,'name'=>'2'),
        array('id'=>3,'name'=>'3'),array('id'=>4,'name'=>'4'),array('id'=>5,'name'=>'5'),
        array('id'=>6,'name'=>'6'),array('id'=>7,'name'=>'7'),array('id'=>8,'name'=>'8'),array('id'=>9,'name'=>'9'),
        array('id'=>10,'name'=>'10'),array('id'=>11,'name'=>'11'),array('id'=>12,'name'=>'12'),array('id'=>13,'name'=>'13'),
        array('id'=>14,'name'=>'14'),array('id'=>15,'name'=>'15'),array('id'=>16,'name'=>'16'),array('id'=>17,'name'=>'17'),
        array('id'=>18,'name'=>'18'),array('id'=>19,'name'=>'19'),array('id'=>20,'name'=>'20'),array('id'=>21,'name'=>'21'),
        array('id'=>22,'name'=>'22'),array('id'=>23,'name'=>'23'),array('id'=>24,'name'=>'24'),array('id'=>25,'name'=>'25'),
        array('id'=>26,'name'=>'26'),array('id'=>27,'name'=>'27'),array('id'=>28,'name'=>'28'),array('id'=>29,'name'=>'29'),
        array('id'=>30,'name'=>'30')    
         
        );
        
        $modes=array('days','weeks','month');
        $zones      = $this->Zone->find("all", array(
            'fields' => array(
                'Zone.zone_name'
            )
        ));
        foreach ($zones as $zone)
        {
            $timezones[]=$zone['Zone']['zone_name'];
        }
        
        $availability = $this->Availability->find("first", array(
            "conditions" => array(
                "Availability.user_id" => $user_id
            )
        ));
        if (!empty($availability)) {
            $days  = explode(",", $availability["Availability"]["days"]);
            $weeks = array(
                "sunday",
                "monday",
                "tuesday",
                "wednesday",
                "thursday",
                "friday",
                "saturday"
            );
            if (!empty($availability["Availability"]["timezone"])) {
                $timezone = $availability["Availability"]["timezone"];
            } else {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                $data = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
                if ($data->status == 'success') {
                    date_default_timezone_set($data->timezone);
                    $timezone = $data->timezone;
                } else {
                    date_default_timezone_set('Asia/Kolkata');
                    $timezone = 'Asia/Kolkata';
                }
            }
            
            $tz_from = "UTC";
            $tz_to   = $timezone;
            $format  = 'H:i';
            $dt      = new DateTime($availability["Availability"]["start_time"], new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $start_time = $dt->format($format);
            
            $dt = new DateTime($availability["Availability"]["end_time"], new DateTimeZone($tz_from));
            $dt->setTimeZone(new DateTimeZone($tz_to));
            $end_time                                      = $dt->format($format);
            $start_time1                                   = date("H:i", strtotime($start_time));
            $end_time1                                     = date("H:i", strtotime($end_time));
            $availability["Availability"]["start_time"]    = array(
                'id' => $start_time1,
                'name' => $availablehours[$start_time1]
            );
            $availability["Availability"]["end_time"]      = array(
                'id' => $end_time1,
                'name' => $availablehours[$end_time1]
            );
            $availability["Availability"]["notice_period"] = !empty($availability["Availability"]["notice_period"]) ? array(
                "id" => $availability["Availability"]["notice_period"],
                "name" => $availability["Availability"]["notice_period"]
            ) : array(
                "id" => 0,
                "name" => "Same day booking"
            );
            $availability["Availability"]["advance"]       = !empty($availability["Availability"]["advance"]) ? array(
                "id" => $availability["Availability"]["advance"],
                "name" => $availability["Availability"]["advance"]
            ) : array(
                "id" => 0,
                "name" => "No limit"
            );
            foreach ($weeks as $week) {
                if (in_array($week, $days)) {
                    $availability["Availability"][$week] = (int) 1;
                } else {
                    $availability["Availability"][$week] = (int) 0;
                }
            }
            $response = array(
                'availability' => $availability["Availability"],
                'timezones' => $timezones,
                'modes'=>$modes,
                'notice'=>$notice,
                'advance'=>$advance,
                'hoursarray'=>$hoursarray
                
            );
            $data     = array(
                "Ack" => 1,
                "response" => $response,
            );
            
            
            
        } else {
            $response = array(
                'availability' => array(),
                'timezones' => $timezones,
                'modes'=>$modes,
                'notice'=>$notice,
                'advance'=>$advance,
                'hoursarray'=>$hoursarray
            );
            $data     = array(
                "Ack" => 0,
                "response" => $response
            );
            
        }
        
        
        
        echo json_encode($data);
        exit;
        
        
    }
    
    
    
    
    function display_time()
    {
        echo date("H:i a", strtotime("1"));
        exit;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * index method
     *
     * @return void
     */
    
    
    
    
    
    
    
    
    
    
    
    
}