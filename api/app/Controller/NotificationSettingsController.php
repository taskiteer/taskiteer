<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class NotificationSettingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
 public $components = array('Paginator');
 public function savesettings() 
 {
    $this->loadModel("User");
    $jsonData = $this->request->input('json_decode');
    $user_id=$jsonData->user_id;
    $mailsetting["NotificationSetting"]["user_id"]=$user_id;
    foreach ($jsonData->email as $key=> $val)
    {
        $mailsetting["NotificationSetting"][$key]=$val;
    }
    $this->NotificationSetting->save($mailsetting);

    $smssetting["NotificationSetting"]["user_id"]=$user_id;
    foreach ($jsonData->sms as $key=> $val)
    {
        $smssetting["NotificationSetting"][$key]=$val;
    }
    $this->NotificationSetting->create();
    $this->NotificationSetting->save($smssetting);
    $data=array("Ack"=>1,"msg"=>"Notification saved successfully");
    echo json_encode($data);exit;

     
 }
 
 public function savesetting_app() 
 {
    $this->loadModel("User");
    $jsonData = $this->request->input('json_decode');
    foreach ($jsonData->response as $json)
    {
        if($json->type=='email')
        {
            $mailsetting["NotificationSetting"]["id"]=!empty($json->id)?$json->id:"";
            $id=!empty($json->id)?$json->id:"";
            $mailsetting["NotificationSetting"]['type']='e';
            $mailsetting["NotificationSetting"]['user_id']=$json->user_id;
            $user_id=$json->user_id;
            $mailsetting["NotificationSetting"][$json->name]=$json->value;
        }
        else
        {
            $smssetting["NotificationSetting"]["id"]=!empty($json->id)?$json->id:"";
            $id=!empty($json->id)?$json->id:"";
            $smssetting["NotificationSetting"]['type']='m';
            $smssetting["NotificationSetting"]['user_id']=$json->user_id;
            $user_id=$json->user_id;
            $smssetting["NotificationSetting"][$json->name]=$json->value;
        }
        
    }

    
      if(empty($mailsetting))
      {
          $notifications=$this->NotificationSetting->find("first",array("conditions"=>array("NotificationSetting.user_id"=>$user_id,"NotificationSetting.type"=>'e')));
          $mailsetting["NotificationSetting"]['id']=!empty($notifications)?$notifications["NotificationSetting"]["id"]:"";
          $mailsetting["NotificationSetting"]['type']='e';
          $mailsetting["NotificationSetting"]['user_id']=$user_id;
      }
      if(empty($smssetting))
      {
          $notifications=$this->NotificationSetting->find("first",array("conditions"=>array("NotificationSetting.user_id"=>$user_id,"NotificationSetting.type"=>'m')));
          $smssetting["NotificationSetting"]['id']=!empty($notifications)?$notifications["NotificationSetting"]["id"]:"";
          $smssetting["NotificationSetting"]['type']='m';
          $smssetting["NotificationSetting"]['user_id']=$user_id;
      }
      if($this->NotificationSetting->save($mailsetting))
      {
          
          $this->NotificationSetting->create();
          $this->NotificationSetting->save($smssetting);
          $data=array("Ack"=>1,"msg"=>"Notification saved successfully"); 
      }
      else
      {
          $data=array("Ack"=>0,"msg"=>"Internal Error"); 
      }
     
    echo json_encode($data);exit;

     
 }
 
 
 function  details($user_id=null)
 {
      
     $SITE_URL=Configure::read("SITE_URL");
     $result=array();
     $notifications=$this->NotificationSetting->find("all",array("conditions"=>array("NotificationSetting.user_id"=>$user_id)));
     if(!empty($notifications))
     {
        foreach ($notifications as $notification)
        {
            if($notification["NotificationSetting"]["type"]=='e')
            {
                $result["email"]["id"]=(int)$notification["NotificationSetting"]["id"];
                $result["email"]["new_customer"]=(int)$notification["NotificationSetting"]["new_customer"];
                $result["email"]["customer_activity"]=(int)$notification["NotificationSetting"]["customer_activity"];
                $result["email"]["request_reminder"]=(int)$notification["NotificationSetting"]["request_reminder"];
                $result["email"]["viewed_quote"]=(int)$notification["NotificationSetting"]["viewed_quote"];
                $result["email"]["quote_activity"]=(int)$notification["NotificationSetting"]["quote_activity"];
                $result["email"]["reminder_email"]=(int)$notification["NotificationSetting"]["reminder_email"];
                $result["email"]["quote_followup"]=(int)$notification["NotificationSetting"]["quote_followup"];
            }
            else
            {
                $result["sms"]["id"]=(int)$notification["NotificationSetting"]["id"];
                $result["sms"]["new_customer"]=(int)$notification["NotificationSetting"]["new_customer"];
                $result["sms"]["customer_activity"]=(int)$notification["NotificationSetting"]["customer_activity"];
                $result["sms"]["request_reminder"]=(int)$notification["NotificationSetting"]["request_reminder"];
                $result["sms"]["viewed_quote"]=(int)$notification["NotificationSetting"]["viewed_quote"];
                $result["sms"]["quote_activity"]=(int)$notification["NotificationSetting"]["quote_activity"];
                $result["sms"]["reminder_email"]=(int)$notification["NotificationSetting"]["reminder_email"];
                $result["sms"]["quote_followup"]=(int)$notification["NotificationSetting"]["quote_followup"];

            }
        }
        $data=array("Ack"=>1,"response"=>$result);
        
     }
     else
     {
        $data=array("Ack"=>0);
     }
     echo json_encode($data);exit;
     
     
      
 }
 
 
 function  details_app($user_id=null)
 {
      
     $SITE_URL=Configure::read("SITE_URL");
     $result=array();
     $notifications=$this->NotificationSetting->find("all",array("conditions"=>array("NotificationSetting.user_id"=>$user_id)));
     $count_row=count($notifications);
     if(!empty($notifications))
     {
        foreach ($notifications as $notification)
        {
               
                $type=$notification["NotificationSetting"]["type"]=='e'?'email':'sms'; 
                $id=!empty($notification["NotificationSetting"]["id"])?$notification["NotificationSetting"]["id"]:0;
                if($type=='email')
                {
                   $result_email=array(
                        array('sl_no'=>1,'id'=>(int)$id,'name'=>'new_customer','value'=>(int)$notification["NotificationSetting"]["new_customer"],'type'=>$type,'label'=>'New customer requests','user_id'=>$notification["NotificationSetting"]["user_id"]),
                        array('sl_no'=>2,'id'=>(int)$id,'name'=>'customer_activity','value'=>(int)$notification["NotificationSetting"]["customer_activity"],'type'=>$type,'label'=>'Request reminders','user_id'=>$notification["NotificationSetting"]["user_id"]),
                        array('sl_no'=>3,'id'=>(int)$id,'name'=>'request_reminder','value'=>(int)$notification["NotificationSetting"]["request_reminder"],'type'=>$type,'label'=>'New customer activity (messages or hires)','user_id'=>$notification["NotificationSetting"]["user_id"]),
                        array('sl_no'=>4,'id'=>(int)$id,'name'=>'viewed_quote','value'=>(int)$notification["NotificationSetting"]["viewed_quote"],'type'=>$type,'label'=>'Customer viewed quote','user_id'=>$notification["NotificationSetting"]["user_id"]),
                        array('sl_no'=>5,'id'=>(int)$id,'name'=>'quote_activity','value'=>(int)$notification["NotificationSetting"]["quote_activity"],'type'=>$type,'label'=>'Other quote activity','user_id'=>$notification["NotificationSetting"]["user_id"]),
                        array('sl_no'=>6,'id'=>(int)$id,'name'=>'reminder_email','value'=>(int)$notification["NotificationSetting"]["reminder_email"],'type'=>$type,'label'=>'ProAssist setup reminder email','user_id'=>$notification["NotificationSetting"]["user_id"]),
                        array('sl_no'=>7,'id'=>(int)$id,'name'=>'quote_followup','value'=>(int)$notification["NotificationSetting"]["quote_followup"],'type'=>$type,'label'=>'Quote follow up','user_id'=>$notification["NotificationSetting"]["user_id"])    

                        ); 
                }
                else
                {
               $result_sms=array(
                array('sl_no'=>8,'id'=>(int)$id,'name'=>'new_customer','value'=>(int)$notification["NotificationSetting"]["new_customer"],'type'=>$type,'label'=>'New customer requests','user_id'=>$notification["NotificationSetting"]["user_id"]),
                array('sl_no'=>9,'id'=>(int)$id,'name'=>'customer_activity','value'=>(int)$notification["NotificationSetting"]["customer_activity"],'type'=>$type,'label'=>'Request reminders','user_id'=>$notification["NotificationSetting"]["user_id"]),
                array('sl_no'=>10,'id'=>(int)$id,'name'=>'request_reminder','value'=>(int)$notification["NotificationSetting"]["request_reminder"],'type'=>$type,'label'=>'New customer activity (messages or hires)','user_id'=>$notification["NotificationSetting"]["user_id"]),
                array('sl_no'=>11,'id'=>(int)$id,'name'=>'viewed_quote','value'=>(int)$notification["NotificationSetting"]["viewed_quote"],'type'=>$type,'label'=>'Customer viewed quote','user_id'=>$notification["NotificationSetting"]["user_id"]),
                array('sl_no'=>12,'id'=>(int)$id,'name'=>'quote_activity','value'=>(int)$notification["NotificationSetting"]["quote_activity"],'type'=>$type,'label'=>'Other quote activity','user_id'=>$notification["NotificationSetting"]["user_id"]),
                array('sl_no'=>13,'id'=>(int)$id,'name'=>'reminder_email','value'=>(int)$notification["NotificationSetting"]["reminder_email"],'type'=>$type,'label'=>'ProAssist setup reminder email','user_id'=>$notification["NotificationSetting"]["user_id"]),
                array('sl_no'=>14,'id'=>(int)$id,'name'=>'quote_followup','value'=>(int)$notification["NotificationSetting"]["quote_followup"],'type'=>$type,'label'=>'Quote follow up','user_id'=>$notification["NotificationSetting"]["user_id"])    
                    
                );
                }
                
                
            
               
          
            
        }
      
        
     }
     else
     {
         $result_email=array(
                        array('sl_no'=>1,'name'=>'new_customer','value'=>0,'type'=>'email','label'=>'New customer requests','user_id'=>$user_id),
                        array('sl_no'=>2,'name'=>'customer_activity','value'=>0,'type'=>'email','label'=>'Request reminders','user_id'=>$user_id),
                        array('sl_no'=>3,'name'=>'request_reminder','value'=>0,'type'=>"email",'label'=>'New customer activity (messages or hires)','user_id'=>$user_id),
                        array('sl_no'=>4,'name'=>'viewed_quote','value'=>0,'type'=>"email",'label'=>'Customer viewed quote','user_id'=>$user_id),
                        array('sl_no'=>5,'name'=>'quote_activity','value'=>0,'type'=>"email",'label'=>'Other quote activity','user_id'=>$user_id),
                        array('sl_no'=>6,'name'=>'reminder_email','value'=>0,'type'=>"email",'label'=>'ProAssist setup reminder email','user_id'=>$user_id),
                        array('sl_no'=>7,'name'=>'quote_followup','value'=>0,'type'=>"email",'label'=>'Quote follow up','user_id'=>$user_id)    

                        ); 
          $result_sms=array(
                array('sl_no'=>8,'name'=>'new_customer','value'=>0,'type'=>"sms",'label'=>'New customer requests','user_id'=>$user_id),
                array('sl_no'=>9,'name'=>'customer_activity','value'=>0,'type'=>"sms",'label'=>'Request reminders','user_id'=>$user_id),
                array('sl_no'=>10,'name'=>'request_reminder','value'=>0,'type'=>"sms",'label'=>'New customer activity (messages or hires)','user_id'=>$user_id),
                array('sl_no'=>11,'name'=>'viewed_quote','value'=>0,'type'=>"sms",'label'=>'Customer viewed quote','user_id'=>$user_id),
                array('sl_no'=>12,'name'=>'quote_activity','value'=>0,'type'=>"sms",'label'=>'Other quote activity','user_id'=>$user_id),
                array('sl_no'=>13,'name'=>'reminder_email','value'=>0,'type'=>"sms",'label'=>'ProAssist setup reminder email','user_id'=>$user_id),
                array('sl_no'=>14,'name'=>'quote_followup','value'=>0,'type'=>"sms",'label'=>'Quote follow up','user_id'=>$user_id)    
                    
                );
     }
     
     $result= array_merge($result_email,$result_sms);

      $data=array("Ack"=>1,"response"=>$result);
     
     echo json_encode($data);exit;
     
     
      
 }













 /**
 * index method
 *
 * @return void
 */
	
        
        
        
        


	


	

}
