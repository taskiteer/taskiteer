<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class ReportsController extends AppController {

	/*function beforeFilter() {
		parent::beforeFilter();
	}*/
/**
 * Components
 *
 * @var array
 */
	public $name = 'Reports';
	public $components = array('Session','RequestHandler','Paginator');
	var $uses = array('Report','User','EmailTemplate','Setting');


	public function admin_index() {

    $this->loadModel('ReportAbuseCustomer');
    $this->ReportAbuseCustomer->recursive = 2;
    $options = array('order' => array('ReportAbuseCustomer.id' =>'ASC'));
    
     $this->Paginator->settings = $options;
     $this->set('abuses', $this->Paginator->paginate('ReportAbuseCustomer'));
     
	}


  public function admin_provider_list($id = null) {

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }
    $this->loadModel('User');
  	//$this->Booking->recursive = 2;
    $options = array('conditions' => array('User.id' => $id));

    $this->set('users', $this->User->find('first', $options));
	}

  public function admin_block($id = null) {

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }
     $this->request->data['User']['id'] = $id;
     $this->request->data['User']['status'] =0;

    if ($this->User->save($this->request->data)) {
       
      $this->Session->setFlash('The user has been blocked.','default', array('class' => 'success'));
      return $this->redirect(array('action' => 'admin_provider_list/'.$id));

    } else {
      $this->Session->setFlash(__('The user could not be blocked. Please, try again.'));
      return $this->redirect(array('action' => 'admin_provider_list/'.$id));
    }
  }

  public function admin_unblock($id = null) {

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }
    $this->request->data['User']['id'] = $id;
    $this->request->data['User']['status'] =1;

    if ($this->User->save($this->request->data)) {
        
      $this->Session->setFlash('The user has been unblocked.','default', array('class' => 'success'));
      return $this->redirect(array('action' => 'admin_provider_list/'.$id));
    } else {
      $this->Session->setFlash(__('The user could not be unblocked. Please, try again.'));
      return $this->redirect(array('action' => 'admin_provider_list/'.$id));
    }
  }

  public function admin_abuse_provider_list() {

    $this->loadModel('ReportAbuseProvider');
    $this->ReportAbuseProvider->recursive = 2;
    $options = array('order' => array('ReportAbuseProvider.id' =>'ASC'));
    
    $this->Paginator->settings = $options;
    $this->set('abuses', $this->Paginator->paginate('ReportAbuseProvider')); 
  }

}