<?php
App::uses('AppController', 'Controller');
/**
 * CarCategories Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class FaqsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


/**
 * index method
 *
 * @return void
 */


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Faq->create();
			if ($this->Faq->save($this->request->data)) {
				$this->Session->setFlash('The faq has been saved.','default', array('class' => 'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The faq template could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		$this->Faq->id = $id;
		if (!$this->Faq->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->Faq->delete()) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The faq has been deleted.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The faq could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_index() {

            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
		$title_for_layout = 'Faq List';
		 $this->paginate = array(
			'order' => array(
		        'Faq.id' => 'desc'
			)
		);
		//$this->Carcategory->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('allfaq', $this->Paginator->paginate());
                //print_r($allfaq);
                //exit;

		$this->set(compact('title_for_layout'));
	}
	
	
	
	
	public function faqlist_service() 
            {
           
            if ($this->request->is(array('post', 'put'))) {
                $faqs=$this->Faq->find("all",array('conditions'=>array("Faq.sattus"=>1)));
                //pr($faqs);exit;
                $SITE_URL=Configure::read("SITE_URL");  
                foreach ($faqs as  $fa)
                {
                    $list_faqs[]=array("question"=>$fa['Faq']['question'],"answer"=>$fa['Faq']['answar'],"status"=>$fa['Faq']['sattus']);
                }
                
                
                
                if(!empty($list_faqs))
                {
                    $data=array('Ack'=>1,'faqList'=>$list_faqs);
                }
                else
                {
                    $data=array('Ack'=>0,'faqList'=>"");
                }
                echo json_encode($data);exit;
                }
                
            }

	public function admin_edit($id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }

		if ($this->request->is(array('post', 'put'))) {

                        $this->request->data['Faq']['id']=$id;

                        //print_r($this->request->data);
                        //exit;

			if ($this->Faq->save($this->request->data)) {

                         $this->Session->setFlash('The faq has been saved.','default', array('class' => 'success'));
                         return $this->redirect(array('action' => 'index'));

			} else {
				$this->Session->setFlash(__('The faq could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Faq.' . $this->Faq->primaryKey => $id));
			$faq = $this->Faq->find('first', $options);
                        $this->set(compact('faq'));
		}
	}

}
