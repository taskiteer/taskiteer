-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2017 at 10:09 AM
-- Server version: 5.5.44-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service_pro`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `sl_no` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(255) NOT NULL DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `bigimage` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `sl_no`, `name`, `image`, `status`, `parent_id`, `slug`, `project_name`, `bigimage`) VALUES
(2, 1, 'Manage a Event', '593a4bbbb9b6c.png', 1, 39, 'manage-a-event', 'Manage a Event', 'smallbanner.jpg'),
(3, 2, 'MEAL PREPARATION', '593a4be634961.png', 1, 5, 'meal-preparation', 'MEAL PREPARATION', 'smallbanner.jpg'),
(4, 3, 'TRANSPORTATION', '593a525298e3d.png', 1, 5, 'transportation', 'TRANSPORTATION', 'smallbanner.jpg'),
(5, 4, 'Miscellaneous', '593a4cf6928c0.png', 1, 0, 'miscellaneous', 'Miscellaneous', 'smallbanner.jpg'),
(7, 5, 'Moving to Home', '593a4d8e4dcf0.png', 1, 5, 'moving-to-home', 'Moving to Home', 'smallbanner.jpg'),
(8, 6, 'Planning a Wedding', '593a4f638f75d.png', 1, 39, 'planning-a-wedding', 'Planning a Wedding', 'smallbanner.jpg'),
(10, 7, 'Bathroom Remodeling', '5951fb110f918.jpg', 1, 45, 'bathroom-remodeling', 'Bathroom Remodeling', 'smallbanner.jpg'),
(11, 8, 'Cabinets', '5951fcd376702.jpeg', 1, 45, 'cabinets', 'Cabinets', 'smallbanner.jpg'),
(12, 9, 'Countertops', '595206edde881.jpeg', 1, 45, 'countertops-1', 'Countertops', 'smallbanner.jpg'),
(13, 10, 'Electrical', '593a492607863.png', 1, 45, 'electrical', 'Electrical', 'smallbanner.jpg'),
(14, 11, 'Flooring', '593a49b8d0c8a.png', 1, 45, 'flooring', 'Flooring', 'smallbanner.jpg'),
(15, 12, 'Glass & Mirrors', '593a4c9babbc3.png', 1, 45, 'glass-mirrors', 'Glass & Mirrors', 'smallbanner.jpg'),
(16, 13, 'Hot Tubs & Spas', '593a4b09b1099.png', 1, 34, 'hot-tubs-spas', 'Hot Tubs & Spas', 'smallbanner.jpg'),
(17, 14, 'Painting & Staining', '593a4de0bdd83.png', 1, 32, 'painting-staining', 'Painting & Staining', 'smallbanner.jpg'),
(18, 15, 'Plumbing', '593a4fb9953b3.png', 1, 45, 'plumbing', 'Plumbing', 'smallbanner.jpg'),
(19, 16, 'Tile', '593a51694fe57.png', 1, 45, 'tile', 'Tile', 'smallbanner.jpg'),
(20, 17, 'Walls & Ceilings', '593a52b4b4bd8.png', 1, 45, 'walls-ceilings', 'Walls & Ceilings', 'smallbanner.jpg'),
(21, 19, 'Windows & Doors', '593a52f63ecd7.png', 1, 45, 'windows-doors', 'Windows & Doors', 'smallbanner.jpg'),
(22, 20, 'Closets', '595206ccee329.jpeg', 1, 45, 'closets-1', 'Closets', 'smallbanner.jpg'),
(23, 21, 'Stairs & Railings', '593a50d7c2e1d.png', 1, 45, 'stairs-railings', 'Stairs & Railings', 'smallbanner.jpg'),
(24, 22, 'Siding, Gutters & Exterior Trim', '593a508eedde5.png', 1, 5, 'siding-gutters-exterior-trim', 'Siding, Gutters & Exterior Trim', 'smallbanner.jpg'),
(25, 23, 'Outbuildings, Sheds & Gazebos', '593a5052563d6.png', 1, 45, 'outbuildings-sheds-gazebos', 'Outbuildings, Sheds & Gazebos', 'smallbanner.jpg'),
(26, 24, 'Framing', '593a4a4866844.png', 1, 45, 'framing', 'Framing', 'smallbanner.jpg'),
(27, 25, 'Finish Carpentry, Trim & Molding', '593a469c30620.png', 1, 45, 'finish-carpentry-trim-molding', 'Finish Carpentry, Trim & Molding', 'smallbanner.jpg'),
(28, 26, 'Doors', '593a48dea8f39.png', 1, 45, 'doors', 'Doors', 'smallbanner.jpg'),
(29, 27, 'Decks, Fences & Ramps', '593a47e757b44.png', 1, 45, 'decks-fences-ramps', 'Decks, Fences & Ramps', 'smallbanner.jpg'),
(30, 28, 'Business', '5951fc60bf341.jpeg', 1, 0, 'business-1', 'Business', 'smallbanner.jpg'),
(31, 29, 'Crafts', '595207340bcd9.jpeg', 1, 32, 'crafts-1', 'Crafts', 'smallbanner.jpg'),
(32, 30, 'Design and web', '593a48a72dbf1.png', 1, 0, 'design-and-web', 'Design and web', 'smallbanner.jpg'),
(33, 31, 'Legal', '593a4b3ff374e.png', 1, 30, 'legal', 'Legal', 'smallbanner.jpg'),
(34, 32, 'Personal', '593a4e0a3ba76.png', 1, 0, 'personal', 'Personal', 'smallbanner.jpg'),
(35, 33, 'Pets', '593a4ebb89c0e.png', 1, 5, 'pets', 'Pets', 'smallbanner.jpg'),
(36, 34, 'Photography', '593a4f251490f.png', 1, 39, 'photography', 'Photography', 'smallbanner.jpg'),
(37, 35, 'Repair and technical support', '593a5009636c7.png', 1, 5, 'repair-and-technical-support', 'Repair and technical support', 'smallbanner.jpg'),
(38, 36, 'Writing, translation, and transcription', '593a51fce9f85.png', 1, 5, 'writing-translation-and-transcription', 'Writing, translation, and transcription', 'smallbanner.jpg'),
(39, 18, 'Events', '593a496d052fb.png', 1, 0, 'events', 'Events', 'smallbanner.jpg'),
(40, 37, 'More Events', '5926c22e700da.jpg', 1, 39, '', 'More Events', 'smallbanner.jpg'),
(41, 38, 'Air Conditioning', '5951fa0119a18.jpeg', 1, 0, 'air-conditioning', 'Air Conditioning', 'smallbanner.jpg'),
(42, 39, 'A/C Install', '592d1deec050b.jpeg', 1, 41, 'a-c-install', 'A/C Install', 'smallbanner.jpg'),
(43, 40, 'A/C repair', '592d1e035c097.jpeg', 1, 41, 'a-c-repair', 'A/C repair', 'smallbanner.jpg'),
(44, 41, 'A/C Testing', '593a5f37acea4.png', 1, 41, 'a-c-testing-1', 'A/C Testing', 'smallbanner.jpg'),
(45, 42, 'Builders', '5951fbc8b8d52.jpeg', 1, 0, 'builders', 'Builders', 'smallbanner.jpg'),
(46, 43, 'Architect', '593a56c089ce5.png', 1, 45, 'architect-1', 'Architect', 'smallbanner.jpg'),
(47, 44, 'Decorator', '593a5679dbdde.png', 1, 45, 'decorator', 'Decorator', 'smallbanner.jpg'),
(48, 45, 'designers', '593a5601474dc.png', 1, 45, 'designers-1', 'designers', 'smallbanner.jpg'),
(49, 46, 'Delivery Days', '593a4838b92b7.png', 1, 5, 'delivery-days', 'Delivery Days', 'smallbanner.jpg'),
(50, 47, 'Cleaning Deals', '595206aa6b142.jpg', 1, 5, 'cleaning-deals-1', 'Cleaning Deals', 'smallbanner.jpg'),
(51, 48, 'Home Improvement Offers', '593a4ab3c5f6c.png', 1, 5, 'home-improvement-offers', 'Home Improvement Offers', 'smallbanner.jpg'),
(52, 49, 'Party Planning', '59394f6dbf773.png', 1, 39, 'party-planning', 'Party Planning', 'smallbanner.jpg'),
(53, 50, 'Tech Supports', '593a510ab10f6.png', 1, 32, 'tech-supports-1', 'Tech Supports', 'smallbanner.jpg'),
(54, 1, '', '', 1, 0, '', '', ''),
(55, 2, '', '', 1, 0, '', '', ''),
(56, 3, '', '', 1, 0, '', '', ''),
(57, 4, '', '', 1, 0, '', '', ''),
(58, 5, '', '', 1, 0, '', '', ''),
(59, 6, '', '', 1, 0, '', '', ''),
(60, 7, '', '', 1, 0, '', '', ''),
(61, 8, '', '', 1, 0, '', '', ''),
(62, 9, '', '', 1, 0, '', '', ''),
(63, 10, '', '', 1, 0, '', '', ''),
(64, 11, '', '', 1, 0, '', '', ''),
(65, 12, '', '', 1, 0, '', '', ''),
(66, 13, '', '', 1, 0, '', '', ''),
(67, 14, '', '', 1, 0, '', '', ''),
(68, 15, '', '', 1, 0, '', '', ''),
(69, 16, '', '', 1, 0, '', '', ''),
(70, 17, '', '', 1, 0, '', '', ''),
(71, 18, '', '', 1, 0, '', '', ''),
(72, 19, '', '', 1, 0, '', '', ''),
(73, 20, '', '', 1, 0, '', '', ''),
(74, 21, '', '', 1, 0, '', '', ''),
(75, 22, '', '', 1, 0, '', '', ''),
(76, 23, '', '', 1, 0, '', '', ''),
(77, 24, '', '', 1, 0, '', '', ''),
(78, 25, '', '', 1, 0, '', '', ''),
(79, 26, '', '', 1, 0, '', '', ''),
(80, 27, '', '', 1, 0, '', '', ''),
(81, 28, '', '', 1, 0, '', '', ''),
(82, 29, '', '', 1, 0, '', '', ''),
(83, 30, '', '', 1, 0, '', '', ''),
(84, 31, '', '', 1, 0, '', '', ''),
(85, 32, '', '', 1, 0, '', '', ''),
(86, 33, '', '', 1, 0, '', '', ''),
(87, 34, '', '', 1, 0, '', '', ''),
(88, 35, '', '', 1, 0, '', '', ''),
(89, 36, '', '', 1, 0, '', '', ''),
(90, 37, '', '', 1, 0, '', '', ''),
(91, 38, '', '', 1, 0, '', '', ''),
(92, 39, '', '', 1, 0, '', '', ''),
(93, 40, '', '', 1, 0, '', '', ''),
(94, 41, '', '', 1, 0, '', '', ''),
(95, 42, '', '', 1, 0, '', '', ''),
(96, 43, '', '', 1, 0, '', '', ''),
(97, 44, '', '', 1, 0, '', '', ''),
(98, 45, '', '', 1, 0, '', '', ''),
(99, 46, '', '', 1, 0, '', '', ''),
(100, 47, '', '', 1, 0, '', '', ''),
(101, 48, '', '', 1, 0, '', '', ''),
(102, 49, '', '', 1, 0, '', '', ''),
(103, 50, '', '', 1, 0, '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
