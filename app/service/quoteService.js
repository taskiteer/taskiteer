app.service(
    "quoteService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
            addquote: addquote,
            postQuote:postQuote,
            projectlist:projectlist,
            quotelist:quotelist,
            hireProvider:hireProvider,
            getassignedprodetails:getassignedprodetails,
            addreview:addreview,
            quotesendlist:quotesendlist,
            postReview:postReview,
            catwisejobs:catwisejobs,
            add_to_fav:add_to_fav,
            checkpaystatus:checkpaystatus,
            makepayment:makepayment,
            postBraintreeQuote:postBraintreeQuote,
            promoValidateCheck:promoValidateCheck,
        });
        function add_to_fav(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"favourites/addfav",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        function checkpaystatus(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"prof_payments/checkpay",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        function makepayment(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"prof_payments/setpayweb",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        function catwisejobs(cat_id)
        {
           //alert(lead_id);
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"leads/job_list_category/"+cat_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
       
        function addquote(lead_id,user_id)
        {
           //alert(lead_id);
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"RequestQuotes/addquote/"+lead_id+"/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        
        function postQuote(data)
        {
           //alert($rootScope.serviceurl);
           //console.log(data);
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"RequestQuotes/post_quote",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }
		
		function postBraintreeQuote(data)
        {
           //alert($rootScope.serviceurl);
           //console.log(data);
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"RequestQuotes/post_quote_braintree",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }
		
		function promoValidateCheck(promo){
			if(promo){
				var request = $http({
				method: "POST",
                url: $rootScope.serviceurl+"Promos/promoValidateCheck/",
				data:{promo:promo},
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                });
				return( request.then( handleSuccess, handleError ) );
			}
		}

        
        function projectlist(user_id,title)
        {
          
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"leads/projects/"+user_id+"/"+title,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        function quotelist(lead_id)
        {
           //alert(lead_id);
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"RequestQuotes/quoteslist/"+lead_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        function quotesendlist(user_id)
        {
           //alert(lead_id);
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"RequestQuotes/quotessendlist/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        function hireProvider(lead_id,provider_id)
        {
            var encodedString ='{"lead_id":"'+ lead_id +'","provider_id":"'+ provider_id +'"}';	
            var request = $http({
                method: "POST",
                data:encodedString,
                url: $rootScope.serviceurl+"leads/hireProvider",
                
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        function getassignedprodetails(provider_id,user_id,lead_id)
        {
           var encodedString ='{"user_id":"'+ user_id +'","provider_id":"'+ provider_id +'","lead_id":"'+ lead_id +'"}';	
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"users/getassignedprodetails",
                data:encodedString,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }

    function addreview(provider_id,user_id,lead_id)
        {
        
        var encodedString ='{"user_id":"'+ user_id +'","provider_id":"'+ provider_id +'","lead_id":"'+ lead_id +'"}';	
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"users/getassignedprodetails",
                data:encodedString,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }
           
        function postReview(data)
        {
           //alert(data);
           //console.log(data);
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"ratings/save_review",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }

        // ---
        // PRIVATE METHODS.
        // ---

        function handleError( response ) {
           
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            
            return( response.data );


        }
    }
);