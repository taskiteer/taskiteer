app.service(
    "PortfolioService", 
    function( $rootScope,$http, $q, $httpParamSerializer,Upload ) {
        // Return public API.
        return({
        savePortfolio:savePortfolio,
        listportfolio:listportfolio,
        deleteportfolio:deleteportfolio
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function savePortfolio(data)
        {
            var request = Upload.upload({
            url: $rootScope.serviceurl+"portfolios/uploadportfolio",
            data: data,
            headers: { 'Content-Type': 'application/json'}  
        });
        return( request.then( handleSuccess,handleError));
        }
         function listportfolio(user_id)
        {
           var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"portfolios/index/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) ); 
        }
        function deleteportfolio(id)
        {
           var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"portfolios/delete/"+id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) ); 
        }
        

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);