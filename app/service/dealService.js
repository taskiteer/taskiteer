app.service('dealService', function($q, $http, $window, $rootScope, Upload,$state) { 
 
    var subcatlist = function() {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"categories/subcat/",
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
          
    var alldeals = function() {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"deals/index/",
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
        
           var details = function(id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"deals/details/"+id,
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
          var mydeals = function(id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"deals/mydeals/"+id,
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
         
    /*var postDeal = function(data) {
	var request = Upload.upload({
            url: $rootScope.serviceurl+"deals/adddeal",
            data: data,
            headers: { 'Content-Type': 'application/json'}  
        });
            return( request.then( handleSuccess, handleError ));
	};*/
    
     var postDeal = function(data) {
         //alert(1);
	return $q(function(resolve, reject) {
	Upload.upload({
            url: $rootScope.serviceurl+"deals/adddeal",
            data: data
        }).then(function (response) {
            //console.log(response.data);
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
        //edit deal............................................
         var updateDeal = function(data) {
         //alert(1);
	return $q(function(resolve, reject) {
	Upload.upload({
            url: $rootScope.serviceurl+"deals/updatedeal",
            data: data
        }).then(function (response) {
            //console.log(response.data);
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
        //inactive deals..............................
        var remove = function(id) {
    return $q(function(resolve, reject) { 
					   
	console.log(id);				   
//var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"deals/inactive/"+id,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
               
                console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
    
    var dealdata = function(id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"deals/dealdata/"+id,
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
      
 return {
    alldeals: alldeals,
    details:details,
    mydeals:mydeals,
    subcatlist:subcatlist,
    postDeal:postDeal,
    remove:remove,
    dealdata:dealdata,
    updateDeal:updateDeal
};
    
});