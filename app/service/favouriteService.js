app.service(
    "favouriteService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
        unfavourite:unfavourite,
        listfavourites:listfavourites
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function unfavourite(id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"favourites/removefav/"+id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        function listfavourites(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"favourites/favpros/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
            
        }

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);