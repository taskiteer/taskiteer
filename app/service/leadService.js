app.service(
    "leadService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
            getMainCat: getMainCat,
            getSubCat:getSubCat,
            getQuestionset:getQuestionset,
            postJob:postJob
        });
       
        function getMainCat()
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"categories/maincategory_service",
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        function getSubCat(parent_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"categories/subcategory_service/"+parent_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        } 
        
        function getQuestionset(cat_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"questions/questionlist/"+cat_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        function postJob(data)
        {
             var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"leads/post_service",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );
        }

        
        
    
                   
        // ---
        // PRIVATE METHODS.
        // ---

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);