app.service(
    "chatdealsService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
        sentMessage:sentMessage,
        conversations:conversations,
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function conversations(sent_by,quote_id,logged_user)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"messagesdeals/conversations/"+sent_by+'/'+quote_id+'/'+logged_user,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        function sentMessage(data)
        {
           var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"messagesdeals/sent_message",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );         
        }
        

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);