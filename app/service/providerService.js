app.service(
    "providerService", 
    function( $rootScope,$http, $q, $httpParamSerializer,Upload) {
        // Return public API.
        return({
        profileimage:profileimage,
        viewprovider:viewprovider,
        coverimage:coverimage,
        saveprofile:saveprofile,
        uploadlisence:uploadlisence,
        sendLinkEmail:sendLinkEmail
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function coverimage(data)
        {
            var request = Upload.upload({
            url: $rootScope.serviceurl+"users/coverimage",
            data: data,
            headers: { 'Content-Type': 'application/json'}  
        });
        return( request.then( handleSuccess,handleError));
        }
        function profileimage(data)
        {
            var request = Upload.upload({
            url: $rootScope.serviceurl+"users/profileimage",
            data: data,
            headers: { 'Content-Type': 'application/json'}  
        });
        return( request.then( handleSuccess,handleError));
        }
        
        function viewprovider(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"users/user_profile/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
            
        }
        
        function saveprofile(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"users/saveprofile",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return( request.then( handleSuccess, handleError ) );
            
        }
        function uploadlisence(data)
        {
            var request = Upload.upload({
            url: $rootScope.serviceurl+"users/uploadlisence",
            data: data,
            headers: { 'Content-Type': 'application/json'}  
        });
        return( request.then( handleSuccess,handleError));
        }
        
        function sendLinkEmail(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"users/sendLinkEmail",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
            });
        return( request.then( handleSuccess,handleError));
        }

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);