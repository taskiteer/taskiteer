app.service(
    "creditCardService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
        saveCard:saveCard,
        viewcard:viewcard
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function saveCard(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"payments/savecreditcard",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        function viewcard(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"payments/viewcard/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
            
        }

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);