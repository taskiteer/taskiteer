app.service('restaurentService', function($q, $http, $window,$rootScope,Upload) { 
 
 var listAllRestaurents = function(user_id) {
        return $q(function(resolve, reject) {
						  	
   var encodedString ='{"user_id":"'+ user_id +'","type":"1"}';				   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/listrResturant",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
 
 
 
 var listMenuCategory = function() {
        return $q(function(resolve, reject) {
						  	
   var encodedString ='{"user_id":"","type":"1"}';				   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/listMenuCategory",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 }; 
 
 var homeResturants = function() {
        return $q(function(resolve, reject) {
						  	
   var encodedString ='{"user_id":"","type":"1"}';				   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/homeResturants",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
 
 
 
 
 var listAllOthers = function(user_id) {
        return $q(function(resolve, reject) {
						  	
   var encodedString ='{"user_id":"'+ user_id +'","type":"2"}';				   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/listrResturant",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 }; 
 
 
  var myMenus = function(user_id) {
        return $q(function(resolve, reject) {
						  	
   var encodedString ='{"user_id":"'+ user_id +'"}';				   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/myMenus",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 }; 
 
 
 var resturantDetailsMenu = function(user_id,restaurent_id) {
        return $q(function(resolve, reject) {
						  	
 var encodedString ='{"user_id":"'+ user_id +'","resturant_id":"'+ restaurent_id +'"}';			   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/resturantDetailsMenu",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };  
 
 
 var getBusinessDetails = function(user_id) {
        return $q(function(resolve, reject) {
						  	
 var encodedString ='{"user_id":"'+ user_id +'","resturant_id":""}';			   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/resturantDetails",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };  
 
 
	var removeMenuItems = function(menu_id) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"menu_id":"'+ menu_id +'"}';			   
		console.log(encodedString);	
//return false;
	
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"users/removeMenuItems",
	data: encodedString,
	headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	console.log(response.data);  
	if(response.data.Ack == "1") {
	console.log('ok');
	resolve(response.data); 
	} else {
	console.log('ok2');
	reject(response);
	}
	//console.log(response); 
	},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};   
 
  var saveBusinessDetails = function(data) {
    return $q(function(resolve, reject) { 
					   
	console.log(data);				   
//	return false;				   
        Upload.upload({
                url: $rootScope.serviceurl+"users/addResturants",
                data: data
            }).then(function (response) {
                  resolve(response.data); 
            }, function (response) {
                reject(response.data);
        }); 
    });
       
 };
 
/* var saveBusinessDetails = function(user,user_id) {
        return $q(function(resolve, reject) {
						  	
 var encodedString ='{"user_id":"'+ user_id +'","name":"'+ user.name +'","address":"'+ user.address +'","lat":"'+ user.lat +'","lang":"'+ user.lang +'","opening_hours":"'+ user.opening_hours +'","description":"'+ user.description +'","phone":"'+ user.phone +'","email":"'+ user.email +'","type":"'+ user.type +'"}';			   
						   
	console.log(encodedString);	
	//return false;
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/addResturants",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 }; */  
 
 
 
 
  var addMenu = function(data) {
    return $q(function(resolve, reject) { 
					   
	console.log(data);				   
//	return false;				   
        Upload.upload({
                url: $rootScope.serviceurl+"users/addMenus",
                data: data
            }).then(function (response) {
                  resolve(response.data); 
            }, function (response) {
                reject(response.data);
        }); 
    });
       
 };
 
 
 
 
/* var addMenu = function(user,user_id) {
        return $q(function(resolve, reject) {
						  	
 var encodedString ='{"vendor_id":"'+ user_id +'","resturant_id":"'+ user.resturant_id +'","category_id":"'+ user.category_id +'","menu_name":"'+ user.menu_name +'","price":"'+ user.price +'","menu_description":"'+ user.menu_description +'","type":"'+ user.type +'"}';			   
						   
	console.log(encodedString);	
	//return false;
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/addMenus",
         data: encodedString,
         headers: {'Content-Type': 'application/json'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };  */ 
 
 
 
 
 
 return {
	 listMenuCategory: listMenuCategory,
    listAllRestaurents: listAllRestaurents,
	listAllOthers: listAllOthers,
	resturantDetailsMenu: resturantDetailsMenu,
	getBusinessDetails: getBusinessDetails,
	saveBusinessDetails: saveBusinessDetails,
	myMenus: myMenus,
	homeResturants: homeResturants,
	addMenu : addMenu,
	removeMenuItems : removeMenuItems,
};
    
});