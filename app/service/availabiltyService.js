app.service(
    "availabiltyService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
        getEvents:getEvents,
        availabilitysetting:availabilitysetting,
        displaysettings:displaysettings
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function getEvents(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"availabilities/index_service/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        function displaysettings(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"availabilities/viewavailabilty/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        function availabilitysetting(data)
        {
            var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"availabilities/add_service",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
            });
            return( request.then( handleSuccess, handleError ) );
            
        }

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);