app.service(
    "NotifysettingService", 
    function( $rootScope,$http, $q, $httpParamSerializer ) {
        // Return public API.
        return({
        showsetting:showsetting,
        saveService:saveService
        
        });      
        // ---
        // PRIVATE METHODS.
        // ---
        function showsetting(user_id)
        {
            var request = $http({
                method: "GET",
                url: $rootScope.serviceurl+"notification_settings/details/"+user_id,
                headers: { 'Content-Type': 'application/json'}
            });
            return( request.then( handleSuccess, handleError ) );
        }
        
        function saveService(data)
        {
           var request = $http({
                method: "POST",
                url: $rootScope.serviceurl+"notification_settings/savesettings",
                data:data,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                
            });
            return( request.then( handleSuccess, handleError ) );         
        }
        

        function handleError( response ) {
            if (! angular.isObject( response.data ) ||! response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );
        }

        function handleSuccess( response ) {
            return( response.data );


        }
    }
);