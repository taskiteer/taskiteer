app.service('proService', function($q, $http, $window, $rootScope, Upload,$state) { 
 
   var subcatlist = function() {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"categories/subcat/",
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
        var searchPro = function(pro) {
	return $q(function(resolve, reject) {
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"providers/list_provider/",
        data:pro,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
            
            //console.log(response);
            //alert(response.data.Ack);
            //alert(3);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                resolve(response.data); 
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};

 
 return {
    subcatlist: subcatlist,
    searchPro: searchPro
};
    
});