app.service('userService', function($q, $http, $window, $rootScope, Upload,$state) { 
 
 var checkLogin = function(user) {
        return $q(function(resolve, reject) {
						   
						   
		var email = user.email;
		var password = user.password;
		var uuid='';
		var lat='';
		var lang='';
	
//var encodedString ='{"email":"'+ email +'","password":"'+ password +'","device_type":"android","token":"2654545454545"}';							   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/login_service",
         data: JSON.stringify(user),
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {               
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
 
 
 
 
 
 
var signup = function(user) {
return $q(function(resolve, reject) {

    
//var encodedString ='{"name":"'+ user.name +'","email":"'+ user.email +'","password":"'+ user.password +'","type":"'+ user.type +'","mobile_no":"'+ user.mobile_no +'"}';
	
	//console.log(encodedString);
	//return false;
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"users/signup_service",
	data: JSON.stringify(user),
        //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
            //alert('Thank you for Signup');
	resolve(response.data); 
	//console.log(response); 
	},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
 
 
 
 
 
 
 
  var updatePassword = function(user, user_id) {
        return $q(function(resolve, reject) {
						   
						
	
	    //var encodedString ='{"new_pwd":"'+ user.password +'","user_id":"'+ user_id +'","confirm_password":"'+ user.confirm_password +'"}';			   
		
		var encodedString ='{"old_password":"'+ user.old_pass +'","new_password":"'+ user.password +'","user_id":"'+ user_id +'","confirm_password":"'+ user.confirm_password +'"}';			   
						   
		//console.log(encodedString);		
		//return false;		   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/changepwd_service",
         data: encodedString,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
 
 
 
 
 
 
 
	var getCounters = function(user_id) {
		return $q(function(resolve, reject) {
		
		var encodedString ='{"user_id":"'+ user_id +'"}';						   
		
		
		
		$http({
		method: 'POST',
		url: $rootScope.serviceurl+"users/getCounters",
		data: encodedString,
		headers: {'Content-Type': 'application/json'}
		}).then(function (response) {
		
		if(response.data.Ack == "1") {
		//console.log('ok');
		resolve(response.data); 
		} else {
		//console.log('ok2');
		reject(response);
		}	
		
		
		},function(response) {
		//console.log(response);  
		reject(response);
		});
		});
		};
 
 
 
 
 
 
	var getAccountDetails = function(user_id) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"user_id":"'+ user_id +'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"users/myprofile_service",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};


	var categoryList = function(user_id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"categories/categorylist_service/"+user_id,
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        
        var catlistusers = function(user_id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"categories/userprovideservice/"+user_id,
        headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};

        
        

        

var getManageService = function(user_id) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"user_id":"'+ user_id +'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"businesses/business_det",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
        
        
        
        
        
 
 /* var updateManageService = function(user) {
        return $q(function(resolve, reject) {
            
            //var userInfo = JSON.parse($window.localStorage["userInfo"]);
	//$scope.user_id=userInfo.user_id;
						   
						
	
	    var encodedString ='{"name":"'+ user.name +'","user_id":"'+ user.user_id +'","email":"'+ user.email +'","ph_no":"'+ user.ph_no +'","address":"'+ user.address +'","cat_id":"","doc_1":"","doc_2":"","doc_3":""}';
	
            //console.log(encodedString);
            //eturn false;					   	   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"businesses/BusinessaddUpdateApi",
         data: encodedString,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           //console.log(response.data);  
           if(response.data.Ack == "1") {
               alert('Service Updated');
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 }; */

 var updateManageService = function(data) {
   return $q(function(resolve, reject) {
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"categories/saveproviderservice",
	data: data,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
 };
 
 
 
 
 
 
  var logout = function(user_id) {
        return $q(function(resolve, reject) {
						   
	
var encodedString ='{"user_id":"'+ user_id +'"}';						   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/logout_service",
         data: encodedString,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           if(response.data.Ack == "1") {
			  // console.log('ok');
              resolve(response.data); 
           } else {
			   // console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
 
 
 
 
 
 var forgetpass = function(user) {
    return $q(function(resolve, reject) {
    
    var encodedString ='{"email":"'+ user.email +'"}';              
    
    console.log(encodedString);

    //return false;
    
    
    $http({
    method: 'POST',
    url: $rootScope.serviceurl+"users/forgotpwd_service",
    data: encodedString,
    headers: {'Content-Type': 'application/json'}
    }).then(function (response) {

      alert(response.data.msg);
      //$('#myModal').modal('hide');
   $window.location.reload();
    
    if(response.data.Ack == "1") {
    //console.log('ok');
    resolve(response.data); 
    } else {
    //console.log('ok2');
    reject(response);
    } 
    
    
    },function(response) {
    //console.log(response);  
    reject(response);
    });
    });
    };
 
 
 
 
 
 
 
 
 
 /*var updateProfile = function(user) {
    return $q(function(resolve, reject) {
					   
	//console.log(data);				   
//	return false;				   
        Upload.upload({
                url: $rootScope.serviceurl+"users/updateProfile",
                data: data
            }).then(function (response) {
                  resolve(response.data); 
            }, function (response) {
                reject(response.data);
        }); 
    });
       
 };*/
 
 
 
var updateProfile = function(data) {
    return $q(function(resolve, reject) { 
					   
//	return false;				   
        Upload.upload({
                url: $rootScope.serviceurl+"users/editprofile_service",
                data: data
            }).then(function (response) {
                  resolve(response.data); 
            }, function (response) {
                reject(response.data);
        }); 
    });
       
 };
 
 
/*var updateProfile = function(data) {
        return $q(function(resolve, reject) {
						   
						
	
	    //var encodedString ='{"new_pwd":"'+ user.password +'","user_id":"'+ user_id +'","confirm_password":"'+ user.confirm_password +'"}';			   
						   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/editprofile_service",
         data: data,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
               alert('Profile Updated');
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };*/
 



 var phoneverify = function(user,user_id) {
        return $q(function(resolve, reject) {
						   
						
	
	    var encodedString ='{"user_id":"'+ user_id +'","mobile_no":"'+ user.phoneverify +'"}';			   
		
		//console.log(encodedString);	
		//return false;			   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/ph_verifyApi",
         data: encodedString,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
               //alert(response.data.msg);
			   //console.log('ok');
              resolve(response.data); 
           } else {
           	 //alert(response.data.msg);
			   // console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };



 var phoneverifyotp = function(user,user_id) {
        return $q(function(resolve, reject) {
						   
						
	
	    var encodedString ='{"user_id":"'+ user_id +'","ph_otp":"'+ user.phoneverifyotp +'"}';			   
		
		console.log(encodedString);	
		//return false;			   
						   
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/otp_verify",
         data: encodedString,
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {
               //alert(response.data.msg);
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
 
 
 
 

var fbsignup = function(fb_id,fb_name) {
return $q(function(resolve, reject) {


var encodedString ='{"email":"","name":"'+ fb_name +'","social_user_id":"'+ fb_id +'","device_type":"web","token":"","social_type":"fb"}';
	
	console.log(encodedString);
	//return false;
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"users/fbsignup_service",
	data: encodedString,
	headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	console.log(response.data);  
	if(response.data.Ack == "1") {
	console.log('ok');
	resolve(response.data); 
	} 

else if(response.data.Ack == "2") {
	$state.go('frontend.facebook_confirm');
	} 
	else {
	console.log('ok2');
	reject(response);
	}
	//console.log(response); 
	},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};




var facebookverifyemail = function(user,fb_id,fb_name) {
return $q(function(resolve, reject) {


var encodedString ='{"email":"'+ user.facebookemail +' ","name":"'+ fb_name +'","social_user_id":"'+ fb_id +'","device_type":"web","token":"","social_type":"fb"}';
	
	console.log(encodedString);
	//return false;
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"users/fbsignup_service",
	data: encodedString,
	headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	console.log(response.data);  
	if(response.data.Ack == "1") {
	console.log('ok');
	resolve(response.data); 
	alert("Thank you for Signup");
	} 


	else {
	console.log('ok2');
	reject(response);
	}
	//console.log(response); 
	},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};



//for getting joblisting..........................................
var getjoblist = function(user_id,status) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"user_id":"'+user_id+'","status":"'+status+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/myjobs",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                resolve(response.data); 
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
 
 
 var removeJob = function(id) {
    return $q(function(resolve, reject) { 
					   
	console.log(id);				   
var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/removejob",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
               
                console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
 
 //get applied provider listing.................
 var getproviderlist = function(lead_id) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"lead_id":"'+lead_id+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/appliedproviders",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
 
 //for accept and reject providers......................................
 var rejectProvider = function(id) {
    return $q(function(resolve, reject) { 
					   
	console.log(id);				   
var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/rejectProvider",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
              

                console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
        
 var acceptProvider = function(id) {
    return $q(function(resolve, reject) { 
					   
	console.log(id);				   
var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/acceptProvider",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
              

                console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
        
        
        //for getting jobdetails..........................................
var getjobdetails = function(id) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"jobs/jobdetails",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
        
        
//for applied joblists.......................................................
var getjoblistpro = function(user_id,status) {
    //alert(user_id);
    //alert(status);
	return $q(function(resolve, reject) {
	
	var encodedString ='{"user_id":"'+user_id+'","status":"'+status+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/myjobspro",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
	//alert(response.data.Ack);
	   if(response.data.Ack == "1") {
               //alert('k');
			   console.log('ok');
              resolve(response.data); 
           } else {
               //alert('k2');
			    console.log('ok2');
                resolve(response.data); 
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
var getnotificationcount = function(user_id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"messages/notification/"+user_id,
        headers: { 'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	}; 
        
var setread = function(user_id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"messages/change_notifystatus/"+user_id,
        headers: { 'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};          
        
 var getnotificationdetail = function(user_id) {
     //alert(3);
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"messages/notificationdetail/"+user_id,
        headers: { 'Content-Type': 'application/json'}
	}).then(function (response) {
            //alert(response.data.Ack);
	   if(response.data.Ack == "1") {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	}; 
 
 var removeJobPro = function(id) {
    return $q(function(resolve, reject) { 
					   
	console.log(id);				   
var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"leads/removejobpro",
	data: encodedString,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
               
                console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
        
        var getprodetails = function(id) {
	return $q(function(resolve, reject) {
	
	var encodedString ='{"id":"'+id+'"}';
	
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"/providers/details/"+id,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
		
	   if(response.data.Ack == "1") {
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }	
		
		
		},function(response) {
	//console.log(response);  
	reject(response);
	});
	});
	};
        var pay = function(data) {
	return $q(function(resolve, reject) {	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"payments/pay",
        data:data,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
        var refundrequest = function(data) {
	return $q(function(resolve, reject) {	
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"payments/refund",
        data:data,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } else {
                reject(response);
           }	
		
		
		},function(response) {
	reject(response);
	});
	});
	};
         var setcompleteserv = function(id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"leads/set_complete/"+id,
        headers: { 'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } 
           else 
           {
                reject(response);
           }	
			
        },function(response) {
	reject(response);
	});
	});
	}; 
        
        var getservloc = function(id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"users/service_location/"+id,
        headers: { 'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } 
           else 
           {
                reject(response);
           }	
			
        },function(response) {
	reject(response);
	});
	});
	}; 
        
        var saveservloc = function(data) {
	return $q(function(resolve, reject) {
	$http({
	method: 'POST',
	url: $rootScope.serviceurl+"users/save_serviceloc/",
        data:data,
	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } 
           else 
           {
                reject(response);
           }	
			
        },function(response) {
	reject(response);
	});
	});
	}; 
        
        var getAllCountries = function() {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"email_templates/list_countries",
	headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } 
           else 
           {
                reject(response);
           }	
			
        },function(response) {
	reject(response);
	});
	});
	}; 
        
        
        var getIsd = function(id) {
	return $q(function(resolve, reject) {
	$http({
	method: 'GET',
	url: $rootScope.serviceurl+"email_templates/getIsd/"+id,
	headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
	   if(response.data.Ack == 1) {
              resolve(response.data); 
           } 
           else 
           {
                reject(response);
           }	
			
        },function(response) {
	reject(response);
	});
	});
	}; 
        
        var saveReview = function(data) {
        return $q(function(resolve, reject) {    
						   
        $http({
         method: 'POST',
         url: $rootScope.serviceurl+"users/saveReview",
         data: JSON.stringify(data),
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
         }).then(function (response) {
           console.log(response.data);  
           if(response.data.Ack == "1") {               
			   console.log('ok');
              resolve(response.data); 
           } else {
			    console.log('ok2');
                reject(response);
           }
           //console.log(response); 
        },function(response) {
                     //console.log(response);  
          reject(response);
            });
        });
 };
        
        
 
 return {
    checkLogin: checkLogin,
	getCounters: getCounters,
	getAccountDetails: getAccountDetails,
	updateProfile: updateProfile,
	updatePassword: updatePassword,
	logout: logout,
	signup: signup,
        fbsignup: fbsignup,
        forgetpass: forgetpass,
        getManageService: getManageService,
        updateManageService: updateManageService,
	phoneverify:phoneverify,
	categoryList:categoryList,
	phoneverifyotp:phoneverifyotp,
	facebookverifyemail:facebookverifyemail,
        getjoblist:getjoblist,
        removeJob:removeJob,
        getproviderlist:getproviderlist,
        rejectProvider:rejectProvider,
        acceptProvider:acceptProvider,
        getjobdetails:getjobdetails,
        getjoblistpro:getjoblistpro,
        removeJobPro:removeJobPro,
        getnotificationcount:getnotificationcount,
        getnotificationdetail:getnotificationdetail,
        getprodetails:getprodetails,
        catlistusers:catlistusers,
        pay:pay,
        refundrequest:refundrequest,
        setcompleteserv:setcompleteserv,
        setread:setread,
        getservloc:getservloc,
        saveservloc:saveservloc,
        getAllCountries:getAllCountries,
        getIsd:getIsd,
        saveReview:saveReview
};
    
});