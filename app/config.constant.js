'use strict';

/**
 * Config constant
 */
app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        'home': ['app/Controllers/homeCtrl.js','app/service/productService.js'],
		'projects': ['app/Controllers/projectsCtrl.js','app/service/quoteService.js'],
		'provider_dshboard': ['app/Controllers/providerDshboardCtrl.js'],
		'my_account': ['app/Controllers/myAccountCtrl.js'],
		'change_password': ['app/Controllers/changePasswordCtrl.js'],
		'notification': ['app/Controllers/notificationCtrl.js','app/service/userService.js'],
		'manage_service': ['app/Controllers/manageServiceCtrl.js'],
		'manage_verifications': ['app/Controllers/verificationsCtrl.js'],
		'cms': ['app/Controllers/cmsCtrl.js','app/service/cmsService.js','app/filters/dateFilter.js'],
		'faq': ['app/Controllers/faqCtrl.js','app/service/cmsService.js'],
		'verify_phone': ['app/Controllers/verifyPhoneCtrl.js'],
		'verify_phone_otp': ['app/Controllers/verifyPhoneotpCtrl.js'],
		'facebook_confirm': ['app/Controllers/facebook_confirmCtrl.js'],
		'question': ['app/Controllers/questionCtrl.js'],
		'questionfinish': ['app/Controllers/questionfinishCtrl.js'],
		'questionsuccess': ['app/Controllers/questionsuccessCtrl.js'],
		'dashboard': ['app/Controllers/dashboardCtrl.js'],
		'messaging': ['app/Controllers/messagingCtrl.js'],
		'myproject': ['app/Controllers/myprojectCtrl.js'],
		'quotes': ['app/Controllers/quotesCtrl.js','app/service/quoteService.js',"app/assets/js/ng-rating.js","app/assets/css/ng-rating.css"],
		'request': ['app/Controllers/requestCtrl.js'],
                'lead': ['app/Controllers/leadsCtrl.js','app/service/leadService.js','app/assets/js/angular-bootstrap-datepicker.js'],
                'appliedproviders': ['app/Controllers/appliedprovidersCtrl.js'],
                'jobdetails': ['app/Controllers/jobdetailsCtrl.js'],
                'myappliedproject': ['app/Controllers/myappliedprojectCtrl.js',"app/service/quoteService.js"],
                'addquote': ['app/Controllers/addquoteCtrl.js','app/service/quoteService.js',"bower_components/sendbird/SendBird.min.js"],
                'chat':["app/Controllers/chatingsCtrl.js","app/service/chatService.js",'app/service/quoteService.js',"bower_components/sendbird/SendBird.min.js"],
                'availability': ['app/Controllers/availabilitiesCtrl.js','app/service/availabiltyService.js',"bower_components/fullcalendar/dist/fullcalendar.css"],
                'assignedprodetail': ['app/Controllers/assignedprodetailCtrl.js','app/service/quoteService.js'],
                'addreview': ['app/Controllers/addreviewCtrl.js','app/service/quoteService.js',"app/assets/js/ng-rating.js","app/assets/css/ng-rating.css"],
                'quotessend': ['app/Controllers/quotessendCtrl.js','app/service/quoteService.js'],
                'creditcard': ['app/Controllers/CreditCardCtrl.js','app/service/creditCardService.js',"app/directives/onlydigit.js",'app/service/quoteService.js'],
                'prodetail': ['app/Controllers/prodetailCtrl.js','app/service/userService.js'],
                'notify_setting': ['app/Controllers/notificationSettingCtrl.js','app/service/NotifysettingService.js'],
                'category': ['app/Controllers/projectsCtrl.js','app/service/quoteService.js'],
                'portfolio': ['app/Controllers/PortfolioCtrl.js','app/service/PortfolioService.js','node_modules/ng-file-upload/dist/ng-file-upload.min.js'],
                'searchpro': ['app/Controllers/searchproCtrl.js','app/service/proService.js'],
                'favourite': ['app/Controllers/favouriteCtrl.js','app/service/favouriteService.js'],
                'prolist': ['app/Controllers/prolistCtrl.js','app/service/proService.js','app/service/quoteService.js'],
		'deals': ['app/Controllers/dealsCtrl.js','app/service/dealService.js','node_modules/ng-file-upload/dist/ng-file-upload.min.js'],
                'mydeals': ['app/Controllers/mydealsCtrl.js','app/service/dealService.js'],
                'postdeal': ['app/Controllers/postdealCtrl.js','app/service/dealService.js','app/assets/js/angular-bootstrap-datepicker.js'],
                'calendersetting': ['app/Controllers/availabilitiesCtrl.js','app/service/availabiltyService.js'],
                'dealdetails': ['app/Controllers/dealdetailsCtrl.js','app/service/dealService.js'],
                'chatsdeals':["app/Controllers/chatsdealsCtrl.js","app/service/chatsdealsService.js"],
                'editdeal': ['app/Controllers/editdealCtrl.js','app/service/dealService.js'],
                'testpage': ['app/Controllers/testCtrl.js'],
                'servicelocation':['app/Controllers/serviceLocCtrl.js','app/service/userService.js'],
                'transaction':['app/Controllers/transactionCtrl.js','app/service/transService.js'],
                'orderhistory':['app/Controllers/orderCtrl.js','app/service/orderService.js'],
                'serviceprovider':['app/Controllers/providersCtrl.js','app/service/providerService.js',"app/directives/onlydigit.js","bower_components/ngmap/build/scripts/ng-map.min.js"],
                'publicprofile':['app/Controllers/publicProfileCtrl.js','app/service/providerService.js',"app/directives/onlydigit.js","bower_components/ngmap/build/scripts/ng-map.min.js"]


	
				
    },
    

    //*** angularJS Modules

    modules: [
        {
            name: 'angularMoment',
            files: ['vendor/moment/angular-moment.min.js']
        },
        {
            name: 'ngFileUpload',
            files: ['node_modules/ng-file-upload/dist/ng-file-upload.min.js','node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js']
        },
         {
            name: 'jkAngularRatingStars',
            files: ['node_modules/angular-jk-rating-stars/dist/jk-rating-stars.js','node_modules/angular-jk-rating-stars/dist/jk-rating-stars.css',"https://fonts.googleapis.com/icon?family=Material+Icons"]
        },
        {
            name: 'ng-bootstrap-datepicker',
            files:["app/assets/js/angular-bootstrap-datepicker.js"]
        },
        {
            name: 'ngMap',
            files:["bower_components/ngmap/build/scripts/ng-map.min.js"]
        },
        
        {
            name: 'ngRating',
            files:["app/assets/js/ng-rating.js","app/assets/css/ng-rating.css"]
        },
        
        
        
        
        
        
    ]
});
