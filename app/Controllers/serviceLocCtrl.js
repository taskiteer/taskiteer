'use strict';
/** 
 * controllers used for the login
 */
app.controller('serviceLocCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $q, userService, $window) {

    $scope.data = {};
    $scope.user = {};
    //alert('a');

    if (!$window.localStorage["userInfo"]) {
        $state.go('frontend.home');
        return false;
    }
    $scope.getCurrentUserType();
    //console.log($scope.current_user_type);  		
    $scope.all_notifications();
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.getlocation=function()
    {
         userService.getservloc($scope.user_id).then(function(data) {
            if(data.Ack==1)
            {
                $scope.user=data.response;
                console.log("User Location",$scope.user);
            }

        }, function(err) {
            console.log(err);
        });
    }
    $scope.savelocation=function()
    {
        
        if(! $scope.servicearea.$invalid)
        {
            userService.saveservloc($scope.user).then(function(data) {
            if(data.Ack==1)
            {
                alert("Service Area Edited successfully");
            }

        }, function(err) {
            console.log(err);
        });
        }
        
    }




});