'use strict';
/** 
 * controllers used for the login
 */
app.controller('publicProfileCtrl', function ($rootScope,$state,$stateParams, $scope, $http, $location,$timeout, $q,providerService,userService,$window) {
$scope.data = {};
$scope.user = {};
$scope.all_notifications();

if (!$window.localStorage["userInfo"]) 
{
    
    $state.go('frontend.home');
} 
$scope.getCurrentUserType(); 
var userInfo = JSON.parse($window.localStorage["userInfo"]);
$scope.user_id=$stateParams.id;
$scope.showall=0;
$scope.showphone=0;
$scope.editname=0;
$scope.editabout=0;
$scope.editbusiness=0;
$scope.editcontact=0;
//$scope.phone=userInfo.phone;
$scope.userloc={id:$scope.user_id};
$scope.redious=['5','20','50','100','100+'];
$scope.viewprovider=function()
{
    providerService.viewprovider($scope.user_id).then(function(data) {
    if(data.Ack==1)
    {
        $scope.response=data.response;
    }
        
    }, function(err) {
        console.log(err);
    });
}

$scope.profileimage=function(file)
{
    $scope.uploaddata={image:file,id:$scope.user_id}
    
    providerService.profileimage($scope.uploaddata).then(function(data) {
    if(data.Ack==1)
    {
        $scope.response.image=data.image;
    }
    
        
    }, function(err) {
        console.log(err);
    });
    
    
    
}

$scope.coverimage=function(file)
{
    $scope.uploadcover={coverimage:file,id:$scope.user_id}
    providerService.coverimage($scope.uploadcover).then(function(data) {
    console.log("Cover IMG",data.Ack);    
    if(data.Ack==1)
    {
        $scope.response.coverimage=data.coverimage;
    }
    
        
    }, function(err) {
        console.log(err);
    });
    
    
    
}

$scope.saveprofile=function(params)
{
    
    if(params)
    {
    providerService.saveprofile($scope.response).then(function(data) {
    if(data.Ack==1)
    {
        //$scope.viewprovider();
        $scope.editname=0;
        $scope.editabout=0;
        $scope.editbusiness=0;
        $scope.editcontact=0;
    }
        
    }, function(err) {
        console.log(err);
    });
    }
}

$scope.uploadlisence=function(file)
{
    $scope.lisencedata={lisence:file,id:$scope.user_id}
    providerService.uploadlisence($scope.lisencedata).then(function(data) {
    if(data.Ack==1)
    {
        alert("License stored successfully");
    }
        
    }, function(err) {
        console.log(err);
    });
    
    
    
}

$scope.uploadlisence1=function(file)
{
    $scope.uploaddata={lisence:file,id:$scope.user_id}
    providerService.uploadlisence($scope.uploaddata).then(function(data) {
    if(data.Ack==1)
    {
        alert("License stored successfully");
    }
        
    }, function(err) {
        console.log(err);
    });
    
    
    
}

$scope.showservice = function() {
        userService.catlistusers($scope.user_id).then(function(response) {
            if (response.Ack == 1) {
                $scope.categoryList = response.categories;
                $scope.provideservice = response.service;

            }
        }, function(err) {
            console.log(err);
        });
    }

    $scope.updatecheckbox = function(service_id, service_val) {
        if (service_id) {

               
                $scope.provideservice.push(service_val);
           

        } else {
            $scope.deleteitem = $scope.provideservice.indexOf(service_val);
            $scope.provideservice.splice($scope.deleteitem, 1);

        }

        console.log("Select Service", $scope.provideservice);
        //$scope.answersheet = $scope.answergroups.toString();


    }

     $scope.updateService = function() {
        $scope.servicedata = {
            user_id: $scope.user_id,
            cat_id: $scope.provideservice.toString(),
            type:"prof"

        };
        
        if($scope.servicedata.cat_id)
        {
        userService.updateManageService($scope.servicedata).then(function(response) {
            if(response.Ack==1)
            {
                $state.go('frontend.step2');
            }
        }, function(err) {
            console.log(err);
        });
      }
      else{
          alert("Please select atleast one service");
      }

    };

    $scope.updatecheckbox = function(service_id, service_val) {
        if (service_id) {

               
                $scope.provideservice.push(service_val);
           

        } else {
            $scope.deleteitem = $scope.provideservice.indexOf(service_val);
            $scope.provideservice.splice($scope.deleteitem, 1);

        }

        console.log("Select Service", $scope.provideservice);
        //$scope.answersheet = $scope.answergroups.toString();


    }

    $scope.savelocation=function()
    {
        
        if(! $scope.locationform.$invalid)
        {
            userService.saveservloc($scope.userloc).then(function(data) {
            if(data.Ack==1)
            {
                $state.go("frontend.verify");
            }

        }, function(err) {
            console.log(err);
        });
        }
        
    }
    
    $scope.showotp=function()
    {
        userService.getAccountDetails($scope.user_id).then(function(data) {
            if(data.Ack==1)
            {
                $scope.ph_otp=data.response.ph_otp;
                console.log("OTP",$scope.ph_otp);
            }

        }, function(err) {
            console.log(err);
        });
    }
    
    $scope.checkotp=function()
    {
        if(! $scope.locationform.$invalid)
        {
            if($scope.ph_otp==$scope.userloc.otp)
            {
               $scope.userloc.is_verify=1;
               $scope.userloc.type= 'SP';
                providerService.saveprofile($scope.userloc).then(function(data) {
                if(data.Ack==1)
                {
                    var userInfo = JSON.parse($window.localStorage["userInfo"]);
                    userInfo.type = 'SP';
                    $window.localStorage["userInfo"] = JSON.stringify(userInfo);
                    //$window.localStorage["userType"] = 'SP';
                    //$rootScope.current_user_type = 'SP';
                    //alert($rootScope.current_user_type);
                    alert("Thank You for Your Requesting become a Service Provider .");
                    $state.go("frontend.providerprofile");

                }

                }, function(err) {
                    console.log(err);
                }); 
            }
            else
            {
               alert("Invalid Code");    
            }
            
            
        }
    }
    
    
    
    
			
  
});

