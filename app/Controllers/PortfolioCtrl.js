'use strict';
/** 
 * controllers used for the login
 */
app.controller('PortfolioCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, PortfolioService,$window) {  
if (!$window.localStorage["userInfo"]) 
{
    
    $state.go('frontend.home');
    return false;
} 
$scope.getCurrentUserType();
$scope.all_notifications();

var userInfo = JSON.parse($window.localStorage["userInfo"]);
$scope.user_id=userInfo.user_id;
$scope.response={};
$scope.listing=function()
{
    PortfolioService.listportfolio($scope.user_id).then(function(data) {
    if(data.Ack==1)
    {
        $scope.response=data.response;
    }
        
    }, function(err) {
        console.log(err);
    });
}

$scope.saveportfolio=function(file)
{
    $scope.portdata={image:file,user_id:$scope.user_id};
    PortfolioService.savePortfolio($scope.portdata).then(function(data) {
    if(data.Ack==1)
    {
        $scope.listing();
    }
        
    }, function(err) {
        console.log(err);
    });
}

$scope.deleteimg=function(id)
{
     $scope.tt = confirm("Are you want to remove this!");
     if($scope.tt)
     {
         PortfolioService.deleteportfolio(id).then(function(data) {
        if(data.Ack==1)
        {
            $scope.listing();
        }

        }, function(err) {
            console.log(err);
        });
     }
     
}













});

