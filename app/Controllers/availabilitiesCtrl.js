'use strict';
/** 
 * controllers used for the login
 */
app.controller('availabilitiesCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, availabiltyService,$window) {  
if (!$window.localStorage["userInfo"]) 
{
    
    $state.go('frontend.home');
    return false;
} 
$scope.response={};
$scope.availablehours=[{id:"00:00",name:"12:00 a.m."},{id:"01:00",name:"1:00 a.m."},{id:"02:00",name:"2:00 a.m."},{id:"03:00",name:"3:00 a.m."},
    {id:"04:00",name:"4:00 a.m."},{id:"05:00",name:"5:00 a.m."},{id:"06:00",name:"6:00 a.m."},{id:"07:00",name:"7:00 a.m."},{id:"08:00",name:"8:00 a.m."},
    {id:"09:00",name:"9:00 a.m."},{id:"10:00",name:"10:00 a.m."},{id:"11:00",name:"11:00 a.m."},{id:"12:00",name:"12:00 p.m."},
    {id:"13:00",name:"1:00 p.m."},{id:"14:00",name:"2:00 p.m."},{id:"15:00",name:"3:00 p.m."},{id:"16:00",name:"4:00 p.m."},
    {id:"17:00",name:"5:00 p.m."},{id:"18:00",name:"6:00 p.m."},{id:"19:00",name:"7:00 p.m."},{id:"20:00",name:"8:00 p.m."},
    {id:"21:00",name:"9:00 p.m."},{id:"22:00",name:"10:00 p.m."},{id:"23:00",name:"11:00 p.m."},{id:"24:00",name:"12:00 midnight"},
    ];    
$scope.modes=["days","weeks","months"];  
$scope.numbers=[
{id:"0",name:'Same day booking'},{id:"1",name:'1'},{id:"2",name:'2'},{id:"3",name:'3'},{id:"4",name:'4'},{id:"5",name:'5'},
{id:"6",name:'6'},{id:"7",name:'7'},{id:"8",name:'8'},{id:"9",name:'9'},{id:"10",name:'10'},{id:"11",name:'11'},
{id:"12",name:'12'},{id:"13",name:'13'},{id:"14",name:'14'},{id:"15",name:'16'},{id:"17",name:'17'},{id:"18",name:'18'},
{id:"19",name:'19'},{id:"20",name:'20'},{id:"21",name:'21'},{id:"22",name:'23'},{id:"24",name:'24'},{id:"25",name:'25'},
{id:"19",name:'19'},{id:"20",name:'20'},{id:"21",name:'21'},{id:"22",name:'23'},{id:"24",name:'24'},{id:"25",name:'25'},
{id:"26",name:'26'},{id:"27",name:'28'},{id:"29",name:'29'},{id:"30",name:'30'},    
]; 
$scope.bookdays=[
{id:"0",name:'No limit'},{id:"1",name:'1'},{id:"2",name:'2'},{id:"3",name:'3'},{id:"4",name:'4'},{id:"5",name:'5'},
{id:"6",name:'6'},{id:"7",name:'7'},{id:"8",name:'8'},{id:"9",name:'9'},{id:"10",name:'10'},{id:"11",name:'11'},
{id:"12",name:'12'},{id:"13",name:'13'},{id:"14",name:'14'},{id:"15",name:'16'},{id:"17",name:'17'},{id:"18",name:'18'},
{id:"19",name:'19'},{id:"20",name:'20'},{id:"21",name:'21'},{id:"22",name:'23'},{id:"24",name:'24'},{id:"25",name:'25'},
{id:"19",name:'19'},{id:"20",name:'20'},{id:"21",name:'21'},{id:"22",name:'23'},{id:"24",name:'24'},{id:"25",name:'25'},
{id:"26",name:'26'},{id:"27",name:'28'},{id:"29",name:'29'},{id:"30",name:'30'},

];

$scope.getCurrentUserType();   
//console.log($scope.current_user_type);  		
$scope.all_notifications(); 
$scope.events={};
var userInfo = JSON.parse($window.localStorage["userInfo"]);
$scope.user_id=userInfo.user_id;
$scope.init=function()
{
 availabiltyService.getEvents($scope.user_id).then(function(data) {
                  
    $scope.events=data.events;
    
    $scope.uiConfig = {
      calendar:{
        height: 600,
        editable: true,
        defaultView:'agendaWeek',
        header:{
          //left: 'agendaWeek',
          right: 'today prev,next',
        },
        events:$scope.events,
        
        eventClick: $scope.alertEventOnClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize
      }
    };
}, function(err) {
alert('Internal Error');
});   
}


$scope.saveavailability=function()
{
    $scope.response.user_id=$scope.user_id;
    availabiltyService.availabilitysetting($scope.response).then(function(data) {
    if(data.Ack==1)
    {
        alert('Your availability set successfully');
        $scope.showavailability();
    }
        
}, function(err) {
alert('Internal Error');
});
}


$scope.showavailability=function()
{
    availabiltyService.displaysettings($scope.user_id).then(function(data) {
    if(data.Ack==1)
    {
        $scope.response=data.response.availability;
    }
    $scope.timezones=data.response.timezones;
        
}, function(err) {
alert('Internal Error');
});
}











});

