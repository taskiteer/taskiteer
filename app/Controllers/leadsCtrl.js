'use strict';
/** 
 * controllers used for the login
 */
app.controller('leadsCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, leadService,$window,$stateParams) {    
$scope.getCurrentUserType();  
$scope.is_disabled=0;
//console.log($scope.current_user_type); 
//alert($stateParams.maincat);
$scope.all_notifications();

$scope.data = {};
$scope.user = {};
$scope.lead={};
$scope.categoryList={};
$scope.subcategoryList={};
$scope.questions={};
$scope.datalist={};
$scope.answersheet=[];
$scope.count=0;
$scope.answergroups=[];
$scope.checkboxval=[];
$scope.start=0;
$scope.selectedQuestion = '';
$scope.selectedQuestionId = '';
$scope.skill_required = '';
$scope.date_required = '';
//$scope.maincategory = {id:$stateParams.maincat_id,name:$stateParams.maincat_name};




 $rootScope.$on('testEvent', function (event,category) {
    $scope.parent_id=category;
    $scope.fromhome=1;

    $scope.populatesubcat();
    console.log("Brodcast Cat",category);
  });
  
  $rootScope.$on('testEventSubCat', function (event,category) {
    $scope.category_id=category;
    $scope.fromhome=1;

    $scope.populatequestions();
    console.log("Brodcast Cat",category);
  });

$scope.showmaincat=function()
{
     leadService.getMainCat().then(function(data) {
                if(data.Ack==1)
                {                  
                    $scope.maincatlists=data.categoryList;
                    console.log("Main Cat",$scope.categoryList);
                   //$scope.maincategory = $stateParams.maincat;
                   
                }
                
              }, function(err) {
                alert('Internal Error');
              });
              
         //for populate subcat..................................
         if($stateParams.maincat_id!='null'&&$stateParams.maincat_id>0)
         {
        $scope.parent_id=$stateParams.maincat_id; 
        leadService.getSubCat($scope.parent_id).then(function(data) {
            //alert(data.Ack);
        if(data.Ack==1)
        {                  
            $scope.subcategoryList=data.categoryList;

        }
        else
        {
            $scope.subcategoryList={};
        }

        }, function(err) {
        alert('Internal Error');
        });
    }
}

$scope.populatesubcat=function()
{
    if($scope.parent_id)
    {
    leadService.getSubCat($scope.parent_id).then(function(data) {
    if(data.Ack==1)
    {                  
        $scope.subcategoryList=data.categoryList;
        $("#JobSelection").modal("hide");
        $("#JobSubcat").modal("show");

    }
    else
    {
        $scope.subcategoryList={};
    }

    }, function(err) {
    alert('Internal Error');
    });
    }
 
 
 

}

$scope.populatequestions=function()
{
    $scope.cat_id=$scope.category_id; 
    if($scope.cat_id)
    {
    leadService.getQuestionset($scope.cat_id).then(function(data) {
    if(data.Ack==1)
    {                  
        $scope.questions=data.questionList;
        $scope.total_question=data.total_question;
        //alert($scope.questions.length);
        $("#JobSubcat").modal("hide");
        //$("#JobLocation").modal("show");
        $timeout( function(){
            //$("#questionModal0").modal("show");
            $("#allquestionpopup").modal("show");
        }, 1000 );
        
        

    }
    else
    {
        $scope.questions={};
    }
    
    }, function(err) {
    alert('Internal Error');
    });
    }
 
 
 

}

$scope.postjob=function()
{
    
//    if(!$scope.jobpost.$invalid)
//    {
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id=userInfo.user_id;
    $scope.questioset=[];
    
    angular.forEach($scope.answersheet, function(value, key) {
    $scope.questioset.push(value);     
    });
    
    //$scope.lead={user_id:$scope.user_id,category_id:$scope.category_id,address:$scope.address,description:$scope.description,title:$scope.title};
    $scope.lead={user_id:$scope.user_id,category_id:$scope.category_id,address:'',description:$scope.description,title:'',skill_required:$scope.skill_required,date_required:$scope.date_required};
    $scope.parmlist={lead:$scope.lead,questions:$scope.questioset};    
    
    leadService.postJob($scope.parmlist).then(function(data) {
    $scope.is_disabled=1; 
    $("#JobLocation").modal("hide");
    $("#questionModal").modal("hide");
    
    if(data.Ack==1)
    {                  
        
       $state.go("frontend.success");
        
    }
    else
    {
        alert("Job post failure");
        $scope.is_disabled=0;     

    }

    }, function(err) {
    alert('Internal Error');
    $scope.is_disabled=0;     

    });
        
    //}
    
    
    
   
   
}

$scope.updateparent=function(maincat)
{
  $scope.parent_id=maincat

}
$scope.updatesubcategory=function(subcat)
{
   $scope.category_id= subcat;
}
$scope.updateQuestionSelect=function(question_selected)
{
   $scope.selectedQuestion = question_selected.name;
   $scope.selectedQuestionId = question_selected.id;
}


$scope.updateradio=function(output)
{
    $scope.obj={q_id:output.q_id,answer:output.name};
    $scope.answersheet[output.q_id]= $scope.obj;
    
    
}

$scope.updateselct=function(output)
{
   $scope.obj={q_id:output.q_id,answer:output.name};
   $scope.answersheet[output.q_id]= $scope.obj;
    
}

$scope.updatecheckbox=function(q_id,answer,ansval)
{

    
    if(answer)
    {
     $scope.answergroups.push(q_id+'-'+answer);
    }
    else{
    $scope.deleteitem=$scope.answergroups.indexOf(q_id+'-'+ansval); 
    $scope.answergroups.splice($scope.deleteitem, 1);  
    angular.forEach($scope.answergroups, function(value, key) {
     $scope.splittedStringArray = value.split("-");
     if($scope.splittedStringArray.indexOf(q_id)!=-1)
     {
         $scope.deleteindex=$scope.checkboxval.indexOf(ansval);
         $scope.checkboxval.splice($scope.deleteindex, 1);  
     }
     
         
    });
    
    
    
        
    }
     angular.forEach($scope.answergroups, function(value, key) {
     $scope.splittedStringArray = value.split("-");
     if($scope.splittedStringArray.indexOf(q_id)!=-1 && $scope.checkboxval.indexOf($scope.splittedStringArray[1])==-1)
     {
         $scope.checkboxval.push($scope.splittedStringArray[1]);
         console.log("Hello");
     }
     
         
    });
    
    
    $scope.checkboxstr=$scope.checkboxval.toString(); 
    
        console.log($scope.checkboxval,"String");

    
    $scope.obj={q_id:q_id,answer:$scope.checkboxstr};
    $scope.answersheet[q_id]= $scope.obj;
    
    
    
     
}

$scope.updatetext=function(questionobj,answer)
{
     $scope.obj={q_id:questionobj.id,answer:answer};
     $scope.answersheet[questionobj.id]= $scope.obj;

}


$scope.goprevious=function(showmodal,hidemodal)
{
     $("#"+hidemodal).modal("hide");
     $("#"+showmodal).modal("show");
}

$scope.savejoblocation=function()
{
    if(! $scope.jobloc.$invalid)
    {
        //$("#JobLocation").modal("hide");
        //$("#questionModal0").modal("show");
        $scope.postjob();
        
    }
    
    
}

$scope.next=function(start)
{ 
$scope.start=start;    
$("#questionModal"+$scope.start).modal("hide");    
$scope.start=$scope.start+1;
$scope.progress = Math.floor(($scope.start*100) / $scope.total_question);
if($scope.total_question==$scope.start)
{
    //$scope.postjob();
    $("#JobLocation").modal("show");

}
else{
      $("#questionModal"+$scope.start).modal("show");    
     
 
}
    
}



$scope.selectquestions=function()
{ 
    $("#allquestionpopup").modal("hide");
    $("#JobLocation").modal("show");
}

$scope.back=function(start)
{
 $scope.start=  start;  
 $("#questionModal"+$scope.start).modal("hide");      
 $scope.start=$scope.start-1;
 $scope.progress = Math.floor(($scope.start*100) / $scope.total_question);
 $("#questionModal"+$scope.start).modal("show");    
 console.log("Progress",$scope.progress);
        
    
}

//alert('a');
$scope.showmainmodal=function()
{
    alert(1);
$("#JobSelection").modal("show");
}
 
});

