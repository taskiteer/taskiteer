'use strict';
/** 
 * controllers used for the login
 */
app.controller('favouriteCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $window, favouriteService) {
    $scope.data = {};
    $scope.user = {};
    $scope.favourites = {};
    if (!$window.localStorage["userInfo"]) {
        $state.go('frontend.home');
        return false;
    }
    $scope.all_notifications();
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.favlist = function() {
        favouriteService.listfavourites($scope.user_id).then(function(data) {
            $scope.Ack = data.Ack;
            if (data.Ack == 1) {
                $scope.favourites = data.List;
            }

        }, function(err) {
            console.log(err);
        });
    }

    $scope.remove = function(id) {
        $scope.tt = confirm("Are you want to unfavourite this provider!");
        if ($scope.tt) {
            favouriteService.unfavourite(id).then(function(data) {
                if (data.Ack == 1) {
                    $scope.favlist();
                }

            }, function(err) {
                console.log(err);
            });
        }

    }
    
    $scope.go_profile=function(user)
    {
        $state.go("frontend.prodetail",{id:user});
        
    }



});