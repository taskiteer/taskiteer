'use strict';
/** 
 * controllers used for the login
 */
app.controller('manageServiceCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $templateCache, $window, userService) {


    $scope.data = {};
    $scope.user = {};
    if (!$window.localStorage["userInfo"]) {
        $state.go('frontend.home');
        return false;
    }
    $scope.getCurrentUserType();
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.showservice = function() {
        userService.catlistusers($scope.user_id).then(function(response) {
            if (response.Ack == 1) {
                $scope.categoryList = response.categories;
                $scope.provideservice = response.service;

            }
        }, function(err) {
            console.log(err);
        });
    }
    $scope.updateService = function() {
        $scope.servicedata = {
            user_id: $scope.user_id,
            cat_id: $scope.provideservice.toString()
        };
        userService.updateManageService($scope.servicedata).then(function(response) {
            if(response.Ack==1)
            {
                alert("Your job type has been saved successfully");
            }
        }, function(err) {
            console.log(err);
        });

    };

    $scope.updatecheckbox = function(service_id, service_val) {
        if (service_id) {

               
                $scope.provideservice.push(service_val);
           

        } else {
            $scope.deleteitem = $scope.provideservice.indexOf(service_val);
            $scope.provideservice.splice($scope.deleteitem, 1);

        }

        console.log("Select Service", $scope.provideservice);
        //$scope.answersheet = $scope.answergroups.toString();


    }


});