'use strict';
/** 
 * controllers used for the login
 */
app.controller('myappliedprojectCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, userService,quoteService,$window,Upload,$stateParams,Notification
) {
//alert('1');
$scope.jobList=[];
$scope.data = {};
$scope.user = {};

/*if (!$window.localStorage["userInfo"]) { 
	$state.go('frontend.home');
	return false;
	}*/


$scope.getCurrentUserType();   
//console.log($scope.current_user_type);  		
$scope.all_notifications(); 
var userInfo = JSON.parse($window.localStorage["userInfo"]);
	$scope.user_id=userInfo.user_id;
  
    
$scope.page_caption_status=$stateParams.status;


//alert($scope.user_id);
//alert($stateParams.status);
    userService.getjoblistpro($scope.user_id,$stateParams.status).then(function(response) {
        //alert(1);
        //alert(response.Ack);
       $('.loader').show();
	$scope.Ack=response.Ack;
        //alert($scope.Ack);
	if(response.Ack == '1') {
            
           //alert('asas'); 
           //alert(response.jobList);
				$scope.jobList=response.jobList;
                                console.log($scope.jobList);
                                $('.loader').hide();
                                
                                 //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.jobList.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.jobList.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();
				
              }else{
		$('.loader').hide();		
		//alert('asas22'); 
                $scope.Ack=0;
			  }
	
	
	
														   
 }, function(err) {
     $('.loader').hide();
         console.log(err); 
    }); 

    
    $scope.remove = function(job_id){	
	userService.removeJobPro(job_id).then(function(response) {
            //alert(response.Ack);
           if(response.Ack == '1') {
            //$scope.submissionSuccess = "Data Updated";
            
	//alert('Successfully deleted');
        $scope.getjoblistpro();       
		} else {
	//alert('Error !!!!');			
			} 
        });
    };
    
     $scope.sendQuote=function(lead)
    {
         $scope.lead_id=lead.id;
         $scope.params={user_id:$scope.user_id,lead_id:lead.id};
		 $state.go("frontend.addquote",{lead_id:lead.id});
         //quoteService.checkpaystatus($scope.params).then(function(data) {
         /*if(data.is_paid==1)
         {
             $state.go("frontend.addquote",{lead_id:lead.id});
         }
         else
         {
              $scope.paydata={user_id:$scope.user_id,lead_id:lead.id,amount:lead.price};
              $scope.tempMsgDetails = {button:"Pay",message:"You have to pay $"+lead.price+" For This "};
              Notification.error({templateUrl:'app/views/messages/payment.html',scope:$scope});
         }*/
         
            

        //}, function(err) {
            console.log(err);
        //});
        
    }
    
    $scope.getpayment=function()
    {
        
        quoteService.makepayment($scope.paydata).then(function(data) {
         if(data.Ack==1)
         {
           $state.go("frontend.addquote",{lead_id:$scope.lead_id});

         }
         else if(data.Ack==2)
         {
             $scope.tempMsgDetails = {button:"OK",message:"Please save credit card details first",title:"Payment"};
             Notification.error({templateUrl:'app/views/messages/credit.html',scope:$scope});
         }
         
         
         else
         {
              $scope.tempMsgDetails = {button:"OK",message:"Internal error from  Braintree"};
              Notification.error({templateUrl:'app/views/messages/payment.html',scope:$scope});
         }
         
            

        }, function(err) {
            console.log(err);
        });
        
        
    }
    
    $scope.gocreditpage=function()
    {
        $state.go("frontend.creditcard");
    }
    
   
 
});

