'use strict';
/** 
 * controllers used for the login
 */
app.controller('homeCtrl', function ($rootScope, $scope, $http, $location,$timeout,$window, productService, $stateParams, $state) {

    
$scope.data = {};
$scope.user = {};
$scope.all_notifications();

 $scope.islogin = $stateParams.islogin;
$scope.issignup = $stateParams.issignup;

 $scope.loginmsg = 'Initial login and signup completion for Service Professionals.User is directed to begin looking at categories and choosing a service.';
$scope.signupmsg = 'Initial signup successful for Service Professionals.Please check your email for confirmation and login to browse categories.';
//alert('a');
$scope.getCurrentUserType();  
if ($window.localStorage["userInfo"]) 
{
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.city = userInfo.city;
    $scope.state = userInfo.state;
} 
$scope.userExist=angular.isUndefined($scope.user_id);
$scope.serachkeyword ={name:'', id:''};
$scope.getSubcat=function(cat)
{
    //console.log(cat);
    if (! $window.localStorage["userInfo"]) 
    {
        $("#loginModal").modal("show");
    }
    else{
            
            $rootScope.$broadcast("testEvent",cat);

    }
}

$scope.getSubCatQuestion=function(cat)
{
    //console.log(cat);
    if (! $window.localStorage["userInfo"]) 
    {
        $("#loginModal").modal("show");
    }
    else{
            
            $rootScope.$broadcast("testEventSubCat",cat);

    }
}


                
                
$scope.setClientData = function(item){
     if (item){
         $scope.serachkeyword.name = item.name;
         $scope.serachkeyword.id = item.id;
         //console.log($scope.serachkeyword);
         
            //$state.go('frontend.projects',{title:$scope.serachkeyword.id});
     }

}

$scope.popupOpenFromSearch = function(){
    if($scope.serachkeyword.name && $scope.serachkeyword.id){
        $scope.getSubCatQuestion($scope.serachkeyword.id);
    }
}


$scope.catImageClick = function(id){    
    $scope.getSubCatQuestion(id);
    
}
 

 
        
        
        productService.homecategorylist().then(function(response) {
		
		$scope.isExists=response.Ack;
		if(response.Ack == '1') {
		$scope.categoryList=response.categoryList;
		console.log($scope.categoryList);	
		
		} else {
                    $scope.categoryList = [];
		}
	
	
	
				   
	}, function(err) {
            $scope.categoryList=[];
	console.log(err); 
	});
        
        productService.homesubcategorylist().then(function(response) {
		
		//$scope.isExists=response.Ack;
		if(response.Ack == '1') {
		$scope.subcategoryList=response.categoryList;
                //console.log('subcat');
		//console.log($scope.subcategoryList);	
		
		} else {
                    $scope.subcategoryList = [];
		}
	
	
	
				   
	}, function(err) {
            $scope.subcategoryList=[];
	console.log(err); 
	});
        
        $scope.testbrodcast=function()
        {
           $rootScope.$emit('testEvent');

        }
        
        
        
//        $scope.searchcat = function(){
//         $scope.searchval=  $scope.name; 	
//         //alert($scope.searchval);
//	//var userInfo = JSON.parse($window.localStorage["userInfo"]);
//        
//        productService.searchcategory($scope.searchval).then(function(response) {	
//
//	//alert(response.Ack);
//	$state.go('frontend.projects',{ 'searchval': $scope.searchval});
//														   
//        }, function(err) {
//                console.log(err); 
//           }); 	
//
//       }
        
       $scope.searchcat = function(){
       $scope.searchval=  $scope.title; 
       $state.go('frontend.projects',{title:$scope.searchval});
														   
       
       }


      productService.professional_service().then(function(response) {
		
		$scope.isExists=response.Ack;
		if(response.Ack == '1') {
			
		$scope.profesionalList=response.response;
		console.log("ok25");
		console.log($scope.profesionalList);	
		
		} else {
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});   
        
       /* $scope.gocategory=function(cat_id)
        {
	$state.go('frontend.category',{ cat_id: cat_id});
        } */
    
    $scope.gocategory=function(cat_id,cat_name)
        {
	//alert(cat_id);
        //alert(cat_name);
       
        $state.go('frontend.postjob',{ 'maincat_id': cat_id,'maincat_name':cat_name});
        }
        
          productService.servicelisthome().then(function(response) {
		

		$scope.isExists=response.Ack;
		if(response.Ack == '1') {
			
		$scope.serviceList=response.response;
		
		//alert(JSON.stringify($scope.serviceList))
		} else {
		}
	
	
	
				   
	}, function(err) {
	console.log(err); 
	});
        
	
});

