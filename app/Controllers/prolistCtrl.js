'use strict';
/** 
 * controllers used for the login
 */
app.controller('prolistCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, proService,quoteService,$window,Upload,$stateParams) {

 

//$scope.list = [];
$scope.data = {};
$scope.user = {};
$scope.pro={};


$scope.getCurrentUserType();
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;

 $scope.subcat_id=$stateParams.subcat_id;  
 $scope.lat=$stateParams.lat;  
 $scope.lng=$stateParams.lng;  
 $scope.pro={subcat_id:$scope.subcat_id,lat:$scope.lat,lng:$scope.lng};       
        //post quote form submit..........................................
 

//alert('submitted');
//$scope.review={from_id:user_id,to_id:detail.id,review_from_name:sidebar_name,review_to_name:detail.name,work_quality:review.work_quality,resposiveness:review.resposiveness,value_money:review.value_money,timely_response:review.timely_response,avg_score:review.firstRate,comment:review.comment};


        proService.searchPro($scope.pro).then(function(response) {
	$('.loader').show();
        $scope.Ack = response.Ack;
	//alert(response.Ack);
	if(response.Ack == '1') {
           $('.loader').hide('fast');
            $scope.list=response.List;
                                
            //console.log($scope.list);
            
            //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.list.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.list.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();
        
		} else {
	$('.loader').hide('fast');
	//alert('No data found');			
			}
	
	
	
	
																	   
       }, function(err) {
         console.log(err); 
    }); 
			
    $scope.addfav = function(provider_id) {
        $scope.params={user_id:$scope.user_id,pro_id:provider_id};
        quoteService.add_to_fav($scope.params).then(function(data) {
            if (data.Ack == 1) 
            {
                alert("Add to favourite successfully");
            } 
            else 
            {
                alert(data.msg);
            }
        });
    };
  
});

