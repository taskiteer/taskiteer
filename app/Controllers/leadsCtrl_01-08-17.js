'use strict';
/** 
 * controllers used for the login
 */
app.controller('leadsCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, leadService,$window,$stateParams) {  
if (!$window.localStorage["userInfo"]) 
{
    
    $state.go('frontend.home');
    return false;
}    
$scope.getCurrentUserType();  
$scope.is_disabled=0;
//console.log($scope.current_user_type); 
//alert($stateParams.maincat);
$scope.Notification(); 
$scope.data = {};
$scope.user = {};
$scope.lead={};
$scope.categoryList={};
$scope.subcategoryList={};
$scope.questions={};
$scope.datalist={};
$scope.answersheet=[];
$scope.count=0;
$scope.answergroups=[];
$scope.checkboxval=[];
$scope.maincategory = {id:$stateParams.maincat_id,name:$stateParams.maincat_name};

$scope.showmaincat=function()
{
     leadService.getMainCat().then(function(data) {
                if(data.Ack==1)
                {                  
                    $scope.categoryList=data.categoryList;
                   //$scope.maincategory = $stateParams.maincat;
                   
                }
                
              }, function(err) {
                alert('Internal Error');
              });
              
         //for populate subcat..................................
         if($stateParams.maincat_id!='null'&&$stateParams.maincat_id>0)
         {
        $scope.parent_id=$stateParams.maincat_id; 
        leadService.getSubCat($scope.parent_id).then(function(data) {
            //alert(data.Ack);
        if(data.Ack==1)
        {                  
            $scope.subcategoryList=data.categoryList;

        }
        else
        {
            $scope.subcategoryList={};
        }

        }, function(err) {
        alert('Internal Error');
        });
    }
}

$scope.populatesubcat=function(maincat)
{
    $scope.parent_id=maincat.id; 
    leadService.getSubCat($scope.parent_id).then(function(data) {
    if(data.Ack==1)
    {                  
        $scope.subcategoryList=data.categoryList;
        $("#JobSelection").modal("hide");
        $("#JobSubcat").modal("show");

    }
    else
    {
        $scope.subcategoryList={};
    }

    }, function(err) {
    alert('Internal Error');
    });
 
 
 

}

$scope.populatequestions=function(maincat)
{
    $scope.cat_id=maincat.id; 
    leadService.getQuestionset($scope.cat_id).then(function(data) {
    if(data.Ack==1)
    {                  
        $scope.questions=data.questionList;
        

    }
    else
    {
        $scope.questions={};
    }

    }, function(err) {
    alert('Internal Error');
    });
 
 
 

}

$scope.postjob=function()
{
    
    if(!$scope.jobpost.$invalid)
    {
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id=userInfo.user_id;
    $scope.questioset=[];
    
    angular.forEach($scope.answersheet, function(value, key) {
    $scope.questioset.push(value);     
    });
    
    $scope.lead={user_id:$scope.user_id,category_id:$scope.category_id.id,address:$scope.address,description:$scope.description,title:$scope.title};
    $scope.parmlist={lead:$scope.lead,questions:$scope.questioset};    
    
    leadService.postJob($scope.parmlist).then(function(data) {
    $scope.is_disabled=1;     
    if(data.Ack==1)
    {                  
        
       $state.go("frontend.success");
        
    }
    else
    {
        alert("Job post failure");
        $scope.is_disabled=0;     

    }

    }, function(err) {
    alert('Internal Error');
    $scope.is_disabled=0;     

    });
        
    }
    
    
    
   
   
}

$scope.updateradio=function(output)
{
    $scope.obj={q_id:output.q_id,answer:output.name};
    $scope.answersheet[output.q_id]= $scope.obj;
    
}

$scope.updateselct=function(output)
{
   $scope.obj={q_id:output.q_id,answer:output.name};
   $scope.answersheet[output.q_id]= $scope.obj;
    
}

$scope.updatecheckbox=function(q_id,answer,ansval)
{

    
    if(answer)
    {
     $scope.answergroups.push(q_id+'-'+answer);
    }
    else{
    $scope.deleteitem=$scope.answergroups.indexOf(q_id+'-'+ansval); 
    $scope.answergroups.splice($scope.deleteitem, 1);     
        
    }
     angular.forEach($scope.answergroups, function(value, key) {
     $scope.splittedStringArray = value.split("-");
     if($scope.splittedStringArray.indexOf(q_id)!=-1)
     {
         $scope.checkboxval.push($scope.splittedStringArray[1]);
     }
     
     
         
    });
    
    
    $scope.checkboxstr=$scope.checkboxval.toString(); 
    
    $scope.obj={q_id:q_id,answer:$scope.checkboxstr};
    $scope.answersheet[q_id]= $scope.obj;
    
    
     
}

$scope.updatetext=function(questionobj,answer)
{
     $scope.obj={q_id:questionobj.id,answer:answer};
     $scope.answersheet[questionobj.id]= $scope.obj;
}




//alert('a');


 
});

