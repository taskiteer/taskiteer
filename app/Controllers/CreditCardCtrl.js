'use strict';
/** 
 * controllers used for the login
 */
app.controller('CreditCardCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, creditCardService,$window, quoteService, Notification) {  
if (!$window.localStorage["userInfo"]) 
{
    
    $state.go('frontend.home');
    return false;
} 
$scope.all_notifications();

$scope.card={};
$scope.getCurrentUserType(); 
var userInfo = JSON.parse($window.localStorage["userInfo"]);
$scope.user_id=userInfo.user_id;
$scope.saveCard=function()
{
    
    $scope.card.user_id=$scope.user_id;
    if(! $scope.creditform.$invalid)
    {
        //Stripe.setPublishableKey('pk_test_qL1KkH10y5otPrp3aebaB51q');
        Stripe.setPublishableKey('pk_live_Gr32LLso92kIcbLQ83GkwcYN');
        $scope.card.expDateYear = '20'+$scope.card.expDateYear;
        Stripe.createToken({
            number: $scope.card.creditCardNumber,
            cvc: $scope.card.cvv2Number,
            exp_month: $scope.card.expDateMonth,
            exp_year: $scope.card.expDateYear
          }, $scope.stripeResponseHandler);
        
    }
    
    
    
    
    
}
$scope.stripeResponseHandler = function(status, response){
    $scope.card.token = response.id;
    //console.log($scope.card);
    //return false;
    creditCardService.saveCard($scope.card).then(function(data) {
        if(data.Ack==1)
        {
            $scope.card={};
            $scope.creditform.$submitted=false;
            
            if($rootScope.payment_from_card_user_id != '' && $rootScope.payment_from_card_lead_id != '' && $rootScope.payment_from_card_amount != ''){
                $scope.paydata = {
                    user_id : $rootScope.payment_from_card_user_id,
                    lead_id : $rootScope.payment_from_card_lead_id,
                    amount : $rootScope.payment_from_card_amount
                }
                quoteService.makepayment($scope.paydata).then(function(data) {
                    if(data.Ack==1)
                    {
                        $rootScope.payment_from_card_user_id = '';
                        $rootScope.payment_from_card_lead_id = '';
                        $rootScope.payment_from_card_amount = '';
                        $scope.tempMsgDetails = {button:"OK",message:"Successfully paid"};
                        Notification.error({templateUrl:'app/views/messages/credit.html',scope:$scope});
                      $state.go("frontend.addquote",{lead_id:$scope.paydata.lead_id});

                    }                  


                    else
                    {
                         //$scope.tempMsgDetails = {button:"OK",message:"Internal error from  Stripe"};
                         $scope.tempMsgDetails = {button:"OK",message:data.err.jsonBody.error.message};
                         Notification.error({templateUrl:'app/views/messages/payment.html',scope:$scope});
                    }



                   }, function(err) {
                       console.log(err);
                   });
            }else{
                alert("Your card information saved successfully");
            }
            
            

        }                
    }, function(err) {
        alert('Internal Error');
    });
}


$scope.view=function()
{
    creditCardService.viewcard($scope.user_id).then(function(data) {
        if(data.Ack==1)
        {
           $scope.card= data.response;
           $scope.Ack=1;
        }
        else
        {
          $scope.Ack=0;    
        }
        }, function(err) {
        alert('Internal Error');
        });
}











});

