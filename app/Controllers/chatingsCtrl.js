'use strict';
/** 
 * controllers used for the login
 */
app.controller('chatingsCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $q, chatService, quoteService, $window, $stateParams) {
    if (!$window.localStorage["userInfo"]) {

        $state.go('frontend.home');
        return false;
    }
    $scope.all_notifications();

    var sb = new SendBird({
        appId: $rootScope.SENDBIRDAPPID
    });
    $scope.getCurrentUserType();
    //console.log($scope.current_user_type);  		
    $scope.chats = {};
    $scope.message = {};
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.sent_by = $scope.user_id;
    $scope.quote_id = $stateParams.quote_id;
    $scope.oldchats = []




    $scope.chat = function() {


        chatService.quotedetails($scope.quote_id, $scope.user_id).then(function(data) {
            if (data.Ack == 1) {
                $scope.quotedetail = data.response;
                $scope.chatuser = $scope.quotedetail.chatuser;
                $scope.Userdetail = $scope.quotedetail.Userdetail;
                $scope.channelUrl = $scope.quotedetail.channelUrl;
                $scope.unique_id = $scope.quotedetail.unique_id;
                $scope.chats = {};
                $scope.oldchats = [];
                sb.connect("service" + $scope.user_id, function(user, error) {
                    if (error) {
                        console.log("Send Bird Error", error);
                    } else {
                        var nickname = userInfo.name;
                        var profileUrl = userInfo.profile_image;
                        sb.updateCurrentUserInfo(nickname, profileUrl, function(response, error) {});
                        var channelUrl = $scope.quotedetail.channelUrl;
                        sb.OpenChannel.getChannel(channelUrl, function(channel, error) {
                            if (error) {
                                console.error(error);
                                return;
                            }

                            channel.enter(function(response, error) {
                                if (error) {
                                    console.error(error);
                                    return;
                                }
                                var messageListQuery = channel.createPreviousMessageListQuery();
                                messageListQuery.load(70, true, function(messageList, error) {
                                    if (error) {
                                        console.error(error);
                                        return;
                                    }

                                    $scope.Oldchat = messageList;

                                    angular.forEach($scope.Oldchat, function(value, key) {
                                        $scope.obj = {};
                                        if (value._sender.userId == 'service' + $scope.user_id) {
                                            $scope.obj = {
                                                sent_by: {
                                                    id: $scope.user_id,
                                                    image: value._sender.profileUrl,
                                                    name: value._sender.nickname
                                                },
                                                msg: value.message,
                                                howlongago: $scope.howlongago(value.createdAt),
                                                posted_through: "sendbird"
                                            };
                                        }
                                        if (value._sender.userId == 'service' + $scope.chatuser) {
                                            $scope.obj = {
                                                sent_by: {
                                                    id: $scope.chatuser,
                                                    image: value._sender.profileUrl,
                                                    name: value._sender.nickname
                                                },
                                                msg: value.message,
                                                howlongago: $scope.howlongago(value.createdAt),
                                                posted_through: "sendbird"
                                            };
                                        }
                                        if (!$scope.isEmpty($scope.obj)) {
                                            $scope.oldchats.push($scope.obj);
                                        }

                                    });
                                    console.log("Old Chat", $scope.oldchats);
                                    $scope.chats = {
                                        is_hired: $scope.is_hired,
                                        conversation: $scope.oldchats,
                                        Userdetail: $scope.Userdetail
                                    };

                                    $scope.$apply();
                                });

                                var ChannelHandler = new sb.ChannelHandler();
                                ChannelHandler.onMessageReceived = function(channel, message) {
                                    if (message._sender.userId == 'service' + $scope.chatuser) {
                                        $scope.obj2 = {
                                            sent_by: {
                                                id: $scope.chatuser,
                                                image: message._sender.profileUrl,
                                                name: message._sender.nickname
                                            },
                                            msg: message.message,
                                            howlongago: $scope.howlongago(message.createdAt),
                                            posted_through: "sendbird"
                                        };
                                        $scope.oldchats.unshift($scope.obj2);
                                        $scope.chats.conversation = $scope.oldchats;
                                        $scope.$apply();

                                    }

                                    //$scope.chat();
                                    console.log("Message Received", message);
                                };
                                sb.addChannelHandler($scope.unique_id, ChannelHandler);

                            });
                        });




                    }

                });




            }
        }, function(err) {
            //alert('Internal Error');
        });


    }


    //setInterval(function () {
    //        $scope.chat();
    //    }, 10000);

    $scope.sent_message = function() {
        $scope.message.sent_by = $scope.user_id;
        $scope.message.quote_id = $scope.quote_id;
        $scope.message.posted_through = 'Web';
        chatService.sentMessage($scope.message).then(function(data) {
            sb.connect("service" + $scope.user_id, function(user, error) {
                if (error) {
                    console.log("Send Bird Error", error);
                } else {
                    var nickname = userInfo.name;
                    var profileUrl = userInfo.profile_image;
                    sb.updateCurrentUserInfo(nickname, profileUrl, function(response, error) {});
                    var channelUrl = $scope.channelUrl;
                    sb.OpenChannel.getChannel(channelUrl, function(channel, error) {
                        if (error) {
                            console.error(error);
                            return;
                        }
                        channel.enter(function(response, error) {
                            if (error) {
                                console.error(error);
                                return;
                            }
                            var message = $scope.message.msg;
                            var data = "";
                            var customType = "";
                            channel.sendUserMessage(message, data, customType, function(message, error) {
                                if (error) {
                                    console.error(error);
                                    return;
                                }

                                // onSent
                                $scope.message = {};
                                $scope.obj1 = {
                                    sent_by: {
                                        id: $scope.user_id,
                                        image: message._sender.profileUrl,
                                        name: message._sender.nickname
                                    },
                                    msg: message.message,
                                    howlongago: $scope.howlongago(message.createdAt),
                                    posted_through: "sendbird"
                                };
                                $scope.oldchats.unshift($scope.obj1);
                                $scope.chats.conversation = $scope.oldchats;
                                $scope.$apply();

                                //$scope.chat();

                                console.log(message, "Message Sent");
                            });




                        });




                    });




                }

            });


        }, function(err) {
            alert('Internal Error');
        });




    }


    $scope.hireProvider = function(lead_id, provider_id) {
        quoteService.hireProvider(lead_id, provider_id).then(function(response) {
            //alert(response.Ack);
            if (response.Ack == '1') {
                $scope.submissionSuccess = "Successfully hired";

                $scope.message = {};
                $scope.chat();
            } else {
                alert('Error !!!! Please try again');
            }
        });
    };
    $scope.addfav = function(provider_id) {
        $scope.params = {
            user_id: $scope.user_id,
            pro_id: provider_id
        };
        quoteService.add_to_fav($scope.params).then(function(data) {
            if (data.Ack == 1) {
                alert("Add to favourite successfully");
            } else {
                alert(data.msg);
            }
        });
    };



});