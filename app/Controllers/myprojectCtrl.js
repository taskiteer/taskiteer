'use strict';
/** 
 * controllers used for the login
 */
app.controller('myprojectCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, userService,$window,Upload,$stateParams
) {
$scope.jobList = [];
$scope.data = {};
$scope.user = {};
$scope.refunddata = {};
$scope.all_notifications();
if (!$window.localStorage["userInfo"]) { 
	$state.go('frontend.home');
	return false;
	}
$scope.getjoblist = function(){

$scope.getCurrentUserType();   
var userInfo = JSON.parse($window.localStorage["userInfo"]);
	$scope.user_id=userInfo.user_id;
$scope.status=$stateParams.status;
//alert($stateParams.status);
//alert($scope.user_id);
    userService.getjoblist($scope.user_id,$stateParams.status).then(function(response) {
        $('.loader').show();
	$scope.Ack = response.Ack;
        $scope.is_savecard = response.is_savecard;
        //alert($scope.Ack);
        //alert(response.Ack);
	if(response.Ack == '1') {
               $('.loader').hide('fast'); 
           //alert('asas'); 
           //alert(response.jobList);
				$scope.jobList=response.jobList;
                                console.log($scope.jobList);
                                
                                 //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.jobList.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.jobList.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();
				
              }else{
		    $('.loader').hide('fast');		
		// alert('asas22'); 		  
			  }
	
	
	
														   
 }, function(err) {
         console.log(err); 
    }); 
    
};
    
    $scope.remove = function(job_id){	
	userService.removeJob(job_id).then(function(response) {
            //alert(response.Ack);
           if(response.Ack == '1') {
            //$scope.submissionSuccess = "Data Updated";
            
	//alert('Successfully deleted');
        $scope.getjoblist();       
		} else {
	//alert('Error !!!!');			
			} 
        });
    };
    
    $scope.payment = function(quote){
        
        if($scope.is_savecard)
        {
        $scope.loader=quote.lead_id;
	userService.pay(quote).then(function(data) {
           if(data.Ack == 1) 
           {
                $scope.getjoblist();
                $scope.loader=0;
                alert("Payment Successfull");
            } 
            else 
            {
              $scope.loader=0;
              alert('Payment Failure');			
			
            } 
        });
        }
        else
        {
            alert("Please Put Credit Card Information First");
            $state.go("frontend.creditcard");
        }
        
    };
    
    $scope.showmodal=function(quote)
    {
        $scope.refunddata=quote;
        $("#refundmodal").modal("show");
        console.log("Refund Data",$scope.refunddata);
    }
    
    $scope.refund=function()
    {
        if(!$scope.refundform.$invalid)
        {
                $scope.showloader=1;
                userService.refundrequest($scope.refunddata).then(function(data) {
               if(data.Ack == 1) 
               {
                    $scope.getjoblist();
                    $scope.showloader=0;
                    alert("Refund Successfull");
                    $scope.refunddata.reason="";
                    $scope.refundform.$submitted=false;
                    $("#refundmodal").modal("hide");
                    
                } 
                else 
                {
                  $scope.showloader=0;
                  alert('Refund Failure');
                  $("#refundmodal").modal("hide");


                } 
            });
        }
        
    }
    
    $scope.setcomplete=function(id)
    {
        userService.setcompleteserv(id).then(function(data) {
		
		if(data.Ack ==1) 
                {
                    $scope.getjoblist();
		} 
                else 
                {
		}				   
	}, function(err) {
	console.log(err); 
	});
    }
    
    
 
});

