'use strict';
/** 
 * controllers used for the login
 */
app.controller('chatsdealsCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, chatdealsService,$window,$stateParams) {  
if (!$window.localStorage["userInfo"]) 
{
    
    $state.go('frontend.home');
    return false;
} 
$scope.getCurrentUserType();   
//console.log($scope.current_user_type);  		
$scope.Notification(); 
$scope.chats={};
$scope.message={};
var userInfo = JSON.parse($window.localStorage["userInfo"]);
$scope.user_id=userInfo.user_id;
$scope.sent_by = $scope.user_id;
$scope.deal_id = $stateParams.deal_id;
var range = [];
var notrated = [];

//alert($scope.deal_id);
$scope.chat=function()
{
    chatdealsService.conversations($scope.sent_by,$scope.deal_id,$scope.user_id).then(function(data) {
        //alert(1);
    if(data.Ack==1)
    {
       $scope.chats=data.chats;
       //$scope.chats.Userdetail.rateing=data.chats.Userdetail.rateing;
       $scope.chats.Userdetail.notrated=parseInt(5-$scope.chats.Userdetail.rateing);
       //alert($scope.chats.Userdetail.notrated);
       for(var i=0;i<$scope.chats.Userdetail.rateing;i++) {
        range.push(i);
      }
      $scope.range = range;
      
      for(var k=0;k<$scope.chats.Userdetail.notrated;k++) {
        notrated.push(k);
      }
      $scope.notrated = notrated;
       //console.log($scope.range);
    }                
    }, function(err) {
    //alert('Internal Error');
    });
}

setInterval(function () {
        $scope.chat();
    }, 10000);
    
$scope.sent_message=function()
{
    $scope.message.sent_by=$scope.user_id;
    $scope.message.quote_id=$scope.deal_id;
    $scope.message.posted_through='Web';
    chatdealsService.sentMessage($scope.message).then(function(data) {
    if(data.Ack==1)
    {
         $scope.message={};
         $scope.chat();
       
    }                
    }, function(err) {
    alert('Internal Error');
    });
    
    
    
    
}
    


});

