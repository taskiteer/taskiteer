'use strict';
/** 
 * controllers used for the login
 */
app.controller('dealsCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q,dealService, $window,Upload) {

 //alert(1);

//$scope.list = [];
$scope.data = {};
$scope.user = {};


$scope.getCurrentUserType();
$scope.all_notifications();

    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;

	 dealService.alldeals().then(function(response) {
	$('.loader').show();
        $scope.Ack = response.Ack;
	//alert(response.Ack);
	if(response.Ack == '1') {
           $('.loader').hide('fast');
            $scope.list=response.response;
                                
            //console.log($scope.list);
            
             //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.list.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.list.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();
        
		} else {
	$('.loader').hide('fast');
	//alert('No data found');			
			}
	
	
	
	
																	   
       }, function(err) {
         console.log(err); 
    }); 
			
  
});

