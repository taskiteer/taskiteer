'use strict';
/** 
 * controllers used for the login
 */
app.controller('quotessendCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, quoteService,$window,Upload,$stateParams) {

$scope.list = [];
$scope.data = {};
$scope.user = {};

$scope.quotesendlist = function(){
$scope.getCurrentUserType();   
//console.log($scope.current_user_type);  		
var userInfo = JSON.parse($window.localStorage["userInfo"]);
	$scope.user_id=userInfo.user_id; 
 //lert($scope.user_id);
    quoteService.quotesendlist($scope.user_id).then(function(response) {
        $('.loader').show();
	$scope.Ack = response.Ack;
        console.log(response);
	if(response.Ack == '1') {
            $('.loader').hide('fast');
            console.log(response);
          
           //alert(response.jobList);
            $scope.list=response.List;

            console.log(response.List);
            //alert(response.Detail.job.id);
            //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.list.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.list.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();                
				
              }else{
			$('.loader').hide('fast');	
              
			  }
	
	
	
														   
 }, function(err) {
         console.log(err); 
    }); 
    
};   

    
	
});

