'use strict';
/** 
 * controllers used for the login
 */
app.controller('editdealCtrl', function ($rootScope,$state, $scope, $http, $location,$timeout, $q, dealService,$window,Upload,$stateParams) {

 

$scope.list = [];
$scope.data = {};
$scope.user = {};
$scope.subcat_id = '';


$scope.getCurrentUserType();
$scope.all_notifications();

    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.id = $stateParams.id;
//alert($scope.user_id);
   
       dealService.subcatlist().then(function(response) {
           //alert(response.Ack);
            if (response.Ack == 1) {
                $scope.categoryList = response.categories;
                $scope.data.user_id=userInfo.user_id;
              
            }
        }, function(err) {
            console.log(err);
        });
        
        dealService.dealdata($scope.id).then(function(response) {
           //alert(response.Ack);
           console.log(response.response);
            if (response.Ack == 1) {
                $scope.data = response.response.Deal;
                $scope.data.user_id=userInfo.user_id;
                $scope.data.location=userInfo.address;
                $scope.data.lat=userInfo.lat;
                $scope.data.lng=userInfo.lang;
              
            }
        }, function(err) {
            console.log(err);
        });
        
        
        //post quote form submit..........................................
    $scope.postDeal = function(data){
        //console.log($scope.data);
        //alert('submitted');
    dealService.updateDeal($scope.data).then(function(response) {
	
	//alert(response.Ack);
	if(response.Ack == '1') {
        $state.go('frontend.mydeals');
        }
        else
        {
            alert("Please try again");
        }
        
         }, function(err) {
         console.log(err); 
    }); 
			
    }; 
  
});

