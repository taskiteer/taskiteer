'use strict';
/** 
 * controllers used for the login
 */
app.controller('addquoteCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $q, quoteService, $window, Upload, $stateParams,Notification) {
	
	

    $scope.data = {};
    $scope.user = {};
    $scope.quote = {};
	$scope.payment_method_nonce = ''
    //$scope.detail.quotePosted={};
    $scope.all_notifications();
	$scope.paymentRequired=1;
	
    if (!$window.localStorage["userInfo"]) {
        $state.go('frontend.home');
        return false;
    }
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;

    var sb = new SendBird({
        appId: $rootScope.SENDBIRDAPPID
    });
    $scope.addquote = function() {
        $scope.getCurrentUserType();
        //console.log($scope.current_user_type);  		
        quoteService.addquote($stateParams.lead_id, $scope.user_id).then(function(response) {
           if (response.Ack == '1') {
                console.log(response);
				$scope.detail = response.Detail;
				$scope.detail.payment_method_nonce='';

            } else {

              $scope.tempMsgDetails = {button:"OK",message:"You have to pay for this",title:"Payment"};
              Notification.error({templateUrl:'app/views/messages/one_button.html',scope:$scope});
              $timeout(function()
              {
               $state.go("frontend.projects");    
              },2000);
              
              
            }




        }, function(err) {
            console.log(err);
        });

    };

	$scope.payQuote=function(detail){
		if (!$scope.quoteform.$invalid && detail.payment_method_nonce) {
			$scope.quote = {
                posted_to: detail.job.userid,
                posted_by: $scope.user_id,
                lead_id: detail.job.id,
                comment: detail.comment,
                price: detail.price,
				payment_method_nonce:detail.payment_method_nonce
            };
			quoteService.postBraintreeQuote($scope.quote).then(function(response) {
				if (response.Ack == '1') {
                    $scope.submissionSuccess = "Quote Submitted";
                    sb.connect("service" + $scope.user_id, function(user, error) {
                    if (error) {
                        console.log("Send Bird Error", error);
                    } else {
                        var nickname = userInfo.name;
                        var profileUrl = userInfo.profile_image;
                        sb.updateCurrentUserInfo(nickname, profileUrl, function(response, error) {});
                        var channelUrl=detail.job.channelUrl;
                        sb.OpenChannel.getChannel(channelUrl, function (channel, error) {
                        if (error) {
                            console.error(error);
                            return;
                        }

                        channel.enter(function(response, error){
                            if (error) {
                                console.error(error);
                                return;
                            }
                      var message=detail.comment;  
                      var data="";
                      var customType="";
                      channel.sendUserMessage(message, data, customType, function(message, error){
                        if (error) {
                            console.error(error);
                            return;
                        }

                        // onSent
                        console.log(message,"Message Sent");
                    });
      
                            
                            
                        });
                    });

                        
                        
                        
                    }

                });


                    $scope.addquote();
                    alert('Quote submitted successfully');




                } else {
                    $scope.getCurrentUserType();
                    alert('Try Again');
                }
			}, function(err) {
                console.log(err);
            });
			console.log(detail);
		}else{
			alert('Please select payment method');
		}
		
	}
	
	//promo_code apply method
	$scope.promoCheck=function(promo){
		if(promo){
			quoteService.promoValidateCheck(promo).then(function(response) {
				if (response.Ack == '1') {
					$scope.paymentRequired=0;
				}else{
					alert('Promo is not valid or expired');
				}
			},function(err){
				
			});
		}
		
		
	}
    //post quote form submit..........................................
    $scope.postQuote = function(detail) {
        //alert('submitted');
        if (!$scope.quoteform.$invalid) {
            $scope.quote = {
                posted_to: detail.job.userid,
                posted_by: $scope.user_id,
                lead_id: detail.job.id,
                comment: detail.comment,
                price: detail.price
            };
			if($scope.promo_code){
				$scope.quote.promo_code=$scope.promo_code;
			}
            quoteService.postQuote($scope.quote).then(function(response) {

                //alert(response.Ack);

                if (response.Ack == '1') {
                    $scope.submissionSuccess = "Quote Submitted";
                    sb.connect("service" + $scope.user_id, function(user, error) {
                    if (error) {
                        console.log("Send Bird Error", error);
                    } else {
                        var nickname = userInfo.name;
                        var profileUrl = userInfo.profile_image;
                        sb.updateCurrentUserInfo(nickname, profileUrl, function(response, error) {});
                        var channelUrl=detail.job.channelUrl;
                        sb.OpenChannel.getChannel(channelUrl, function (channel, error) {
                        if (error) {
                            console.error(error);
                            return;
                        }

                        channel.enter(function(response, error){
                            if (error) {
                                console.error(error);
                                return;
                            }
                      var message=detail.comment;  
                      var data="";
                      var customType="";
                      channel.sendUserMessage(message, data, customType, function(message, error){
                        if (error) {
                            console.error(error);
                            return;
                        }

                        // onSent
                        console.log(message,"Message Sent");
                    });
      
                            
                            
                        });
                    });

                        
                        
                        
                    }

                });


                    $scope.addquote();
                    alert('Quote submitted successfully');




                } else {
                    $scope.getCurrentUserType();
                    alert('Try Again');
                }




            }, function(err) {
                console.log(err);
            });
            $scope.quoteform.$submitted = false;

        }


    };
	
	$http.get($rootScope.basePath+"client-token.php")
    .then(function(response) {
		$scope.clientToken=response;
		braintree.setup($scope.clientToken.data, 'custom', {
			onPaymentMethodReceived: function (obj) {
				$scope.detail.payment_method_nonce=obj.nonce;
			},
			paypal: {
				container: 'bt-dropin',
			}
		});
	});


});