app.controller("RootScopeCtrl", function($scope, $filter, $location, $rootScope, $http, $state, $window, userService,leadService,$q, $templateCache, userService,$timeout) {
    //    $scope.loggedindetails = myAuth.getAdminAuthorisation();
    //
    //    if ($scope.loggedindetails) {
    //        if ($scope.loggedindetails.roleName.toLowerCase() == 'admin') {
    //            $location.path("/admin/home");
    //        }
    //        else if ($scope.loggedindetails.roleName.toLowerCase() == 'staff') {
    //            $location.path("/staff/home");
    //        }
    //    }
    //    else {
    //        $location.path("/home");
    //    }

    $rootScope.payment_from_card_user_id = '';
    $rootScope.payment_from_card_lead_id = '';
    $rootScope.payment_from_card_amount = '';

    $scope.swipeValue = true;
    $scope.current_user_type="";
    $scope.tab=1;
    $scope.oneAtATime = true;
    $scope.showMenu = function() {
        $scope.menuVisible = !$scope.menuVisible;
    };

    //added for DOB dd...............................................
    var dayRange = [];
    for (var i = 1; i <= 31; i++) {
        dayRange.push(i);
    }
    $scope.days = dayRange;

    var monthRange = [];
    for (var j = 1; j <= 12; j++) {
        monthRange.push(j);
    }
    $scope.months = monthRange;

    var yearRange = [];
    //var currentYear = new Date().getFullYear();
    for (var k = 1950; k <= 2017; k++) {
        yearRange.push(k);
    }
    $scope.years = yearRange;



    $templateCache.removeAll();

    $scope.user = {};
    //$scope.user.user_type_select='U';



    $scope.getCurrentUserType = function() {
        $scope.current_user_login = '';
        if ($window.localStorage["userInfo"]) {
            //alert(userInfo.type);
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            //console.log(userInfo);
            $scope.current_user_login = '1';
            if (userInfo.type == 'SP') {
                $window.localStorage["userType"] = 'SP';
                $scope.current_user_type = 'SP';
            } else {
                $window.localStorage["userType"] = 'U';
                $scope.current_user_type = 'U';
            }
            //console.log($scope.current_user_type);	
            // GET USER DETAILS ======================	



            userService.getAccountDetails(userInfo.user_id).then(function(response) {

                //console.log(response);
                //alert(response.response.name);

                if (response.Ack == '1') {
                    $scope.sidebar_name = response.response.name;
                    $scope.sidebar_email = response.response.email;
                    $scope.sidebar_image = response.response.image;

                } else {

                    $scope.sidebar_name = '';
                    $scope.sidebar_email = '';
                    $scope.sidebar_image = '';

                }




            }, function(err) {
                console.log(err);
            });
        }

        //	$scope.getCounters();
    }
    $scope.showmainmodal=function()
    {

        $("#JobSelection").modal("show");
    }
    


    $scope.getCounters = function() {
        var userInfo = JSON.parse($window.localStorage["userInfo"]);
        userService.getCounters(userInfo.user_id).then(function(response) {

            if (response.Ack == '1') {

                $scope.cart_count = response.cart_count;
                $scope.notification_count = response.notification_count;
                $scope.pending_order_count = response.pending_order_count;
                $scope.accepted_order_count = response.accepted_order_count;



            } else {}




        }, function(err) {
            console.log(err);
        });

    }

   $scope.all_notifications = function() {

        if ($window.localStorage["userInfo"]) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);
            userService.getnotificationcount(userInfo.user_id).then(function(response) {

                if (response.Ack == 1) {
                    $scope.notification_count = response.notification;
                    $scope.notificationArr = response.notificationArr;

                } else {}




            }, function(err) {
                console.log(err);
            });

        }

    }    
$scope.setread = function() {

        if ($window.localStorage["userInfo"]) {
            var userInfo = JSON.parse($window.localStorage["userInfo"]);

            userService.setread(userInfo.user_id).then(function(response) {
                if (response.Ack == 1) {
                    $scope.notification_count=0;
                    //$scope.Notification();
                }

            }, function(err) {
                console.log(err);
            });

        }

    } 

   


    /*$scope.getCounters = function(){
	
    var userInfo = JSON.parse($window.localStorage["userInfo"]);	
	
    var encodedString ='{"user_id":"'+ userInfo.user_id +'"}';
    $http({
    method: 'POST',
    url: $rootScope.serviceurl+"users/getCounters",
    data: encodedString,
    headers: {'Content-Type': 'application/json'}
    }).success(function (response) {
    console.log(response); 
    // alert(response.UserDetails.user_id);
	
    if(response.Ack == '1') {
	
    $scope.cart_count=response.cart_count;
    $scope.notification_count=response.notification_count;
    $scope.pending_order_count=response.pending_order_count;
    $scope.accepted_order_count=response.accepted_order_count;
    //$ionicLoading.hide();
	

	
    } else {
    $ionicLoading.hide();
    // reject('Login Failed.');
	
	
	
    }
    }).error(function () {
    $ionicLoading.hide(); 
	
    var alertPopup = $ionicPopup.alert({
    title: 'Error!!',
    template: 'Unable to load data!!!'
    });	  	
	
	
    });	
    	
    }*/




    /*	$scope.login = function(user) {
        //document.getElementById('closeModalButton').click();
        userService.checkLogin(user).then(function(response) {
            console.log(response); 

           $window.sessionStorage.isLoggedIn=1;

    		userInfo = {
    		user_id: response.UserDetails.user_id,
    		full_name: response.UserDetails.full_name,
    		user_type: response.UserDetails.user_type,
    		email: response.UserDetails.email,
    		username : response.UserDetails.username, 
    		address : response.UserDetails.address,
    		phone: response.UserDetails.phone,
    		device_type: response.UserDetails.device_type,
    		device_token_id: response.UserDetails.device_token_id,
    		lat: response.UserDetails.lat,
    		lang: response.UserDetails.lang,
    		profile_image : response.UserDetails.profile_image
    		};
    		$window.localStorage["userInfo"] = JSON.stringify(userInfo);

    		//console.log(response.UserDetails.user_type.trim());
    		
    		//$scope.modal.hide();
    		if(response.UserDetails.user_type=='S'){
    		
    		$window.localStorage["userType"]='S';
    		//$state.go('frontend.my_account');	
    		$location.path('my_account');	
    		}
    		else if(response.UserDetails.user_type.trim()=='D'){
    		$window.localStorage["userType"]='D';
    		$state.go('frontend.my_account');
    		}
    		else{
    		$window.localStorage["userType"]='U';	
    		$state.go('frontend.my_account');
    		}


        }, function(err) {
    		alert('Invalid Login. Please Try Again');
             console.log(err); 
        }); 
    };*/


    $scope.openGoogleLogin = function() {



        var myParams = {
            'clientid': '881075240755-jg18ifulo20066g0up3n6bce2t44ba89.apps.googleusercontent.com', //You need to set client id
            'cookiepolicy': 'single_host_origin',
            'callback': 'googleloginCallback', //callback function
            'approvalprompt': 'force',
            'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
        };
        gapi.auth.signIn(myParams);



    }

    function googleloginCallback(result) {
        if (result['status']['signed_in']) {
            //console.log(result);
            //alert("Login Success");
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });

            request.execute(function(resp) {

                alert('PERFECT');
                var email = resp.emails[0].value;
                var gpId = resp.id;
                var fname = resp.name.familyName;
                var lname = resp.name.givenName;




            });
        }

    }




    /*		$scope.googleloginCallback= function(result) {
    		if (result['status']['signed_in'])
    		{
    		//console.log(result);
    		//alert("Login Success");
    		var request = gapi.client.plus.people.get({
    		'userId': 'me'
    		});
    		
    		request.execute(function (resp) {
    		alert('avik');						  
    		console.log(resp);
    		var email = resp.emails[0].value;
    		var gpId = resp.id;
    		var fname = resp.name.familyName;
    		var lname = resp.name.givenName;
    		

    		
    		
    		});
    		}
    		
    		}*/


    $scope.isEmpty=function(obj)
        {
            for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
            }
            return true;
        }

    $scope.openFBLogin = function() {
        //	alert ('Perfect');
        // alert('hi');
        //  e.preventDefault();       
        FB.login(function(response) {
            // FB Login Failed //
            if (!response || response.status !== 'connected') {
                alert("Account Access Not Authorized", "Facebook says");
            } else {
                // FB Login Successfull //
                FB.api('/me', function(fbdata) {
                    console.log(fbdata); //
                    //  alert(JSON.stringify(fbdata));
                    //alert(fbdata.toString())
                    var fb_id = fbdata.id;
                    var fb_name = fbdata.name;
                    //var fb_image = fbdata.image;
                    //var last_name = fbdata.last_name;
                    var fb_email = fbdata.email;
                    //var fb_username = fbdata.username;


                    values = "fb_id=" + fb_id + "&fb_name=" + fb_name;
                    //values = "fb_id="+fb_id+"&fb_name="+fb_name+"&fb_email="+fb_email;

                    $scope.user.fb_id = fbdata.id;
                    console.log($scope.user.fb_id);

                    //$scope.openFBLogin();	


                    userService.fbsignup(fb_id, fb_name).then(function(response) {
                        $('#loginModal').modal('hide');
                        $('.modal-backdrop').css('display', 'none');

                        console.log(response);
                        alert('a');


                    }, function(err) {
                        //	alert('Error Occured.');
                        console.log(err);
                    });




                    /*    $.ajax({
                        url: "<?php echo SITE_URL;?>fblogin.php?type=1",
                        type: "post",    
                        data: values,
                        success: function(newData){
                        location.href='<?php echo SITE_URL;?>my_account.php';   
                        
                        }
                        
                        });*/

                })
            }
        }, {
            scope: "email"
        });




    }

    $scope.howlongago = function(timestamp) {
        $scope.current = Math.floor((new Date()).getTime() / 1000);
        $scope.timestamp = Math.floor(timestamp / 1000);
        $scope.difference = (($scope.current - $scope.timestamp));
        if ($scope.difference < 60) {
            return $scope.difference + " seconds ago";
        } else {
            $scope.difference = Math.floor(($scope.difference / 60));
            if ($scope.difference < 60) {
                return $scope.difference + " minute(s) ago";
            } else {
                $scope.difference = Math.floor(($scope.difference / 60));
                if ($scope.difference < 24) {
                    return $scope.difference + " hour(s) ago";
                } else {
                    $scope.difference = Math.floor(($scope.difference / 24));
                    if ($scope.difference < 7) {
                        return $scope.difference + " days(s) ago";
                    } else {
                        $scope.difference = Math.floor(($scope.difference / 7));
                        return $scope.difference + " week(s) ago";

                    }

                }

            }



        }


    }


    $scope.login = function(user) {
        //alert('OK');
        //exit;

        //document.getElementById('closeModalButton').click();
        userService.checkLogin(user).then(function(response) {
            // console.log(response); 

            $window.sessionStorage.isLoggedIn = 1;

            userInfo = {
                user_id: response.response.id,
                name: response.response.name,
                //lname: response.UserDetails.lname,
                //user_type: response.UserDetails.type,
                email: response.response.email,
                //username : response.UserDetails.username, 
                //address : response.UserDetails.address,
                phone: response.response.mobile_no,
                type: response.response.type,
                device_type: response.response.device_type,
                token: response.response.token,
                profile_image: response.response.image,
                city: response.response.city,
                state: response.response.state,
                //lat: response.UserDetails.my_latitude,
                //lang: response.UserDetails.my_longitude,
                //profile_image : response.UserDetails.profile_image
            };
            $window.localStorage["userInfo"] = JSON.stringify(userInfo);
            $scope.all_notifications();
            $('#loginModal').modal('hide');
            $('.modal-backdrop').css('display', 'none');
            //console.log(response.UserDetails.user_type.trim());

            //$scope.modal.hide();
            /* if(response.UserDetails.user_type=='S'){
		
            $window.localStorage["userType"]='S';
            //$state.go('frontend.my_account');	
            $location.path('my_account');	
            }
            else if(response.UserDetails.user_type.trim()=='D'){
            $window.localStorage["userType"]='D';
            $state.go('frontend.my_account');
            }
            else{
            $window.localStorage["userType"]='U';	
            $state.go('frontend.my_account'); 
            } */
            // alert(response.id);

            //alert($window.localStorage["userInfo"]);



            if (response.response.id != '') {
                $window.localStorage["userId"] = response.response.id;
                //alert($window.localStorage["userId"]);
                $scope.current_user_login = '1';
                $scope.current_user_type = response.response.type;
               console.log("Current User Type",$scope.current_user_type);
                //$state.go('frontend.my_account');
                $state.go('frontend.home', {
                    'islogin': 1
                });
                //$window.location.reload();
            }



        }, function(err) {
            //alert('Invalid Login. Please Try Again');

            $scope.submissionError = "Invalid Login. Please Try Again!";
            //$scope.submissionSuccess = true;                

            console.log(err);
        });
    };




    /*$scope.signup = function(user) {
        //document.getElementById('closeModalButton').click();
        userService.signup(user).then(function(response) {
    										   
    										   
    										   
    	}, function(err) {
    		alert('Error Occured.');
             console.log(err); 
        }); 
    };*/




    $scope.signup = function(user) {

        //var password= user.password;
        //var cnfpass= user.cnfpass;


        //if(angular.equals(pass, cnfpass))  {

        //document.getElementById('closeModalButton').click();
        if(! angular.isUndefined(user.name) && user.email!='' && ! angular.isUndefined(user.countries.id) && ! angular.isUndefined(user.mobile_no) && ! angular.isUndefined(user.password) && ! angular.isUndefined(user.cnfpass))
        {
        userService.signup(user).then(function(response) {
            //alert('Thank you for Signup');
            
            if(response.Ack==1)
            {
                $scope.submissionSuccsignup = "Thank you for Signup";
                $('#signupModal').modal('hide');
                $('.modal-backdrop').css('display', 'none');
                $scope.all_notifications();

                $state.go('frontend.home', {
                    'issignup': 1
                });
            }
            else
            {
              $scope.submissionErrorsignup = response.msg;

            }
            

        }, function(err) {
            $scope.submissionErrorsignup = "Error Occured!";
            //alert('Error Occured.');
            console.log(err);
        });
       }


        // }else{		  
        //$scope.showErrorMsg=1;	  
        // }



    };




    /*
    $scope.facebookSign = function(user) {
        
        //alert('OK');
        //exit;
        
        userService.fbsignup(user).then(function(response) {
        	//$('#myModal').modal('hide');				   
    										   
    	}, function(err) {
    		alert('Error Occured.');
             console.log(err); 
        });
        
        
        
        
    }*/




    $scope.forgetpass = function(user) {

        //var userInfo = JSON.parse($window.localStorage["userInfo"]);
        userService.forgetpass(user).then(function(response) {


            //alert(response.data.msg);




        }, function(err) {
            console.log(err);
        });

    }




    $scope.logout = function() {
        console.log('Calling Logout');
        var userInfo = JSON.parse($window.localStorage["userInfo"]);
        $scope.user_id = userInfo.user_id;

        userService.logout(userInfo.user_id).then(function(response) {

            $window.localStorage["userInfo"] = '';
            $window.localStorage.clear();
            $scope.current_user_type="";
            $scope.current_user_login='';
            $state.go('frontend.home');


        }, function(err) {
            console.log(err);
        });




    }


    $scope.goToCart = function() {
        $state.go('frontend.cart');
    }

    $scope.goToCheckout = function() {
        $state.go('frontend.checkout');
    }


    $scope.goToPaymentNow = function() {
        $state.go('frontend.pay_now');
    }

    $scope.viewDetails = function(id) {
        $location.path('order_details/' + id);
    }



    if ($window.localStorage["appTitle"] != undefined && $window.localStorage["appTitle"] != null) {
        $rootScope.appTitle = $window.localStorage["appTitle"];

    } else {
        $rootScope.appTitle = "Taskiteer: Find Local Trusted Service Professionals";
    }

   
    $scope.IsVisible = false;
    $scope.ShowHide = function() {
        //If DIV is visible it will be hidden and vice versa.
        $scope.IsVisible = $scope.IsVisible ? false : true;
    }
    $scope.showjobmodal=function()
    {  
        
        $timeout(function () {
        $("#JobSelection").modal("show");
    }, 2000);


                 //for populate subcat..................................

        
    }
    $scope.profiletab=1;
    $scope.providertab=0;
    $scope.jobtab=0;
    $scope.dealtab=0;
    $scope.setprofile=function()
    {
        $scope.profiletab=!$scope.profiletab;
    }
    $scope.setprovider=function()
    {
        $scope.providertab=!$scope.providertab;
    }
    $scope.setjob=function()
    {
        $scope.jobtab=!$scope.jobtab;
    }
    $scope.setdeal=function()
    {
        $scope.dealtab=!$scope.dealtab;
    }
    $scope.dropdown=function()
    {
    userService.getAllCountries().then(function(data) {
               if(data.Ack==1)
               {                  
                   $scope.countries=data.response;

               }

             }, function(err) {
               alert('Internal Error');
             });
}

    $scope.getisd=function(country)
    {
    userService.getIsd(country).then(function(data) {
               if(data.Ack==1)
               {     
                   console.log(data.isd,"ISD CODE"); 
                   $scope.user.isd=data.isd;

               }

             }, function(err) {
               alert('Internal Error');
             });
}

	
	
});