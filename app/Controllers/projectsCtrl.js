'use strict';
/** 
 * controllers used for the login
 */
app.controller('projectsCtrl', function($rootScope, $scope, $http, $location, $timeout, $q, quoteService, $window, $stateParams,Notification,$state) {

    $scope.list = [];
    $scope.data = {};
    $scope.user = {};

    $scope.getCurrentUserType();
    //console.log($scope.current_user_type);
    //alert($stateParams.searchval);

    $scope.all_notifications();
    if ($window.localStorage["userInfo"]) 
    {
        var userInfo = JSON.parse($window.localStorage["userInfo"]);
        $scope.user_id=userInfo.user_id;
    }else{
        $scope.user_id='';
    }
    //var userInfo = JSON.parse($window.localStorage["userInfo"]);    
    $scope.categoryName='';
    

    $scope.intitprojects=function()
    {
     $scope.searchval = $stateParams.title;
//     if($scope.user_id == ''){
//         $scope.user_id =0;
//     }
    quoteService.projectlist($scope.user_id,$scope.searchval).then(function(response) {
        $('.loader').show();
        $scope.Ack = response.Ack;
        //alert(response.Ack);

        //alert($stateParams.searchval);
        if (response.Ack == '1') {
            $('.loader').hide('fast');

            //alert(response.jobList);
            $scope.list = response.List;
            

            //alert(response.Detail.job.id);
            
             //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.list.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.list.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();

        } else {

            $('.loader').hide('fast');
        }




    }, function(err) {
        console.log(err);
    });
    }

    $scope.categoryjobs = function() {
        $scope.cat_id=$stateParams.cat_id;
        quoteService.catwisejobs($scope.cat_id).then(function(data) {
            $('.loader').show();
            $scope.Ack = data.Ack;
            $scope.cat_name = data.cat_name;
            if (data.Ack == 1) {
            $('.loader').hide('fast');
            $scope.list=data.response;
            } else 
            {

                $('.loader').hide('fast');
            }

        }, function(err) {
            console.log(err);
        });

    }
    $scope.sendQuote=function(lead)
    {
       
         $scope.lead_id=lead.id;
         $scope.params={user_id:$scope.user_id,lead_id:lead.id,category_id:lead.category_id};
         quoteService.checkpaystatus($scope.params).then(function(data) {
             
         if(data.is_paid==1)
         {
             $state.go("frontend.addquote",{lead_id:lead.id});
         }
         else
         {
              $scope.paydata={user_id:$scope.user_id,lead_id:lead.id,amount:lead.price};
              $scope.tempMsgDetails = {button:"Make Payment",message:"You have to pay $"+lead.price+" for this"};
              Notification.error({templateUrl:'app/views/messages/payment.html',scope:$scope});
         }
         
            

        }, function(err) {
            console.log(err);
        });
        
    }
    
    $scope.getpayment=function()
    {
        
        quoteService.makepayment($scope.paydata).then(function(data) {
         if(data.Ack==1)
         {
           $state.go("frontend.addquote",{lead_id:$scope.lead_id});

         }
         else if(data.Ack==2)
         {
             $rootScope.payment_from_card_user_id = $scope.paydata.user_id;
            $rootScope.payment_from_card_lead_id = $scope.paydata.lead_id;
            $rootScope.payment_from_card_amount = $scope.paydata.amount;
             $scope.tempMsgDetails = {button:"OK",message:"Please save credit card details first",title:"Payment"};
             Notification.error({templateUrl:'app/views/messages/credit.html',scope:$scope});
         }
         
         
         else
         {
             console.log(data);
              //$scope.tempMsgDetails = {button:"OK",message:"Payment could not proceed, please try again"};
              $scope.tempMsgDetails = {button:"OK",message:data.err.jsonBody.error.message};
              Notification.error({templateUrl:'app/views/messages/payment.html',scope:$scope});
         }
         
            

        }, function(err) {
            console.log(err);
        });
        
        
    }
    
    $scope.gocreditpage=function()
    {
        $state.go("frontend.creditcard");
    }
    


});