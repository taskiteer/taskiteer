'use strict';
/** 
 * controllers used for the login
 */
app.controller('quotesCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $q, quoteService, $window, Upload, $stateParams) {

    $scope.list = [];
    $scope.data = {};
    $scope.user = {};
    $scope.readOnly = true;
    $scope.starico="ion-android-star"
    $scope.all_notifications();

    if (!$window.localStorage["userInfo"]) 
    {

        $state.go('frontend.home');
        return false;
    }  
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id=userInfo.user_id;
    $scope.quotelist = function() {
        $scope.getCurrentUserType();
        //console.log($scope.current_user_type);  		

        quoteService.quotelist($stateParams.lead_id).then(function(response) {
            $('.loader').show();
            $scope.Ack = response.Ack;
            //alert(response.Ack);
            //console.log(response);
            if (response.Ack == '1') {
                $('.loader').hide('fast');
                //console.log(response);

                //alert(response.jobList);
                $scope.list = response.List;

                //console.log(response.List);
                //alert(response.Detail.job.id);
                
                 //for pagination...........................................
            $scope.perPage = 6;
		$scope.offset = 0;
    	$scope.navButtons = [];
		//alert ($scope.menusList.length);
		$scope.buildNavButtons = function () {
        for (var i = 0, len = ($scope.list.length / $scope.perPage); i < len; i = i + 1) {
            $scope.navButtons.push(i);
        }
    }
	
	$scope.paginate = function() {
        $scope.data = $scope.list.slice($scope.offset, $scope.offset + $scope.perPage);
   		$window.scrollTo(0, 0);
    };

    $scope.previous = function() {
        $scope.offset = $scope.offset - $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.next = function() {
        $scope.offset = $scope.offset + $scope.perPage;
        $window.scrollTo(0, 0);
    };

    $scope.$watch('offset', function() {
        $scope.paginate();
    });
    
    $scope.buildNavButtons();

            } else {

                $('.loader').hide('fast');
            }




        }, function(err) {
            console.log(err);
        });

    };
    $scope.showmodal=function(lead_id,userid)
    {
        $scope.lead_id=lead_id;
        $scope.userid=userid;
        $("#hireModel").modal("show");
        
    }
    
    $scope.hireProvider = function(lead_id, provider_id) {
        quoteService.hireProvider(lead_id, provider_id).then(function(response) {
            //alert(response.Ack);
            if (response.Ack == '1') {
                $scope.submissionSuccess = "Successfully hired";

                //alert('Successfully deleted');
                $("#hireModel").modal("hide");
                $scope.quotelist();
            } else {
                alert('Error !!!! Please try again');
            }
        });
    };
    $scope.addfav = function(provider_id) {
        $scope.params={user_id:$scope.user_id,pro_id:provider_id};
        quoteService.add_to_fav($scope.params).then(function(data) {
            if (data.Ack == 1) 
            {
                alert("Add to favourite successfully");
            } 
            else 
            {
                alert(data.msg);
            }
        });
    };

});