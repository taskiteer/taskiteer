'use strict';
/** 
 * controllers used for the login
 */
app.controller('notificationSettingCtrl', function($rootScope, $state, $scope, $http, $location, $timeout, $window, NotifysettingService) {


    $scope.data = {};
    $scope.user = {};

    if (!$window.localStorage["userInfo"]) {
        $state.go('frontend.home');
        return false;
    }
    $scope.getCurrentUserType();
    $scope.all_notifications();
    var userInfo = JSON.parse($window.localStorage["userInfo"]);
    $scope.user_id = userInfo.user_id;
    $scope.response={};
    $scope.response.sms={};
    $scope.response.email={};
    $scope.response.email.type='e';
    $scope.response.sms.type='m';
    $scope.details = function() {
    NotifysettingService.showsetting($scope.user_id).then(function(data) {
    if (data.Ack == 1) 
    {
        $scope.response=data.response;
        console.log("Notification Settings",$scope.response);
    }

    }, function(err) {
        console.log(err);
    });

    }
    
    $scope.savesettings=function()
    {
        $scope.response.user_id=$scope.user_id;
        
        NotifysettingService.saveService($scope.response).then(function(data) {
        if (data.Ack == 1) 
        {
            $scope.details();
        }

        }, function(err) {
            console.log(err);
        });
    }
    


});