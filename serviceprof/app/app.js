var app = angular.module("angularblank", ['oc.lazyLoad','ncy-angular-breadcrumb','ui.router','ngRoute','ngTouch',
        'ngSanitize','ngFileUpload','vsGoogleAutocomplete','ui.calendar']);
app.run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
        // Attach Fastclick for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers
      // FastClick.attach(document.body);

        // Set some reference to access them from any scope
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        // GLOBAL APP SCOPE
        // set below basic information
        
        $rootScope.serviceurl = "http://159.203.181.31/serviceprof/api/";
        $rootScope.siteurl = "http://159.203.181.31/serviceprof/api/";
        $rootScope.SENDBIRDAPPID = "DF903201-4FD3-46FB-82E0-38EB227FE29B";

        $rootScope.app = {
            name: 'Service Market Place', // name of your project
            author: '', // author's name or company name
            description:'Service Market Place',
            keywords:'Service Market Place',
            version: '1.0', // current version
            year: ((new Date()).getFullYear()), // automatic current year (for copyright information)
            isMobile: (function () {// true if the browser is a mobile device
                var check = false;
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    check = true;
                };
                return check;
            })()
        };

    }]);

angular.module('angularblank').run(['$http',function($http){
   //$http.defaults.headers.common.responsetype = 'json';
}])

angular.module('angularblank').filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
});




app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES','$locationProvider',
    function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires,$locationProvider) {

     app.controller = $controllerProvider.register;
     app.directive = $compileProvider.directive;
     app.filter = $filterProvider.register;
     app.factory = $provide.factory;
     app.service = $provide.service;
     app.constant = $provide.constant;
     app.value = $provide.value;

        // LAZY MODULES
        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: jsRequires.modules
        });

///Remove Hash from URL//////
 //$locationProvider.html5Mode(true).hashPrefix('');
 $locationProvider.hashPrefix('');

    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/dashboard
        $urlRouterProvider.otherwise("/home");
        
    //
    // Set up the states

        $stateProvider
        //Login state
            .state('frontend', {
                url: '',
                templateUrl: 'app/views/app.html',
                abstract :true,
                //resolve: loadSequence('footer')

            })
            
            .state('frontend.home', {
                url: '/home',
                templateUrl: 'app/views/home.html',
                title: 'Home',
                controller: 'homeCtrl', 
                  params:  {'islogin': null,'issignup':null},
                ncyBreadcrumb: {
                    label: 'Home page'
                },
                resolve: loadSequence('home')
            })
			
			
			.state('frontend.projects', {
			url: '/projects',
			templateUrl: 'app/views/projects.html',
			title: 'Projects',
			controller: 'projectsCtrl', 
                         params:  {'searchval': null},
			ncyBreadcrumb: {
			label: 'Projects'
			},
			resolve: loadSequence('projects')
			})
						
			.state('frontend.provider_dshboard', {
			url: '/provider_dshboard',
			templateUrl: 'app/views/provider_dshboard.html',
			title: 'Home',
			controller: 'providerDshboardCtrl', 
			ncyBreadcrumb: {
			label: 'Home page'
			},
			resolve: loadSequence('provider_dshboard')
			})	
			
			
            .state('frontend.my_account', {
                url: '/my_account',
                templateUrl: 'app/views/my_account.html',
                title: 'My Account',
				controller: 'myAccountCtrl', 
				cache:false,
                ncyBreadcrumb: {
                    label: 'My Account'
                },
                resolve: loadSequence('my_account')
            })			
			
			
			
		.state('frontend.change_password', {
                url: '/change_password',
                templateUrl: 'app/views/change_password.html',
                title: 'Change Password',
		controller: 'changePasswordCtrl', 
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                resolve: loadSequence('change_password')
            })	
			
		.state('frontend.verify_phone', {
                url: '/verify_phone',
                templateUrl: 'app/views/verify_phone.html',
                title: 'Change Password',
				controller: 'verifyPhoneCtrl', 
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                resolve: loadSequence('verify_phone')
            })				
			
			.state('frontend.verify_phone_otp', {
                url: '/verify_phone_otp',
                templateUrl: 'app/views/verify_phone_otp.html',
                title: 'Verify Phone otp',
                controller: 'verifyPhoneotpCtrl', 
                ncyBreadcrumb: {
                    label: 'Verify Phone otp'
                },
                resolve: loadSequence('verify_phone_otp')
            })              
            
			
			
			.state('frontend.manage_service', {
                url: '/manage_service',
                templateUrl: 'app/views/manage_service.html',
                title: 'Change Password',
				controller: 'manageServiceCtrl', 
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                resolve: loadSequence('manage_service')
            })				
			
			.state('frontend.manage_verifications', {
                url: '/manage_verifications',
                templateUrl: 'app/views/verifications.html',
                title: 'Change Password',
				controller: 'verificationsCtrl', 
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                resolve: loadSequence('manage_verifications')
            })	
			
			
			
			.state('frontend.notification', {
                url: '/notification',
                templateUrl: 'app/views/notification.html',
                title: 'Notification',
                controller: 'notificationCtrl', 
                ncyBreadcrumb: {
                    label: 'Notification'
                },
                resolve: loadSequence('notification')
            })					
			
			.state('frontend.faq', {
                url: '/faq',
                templateUrl: 'app/views/faq.html',
                title: 'Change Password',
				controller: 'faqCtrl', 
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                resolve: loadSequence('faq')
            })		
			
			
			.state('frontend.cms', {
                url: '/cms/:id',
                templateUrl: 'app/views/cms.html',
                title: 'Change Password',
				controller: 'cmsCtrl', 
                ncyBreadcrumb: {
                    label: 'Change Password'
                },
                resolve: loadSequence('cms')
            })	
			
			
			
			// END OF THE LINKS ===========================================================
			
			.state('frontend.others', {
			url: '/others',
			templateUrl: 'app/views/others.html',
			title: 'Home',
			controller: 'othersCtrl', 
			ncyBreadcrumb: {
			label: 'Home page'
			},
			resolve: loadSequence('others')
			})	
			
			
			
			
			
			

			
			.state('frontend.manage_business', {
                url: '/manage_business',
                templateUrl: 'app/views/manage_business.html',
                title: 'Manage Business',
				controller: 'manageBusinessCtrl', 
                ncyBreadcrumb: {
                    label: 'Manage Business'
                },
                resolve: loadSequence('manage_business')
            })	
			
			.state('frontend.add_products', {
                url: '/add_products',
                templateUrl: 'app/views/add_products.html',
                title: 'Add Products',
				controller: 'addProductsCtrl', 
                ncyBreadcrumb: {
                    label: 'Add Products'
                },
                resolve: loadSequence('add_products')
            })
			
			.state('frontend.list_products', {
                url: '/list_products',
                templateUrl: 'app/views/list_products.html',
                title: 'List Products',
				controller: 'listProductsCtrl',
                ncyBreadcrumb: {
                    label: 'List Products'
                },
                resolve: loadSequence('list_products')
            })
			
			
			.state('frontend.order_history', {
                url: '/order_history',
                templateUrl: 'app/views/order_history.html',
                title: 'Order History',
				controller: 'orderHistoryCtrl',
				cache:false,
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('order_history')
            })
			
			
			.state('frontend.pending_orders', {
                url: '/pending_orders',
                templateUrl: 'app/views/pending_orders.html',
                title: 'Order History',
				controller: 'pendingOrdersCtrl',
				cache:false,
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('pending_orders')
            })
			
			
			.state('frontend.accepted_orders', {
                url: '/accepted_orders',
                templateUrl: 'app/views/accepted_orders.html',
                title: 'Order History',
				controller: 'acceptedOrdersCtrl',
				cache:false,
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('accepted_orders')
            })
			
			
			.state('frontend.order_details', {
                url: '/order_details/:id',
                templateUrl: 'app/views/order_details.html',
                title: 'Order Details',
				controller: 'orderDetailsCtrl',
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('order_details')
            })
			
			.state('frontend.details', {
                url: '/details/:id',
                templateUrl: 'app/views/details.html',
                title: 'Order Details',
				controller: 'detailsCtrl',
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('details')
            })			
			
			
			.state('frontend.track_listing', {
                url: '/track_listing',
                templateUrl: 'app/views/track_listing.html',
                title: 'Order Details',
				controller: 'trackListingCtrl',
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('track_listing')
            })			
			
			
			
			.state('frontend.driver_track_details', {
			url: '/driver_track_details/:id',
			templateUrl: 'app/views/driver_track_details.html',
			title: 'Order Details',
			controller: 'driverTrackDetails',
			ncyBreadcrumb: {
			label: 'Order History'
			},
			resolve: loadSequence('driver_track_details')
			})	
			
			
			
			.state('frontend.make_payment', {
                url: '/make_payment/:id',
                templateUrl: 'app/views/make_payment.html',
                title: 'Order Details',
				controller: 'makePaymentCtrl',
                ncyBreadcrumb: {
                    label: 'Order History'
                },
                resolve: loadSequence('make_payment')
            })				
			

			
			
			.state('frontend.resturant_accept', {
			url: '/resturant_accept/:id',
			templateUrl: 'app/views/resturant_accept.html',
			title: 'CMS',
			controller: 'resturantAcceptCtrl',
			ncyBreadcrumb: {
			label: 'CMS'
			},
			resolve: loadSequence('resturant_accept')
			})

            .state('frontend.facebook_confirm', {
            url: '/facebook_confirm',
            templateUrl: 'app/views/facebook_confirm.html',
            title: 'Facebook Confirm',
            controller: 'facebook_confirmCtrl',
            ncyBreadcrumb: {
            label: 'Facebook Confirm'
            },
            resolve: loadSequence('facebook_confirm')
            })
			
			
			.state('frontend.payment_success', {
			url: '/payment_success/:id',
			templateUrl: 'app/views/payment_success.html',
			title: 'CMS',
			controller: 'paymentSuccessCtrl',
			ncyBreadcrumb: {
			label: 'CMS'
			},
			resolve: loadSequence('payment_success')
			})
			
			
			.state('frontend.payment_failure', {
			url: '/payment_failure/:id',
			templateUrl: 'app/views/payment_failure.html',
			title: 'CMS',
			controller: 'paymentFailureCtrl',
			ncyBreadcrumb: {
			label: 'CMS'
			},
			resolve: loadSequence('payment_failure')
			})

			
			
			.state('frontend.cart', {
			url: '/cart',
			templateUrl: 'app/views/cart.html',
			title: 'Cart',
			controller: 'cartCtrl',
			ncyBreadcrumb: {
			label: 'Cart'
			},
			resolve: loadSequence('cart')
			})	
			
			
			.state('frontend.checkout', {
			url: '/checkout',
			templateUrl: 'app/views/checkout.html',
			controller: 'checkoutCtrl',
			title: 'Checkout',
			ncyBreadcrumb: {
			label: 'Checkout'
			},
			resolve: loadSequence('checkout')
			})	
			
			.state('frontend.pay_now', {
			url: '/pay_now',
			templateUrl: 'app/views/pay_now.html',
			title: 'Pay Now',
			controller: 'payNowCtrl',
			ncyBreadcrumb: {
			label: 'Pay Now'
			},
			resolve: loadSequence('pay_now')
			})	

            .state('frontend.question', {
            url: '/question',
            templateUrl: 'app/views/question.html',
            title: 'question',
            controller: 'questionCtrl',
            ncyBreadcrumb: {
            label: 'question'
            },
            resolve: loadSequence('question')
            })			
			
             .state('frontend.questionfinish', {
            url: '/questionfinish',
            templateUrl: 'app/views/question-finish.html',
            title: 'Question Finish',
            controller: 'questionfinishCtrl',
            ncyBreadcrumb: {
            label: 'Question Finish'
            },
            resolve: loadSequence('questionfinish')
            })  


             .state('frontend.questionsuccess', {
            url: '/questionsuccess',
            templateUrl: 'app/views/question-success.html',
            title: 'Question Success',
            controller: 'questionsuccessCtrl',
            ncyBreadcrumb: {
            label: 'Question Success'
            },
            resolve: loadSequence('questionsuccess')
            })

            .state('frontend.dashboard', {
            url: '/dashboard',
            templateUrl: 'app/views/dashboard.html',
            title: 'Dashboard',
            controller: 'dashboardCtrl',
            ncyBreadcrumb: {
            label: 'Dashboard'
            },
            resolve: loadSequence('dashboard')
            })

            .state('frontend.messaging', {
            url: '/messaging',
            templateUrl: 'app/views/messaging.html',
            title: 'messaging',
            controller: 'messagingCtrl',
            ncyBreadcrumb: {
            label: 'messaging'
            },
            resolve: loadSequence('messaging')
            })

            .state('frontend.myproject', {
            url: '/myproject/:status',
            templateUrl: 'app/views/my_project.html',
            title: 'My project',
            controller: 'myprojectCtrl',
            ncyBreadcrumb: {
            label: 'My Project'
            },
            resolve: loadSequence('myproject')
            })

            .state('frontend.quotes', {
            url: '/quotes/:lead_id',
            templateUrl: 'app/views/quotes.html',
            title: 'Quotes',
            controller: 'quotesCtrl',
            ncyBreadcrumb: {
            label: 'Quotes'
            },
            resolve: loadSequence('quotes','jkAngularRatingStars')
            })
            
            .state('frontend.chats', {
            url: '/chats/:quote_id',
            templateUrl: 'app/views/chat.html',
            title: 'Chats',
            controller: 'chatingsCtrl',
            ncyBreadcrumb: {
            label: 'Chats'
            },
            resolve: loadSequence('chat')
            })
            

              .state('frontend.request', {
            url: '/request',
            templateUrl: 'app/views/request.html',
            title: 'Quotes',
            controller: 'requestCtrl',
            ncyBreadcrumb: {
            label: 'request'
            },
            resolve: loadSequence('request')
            })
            
            .state('frontend.postjob', {
                url: '/postjob',
                templateUrl: 'app/views/postjob.html',
                title: 'Post Job',
		controller: 'leadsCtrl', 
                 params:  {'maincat_id': null,'maincat_name': null},
                ncyBreadcrumb: {
                    label: 'Post Job'
                },
                resolve: loadSequence('lead')
            })
            
            .state('frontend.availability', {
                url: '/availability',
                templateUrl: 'app/views/availability.html',
                title: 'Availability',
		controller: 'availabilitiesCtrl', 
                ncyBreadcrumb: {
                    label: 'Availability'
                },
                resolve: loadSequence('availability')
            })
            
            .state('frontend.success', {
                url: '/success',
                templateUrl: 'app/views/success.html',
                title: 'Success',
		controller: 'leadsCtrl', 
                ncyBreadcrumb: {
                    label: 'Success'
                },
                resolve: loadSequence('lead')
            })
			
            
            .state('frontend.test', {
                url: '/test',
                templateUrl: 'app/views/test.html',
                title: 'Home',
                ncyBreadcrumb: {
                    label: 'test page'
                },
                resolve: loadSequence('home')
            })
            
             .state('frontend.appliedproviders', {
                url: '/appliedproviders/:lead_id',
                templateUrl: 'app/views/appliedproviders.html',
                title: 'Applied Providers',
                controller: 'appliedproviders', 
                ncyBreadcrumb: {
                    label: 'Applied Providers'
                },
                resolve: loadSequence('appliedproviders')
            })
            
             .state('frontend.jobdetails', {
            url: '/jobdetails/:id',
            templateUrl: 'app/views/jobdetails.html',
            title: 'Job Details',
            controller: 'jobdetailsCtrl',
            ncyBreadcrumb: {
            label: 'Job Details'
            },
            resolve: loadSequence('jobdetails')
            })
            
             .state('frontend.myappliedproject', {
            url: '/myappliedproject/:status',
            templateUrl: 'app/views/my_appliedproject.html',
            title: 'My project',
            controller: 'myappliedprojectCtrl',
            ncyBreadcrumb: {
            label: 'My Project'
            },
            resolve: loadSequence('myappliedproject')
            })
            
            /* .state('frontend.searchjob', {
            url: '/searchjob/:status',
            templateUrl: 'app/views/searchjob.html',
            title: 'My project',
            controller: 'searchjobCtrl',
            ncyBreadcrumb: {
            label: 'My Project'
            },
            resolve: loadSequence('searchjob')
            }) */
        
         .state('frontend.addquote', {
            url: '/addquote/:lead_id',           
            controller: 'addquoteCtrl',
            templateUrl: 'app/views/addquote.html',
            title: 'Add Quote',
            ncyBreadcrumb: {
            label: 'Add Quote'
            },
            resolve: loadSequence('addquote')
            })
            
            .state('frontend.assignedprodetail', {
            url: '/assignedprodetail/:provider_id/:lead_id',           
            controller: 'assignedprodetailCtrl',
            templateUrl: 'app/views/assignedprodetail.html',
            title: 'Assigned pro',
            ncyBreadcrumb: {
            label: 'Assigned pro'
            },
            resolve: loadSequence('assignedprodetail')
            })
            
            .state('frontend.addreview', {
            url: '/addreview/:provider_id/:lead_id',           
            controller: 'addreviewCtrl',
            templateUrl: 'app/views/addreview.html',
            title: 'Review',
            ncyBreadcrumb: {
            label: 'Review'
            },
            resolve: loadSequence('addreview','jkAngularRatingStars')
            })
            
            .state('frontend.quotessend', {
            url: '/quotessend',           
            controller: 'quotessendCtrl',
            templateUrl: 'app/views/quotessend.html',
            title: 'quotessend',
            ncyBreadcrumb: {
            label: 'quotessend'
            },
            resolve: loadSequence('quotessend')
            })
            
             .state('frontend.creditcard', {
            url: '/creditcard',           
            controller: 'CreditCardCtrl',
            templateUrl: 'app/views/creditcard.html',
            title: 'Credit Card',
            ncyBreadcrumb: {
            label: 'Credit Card'
            },
            resolve: loadSequence('creditcard')
            })
                    
            .state('frontend.prodetail', {
            url: '/prodetail/:id',           
            controller: 'prodetailCtrl',
            templateUrl: 'app/views/prodetail.html',
            title: 'prodetail',
            ncyBreadcrumb: {
            label: 'prodetail'
            },
            resolve: loadSequence('prodetail')
            })
            .state('frontend.notify_setting', {
            url: '/notify_setting',           
            controller: 'notificationSettingCtrl',
            templateUrl: 'app/views/notification-settings.html',
            title: 'Notification Settings',
            ncyBreadcrumb: {
            label: 'Notification Settings'
            },
            resolve: loadSequence('notify_setting')
            })
            
            .state('frontend.category', {
            url: '/category/:cat_id',           
            controller: 'projectsCtrl',
            templateUrl: 'app/views/category.html',
            title: 'Category',
            ncyBreadcrumb: {
            label: 'Category'
            },
            resolve: loadSequence('category')
            })
            
            .state('frontend.portfolio', {
            url: '/portfolio',           
            controller: 'PortfolioCtrl',
            templateUrl: 'app/views/portfolio.html',
            title: 'Portfolio',
            ncyBreadcrumb: {
            label: 'Portfolio'
            },
            resolve: loadSequence('portfolio')
            })
            .state('frontend.viewcard', {
            url: '/viewcard',           
            controller: 'CreditCardCtrl',
            templateUrl: 'app/views/viewcard.html',
            title: 'Credit Card',
            ncyBreadcrumb: {
            label: 'Credit Card'
            },
            resolve: loadSequence('creditcard')
            })
            .state('frontend.searchpro', {
            url: '/searchpro',           
            controller: 'searchproCtrl',
            templateUrl: 'app/views/searchpro.html',
            title: 'Search Pro',
            ncyBreadcrumb: {
            label: 'Search Pro'
            },
            resolve: loadSequence('searchpro')
            })  
            
            .state('frontend.favourite', {
            url: '/favourite',           
            controller: 'favouriteCtrl',
            templateUrl: 'app/views/myfavourites.html',
            title: 'My Favourites Provider',
            ncyBreadcrumb: {
            label: 'My Favourites Provider'
            },
            resolve: loadSequence('favourite')
            }) 
            
            .state('frontend.prolist', {
            url: '/prolist',           
            controller: 'prolistCtrl',
             params:  {'searchval': null},
            templateUrl: 'app/views/prolist.html',
            title: 'Pro List',
            ncyBreadcrumb: {
            label: 'Pro List'
            },
            resolve: loadSequence('prolist')
            }) 
            
            .state('frontend.deals', {
            url: '/deals',           
            controller: 'dealsCtrl',
            templateUrl: 'app/views/deals.html',
            title: 'Deals',
            ncyBreadcrumb: {
            label: 'Deals'
            },
            resolve: loadSequence('deals')
            })  
            
            .state('frontend.mydeals', {
            url: '/mydeals',           
            controller: 'mydealsCtrl',
            templateUrl: 'app/views/mydeals.html',
            title: 'My Deals',
            ncyBreadcrumb: {
            label: 'My Deals'
            },
            resolve: loadSequence('mydeals')
            })  
            
            .state('frontend.postdeal', {
            url: '/postdeal',           
            controller: 'postdealCtrl',
            templateUrl: 'app/views/postdeal.html',
            title: 'Post deal',
            ncyBreadcrumb: {
            label: 'Post deal'
            },
            resolve: loadSequence('postdeal','ng-bootstrap-datepicker')
            })
            
             .state('frontend.calendersetting', {
            url: '/calendersetting',           
            controller: 'availabilitiesCtrl',
            templateUrl: 'app/views/calender-settings.html',
            title: 'Calender-Settings',
            ncyBreadcrumb: {
            label: 'Calender-Settings'
            },
            resolve: loadSequence('calendersetting')
            }) 
            
            .state('frontend.dealdetails', {
            url: '/dealdetails/:id',           
            controller: 'dealdetailsCtrl',
            templateUrl: 'app/views/dealdetails.html',
            title: 'Deal details',
            ncyBreadcrumb: {
            label: 'Deal details'
            },
            resolve: loadSequence('dealdetails')
            }) 
            
            .state('frontend.chatsdeals', {
            url: '/chatsdeals/:deal_id',
            templateUrl: 'app/views/chatsdeals.html',
            title: 'chatsdeals',
            controller: 'chatsdealsCtrl',
            ncyBreadcrumb: {
            label: 'chatsdeals'
            },
            resolve: loadSequence('chatsdeals')
            })
            
             .state('frontend.editdeal', {
            url: '/editdeal/:id',           
            controller: 'editdealCtrl',
            templateUrl: 'app/views/editdeal.html',
            title: 'Deal update',
            ncyBreadcrumb: {
            label: 'Deal update'
            },
            resolve: loadSequence('editdeal')
            }) 
            
             .state('frontend.testpage', {
                url: '/testpage',
                controller: 'testCtrl', 
                templateUrl: 'app/views/testpage.html',
                title: 'Test',
                ncyBreadcrumb: {
                    label: 'Test page'
                },
                resolve: loadSequence('testpage')
            })
            
            .state('frontend.servicearea', {
            url: '/servicearea',
            templateUrl: 'app/views/servloc.html',
            title: 'Service Area',
            controller: 'serviceLocCtrl',
            ncyBreadcrumb: {
            label: 'servicearea'
            },
            resolve: loadSequence('servicelocation')
            })
            .state('frontend.transaction', {
            url: '/transaction',
            templateUrl: 'app/views/transaction.html',
            title: 'Transactions History',
            controller: 'transactionCtrl',
            ncyBreadcrumb: {
            label: 'Transactions History'
            },
            resolve: loadSequence('transaction')
            })

function loadSequence() {
            var _args = arguments;
            return {
                deps: ['$ocLazyLoad', '$q',
                    function ($ocLL, $q) {
                        var promise = $q.when(1);
                        for (var i = 0, len = _args.length; i < len; i++) {
                            promise = promiseThen(_args[i]);
                        }
                        return promise;

                        function promiseThen(_arg) {
                            if (typeof _arg == 'function')
                                return promise.then(_arg);
                            else
                                return promise.then(function () {
                                    var nowLoad = requiredData(_arg);
                                    if (!nowLoad)
                                        return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                    return $ocLL.load(nowLoad);
                                });
                        }

                        function requiredData(name) {
                            if (jsRequires.modules)
                                for (var m in jsRequires.modules)
                                    if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
                                        return jsRequires.modules[m];
                            return jsRequires.scripts && jsRequires.scripts[name];
                        }
                    }]
            };
        }

  }]);
