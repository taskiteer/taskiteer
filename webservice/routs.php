<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require ('./vendor/autoload.php');
$app = new \Slim\Slim(array(
    'debug' => true
        ));

class AllCapsMiddleware extends \Slim\Middleware {

    public function call() {
        // Get reference to application
        $app = $this->app;

        $req = $app->request;

        //print_r($req);exit;
        // Run inner middleware and application
        $this->next->call();

        // Capitalize response body

        $res = $app->response;
        //$body = $res->getBody();
        if ($req->headers->get('Token') != "123456") {
            $res->setStatus(401);
            $res->setBody("{\"msg\":\"not authorised\"}");
        }
    }

}

$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new \CorsSlim\CorsSlim($corsOptions);

$app->add($cors);



		//$app->add(new \CorsSlim\CorsSlim());
		//$app->add(new \AllCapsMiddleware());
		//$app->response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
		$app->response->headers->set('Content-Type', 'application/json');
		
		
		$app->group('/users', function () use ($app) {
		$app->post('/appsignup', 'userSignup');
		$app->post('/appsignin', 'login');   
		$app->post('/addProducts', 'addProducts');
		$app->post('/updateProducts', 'updateProducts');
		$app->post('/deleteProduct', 'deleteProduct');
		$app->post('/logout', 'logout');
		$app->post('/forgetpassword', 'forgetpassword');
		$app->post('/changepwd', 'changepwd');
		$app->post('/userprofile', 'userprofile');
		$app->post('/updateProfile', 'updateProfile');
		$app->post('/updateProfileImage', 'updateProfileImage');
		$app->post('/listProductCategory', 'listProductCategory');
		$app->post('/homeProductCategory', 'homeProductCategory');
		$app->post('/listProducts', 'listProducts');
		$app->post('/listProductsByCategory', 'listProductsByCategory');
		$app->post('/listSwapProducts', 'listSwapProducts');
		$app->post('/productsDetails', 'productsDetails');
		$app->post('/addToWishlist', 'addToWishlist');
		$app->post('/removeFromWishlist', 'removeFromWishlist');
		$app->post('/getWishlist', 'getWishlist');
		$app->post('/myNotifications', 'myNotifications');
		$app->post('/myPlacedOffers', 'myPlacedOffers');
		$app->post('/myRecievedOffers', 'myRecievedOffers');
		
		$app->post('/offerReceived', 'offerReceived');
		$app->post('/placeOffer', 'placeOffer');
		$app->post('/listGroup', 'listGroup');
		$app->post('/myOfferingProducts', 'myOfferingProducts');
		$app->post('/myProducts', 'myProducts');
		$app->post('/groupJoin', 'groupJoin');
		$app->post('/eventJoin', 'eventJoin');
		$app->post('/groupDetails', 'groupDetails');
		$app->post('/groupeventDetails', 'groupeventDetails');
		$app->post('/eventDetails', 'eventDetails');
		$app->post('/listFreeProducts', 'listFreeProducts');
		$app->post('/getCms', 'getCms');
		
		$app->post('/addForumTopics', 'addForumTopics'); 
		$app->post('/forumDetails', 'forumDetails');  
		$app->post('/groupForumDetails', 'groupForumDetails');  
		$app->post('/listgroupCategory', 'listgroupCategory');
		
		$app->post('/myGroup', 'myGroup');
		//$app->post('/listSocial', 'listSocial');
		$app->post('/addForumComments', 'addForumComments');
		
		$app->post('/listingComplete', 'listingComplete');
		$app->post('/cancelOffer', 'cancelOffer');
		$app->post('/otheruserProfile', 'otheruserProfile');
                
		$app->post('/sendChat', 'sendChat');
		$app->post('/getallConvesssion', 'getallConvesssion');
                $app->post('/unreadChat', 'unreadChat');
                $app->post('/countChat', 'countChat');
                $app->post('/listunreadchat', 'listunreadchat');
                
                
                $app->post('/blockUser', 'blockUser');  
		$app->post('/unblockUser', 'unblockUser'); 
		$app->post('/getBlockedUsers', 'getBlockedUsers'); 
		$app->post('/listSocial', 'listSocial');
		$app->post('/chatBlockuser', 'chatBlockuser');
                
                
                
                
                
        $app->post('/groupEventSetting', 'groupEventSetting');
        $app->post('/offerReceiveStatus', 'offerReceiveStatus');
        $app->post('/messageStatus', 'messageStatus');
         $app->post('/forum_reply_status', 'forum_reply_status');
        $app->post('/watchlist_complete_status', 'watchlist_complete_status');
        $app->post('/other_notification_status', 'other_notification_status');
        $app->post('/notificationsettingDetails', 'notificationsettingDetails');
		
		       
                //$app->post('/userDetails', 'userDetails');
        



	
});
?>