<?php

//error_reporting(1);

require 'vendor/autoload.php';

require 'config.php';
include('routs.php');
include('crud.php');
//include('Stripe.php');

date_default_timezone_set('UTC');



function userSignup() {
    
    $data = array();
    
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    

    $name = isset($body->name) ? $body->name : '';
    //$lname = isset($body->lname) ? $body->lname : '';    
    $email = isset($body->email) ? $body->email : '';    
    $password = isset($body->password) ? $body->password : '';
    $mobile_no = isset($body->mobile_no) ? $body->mobile_no : '';

    $type = isset($body->type) ? $body->type : '';
    
    $device_type = isset($body->device_type) ? $body->device_type : '';
    $token = isset($body->token) ? $body->token : '';
    //$my_latitude = isset($body->my_latitude) ? $body->my_latitude : '';
    //$my_longitude = isset($body->my_longitude) ? $body->my_longitude : '';
           
    $db = getConnection();


    $sql = "SELECT * FROM users WHERE email=:email";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("email", $email);
    $stmt->execute();
    $usersCount = $stmt->rowCount();

    if ($usersCount == 0) {

        $newpass = md5($password);
		$status=1;

      $sql = "INSERT INTO users (name, email, password, mobile_no, type, device_type, token) VALUES (:name, :email, :password, :mobile_no, :type, :device_type :token)";
        try {

            $stmt = $db->prepare($sql);
            $stmt->bindParam("name", $name);
            //$stmt->bindParam("lname", $lname);
            $stmt->bindParam("email", $email);
            $stmt->bindParam("password", $newpass);
            $stmt->bindParam("mobile_no", $mobile_no);
            $stmt->bindParam("type", $type);
            $stmt->bindParam("device_type", $device_type);
            $stmt->bindParam("token", $token);
            //$stmt->bindParam("my_latitude", $my_latitude);
            //$stmt->bindParam("my_longitude", $my_longitude);            
            $stmt->execute();
   

            $lastID = $db->lastInsertId();
            $data['last_id'] = $lastID;
            $data['Ack'] = '1';
            $data['msg'] = 'Registered Successfully...';


            $sql = "SELECT * FROM users WHERE id=:id ";
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id", $lastID);
            $stmt->execute();
            $getUserdetails = $stmt->fetchObject();

            if ($getUserdetails->image != '') {
                $profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
            } else {
                $profile_image = '';
            }

            $data['UserDetails'] = array(
                "id" => stripslashes($getUserdetails->id),
                "name" => stripslashes($getUserdetails->name),
                //"lname" => stripslashes($getUserdetails->lname),
                "email" => stripslashes($getUserdetails->email),
                "password" => stripslashes($getUserdetails->password),
                "mobile_no" => stripslashes($getUserdetails->mobile_no),
                "type" => stripslashes($getUserdetails->type),
                //"profile_image" => stripslashes($profile_image),
                "device_type" => stripslashes($getUserdetails->device_type),
                "token" => stripslashes($getUserdetails->token),
                //"my_latitude" => stripslashes($getUserdetails->my_latitude),
                //"my_longitude" => stripslashes($getUserdetails->my_longitude)
                );


            $to = $email;

            $subject = "service.com- Thank you for registering";            

            $TemplateMessage = "Wellcome and thank you for registering at service.com!<br />";
            $TemplateMessage .= "Your account has now been created and you can login using your email address and password. But Please wait until Admin Approves your account. Your account will be activated shortly and you will be notified.<br />";
            
            $TemplateMessage .= "Thanks,<br />";
            $TemplateMessage .= "service.com<br />";

            $header = "From:info@service.com \r\n";
            // $header .= "Cc:nits.nits.bsm@gmail.com \r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-type: text/html\r\n";

            $retval = mail($to, $subject, $TemplateMessage, $header);

            $app->response->setStatus(200);

            $db = null;
//echo json_encode($user);
        } catch (PDOException $e) {
//error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $data['user_id'] = '';
            $data['Ack'] = '0';
            $data['msg'] = $e->getMessage();
//    echo '{"error":{"text":'. $e->getMessage() .'}}';
            $app->response->setStatus(401);
        }
    } else {
        $data['user_id'] = '';
        $data['Ack'] = '0';
        $data['msg'] = 'User already exists';
        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));    
    
}





		function login() { 
		
		$data = array();
		
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$email = $body->email;
		$password = $body->password;
		$device_type = isset($body->device_type) ? $body->device_type : '';
		$device_token_id = isset($body->device_token_id) ? $body->device_token_id : '';
		$my_latitude = isset($body->my_latitude) ? $body->my_latitude : '';
		$my_longitude = isset($body->my_longitude) ? $body->my_longitude : '';
		$status = 1;
		
		$sql = "SELECT * FROM barter_user WHERE email=:email AND password=:password AND status=:status";
		
		try {
		
		$pass = md5($password); 
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("email", $email);
	//	$stmt->bindParam("phone", $phone);
		$stmt->bindParam("password", $pass);
		$stmt->bindParam("status", $status);
		$stmt->execute();
		$stmt->queryString;
		//  print_r(DB::getQueryLog());
		$user = $stmt->fetchObject();
		
		$userCount = $stmt->rowCount(); 
		if ($userCount == 0) {
		$data['user_id'] = '';
		$data['Ack'] = 0;
		$data['msg'] = 'Username Or Password Is Invalid !!!';
		
		$app->response->setStatus(401);
		} else {
		
		
		if ($user->status == 0) {
		$data['user_id'] = '';
		$data['Ack'] = 0;
		$data['msg'] = 'Your Account Is Not Activated yet';
		
		$app->response->setStatus(200);
		} else {
		
		$data['user_id'] = $user->id;
		$data['Ack'] = 1;
		$data['msg'] = 'Loggedin Successfully';
		
		
		$sql = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $user->id);
		$stmt->execute();
		$getUserdetails = $stmt->fetchObject();
		
		if ($getUserdetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
		} else {
		 $profile_image =  SITE_URL . 'webservice/no-user.png';
		}
		
		
		$data['UserDetails'] = array(
		
		"id" => stripslashes($getUserdetails->id),		
		"fname" => stripslashes($getUserdetails->fname),
		"lname" => stripslashes($getUserdetails->lname),
		"phone" => stripslashes($getUserdetails->phone),
		"profile_image" => $profile_image,
		"email" => stripslashes($getUserdetails->email),
		"address" => stripslashes($getUserdetails->address),
		"device_type" => stripslashes($getUserdetails->device_type),
		"device_token_id" => stripslashes($getUserdetails->device_token_id),
		"my_latitude" => stripslashes($getUserdetails->my_latitude),
		"my_longitude" => stripslashes($getUserdetails->my_longitude),
		"add_date" => stripslashes($getUserdetails->add_date));
		
		
		$userID = $getUserdetails->id;
		$is_logged_in = 1;
		// $sqlUserUpdate = "UPDATE barter_users SET device_type=:device_type, device_token_id=:device_token_id, is_logged_in=:is_logged_in,lat=:lat,lang=:lang WHERE id=:id";
		$sqlUserUpdate = "UPDATE barter_user SET device_type=:device_type, device_token_id=:device_token_id WHERE id=:id";
		$stmt1 = $db->prepare($sqlUserUpdate);
		$stmt1->bindParam("device_type", $device_type);
		$stmt1->bindParam("device_token_id", $device_token_id);                
		//$stmt1->bindParam("my_latitude", $lat);
		//$stmt1->bindParam("my_longitude", $lang);
		$stmt1->bindParam("id", $userID);
		$stmt1->execute();
		
		$app->response->setStatus(200);
		}
		}
		
		$db = null;
		//    print_r($user);
		} catch (PDOException $e) {
		print_r($e);
		$data['user_id'] = '';
		$data['Ack'] = 0;
		$data['msg'] = 'Login Error!!!';
		
		$app->response->setStatus(401);
		}
		
		$app->response->write(json_encode($data));
		//  echo json_encode($data);
		//exit;
}





		function otheruserProfile() {
		
		$data = array();
		
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		
		$user_id = isset($body->user_id) ? $body->user_id : '';

		
		$db = getConnection();
		
       	$sql = "SELECT * FROM barter_user WHERE id=:id ";
		
		try {
		
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$getUserdetails = $stmt->fetchObject();
		
		if ($getUserdetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
		} else {
		$profile_image = '';
		}
		
		$data['UserDetails'] = array(
		"id" => stripslashes($getUserdetails->id),
		"fname" => stripslashes($getUserdetails->fname),
		"lname" => stripslashes($getUserdetails->lname),
		"email" => stripslashes($getUserdetails->email),
		"password" => stripslashes($getUserdetails->password),
		"jikoo_number" => stripslashes($getUserdetails->jikoo_number),
		"profile_image" => stripslashes($profile_image),
		"device_type" => stripslashes($getUserdetails->device_type),
		"device_token_id" => stripslashes($getUserdetails->device_token_id),
		"my_latitude" => stripslashes($getUserdetails->my_latitude),
		"my_longitude" => stripslashes($getUserdetails->my_longitude)
		);
		
		
		
		
		$app->response->setStatus(200);
		
		$db = null;
		//echo json_encode($user);
		} catch (PDOException $e) {
		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
		$data['user_id'] = '';
		$data['Ack'] = '0';
		$data['msg'] = $e->getMessage();
		//    echo '{"error":{"text":'. $e->getMessage() .'}}';
		$app->response->setStatus(401);
		}
		
		$app->response->write(json_encode($data));    
		
}





/* function addProducts() {           
    
    $data = array();

    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    $name= isset($body->title) ? $body->title: '';
    $description= isset($body->description) ? $body->description: '';  
    $condition= isset($body->condition) ? $body->condition: '';  
    $regular_price= isset($body->value) ? $body->value: '';
    $user_id = isset($body->user_id) ? $body->user_id: '';
    $cat_id = isset($body->cat_id) ? $body->cat_id: '';
     $group_id = isset($body->group_id) ? $body->group_id: '';
     $want_to_return = isset($body->want_to_return) ? $body->want_to_return: '';
   // $offer_type = isset($body->offer_type) ? $body->offer_type : '';
   // $inventory = isset($body->inventory) ? $body->inventory : '';    
    $lat = isset($body->lat) ? $body->lat : '';
    $lang = isset($body->lang) ? $body->lang : '';
    
    $date = date('Y-m-d H:i:s');
              
    $db = getConnection();
        
        $sql = "INSERT INTO barter_product (name, description, regular_price, user_id, cat_id,prod_condition, want_to_return, group_id, lat, lang,datetime) VALUES (:name, :description, :regular_price, :user_id, :cat_id,:condition, :want_to_return, :group_id, :lat, :lang,:date)";
        try {
           
            $stmt = $db->prepare($sql);
            $stmt->bindParam("name", $name);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("regular_price", $regular_price);
            $stmt->bindParam("user_id", $user_id);
            $stmt->bindParam("cat_id", $cat_id);
            $stmt->bindParam("group_id", $group_id);
            $stmt->bindParam("want_to_return", $want_to_return);
            $stmt->bindParam("condition", $condition);
            $stmt->bindParam("lat", $lat);
            $stmt->bindParam("lang", $lang);
            $stmt->bindParam("date", $date);

            $stmt->execute();
         

            $lastID = $db->lastInsertId();
            $data['last_id'] = $lastID;
            $data['Ack'] = '1';
            $data['msg'] = 'Product Added Successfully...';

            $app->response->setStatus(200);

            $db = null;
            //echo json_encode($user);
        } catch (PDOException $e) {
            print_r($e);
            exit;
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $data['user_id'] = '';
            $data['Ack'] = '0';
            $data['msg'] = 'Error !!!';
            //echo '{"error":{"text":'. $e->getMessage() .'}}';
            $app->response->setStatus(401);
        }    


    $app->response->write(json_encode($data));
}  */ //18.5.2017


function addProducts() {           
    
  $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body = ($request->post());

 $name = $body['title'];
    $description = $body['description'];
    $condition = $body['condition'];
    $regular_price = $body['value'];
    $user_id = $body['user_id'];
    $cat_id = $body['cat_id'];
    $group_id = $body['group_id'];
    $want_to_return = $body['want_to_return'];
    $lat = $body['lat'];
    $lang = $body['lang'];
    
    //print_r($body);
   // print_r($_FILES['product_image']);
  // 
   

   $proimg = $_FILES['product_image'];

  // echo "<pre>";
//print_r($_FILES['product_image']);
//echo "</pre>"; 

//exit;
 

    /* $name= isset($body->title) ? $body->title: '';
    $description= isset($body->description) ? $body->description: '';  
    $condition= isset($body->condition) ? $body->condition: '';  
    $regular_price= isset($body->value) ? $body->value: '';
    $user_id = isset($body->user_id) ? $body->user_id: '';
    $cat_id = isset($body->cat_id) ? $body->cat_id: '';
     $group_id = isset($body->group_id) ? $body->group_id: '';
     $want_to_return = isset($body->want_to_return) ? $body->want_to_return: '';
   
    $lat = isset($body->lat) ? $body->lat : '';
    $lang = isset($body->lang) ? $body->lang : ''; */
    
    $date = date('Y-m-d H:i:s');
              
    $db = getConnection();
        
        $sql = "INSERT INTO barter_product (name, description, regular_price, user_id, cat_id,prod_condition, want_to_return, group_id, lat, lang,datetime) VALUES (:name, :description, :regular_price, :user_id, :cat_id,:condition, :want_to_return, :group_id, :lat, :lang,:date)";
        try {
           
            $stmt = $db->prepare($sql);
            $stmt->bindParam("name", $name);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("regular_price", $regular_price);
            $stmt->bindParam("user_id", $user_id);
            $stmt->bindParam("cat_id", $cat_id);
            $stmt->bindParam("group_id", $group_id);
            $stmt->bindParam("want_to_return", $want_to_return);
            $stmt->bindParam("condition", $condition);
            $stmt->bindParam("lat", $lat);
            $stmt->bindParam("lang", $lang);
            $stmt->bindParam("date", $date);

            $stmt->execute();
         

            $lastID = $db->lastInsertId();
            $data['last_id'] = $lastID; 
            
            
        
            
            
 foreach($_FILES['product_image'] as $key => $val)
            {
                 echo "aa".$filename = $_FILES['product_image'][$key];
                 echo "<br>";
                $filesize = $_FILES['product_image']['size'][$key];
                $filetempname = $_FILES['product_image']['tmp_name'][$key];

                $fileext = pathinfo($fileName, PATHINFO_EXTENSION);
                $fileext = strtolower($fileext);
     /* $target_path = "../upload/product/";
            $userfile_name = $_FILES['image']['name'];
            $userfile_tmp = $_FILES['image']['tmp_name'];
            $img_name = time() . $userfile_name;
            $img = $target_path . $img_name;
            move_uploaded_file($userfile_tmp, $img); */

    //echo $image;
    //echo "<br>";
     
     //echo "1";
     //echo $sql = "INSERT INTO barter_moreimage (pro_id, image) VALUES (:pro_id, :image)";

           // echo "INSERT INTO `barter_moreimage` (pro_id, image) VALUES ('".$lastID."', '".$image."')";

    //$sql = query("INSERT INTO `barter_moreimage` (pro_id, image) VALUES ('".$lastID."', '".$image."')");
                 
                    
 }  

 exit; 
 
 
 
 
 
            $data['Ack'] = '1';
            $data['msg'] = 'Product Added Successfully...';

            $app->response->setStatus(200);

            $db = null;
            //echo json_encode($user);
        } catch (PDOException $e) {
            print_r($e);
            exit;
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $data['user_id'] = '';
            $data['Ack'] = '0';
            $data['msg'] = 'Error !!!';
            //echo '{"error":{"text":'. $e->getMessage() .'}}';
            $app->response->setStatus(401);
        }    


    $app->response->write(json_encode($data));
}




function addForumTopics() {           
    
    $data = array();

    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    $name= isset($body->name) ? $body->name: '';
    $description= isset($body->description) ? $body->description: '';    
    $group_id= isset($body->group_id) ? $body->group_id: '';
    $user_id = isset($body->user_id) ? $body->user_id: '';
    $posted_on = date('Y-m-d H:i:s');
              
    $db = getConnection();
        
        $sql = "INSERT INTO barter_forum (group_id, user_id, name, description, posted_on) VALUES (:group_id, :user_id, :name, :description, :posted_on)";
        try {
           
            $stmt = $db->prepare($sql);
            $stmt->bindParam("group_id", $group_id);
            $stmt->bindParam("user_id", $user_id);
            $stmt->bindParam("name", $name);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("posted_on", $posted_on);
            $stmt->execute();
         

            $lastID = $db->lastInsertId();
            $data['last_id'] = $lastID;
            $data['Ack'] = '1';
            $data['msg'] = 'Forum Posted Successfully...';

            $app->response->setStatus(200);

            $db = null;
            //echo json_encode($user);
        } catch (PDOException $e) {
            print_r($e);
            exit;
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $data['user_id'] = '';
            $data['Ack'] = '0';
            $data['msg'] = 'Error !!!';
            //echo '{"error":{"text":'. $e->getMessage() .'}}';
            $app->response->setStatus(401);
        }    


    $app->response->write(json_encode($data));
}



function addForumComments() {           
    
    $data = array();

    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    $forum_id= isset($body->forum_id) ? $body->forum_id: '';
    $user_id= isset($body->user_id) ? $body->user_id: '';    
    $comments= isset($body->comments) ? $body->comments: '';
    $posted_on = date('Y-m-d H:i:s');
              
    $db = getConnection();
        
        $sql = "INSERT INTO barter_forum_comments (forum_id, user_id, comments, posted_on) VALUES (:forum_id, :user_id, :comments, :posted_on)";
        try {
           
            $stmt = $db->prepare($sql);
            $stmt->bindParam("forum_id", $forum_id);
            $stmt->bindParam("user_id", $user_id);
            $stmt->bindParam("comments", $comments);
            $stmt->bindParam("posted_on", $posted_on);
            $stmt->execute();
         

            $lastID = $db->lastInsertId();
            
            	
	// SENDING NOTIFICATIONS ===========================================


	$sqlProductDetails = "SELECT * FROM barter_forum where id = '".$forum_id."'";
	$stmtProductDetails = $db->query($sqlProductDetails);
	$getstmtProductDetails = $stmtProductDetails->fetchObject();
	$to_id=$getstmtProductDetails->user_id;
			
		$from_id=$user_id;
                $sqlSenderDetails = "SELECT * FROM barter_user  WHERE id='" . $from_id . "'";
		    $stmtSenderDetails = $db->prepare($sqlSenderDetails);
		    $stmtSenderDetails->execute();
		    $getSenderDetails = $stmtSenderDetails->fetchObject();
                    $from_name= $getSenderDetails->fname;
                    $forum_id=$forum_id;
                    $sqlForumDetails = "SELECT * FROM barter_forum  WHERE id='" . $forum_id . "'";
		    $stmtForumDetails = $db->prepare($sqlForumDetails);
		    $stmtForumDetails->execute();
		    $getForumDetails = $stmtForumDetails->fetchObject();
                    $forum_name=$getForumDetails->name;
		$type='4';
		$message=$from_name." Has Commented on ".$forum_name." Forum";
		$date=date('Y-m-d H:i:s');
		$product_or_group=$forum_id;
		$sql = "INSERT INTO barter_notification (product_or_group,from_id, to_id, type, msg, date, last_id) VALUES (:product_or_group, :from_id, :to_id, :type, :msg, :date, :last_id)";
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("product_or_group", $product_or_group);
		$stmt->bindParam("from_id", $from_id);
		$stmt->bindParam("to_id", $to_id);
		$stmt->bindParam("type", $type);
		$stmt->bindParam("msg", $message);
		$stmt->bindParam("date", $date);
		$stmt->bindParam("last_id", $lastID);
		$stmt->execute();			
		
// END OF NOTIFICATIONS =================================================		
	
	
            $data['last_id'] = $lastID;
            $data['Ack'] = '1';
            $data['msg'] = 'Comment Posted Successfully...';

            $app->response->setStatus(200);

            $db = null;
            //echo json_encode($user);
        } catch (PDOException $e) {
            print_r($e);
            exit;
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $data['user_id'] = '';
            $data['Ack'] = '0';
            $data['msg'] = 'Error !!!';
            //echo '{"error":{"text":'. $e->getMessage() .'}}';
            $app->response->setStatus(401);
        }    


    $app->response->write(json_encode($data));
}




function groupForumDetails() {
		$data = array();
		$GroupDetails = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$group_id = isset($body->group_id) ? $body->group_id : '';
		
		$db = getConnection();
		
		$sql = "SELECT * from barter_group WHERE id='" . $group_id . "'";
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$groupList = $stmt->fetchObject();		
		// exit;		
		if (!empty($groupList)) {
		
		
		$description=$groupList->description;
		$group_owner='Admin';
		$owner_image = SITE_URL . 'upload/user_images/allpro_2.png';
		if ($groupList->img != '') {
		$image = SITE_URL . 'upload/group/' . $groupList->img;
		} else {
		$image =  SITE_URL . 'upload/no.png';
		}
		
		$product_cat=$groupList->product_cat;
		$grpids = explode(',', $product_cat);
		$final_groupids = "'" . implode("','", $grpids) . "'";
		
		$sqlpro = "SELECT * from barter_forum WHERE `group_id`='".$group_id."'";
		
		$stmt7 = $db->query($sqlpro);
		
		$productList = $stmt7->fetchAll(PDO::FETCH_OBJ);
		
		foreach ($productList as $products) {


		$sql11 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt11 = $db->prepare($sql11);
		$stmt11->bindParam("id", $products->user_id);
		$stmt11->execute();
		$getForumOwnerdetails = $stmt11->fetchObject();
		
				
		$user_nameee=$getForumOwnerdetails->fname.' '.$getForumOwnerdetails->lname;
		$emaill=$getForumOwnerdetails->email;
		
		if ($getForumOwnerdetails->image != '') {
		$profile_imagee = SITE_URL . 'upload/user_images/' . $getForumOwnerdetails->image;
		} else {
		$profile_imagee =  SITE_URL . 'webservice/no-user.png';
		}
		
		
		
		
		$forumDetails[] = array(
		"id" => stripslashes($products->id),
		"user_name" => stripslashes($getForumOwnerdetails->fname.' '.$getForumOwnerdetails->lname),
		"name" => stripslashes($products->name),
		"description" => stripslashes(substr(strip_tags($description),0,50)),
		"image" => $profile_imagee,
		"posted_on" => stripslashes($products->posted_on)
		);
		
		
		}
		
		
		
		
		
		$memberImage = "select * FROM barter_group_members WHERE `group_id`='" . $group_id . "'";
		$stmtmemberImage = $db->query($memberImage);
		$memberImages = $stmtmemberImage->fetchAll(PDO::FETCH_OBJ);
		$nomember = $stmtmemberImage->rowCount();
		
		$memberDetails=array();
		
		foreach ($memberImages as $mymemberImage) {
		
		$sql11 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt11 = $db->prepare($sql11);
		$stmt11->bindParam("id", $mymemberImage->user_id);
		$stmt11->execute();
		$getUserdetailss = $stmt11->fetchObject();
		
		if(!empty($getUserdetailss))
		{		
		$user_nameee=$getUserdetailss->fname.' '.$getUserdetailss->lname;
		$emaill=$getUserdetailss->email;
		
		if ($getUserdetailss->image != '') {
		$profile_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailss->image;
		} else {
		$profile_imagee =  SITE_URL . 'webservice/no-user.png';
		}
		}
		else
		{
		$profile_imagee='';  
		}
		
		
		
		$memberDetails[] = array(
		"id" => stripslashes($mymemberImage->user_id),
		"username" => stripslashes($user_nameee),
		"email" => stripslashes($emaill),
		"user_image" => stripslashes($profile_imagee),
		);
		
		
		}
		
		
		$GroupDetails = array(
		"group_id" => stripslashes($groupList->id),
		"group_name" => stripslashes($groupList->name),
		"group_owner" => stripslashes($group_owner),
		"owner_image" => stripslashes($owner_image),
		"group_image" => $image,
		"no_member" => $nomember,
		"allforums" => (!empty($forumDetails)?$forumDetails:''),
		"memberdetails" => (!empty($memberDetails)?$memberDetails:'')
		
		);
		$data['GroupDetails'] = $GroupDetails;
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}



	function forumDetails() {
	$data = array();
	$GroupDetails = array();
	$commentDetails=array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$forum_id = isset($body->forum_id) ? $body->forum_id : '';	
	$db = getConnection();	
	$sql = "SELECT * from barter_forum WHERE id='" . $forum_id . "'";
	
	$db = getConnection();
	$stmt = $db->prepare($sql);
	//	$stmt->bindParam("user_id", $user_id);
	$stmt->execute();
	$forumDetails = $stmt->fetchObject();
	
	// exit;
	
	if (!empty($forumDetails)) {
	
	$memberImage = "select * FROM barter_forum_comments WHERE `forum_id`='" . $forum_id . "' order by `id` DESC";
	$stmtmemberImage = $db->query($memberImage);
	$memberImages = $stmtmemberImage->fetchAll(PDO::FETCH_OBJ);
	$nomember = $stmtmemberImage->rowCount();
	
	$memberDetails=array();
	
	foreach ($memberImages as $mymemberImage) {
	
	$sql11 = "SELECT * FROM barter_user WHERE id=:id ";
	$stmt11 = $db->prepare($sql11);
	$stmt11->bindParam("id", $mymemberImage->user_id);
	$stmt11->execute();
	$getUserdetailss = $stmt11->fetchObject();
	
	if(!empty($getUserdetailss))
	{		
	$user_nameee=$getUserdetailss->fname.' '.$getUserdetailss->lname;
	$emaill=$getUserdetailss->email;
	
	if ($getUserdetailss->image != '') {
	$profile_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailss->image;
	} else {
	$profile_imagee =  SITE_URL . 'webservice/no-user.png';
	}
	}
	else
	{
	$profile_imagee='';  
	}
	
	
	
	$commentDetails[] = array(
	"id" => stripslashes($mymemberImage->user_id),
	"username" => stripslashes($user_nameee),
	"comments" => stripslashes($mymemberImage->comments),
	"posted_on" => stripslashes($mymemberImage->posted_on),
	"email" => stripslashes($emaill),
	"user_image" => stripslashes($profile_imagee),
	);
	
	
	}
	
	
/*	if ($eventDetails->img != '') {
	$event_imagee = SITE_URL . 'upload/group/' . $eventDetails->img;
	} else {
	$event_imagee =  SITE_URL . 'webservice/no-user.png';
	}*/


// GET OWNER DETAILS ================================


	$sqlCreator = "SELECT * FROM barter_user WHERE id=:id ";
	$stmtSqlCreator = $db->prepare($sqlCreator);
	$stmtSqlCreator->bindParam("id", $forumDetails->user_id);
	$stmtSqlCreator->execute();
	$getUserdetailsCreator = $stmtSqlCreator->fetchObject();
	

	
	if ($getUserdetailsCreator->image != '') {
	$creator_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailsCreator->image;
	} else {
	$creator_imagee =  SITE_URL . 'webservice/no-user.png';
	}

	
/*echo '<pre>';	
print_r($forumDetails);*/
	
	
	$eventDetailsData = array(
	"id" => stripslashes($forumDetails->id),
	"name" => stripslashes($forumDetails->name),
	"group_id" => stripslashes($forumDetails->group_id),
	"creator_name" => $getUserdetailsCreator->fname.' '.$getUserdetailsCreator->lname,
	"creator_image" => $creator_imagee,
	"description" => $forumDetails->description,
	"commentDetails" => $commentDetails,
	"posted_on" => stripslashes($forumDetails->posted_on)
	);
	
	
	$data['forumDetailsData'] = $eventDetailsData;
	$data['Ack'] = 1;
	$app->response->setStatus(200);
	} else {
	$data = array();
	$data['Ack'] = 0;
	//$data['msg'] = $e->getMessage();
	$app->response->setStatus(200);
	}
	
	$app->response->write(json_encode($data));
}


function updateProducts() {
    
    $data = array();

    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);    
    
    $id= isset($body->id) ? $body->id: '';
    $name= isset($body->title) ? $body->title: '';
    $description= isset($body->description) ? $body->description: '';
    $regular_price = isset($body->value) ? $body->value : '';
     $condition= isset($body->condition) ? $body->condition: '';  
    $user_id = isset($body->user_id) ? $body->user_id: '';    
    $cat_id = isset($body->cat_id) ? $body->cat_id : '';
    $group_id = isset($body->group_id) ? $body->group_id: '';
    $want_to_return = isset($body->want_to_return) ? $body->want_to_return: '';
   // $offer_type = isset($body->offer_type) ? $body->offer_type : '';
   // $inventory = isset($body->inventory) ? $body->inventory : '';
    $lat = isset($body->lat) ? $body->lat : '';
    $lang = isset($body->lang) ? $body->lang : '';
               
    $db = getConnection();

        
        $sql = "UPDATE barter_product set name=:name, description=:description, regular_price=:regular_price, user_id=:user_id, cat_id=:cat_id, group_id=:group_id, want_to_return=:want_to_return,prod_condition=:condition, lat=:lat, lang=:lang WHERE id=:id";
        try {

           
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id", $id);
            $stmt->bindParam("name", $name);
            $stmt->bindParam("description", $description);
            $stmt->bindParam("regular_price", $regular_price);
            $stmt->bindParam("user_id", $user_id);
            $stmt->bindParam("cat_id", $cat_id);
            $stmt->bindParam("group_id", $group_id);
            $stmt->bindParam("want_to_return", $want_to_return);
            $stmt->bindParam("condition", $condition);           
            $stmt->bindParam("lat", $lat);
            $stmt->bindParam("lang", $lang);
            			
            $stmt->execute();
         
            $lastID = $db->lastInsertId();
            $data['id'] = $id;
            $data['Ack'] = '1';
            $data['msg'] = 'Product Updated Successfully...';

            $app->response->setStatus(200);

            $db = null;
            //echo json_encode($user);
        } catch (PDOException $e) {
            print_r($e);
            exit;
            //error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $data['user_id'] = '';
            $data['Ack'] = '0';
            $data['msg'] = 'Error !!!';
            //echo '{"error":{"text":'. $e->getMessage() .'}}';
            $app->response->setStatus(401);
        }    

    $app->response->write(json_encode($data));
    
}


function deleteProduct() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);

    $id = $body->id;
    //$user_id = isset($body->user_id) ? $body->user_id: '';
     //$is_del=1;

    $sql = "DELETE from barter_product WHERE id=:id";    

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        //$stmt->bindParam("is_del", $is_del);
        $stmt->execute();

        
        $data['Ack'] = '1';
        $data['msg'] = 'Product deleted';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Deletion Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}


function logout() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);

    $user_id = $body->user_id;
    $is_loggedin = '0';

    $sql = "UPDATE barter_user SET is_loggedin=:is_logged_in WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("is_logged_in", $is_loggedin);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Logout Successfully';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}


	function forgetpassword() {
	//error_log('addWine\n', 3, '/var/tmp/php.log');
	$data = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	$email = $body->email;
	
	
	$byeamil = findByConditionArray(array('email' => $email), 'barter_user');
	if (!empty($byeamil)) {
	
	
	
	$sql = "SELECT * FROM barter_user WHERE email=:email ";
	$db = getConnection();
	$stmt = $db->prepare($sql);
	$stmt->bindParam("email", $email);
	$stmt->execute();
	$getUserdetails = $stmt->fetchObject();
	
	$to = $email;
	$password=rand(1111, 9999);
	$forgot_pass_otp = md5($password);
	
	
	$sql = "UPDATE barter_user SET password=:password WHERE email=:email";
	$stmt = $db->prepare($sql);
	$stmt->bindParam("password", $forgot_pass_otp);
	$stmt->bindParam("email", $email);
	$stmt->execute();
	
	$subject = "baybarter.com- Your Password Request";
	$TemplateMessage = "Hello " . $getUserdetails->fname . "<br />";
	$TemplateMessage .= "You have asked for your new password. Your Password is OTP below :<br />";
	$TemplateMessage .= "Password :" . $password;
	
	$TemplateMessage .= "<br /> Thanks,<br />";
	$TemplateMessage .= "baybarter.com<br />";
	
	
	
	$header = "From:info@baybarter.com \r\n";
	// $header .= "Cc:nits.sarojkumar@gmail.com \r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-type: text/html\r\n";
	
	$retval = mail($to, $subject, $TemplateMessage, $header);
	
	$db = null;
	$data['ACK'] = '1';
	$data['msg'] = 'Mail Send Successfully';
	$app->response->setStatus(200);
	} else {
	$data['last_id'] = '';
	$data['ACK'] = '0';
	$data['msg'] = 'Email not found in our database';
	$app->response->setStatus(200);
	}
	
	
	$app->response->write(json_encode($data));
}

	function changepwd() {
	$data = array();
	
	$app = \Slim\Slim::getInstance();
	
	
	$request = $app->request();
	
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	$new_pwd = $body->new_pwd;
	$user_id = $body->user_id;
	
	$db = getConnection();
	
	$sql_exists = "SELECT * from barter_user WHERE id=:id";
	
	$stmtexists = $db->prepare($sql_exists);
	$stmtexists->bindParam("id", $user_id);
	$stmtexists->execute();
	$count = $stmtexists->rowCount();
	if ($count > 0) {
	
	
	
	$sql = "SELECT * FROM barter_user WHERE id=:id";
	
	$stmt = $db->prepare($sql);
	$stmt->bindParam("id", $user_id);
	$stmt->execute();
	$getUserdetails = $stmt->fetchObject();
	
	$to = $getUserdetails->email;
	
	$password_for_db = md5($new_pwd);
	
	$sqlupd = "UPDATE barter_user SET password=:password WHERE id=:id";
	$stmtupd = $db->prepare($sqlupd);
	$stmtupd->bindParam("password", $password_for_db);
	$stmtupd->bindParam("id", $user_id);
	$stmtupd->execute();
	
	$subject = "baybarter.com- Your Password Request";
	$TemplateMessage = "Hello " . $getUserdetails->fname . "<br />";
	$TemplateMessage .= "You have Successfully changed your password. Your new password is  as follows :<br />";
	$TemplateMessage .= "Password :" . $new_pwd . "<br />";
	
	$TemplateMessage .= "Thanks,<br />";
	$TemplateMessage .= "baybarter.com<br />";
	
	
	
	$header = "From:info@baybarter.com \r\n";
	// $header .= "Cc:nits.sarojkumar@gmail.com \r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-type: text/html\r\n";
	
	$retval = mail($to, $subject, $TemplateMessage, $header);
	
	$db = null;
	$data['Ack'] = '1';
	$data['msg'] = 'Password updated and Mail Send Successfully';
	} else {
	$data['last_id'] = '';
	$data['Ack'] = '0';
	$data['msg'] = 'User not found in our database';
	}
	$app->response->write(json_encode($data));
}




function userprofile() {

    $friendlist = array();
    $data = array();
    $followingList = array();
    $followerList = array();
    $allPortfolios = array();

    $app = \Slim\Slim::getInstance();
    $request = $app->request();


    $body2 = $app->request->getBody();
    $body = json_decode($body2);

// $email = $body['email'];
    $user_id = $body->user_id;

    try {
        $db = getConnection();


        $sql = "SELECT * FROM barter_user WHERE id=:id ";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $user_id);
        $stmt->execute();
        $getUserdetails = $stmt->fetchObject();

        if ($getUserdetails->image != '') {
            $profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
        } else {
          $profile_image =  SITE_URL . 'webservice/no-user.png';
        }



        $data['UserDetails'] = array(
            "user_id" => stripslashes($getUserdetails->id),
            "fname" => stripslashes($getUserdetails->fname),
			"lname" => stripslashes($getUserdetails->lname),
            "user_type" => stripslashes($getUserdetails->type),
            "email" => stripslashes($getUserdetails->email),
            "phone" => stripslashes($getUserdetails->phone),
            "address" => stripslashes($getUserdetails->address),
			"about_me" => stripslashes($getUserdetails->about_me),
            "profile_image" => stripslashes($profile_image),
            "device_type" => stripslashes($getUserdetails->device_type),
            "device_token_id" => stripslashes($getUserdetails->device_token_id),
            "lat" => stripslashes($getUserdetails->my_latitude),
            "lang" => stripslashes($getUserdetails->my_longitude),
            "date" => stripslashes($getUserdetails->add_date));


        $data['Ack'] = '1';
        $data['msg'] = 'Success';
        $app->response->setStatus(200);


        $db = null;
    } catch (PDOException $e) {
        $data['id'] = '';
        $data['Ack'] = '0';
        $data['msg'] = $e->getMessage();
        $app->response->setStatus(401);
    }
    $app->response->write(json_encode($data));
}




/*  function updateProfile() {

//error_log('addWine\n', 3, '/var/tmp/php.log');
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();


    $body2 = $app->request->getBody();
    $body = json_decode($body2);

    $user_id = $body->user_id;
    $fname = $body->fname;
	$lname = $body->lname;
   // $email = $body->email;
    $address = $body->address;
    $phone = $body->phone;
	$about_me = $body->about_me;


    $date = date('Y-m-d');


    $sql = "UPDATE barter_user set fname=:fname, lname=:lname, phone=:phone, about_me=:about_me, address=:address WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("fname", $fname);
        $stmt->bindParam("lname", $lname);
        $stmt->bindParam("phone", $phone);
		$stmt->bindParam("about_me", $about_me);
		$stmt->bindParam("address", $address);
        $stmt->bindParam("id", $user_id);
        $stmt->execute();

		if (@$_FILES['profile_image']['tmp_name'] != '') {
		//	$id = $request->post("user_id");
		
		$target_path = "../upload/user_images/";
		
		//	$target_path = "user_images/";
		$userfile_name = $_FILES['profile_image']['name'];
		$userfile_tmp = $_FILES['profile_image']['tmp_name'];
		$image = time() . $userfile_name;
		$img = $target_path . $image;
		move_uploaded_file($userfile_tmp, $img);
		
		$sqlimg = "UPDATE barter_user SET image=:image WHERE id=:id";
		$stmt1 = $db->prepare($sqlimg);
		$stmt1->bindParam("id", $user_id);
		$stmt1->bindParam("image", $image);
		$stmt1->execute();
		} 


        $data['last_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Profile Updated Successfully...';


        $app->response->setStatus(200);

        $db = null;
//echo json_encode($user);
    } catch (PDOException $e) {
//error_log($e->getMessage(), 3, '/var/tmp/php.log');
        $data['last_id'] = '';
        $data['Ack'] = '0';
        $data['msg'] = 'Updation Error !!!';
        echo '{"error":{"text":' . $e->getMessage() . '}}';
        $app->response->setStatus(401);
    }


    $app->response->write(json_encode($data));
}  */   //17.5.2017 


function updateProfile() {

//error_log('addWine\n', 3, '/var/tmp/php.log');
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body = ($request->post());

    //$body2 = $app->request->getBody();
    //$body = json_decode($body2);

    $user_id = $body["user_id"];
    $fname = $body["fname"];
    $lname = $body["lname"];
   // $email = $body->email;
    $address = $body["address"];
    $phone = $body["phone"];
    $about_me = $body["about_me"];


    $date = date('Y-m-d');


    $sql = "UPDATE barter_user set fname=:fname, lname=:lname, phone=:phone, about_me=:about_me, address=:address WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("fname", $fname);
        $stmt->bindParam("lname", $lname);
        $stmt->bindParam("phone", $phone);
        $stmt->bindParam("about_me", $about_me);
        $stmt->bindParam("address", $address);
        $stmt->bindParam("id", $user_id);
        $stmt->execute();

        if (@$_FILES['profile_image']['tmp_name'] != '') {
        //  $id = $request->post("user_id");
        
        $target_path = "../upload/user_images/";
        
        //  $target_path = "user_images/";
        $userfile_name = $_FILES['profile_image']['name'];
        $userfile_tmp = $_FILES['profile_image']['tmp_name'];
        $image = time() . $userfile_name;
        $img = $target_path . $image;
        move_uploaded_file($userfile_tmp, $img);
        
        $sqlimg = "UPDATE barter_user SET image=:image WHERE id=:id";
        $stmt1 = $db->prepare($sqlimg);
        $stmt1->bindParam("id", $user_id);
        $stmt1->bindParam("image", $image);
        $stmt1->execute();
        } 


        $data['last_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Profile Updated Successfully...';


        $app->response->setStatus(200);

        $db = null;
//echo json_encode($user);
    } catch (PDOException $e) {
//error_log($e->getMessage(), 3, '/var/tmp/php.log');
        $data['last_id'] = '';
        $data['Ack'] = '0';
        $data['msg'] = 'Updation Error !!!';
        echo '{"error":{"text":' . $e->getMessage() . '}}';
        $app->response->setStatus(401);
    }


    $app->response->write(json_encode($data));
}


function updateProfileImage() {

//error_log('addWine\n', 3, '/var/tmp/php.log');
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body = ($request->post());

    $user_id = $body['user_id'];

// $date = date('Y-m-d');

    try {
        $db = getConnection();


        if ($_FILES['profile_image']['tmp_name'] != '') {

            $target_path = "../upload/user_images/";

            $userfile_name = $_FILES['profile_image']['name'];
            $userfile_tmp = $_FILES['profile_image']['tmp_name'];
            $image = time() . $userfile_name;
            $img = $target_path . $image;
            move_uploaded_file($userfile_tmp, $img);

            $sqlimg = "UPDATE barter_user SET image=:image WHERE id=:id";
            $stmt1 = $db->prepare($sqlimg);
            $stmt1->bindParam("id", $user_id);
            $stmt1->bindParam("image", $image);
            $stmt1->execute();
        }


        $data['last_id'] = $user_id;
        $data['image'] = SITE_URL . $img;
        $data['Ack'] = '1';
        $data['msg'] = 'Profile Updated Successfully...';


        $app->response->setStatus(200);

        $db = null;
//echo json_encode($user);
    } catch (PDOException $e) {
//error_log($e->getMessage(), 3, '/var/tmp/php.log');
        $data['last_id'] = '';
        $data['Ack'] = '0';
        $data['msg'] = 'Updation Error !!!';
//echo '{"error":{"text":'. $e->getMessage() .'}}';
        $app->response->setStatus(401);
    }


    $app->response->write(json_encode($data));
}



function listProductCategory() {
	$data = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$user_id = isset($body->user_id) ? $body->user_id : '';
	
	$sql = "SELECT * from barter_category WHERE `status`='1'";
	$db = getConnection();
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$getAllCuisine = $stmt->fetchAll(PDO::FETCH_OBJ);
	
	if (!empty($getAllCuisine)) {
	foreach ($getAllCuisine as $cuisine) {


            if ($cuisine->image != '') {
                $image = SITE_URL . 'upload/categoryimage/' . $cuisine->image;
            } else {
            $image =  SITE_URL . 'webservice/noimg.png';
            }

	
	$data['categoryList'][] = array(
	"id" => stripslashes($cuisine->id),
	"image" => stripslashes($image),
	"name" => stripslashes($cuisine->name));
	}
	
	
	$data['Ack'] = 1;
	$app->response->setStatus(200);
	} else {
	$data = array();
	$data['Ack'] = 0;
	$app->response->setStatus(401);
	}
	
	$app->response->write(json_encode($data));
}


function listgroupCategory() {
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    $user_id = isset($body->user_id) ? $body->user_id : '';
    
    $sql = "SELECT * from barter_group WHERE `status`='1'";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $getAllCuisine = $stmt->fetchAll(PDO::FETCH_OBJ);
    
    if (!empty($getAllCuisine)) {
    foreach ($getAllCuisine as $cuisine) {


           
    
    $data['groupList'][] = array(
    "id" => stripslashes($cuisine->id),
    "name" => stripslashes($cuisine->name));
    }
    
    
    $data['Ack'] = 1;
    $app->response->setStatus(200);
    } else {
    $data = array();
    $data['Ack'] = 0;
    $app->response->setStatus(401);
    }
    
    $app->response->write(json_encode($data));
}

function homeProductCategory() {
	$data = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$user_id = isset($body->user_id) ? $body->user_id : '';
	
	$sql = "SELECT * from barter_category WHERE `status`='1' order by `id` DESC limit 5";
	$db = getConnection();
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$getAllCuisine = $stmt->fetchAll(PDO::FETCH_OBJ);
	
	if (!empty($getAllCuisine)) {
	foreach ($getAllCuisine as $cuisine) {


            if ($cuisine->image != '') {
                $image = SITE_URL . 'upload/categoryimage/' . $cuisine->image;
            } else {
            $image =  SITE_URL . 'webservice/noimg.png';
            }

	
	$data['categoryList'][] = array(
	"id" => stripslashes($cuisine->id),
	"image" => stripslashes($image),
	"name" => stripslashes($cuisine->name));
	}
	
	
	$data['Ack'] = 1;
	$app->response->setStatus(200);
	} else {
	$data = array();
	$data['Ack'] = 0;
	$app->response->setStatus(401);
	}
	
	$app->response->write(json_encode($data));
}


		function listProducts() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = isset($body->user_id) ? $body->user_id : '';
		$key_txt = isset($body->key_txt) ? $body->key_txt : '';
		
		$db = getConnection();
		
		$sql = "SELECT * FROM barter_user WHERE id=:id";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$userDetails = $stmt->fetchObject();
		
		$lat = isset($userDetails->my_latitude) ? $userDetails->my_latitude : '';
		$lang = isset($userDetails->my_longitude) ? $userDetails->my_longitude : '';
		
		/*		$sqlGetDistance = "SELECT  * FROM velo_distance_radious WHERE `id`='1'";
		$stmtGetDistance = $db->prepare($sqlGetDistance);
		$stmtGetDistance->execute();
		$getDistance = $stmtGetDistance->fetchObject();
		$admin_distance_radious=$getDistance->distance_radious;*/
		
		$admin_distance_radious=1000;
		
		
		$distanceToCheck = $admin_distance_radious;
		//$user_id = 0;
		if ($lat != '' && $lang != '') {
		
		/* $sql = "SELECT
		*, (
		3959 * acos (
		cos ( radians('" . $lat . "') )
		* cos( radians( lat ) )
		* cos( radians( lang ) - radians('" . $lang . "') )
		+ sin ( radians('" . $lat . "') )
		* sin( radians( lat ) )
		)
		) AS distance
		FROM barter_product
		HAVING distance < $distanceToCheck and `user_id`!='" . $user_id . "'"; */
		$sql = "SELECT * FROM barter_product WHERE `user_id`!='".$user_id."' AND `is_sold`='0' ORDER BY rand()";
		//exit;
		
		/* if ($type != '') {
		
		$sql .=" AND find_in_set('" . $type . "',car_types)";
		} */
		
		//$sql .=" ORDER BY distance";
		} else {
		if($key_txt==''){
		$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' AND `is_sold`='0' ORDER BY rand()";
		}
		else{
		$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' AND `is_sold`='0' AND `name` LIKE '%".$key_txt."%' ORDER BY rand()";
		}
		}
		//echo $sql;
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		// exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
		
		$description=substr($products->description,0,100);
		$sql1 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt1 = $db->prepare($sql1);
		$stmt1->bindParam("id", $products->user_id);
		$stmt1->execute();
		$getUserdetails = $stmt1->fetchObject();
		
		if(!empty($getUserdetails))
		{
		
		$user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
		$email=$getUserdetails->email;
		
		if ($getUserdetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
		} else {
		$profile_image =  SITE_URL . 'webservice/no-user.png';
		}
		}
		else
		{
		$profile_image='';  
		}
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $products->cat_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		if(!empty($getcategory))
		{
		$categoryname=$getcategory->name;
		}
		
		
		$productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
            $stmtProductImage = $db->query($productImage);
            $productImages = $stmtProductImage->fetchAll(PDO::FETCH_OBJ);

//print_r($productImages); exit;
            $imdarray = array();
            foreach ($productImages as $myproductImage) {

                $image_url = '';
                if ($myproductImage->image != '') {
                    $imdarray[] = array("images" => SITE_URL . 'upload/product/' . $myproductImage->image);
                } else {
                    $imdarray[] = array("images" => SITE_URL . 'upload/no.png');
                }
//$imdarray[] = array("images" => $image_url);
            }
		
		// print_r($products);
		
		/* if ($resturants->image != '') {
		$resturants_image = SITE_URL . 'upload/product/' . $resturants->image;
		} else {
		$resturants_image = '';
		} */
		
		/*		if(isset($resturants->distance)){
		$finalDiostance=$resturants->distance;  
		}
		else{
		$finalDiostance=10;  
		}*/
		
		/*		$sqlRating = "SELECT AVG(`rating`) as rating FROM `velo_feedback` WHERE `rest_id`='" . $resturants->id . "'";
		$stmtRating = $db->prepare($sqlRating);
		$stmtRating->execute();
		$getRating = $stmtRating->fetchObject();*/
		
		
		
		$data['productsList'][] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		
		"lat" => stripslashes($products->lat),
		
		"lang" => stripslashes($products->lang),
		
		"description" => stripslashes($description),
		"regular_price" => stripslashes($products->regular_price),
		
		"user_id" => stripslashes($products->user_id),
		"user_image" => stripslashes($profile_image),
		"user_name" => stripslashes($user_namee),
		"user_email" => stripslashes($email),
		"cat_id" => stripslashes($products->cat_id),
		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => $imdarray,
		"inventory" => stripslashes($products->inventory),
		);
		}
		
		
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}



function listSwapProducts() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = isset($body->user_id) ? $body->user_id : '';
		$key_txt = isset($body->key_txt) ? $body->key_txt : '';
		
		$db = getConnection();
		
		$sql = "SELECT * FROM barter_user WHERE id=:id";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$userDetails = $stmt->fetchObject();
		
		$lat = isset($userDetails->my_latitude) ? $userDetails->my_latitude : '';
		$lang = isset($userDetails->my_longitude) ? $userDetails->my_longitude : '';
		
		/*		$sqlGetDistance = "SELECT  * FROM velo_distance_radious WHERE `id`='1'";
		$stmtGetDistance = $db->prepare($sqlGetDistance);
		$stmtGetDistance->execute();
		$getDistance = $stmtGetDistance->fetchObject();
		$admin_distance_radious=$getDistance->distance_radious;*/
		
		$admin_distance_radious=1000;
		
		
		$distanceToCheck = $admin_distance_radious;
		//$user_id = 0;
		if ($lat != '' && $lang != '') {
		
		/* $sql = "SELECT
		*, (
		3959 * acos (
		cos ( radians('" . $lat . "') )
		* cos( radians( lat ) )
		* cos( radians( lang ) - radians('" . $lang . "') )
		+ sin ( radians('" . $lat . "') )
		* sin( radians( lat ) )
		)
		) AS distance
		FROM barter_product
		HAVING distance < $distanceToCheck and `user_id`!='" . $user_id . "'"; */
		$sql = "SELECT * FROM barter_product WHERE `user_id`!='".$user_id."' AND `is_sold`='0' ORDER BY rand()";
		//exit;
		
		/* if ($type != '') {
		
		$sql .=" AND find_in_set('" . $type . "',car_types)";
		} */
		
		//$sql .=" ORDER BY distance";
		} else {
		if($key_txt==''){
		$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' AND `is_sold`='0' ORDER BY rand()";
		}
		else{
		$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' AND `is_sold`='0' AND `name` LIKE '%".$key_txt."%' ORDER BY rand()";
		}
		}
		//echo $sql;
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		// exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
		
		$description=substr($products->description,0,100);
		$sql1 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt1 = $db->prepare($sql1);
		$stmt1->bindParam("id", $products->user_id);
		$stmt1->execute();
		$getUserdetails = $stmt1->fetchObject();
		
		if(!empty($getUserdetails))
		{
		
		$user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
		$email=$getUserdetails->email;
		
		if ($getUserdetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
		} else {
		$profile_image =  SITE_URL . 'webservice/no-user.png';
		}
		}
		else
		{
		$profile_image='';  
		}
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $products->cat_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		if(!empty($getcategory))
		{
		$categoryname=$getcategory->name;
		}
		
		
		$productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
            $stmtProductImage = $db->query($productImage);
            $productImages = $stmtProductImage->fetchAll(PDO::FETCH_OBJ);

//print_r($productImages); exit;
            $imdarray = array();
            foreach ($productImages as $myproductImage) {

                $image_url = '';
                if ($myproductImage->image != '') {
                    $imdarray[] = array("images" => SITE_URL . 'upload/product/' . $myproductImage->image);
                } else {
                    $imdarray[] = array("images" => SITE_URL . 'upload/no.png');
                }
//$imdarray[] = array("images" => $image_url);
            }
		
		// print_r($products);
		
		/* if ($resturants->image != '') {
		$resturants_image = SITE_URL . 'upload/product/' . $resturants->image;
		} else {
		$resturants_image = '';
		} */
		
		/*		if(isset($resturants->distance)){
		$finalDiostance=$resturants->distance;  
		}
		else{
		$finalDiostance=10;  
		}*/
		
		/*		$sqlRating = "SELECT AVG(`rating`) as rating FROM `velo_feedback` WHERE `rest_id`='" . $resturants->id . "'";
		$stmtRating = $db->prepare($sqlRating);
		$stmtRating->execute();
		$getRating = $stmtRating->fetchObject();*/
		
		
		
		$data['productsList'][] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		
		"lat" => stripslashes($products->lat),
		
		"lang" => stripslashes($products->lang),
		
		"description" => stripslashes($description),
		"regular_price" => stripslashes($products->regular_price),
		
		"user_id" => stripslashes($products->user_id),
		"user_image" => stripslashes($profile_image),
		"user_name" => stripslashes($user_namee),
		"user_email" => stripslashes($email),
		"cat_id" => stripslashes($products->cat_id),
		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => $imdarray,
		"inventory" => stripslashes($products->inventory),
		);
		}
		
		
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}


		function listProductsByCategory() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = isset($body->user_id) ? $body->user_id : '';
		$category_id = isset($body->category_id) ? $body->category_id : '';
		$key_txt = isset($body->key_txt) ? $body->key_txt : '';
		
		$db = getConnection();
		
		$sql = "SELECT * FROM barter_user WHERE id=:id";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$userDetails = $stmt->fetchObject();
		
		$lat = isset($userDetails->my_latitude) ? $userDetails->my_latitude : '';
		$lang = isset($userDetails->my_longitude) ? $userDetails->my_longitude : '';
		$key_text = isset($userDetails->key_text) ? $userDetails->key_text : '';
		
		/*		$sqlGetDistance = "SELECT  * FROM velo_distance_radious WHERE `id`='1'";
		$stmtGetDistance = $db->prepare($sqlGetDistance);
		$stmtGetDistance->execute();
		$getDistance = $stmtGetDistance->fetchObject();
		$admin_distance_radious=$getDistance->distance_radious;*/
		
		$admin_distance_radious=1000;
		
		
		$distanceToCheck = $admin_distance_radious;
		//$user_id = 0;
		
		if($key_txt==''){
		$sql = "SELECT * from barter_product WHERE cat_id='" . $category_id . "'  AND `is_sold`='0' order by `id` DESC";
		}
		else{
		$sql = "SELECT * from barter_product WHERE cat_id='" . $category_id . "'  AND `is_sold`='0' AND `name` LIKE '%".$key_txt."%' order by `id` DESC";

		}
		
		
		
		//echo $sql;
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		// exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
		
		$description=substr($products->description,0,100);
		$sql1 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt1 = $db->prepare($sql1);
		$stmt1->bindParam("id", $products->user_id);
		$stmt1->execute();
		$getUserdetails = $stmt1->fetchObject();
		
		if(!empty($getUserdetails))
		{
		
		$user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
		$email=$getUserdetails->email;
		
		if ($getUserdetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
		} else {
		$profile_image =  SITE_URL . 'webservice/no-user.png';
		}
		}
		else
		{
		$profile_image='';  
		}
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $products->cat_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		if(!empty($getcategory))
		{
		$categoryname=$getcategory->name;
		}
		
		
		$productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
		$stmtProductImage = $db->query($productImage);
		$productImages = $stmtProductImage->fetchAll(PDO::FETCH_OBJ);
		
		//print_r($productImages); exit;
		$imdarray = array();
		foreach ($productImages as $myproductImage) {
		
		$image_url = '';
		if ($myproductImage->image != '') {
		$imdarray[] = array("images" => SITE_URL . 'upload/product/' . $myproductImage->image);
		} else {
		$imdarray[] = array("images" => SITE_URL . 'upload/no.png');
		}
		//$imdarray[] = array("images" => $image_url);
		}
		
		// print_r($products);
		
		/* if ($resturants->image != '') {
		$resturants_image = SITE_URL . 'upload/product/' . $resturants->image;
		} else {
		$resturants_image = '';
		} */
		
		/*		if(isset($resturants->distance)){
		$finalDiostance=$resturants->distance;  
		}
		else{
		$finalDiostance=10;  
		}*/
		
		/*		$sqlRating = "SELECT AVG(`rating`) as rating FROM `velo_feedback` WHERE `rest_id`='" . $resturants->id . "'";
		$stmtRating = $db->prepare($sqlRating);
		$stmtRating->execute();
		$getRating = $stmtRating->fetchObject();*/
		
		
		
		$data['productsList'][] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		
		"lat" => stripslashes($products->lat),
		
		"lang" => stripslashes($products->lang),
		
		"description" => stripslashes($description),
		"regular_price" => stripslashes($products->regular_price),
		
		"user_id" => stripslashes($products->user_id),
		"user_image" => stripslashes($profile_image),
		"user_name" => stripslashes($user_namee),
		"user_email" => stripslashes($email),
		"cat_id" => stripslashes($products->cat_id),
		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => $imdarray,
		"inventory" => stripslashes($products->inventory),
		);
		}
		
		
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $category_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		$categorynameHeading=$getcategory->name;
		
		
		$data['categorynameHeading']=$categorynameHeading;
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $category_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		$categorynameHeading=$getcategory->name;
		
		
		$data['categorynameHeading']=$categorynameHeading;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}






		function myProducts() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = isset($body->user_id) ? $body->user_id : '';
		
		$db = getConnection();
		

		
		$sql = "SELECT * from barter_product WHERE user_id='" . $user_id . "'";
		
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		// exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
		
		$description=substr($products->description,0,100);
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $products->cat_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		if(!empty($getcategory))
		{
		$categoryname=$getcategory->name;
		}
		
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		
		
		$data['productsList'][] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		"lat" => stripslashes($products->lat),
		"lang" => stripslashes($products->lang),		
		"description" => stripslashes($description),
		"regular_price" => stripslashes($products->regular_price),	
		"user_id" => stripslashes($products->user_id),
		"cat_id" => stripslashes($products->cat_id),
		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($products->inventory),
		);
		}
		
		
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		//$data['msg'] = $e->getMessage();
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}


		function myOfferingProducts() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = isset($body->user_id) ? $body->user_id : '';
		
		$db = getConnection();
		

		
		$sql = "SELECT * from barter_product WHERE user_id='" . $user_id . "' AND `is_sold`='0'";
		
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		// exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
		
		$description=substr($products->description,0,100);
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $products->cat_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		if(!empty($getcategory))
		{
		$categoryname=$getcategory->name;
		}
		
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		
		
		$data['productsList'][] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		"lat" => stripslashes($products->lat),
		"lang" => stripslashes($products->lang),		
		"description" => stripslashes($description),
		"regular_price" => stripslashes($products->regular_price),	
		"user_id" => stripslashes($products->user_id),
		"cat_id" => stripslashes($products->cat_id),
//		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($products->inventory),
		);
		}
		
		
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		//$data['msg'] = $e->getMessage();
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}



		function placeOffer() {
		$data = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = $body->user_id;
		$product_id = $body->product_id;
		$offer_product_id = $body->offer_product_id;
		$additional_price = $body->additional_price;
		$datetime = date("Y-m-d H:i:s");
		$status = 1;
		
		
		$sql = "SELECT * FROM baybarter_product_offers WHERE user_id=:user_id AND product_id=:product_id";
		
		try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("user_id", $user_id);
		$stmt->bindParam("product_id", $product_id);
		$stmt->execute();
		$isExistsWishlist = $stmt->fetchObject();
		$count = $stmt->rowCount();
		if ($count == 0) {
		
		
		$sqlProductDetails = "SELECT * FROM barter_product where id = '".$product_id."'";
		$stmtProductDetails = $db->query($sqlProductDetails);
		$getstmtProductDetails = $stmtProductDetails->fetchObject();

		
		$to_user_id=$getstmtProductDetails->user_id;
		
		
		
		$sql = "INSERT INTO baybarter_product_offers (user_id, to_user_id, product_id,datetime,offer_product_id,additional_price) VALUES (:user_id, :to_user_id, :product_id, :datetime, :offer_product_id, :additional_price)";
		
		$stmt = $db->prepare($sql);
		$stmt->bindParam("user_id", $user_id);
		$stmt->bindParam("to_user_id", $to_user_id);
		$stmt->bindParam("product_id", $product_id);
		$stmt->bindParam("datetime", $datetime);
		$stmt->bindParam("offer_product_id", $offer_product_id);
		$stmt->bindParam("additional_price", $additional_price);
		$stmt->execute();
		$lastID = $db->lastInsertId();
		$data['id'] = $lastID;
		$data['ACK'] = '1';
		



// SENDING NOTIFICATIONS ===========================================


		$sqlProductDetails = "SELECT * FROM barter_product where id = '".$product_id."'";
		$stmtProductDetails = $db->query($sqlProductDetails);
		$getstmtProductDetails = $stmtProductDetails->fetchObject();
	
			
		$to_id=$getstmtProductDetails->user_id;
                 $from=$user_id;
                    $sqlSenderDetails = "SELECT * FROM barter_user  WHERE id='" . $from . "'";
		    $stmtSenderDetails = $db->prepare($sqlSenderDetails);
		    $stmtSenderDetails->execute();
		    $getSenderDetails = $stmtSenderDetails->fetchObject();
                    $from_name= $getSenderDetails->fname;
                    $productid=$product_id;
                    $sqlProductDetails = "SELECT * FROM barter_product  WHERE id='" . $offer_product_id . "'";
		    $stmtProductDetails = $db->prepare($sqlProductDetails);
		    $stmtProductDetails->execute();
		    $getProductDetails = $stmtProductDetails->fetchObject();
                    $product_name=$getProductDetails->name;
		    $type='1';
		    $msg=$from_name.' Has Offered Product '.$product_name;
		    $date=date('Y-m-d H:i:s');
		    $product_or_group=$product_id;
		     $sql = "INSERT INTO barter_notification (product_or_group,from_id, to_id, type, msg, date, last_id) VALUES (:product_or_group, :from_id, :to_id, :type, :msg, :date, :last_id)";
		//$db = getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("product_or_group", $offer_product_id);
		    $stmt->bindParam("from_id", $user_id);
		    $stmt->bindParam("to_id", $to_id);
		    $stmt->bindParam("type", $type);
		    $stmt->bindParam("msg", $msg);
		    $stmt->bindParam("date", $date);
		    $stmt->bindParam("last_id", $lastID);
		    $stmt->execute();			
		
// END OF NOTIFICATIONS =================================================		
		
		$app->response->setStatus(200);
		$data['msg'] = 'Offer Placed Successfully';
		$db = null;
		} else {
		
		$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);
		$data['msg'] = 'Already Placed Offer';
		$db = null;
		}
		//echo json_encode($user);
		} catch (PDOException $e) {
		
		$data['last_id'] = '';
		$data['msg'] = $e->getMessage();;
		$data['Ack'] = '0';
		$app->response->setStatus(401);
		}
		$app->response->write(json_encode($data));
}




		function offerReceived() {
		$data = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$product_id = $body->product_id;
		$datetime = date("Y-m-d H:i:s");
		$status = 1;
		
		
		$sql = "SELECT * FROM baybarter_product_offers WHERE product_id=:product_id ORDER BY `id` DESC";
		
		try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("product_id", $product_id);
		$stmt->execute();
		$offersList = $stmt->fetchAll(PDO::FETCH_OBJ);
		$count = $stmt->rowCount();
		if ($count > 0) {
		
		
		
		foreach ($offersList as $offers) {
		
		$sql2 = "SELECT * FROM  barter_product WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $offers->product_id);
		$stmt2->execute();
		$getProductDetails = $stmt2->fetchObject();
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $offers->product_id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		$productData = array(
		"id" => stripslashes($getProductDetails->id),
		"name" => stripslashes($getProductDetails->name),
		"regular_price" => stripslashes($getProductDetails->regular_price),	
		"user_id" => stripslashes($getProductDetails->user_id),
		"datetime" => stripslashes($getProductDetails->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($getProductDetails->inventory),
		);
		
		
		
		
		$sql3 = "SELECT * FROM  barter_product WHERE id=:id ";
		$stmt3 = $db->prepare($sql3);
		$stmt3->bindParam("id", $offers->offer_product_id);
		$stmt3->execute();
		$getOfferingProductDetails = $stmt3->fetchObject();	
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $offers->offer_product_id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		$offeringProductData = array(
		"id" => stripslashes($getOfferingProductDetails->id),
		"name" => stripslashes($getOfferingProductDetails->name),
		"regular_price" => stripslashes($getOfferingProductDetails->regular_price),	
		"user_id" => stripslashes($getOfferingProductDetails->user_id),
		"datetime" => stripslashes($getOfferingProductDetails->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($getOfferingProductDetails->inventory)
		);		
		
		
		
	$data['offersList'][] = array(
		"id" => stripslashes($offers->id),
		"user_id" => stripslashes($offers->user_id),
		"datetime" => stripslashes($offers->datetime),
		"additional_price" => stripslashes($offers->additional_price),		
		"is_accepted" => stripslashes($offers->is_accepted),
		"accepted_date" => stripslashes($offers->accepted_date),	
		"productData" => $productData,
		"offeringProductData" => $offeringProductData
		);
		}
		
		
			$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);	
		
		
		
		
		} else {
		
		$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);
		$data['msg'] = 'Already Placed Offer';
		$db = null;
		}
		//echo json_encode($user);
		} catch (PDOException $e) {
		
		$data['last_id'] = '';
		$data['msg'] = 'Error!!';
		$data['Ack'] = '0';
		$app->response->setStatus(401);
		}
		$app->response->write(json_encode($data));
}

		function myPlacedOffers() {
		$data = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = $body->user_id;
		$datetime = date("Y-m-d H:i:s");
		$status = 1;
		
		
		$sql = "SELECT * FROM baybarter_product_offers WHERE user_id=:user_id AND status=0 ORDER BY `id` DESC";
		
		try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$offersList = $stmt->fetchAll(PDO::FETCH_OBJ);
		$count = $stmt->rowCount();
		if ($count > 0) {
		
		
		
		foreach ($offersList as $offers) {
		
		$sql2 = "SELECT * FROM  barter_product WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $offers->product_id);
		$stmt2->execute();
		$getProductDetails = $stmt2->fetchObject();
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $offers->product_id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		$productData = array(
		"id" => stripslashes($getProductDetails->id),
		"name" => stripslashes($getProductDetails->name),
		"regular_price" => stripslashes($getProductDetails->regular_price),	
		"uploader_user_id" => stripslashes($getProductDetails->user_id),
		"datetime" => stripslashes($getProductDetails->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($getProductDetails->inventory),
		);
		
		
		
		
		$sql3 = "SELECT * FROM  barter_product WHERE id=:id ";
		$stmt3 = $db->prepare($sql3);
		$stmt3->bindParam("id", $offers->offer_product_id);
		$stmt3->execute();
		$getOfferingProductDetails = $stmt3->fetchObject();	
		
		if($getOfferingProductDetails){
		
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $offers->offer_product_id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		$offeringProductData = array(
		"id" => stripslashes($getOfferingProductDetails->id),
		"name" => stripslashes($getOfferingProductDetails->name),
		"regular_price" => stripslashes($getOfferingProductDetails->regular_price),	
		"uploader_user_id" => stripslashes($getOfferingProductDetails->user_id),
		"datetime" => stripslashes($getOfferingProductDetails->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($getOfferingProductDetails->inventory)
		);		
		
		
		
		
		$data['offersList'][] = array(
		"id" => stripslashes($offers->id),
		"user_id" => stripslashes($offers->user_id),
		"datetime" => stripslashes($offers->datetime),
		"additional_price" => stripslashes($offers->additional_price),		
		"is_accepted" => stripslashes($offers->is_accepted),
		"accepted_date" => stripslashes($offers->accepted_date),	
		"productData" => $productData,
		"offeringProductData" => $offeringProductData
		);
		
		}
		
		}
		
		
		$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);	
		
		
		
		
		} else {
		
		$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);
		$data['msg'] = 'Already Placed Offer';
		$db = null;
		}
		//echo json_encode($user);
		} catch (PDOException $e) {
		
		$data['last_id'] = '';
		$data['msg'] = 'Error!!';
		$data['Ack'] = '0';
		$app->response->setStatus(401);
		}
		$app->response->write(json_encode($data));
}


		function myRecievedOffers() {
		$data = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = $body->user_id;
		$datetime = date("Y-m-d H:i:s");
		$status = 1;
		
		
		$sql = "SELECT * FROM baybarter_product_offers WHERE to_user_id=:to_user_id AND status=0 ORDER BY `id` DESC";
		
		try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("to_user_id", $user_id);
		$stmt->execute();
		$offersList = $stmt->fetchAll(PDO::FETCH_OBJ);
		$count = $stmt->rowCount();
		if ($count > 0) {
		
		
		
		foreach ($offersList as $offers) {
		
		$sql2 = "SELECT * FROM  barter_product WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $offers->product_id);
		$stmt2->execute();
		$getProductDetails = $stmt2->fetchObject();
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $offers->product_id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		$productData = array(
		"id" => stripslashes($getProductDetails->id),
		"name" => stripslashes($getProductDetails->name),
		"regular_price" => stripslashes($getProductDetails->regular_price),	
		"uploader_user_id" => stripslashes($getProductDetails->user_id),
		"datetime" => stripslashes($getProductDetails->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($getProductDetails->inventory),
		);
		
		
		
		
		$sql3 = "SELECT * FROM  barter_product WHERE id=:id ";
		$stmt3 = $db->prepare($sql3);
		$stmt3->bindParam("id", $offers->offer_product_id);
		$stmt3->execute();
		$getOfferingProductDetails = $stmt3->fetchObject();	
		
		if($getOfferingProductDetails){
		
		
		$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $offers->offer_product_id . "'";
		$stmtProductImage1 = $db->query($productImage1);
		$productImages = $stmtProductImage1->fetchObject();
		
		$isImageExists = $stmtProductImage1->rowCount();
		$image_url = "";
		if ($isImageExists > 0) {
		$image_url = SITE_URL . 'upload/product/' . $productImages->image;
		} else {
		$image_url = SITE_URL . 'upload/no.png';
		}
		
		$offeringProductData = array(
		"id" => stripslashes($getOfferingProductDetails->id),
		"name" => stripslashes($getOfferingProductDetails->name),
		"regular_price" => stripslashes($getOfferingProductDetails->regular_price),	
		"uploader_user_id" => stripslashes($getOfferingProductDetails->user_id),
		"datetime" => stripslashes($getOfferingProductDetails->datetime),
		"image" => stripslashes($image_url),
		"inventory" => stripslashes($getOfferingProductDetails->inventory)
		);		
		
		
		
		
		$data['offersList'][] = array(
		"id" => stripslashes($offers->id),
		"user_id" => stripslashes($offers->user_id),
		"datetime" => stripslashes($offers->datetime),
		"additional_price" => stripslashes($offers->additional_price),		
		"is_accepted" => stripslashes($offers->is_accepted),
		"accepted_date" => stripslashes($offers->accepted_date),	
		"productData" => $productData,
		"offeringProductData" => $offeringProductData
		);
		
		}
		
		}
		
		
		$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);	
		
		
		
		
		} else {
		
		$data['last_id'] = '';
		$data['Ack'] = '1';
		$app->response->setStatus(200);
		$data['msg'] = 'Already Placed Offer';
		$db = null;
		}
		//echo json_encode($user);
		} catch (PDOException $e) {
		
		$data['last_id'] = '';
		$data['msg'] = 'Error!!';
		$data['Ack'] = '0';
		$app->response->setStatus(401);
		}
		$app->response->write(json_encode($data));
}







function addToWishlist() {
    $data = array();
    $app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);

    $user_id = $body->user_id;
    $product_id = $body->product_id;
    $date = date("Y-m-d");
    $status = 1;


    $sql = "SELECT * FROM barter_wishlist WHERE user_id=:user_id AND product_id=:product_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("product_id", $product_id);
        $stmt->execute();
        $isExistsWishlist = $stmt->fetchObject();
        $count = $stmt->rowCount();
        if ($count == 0) {
            $sql = "INSERT INTO barter_wishlist (user_id, product_id,date,status) VALUES (:user_id, :product_id, :date, :status)";

            $stmt = $db->prepare($sql);
            $stmt->bindParam("user_id", $user_id);
            $stmt->bindParam("product_id", $product_id);
            $stmt->bindParam("date", $date);
            $stmt->bindParam("status", $status);
            $stmt->execute();
            $lastID = $db->lastInsertId();
            $data['id'] = $lastID;
            $data['ACK'] = '1';
            $app->response->setStatus(200);
            $data['msg'] = 'Added In Your Wishlist';
            $db = null;
        } else {

            $data['last_id'] = '';
            $data['Ack'] = '1';
            $app->response->setStatus(200);
            $data['msg'] = 'Already In Your Wishlist';
            $db = null;
        }
//echo json_encode($user);
    } catch (PDOException $e) {

        $data['last_id'] = '';
        $data['msg'] = 'Error!!';
        $data['Ack'] = '0';
          $app->response->setStatus(401);
    }
    $app->response->write(json_encode($data));
}

function removeFromWishlist() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);

    $user_id = $body->user_id;
    $product_id = $body->product_id;

    $sql = "DELETE FROM barter_wishlist WHERE user_id=:user_id AND product_id=:product_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
      $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("product_id", $product_id);
        //$stmt->bindParam("is_del", $is_del);
        $stmt->execute();

        
        $data['Ack'] = '1';
        $data['msg'] = 'Deleted from wishlist';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Deletion Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}
function productsDetails() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$product_id = isset($body->product_id) ? $body->product_id : '';

		$db = getConnection();
		
		
		
		
		$sql = "SELECT * from barter_product WHERE id='" . $product_id . "'";
		
		
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
	//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);

       // exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
                    
                     $description1=$products->description;
                     $sql1 = "SELECT * FROM barter_user WHERE id=:id ";
                     $stmt1 = $db->prepare($sql1);
                     $stmt1->bindParam("id", $products->user_id);
                     $stmt1->execute();
                     $getUserdetails = $stmt1->fetchObject();



                     
                     if(!empty($getUserdetails))
                     {
                    
                         $user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
                          $email=$getUserdetails->email;
                    
                    if ($getUserdetails->image != '') {
            $profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
        } else {
          $profile_image =  SITE_URL . 'webservice/no-user.png';
        }
                     }
                     else
                     {
                       $profile_image='';  
                     }

                  $sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
                     $stmt2 = $db->prepare($sql2);
                     $stmt2->bindParam("id", $products->cat_id);
                     $stmt2->execute();
                     $getcategory = $stmt2->fetchObject();
                     if(!empty($getcategory))
                     {
                         $categoryname=$getcategory->name;
                     }
                     
                     
				$productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
				$stmtProductImage = $db->query($productImage);
				$productImages = $stmtProductImage->fetchAll(PDO::FETCH_OBJ);				
				//print_r($productImages); exit;
				$imdarray = array();
				foreach ($productImages as $myproductImage) {
				
				$image_url = '';
				if ($myproductImage->image != '') {
				$imdarray[] = array("images" => SITE_URL . 'upload/product/' . $myproductImage->image);
				} else {
				$imdarray[] = array("images" => SITE_URL . 'upload/no.png');
				}
				//$imdarray[] = array("images" => $image_url);
				}
				
				//Similar Product
                                
                                $procatid = $products->cat_id;

             $similarcat = "select * FROM barter_product WHERE `cat_id`='" . $procatid . "' AND id !='".$product_id."' limit 4";
            $stmtsimilarcat = $db->query($similarcat);
            $similarproducts = $stmtsimilarcat->fetchAll(PDO::FETCH_OBJ);

            //echo $similarproducts; 
           
            


            if (!empty($similarproducts)) {
                
               unset($similarproducts1);
	          $similarproducts1=array();

                 // $similarproducts=array();
                foreach ($similarproducts as $similarpro) {

                	//echo $similarpro->id;
                      $name=$similarpro->name;
                      $description=substr($similarpro->description,0,100);
                  
                    $productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $similarpro->id . "'";
                    $stmtProductImage = $db->query($productImage);
                    $productImages = $stmtProductImage->fetchObject();

                    $isImageExists = $stmtProductImage->rowCount();
                    if ($isImageExists > 0) {

                        $image_url_pro1 = SITE_URL . 'upload/product/' . $productImages->image;
                    } else {
                        $image_url_pro1 = SITE_URL . 'upload/no.png';
                    }

               

                 $similarproducts1[] = array(
			"product_id" => stripslashes($similarpro->id),
                         "product_name" => stripslashes($name),
                          "product_description" => stripslashes($description),
			"image" => $image_url_pro1,
			//"desc" => stripslashes(strip_tags($similarpro->description))
			);  


                 //print_r($similarproducts);

                 // $data['similar_products'] = $similarproducts;
                
             
             }

               }


               else {

               	 $similarproducts1= array();
               } 


                //End Similar Product
				
				
           // print_r($products);
		
		/* if ($resturants->image != '') {
		$resturants_image = SITE_URL . 'upload/product/' . $resturants->image;
		} else {
		$resturants_image = '';
		} */
		
/*		if(isset($resturants->distance)){
		$finalDiostance=$resturants->distance;  
		}
		else{
		$finalDiostance=10;  
		}*/
		
/*		$sqlRating = "SELECT AVG(`rating`) as rating FROM `velo_feedback` WHERE `rest_id`='" . $resturants->id . "'";
		$stmtRating = $db->prepare($sqlRating);
		$stmtRating->execute();
		$getRating = $stmtRating->fetchObject();*/
		if($products->regular_price == 0.00) {
            $is_free= 1;
        }
		else {
            $is_free= 0;
        }
		
		$data['productsDetails'] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),		
		"lat" => stripslashes($products->lat),	
		"lang" => stripslashes($products->lang),
		"description" => stripslashes($description1),
		"regular_price" => stripslashes($products->regular_price),
		"user_id" => stripslashes($products->user_id),
		"user_image" => stripslashes($profile_image),
		"user_name" => stripslashes($user_namee),
		"user_email" => stripslashes($email),
		"cat_id" => stripslashes($products->cat_id),
		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => $imdarray,
                "similar_product"=>$similarproducts1,
		"inventory" => stripslashes($products->inventory),
        "prod_condition" => stripslashes($products->prod_condition),
        "group_id"=> stripslashes($products->group_id),
        "want_to_return"=> stripslashes($products->want_to_return),
        "is_free"=> stripslashes($is_free),
        
		);
		}
		
		
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(401);
		}
		
		$app->response->write(json_encode($data));
}



function getWishlist() {
   $data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		
                $user_id = isset($body->user_id) ? $body->user_id : '';

   

        $sql = "SELECT * FROM barter_wishlist WHERE user_id=:user_id";

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->execute();
        $getproducts = $stmt->fetchAll(PDO::FETCH_OBJ);

        $count = $stmt->rowCount();
        if ($count > 0) {
            foreach ($getproducts as $products) {
                $productDetails = "SELECT * from `barter_product` where `id`=" . $products->product_id;

//$productDetails = "select * FROM makeoffer_product WHERE `id`='" . $products->product_id . "'";
                $stmtProductDetails = $db->query($productDetails);
                if ($stmtProductDetails->rowCount() > 0) {
                    $productData = $stmtProductDetails->fetchObject();


                    $productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $productData->id . "'";
                    $stmtProductImage1 = $db->query($productImage1);
                    $productImages = $stmtProductImage1->fetchObject();

                    $isImageExists = $stmtProductImage1->rowCount();
                    $image_url = "";
                    if ($isImageExists > 0) {
                        $image_url = SITE_URL . 'upload/product/' . $productImages->image;
                    } else {
                        $image_url = SITE_URL . 'upload/no.png';
                    }

                    $description=substr($productData->description,0,100);
                     $sql1 = "SELECT * FROM barter_user WHERE id=:id ";
                     $stmt1 = $db->prepare($sql1);
                     $stmt1->bindParam("id", $productData->user_id);
                     $stmt1->execute();
                     $getUserdetails = $stmt1->fetchObject();
                     
                     if(!empty($getUserdetails))
                     {
                    
                         $user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
                          $email=$getUserdetails->email;
                    
                    if ($getUserdetails->image != '') {
            $profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
        } else {
          $profile_image =  SITE_URL . 'webservice/no-user.png';
        }
                     }
                     else
                     {
                       $profile_image='';  
                     }

                  $sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
                     $stmt2 = $db->prepare($sql2);
                     $stmt2->bindParam("id", $productData->cat_id);
                     $stmt2->execute();
                     $getcategory = $stmt2->fetchObject();
                     if(!empty($getcategory))
                     {
                         $categoryname=$getcategory->name;
                     }
                     

                   	$data['productsList'][] = array(
		"id" => stripslashes($productData->id),
		"name" => stripslashes($productData->name),
		
		"lat" => stripslashes($productData->lat),
		
		"lang" => stripslashes($productData->lang),
		
		"description" => stripslashes($description),
                "regular_price" => stripslashes($productData->regular_price),
		
		"user_id" => stripslashes($productData->user_id),
                "user_image" => stripslashes($profile_image),
                     "user_name" => stripslashes($user_namee),
                      "user_email" => stripslashes($email),
		"cat_id" => stripslashes($productData->cat_id),
                "categoryname" => stripslashes($categoryname),
                "offer_type" => stripslashes($productData->offer_type),
                    "datetime" => stripslashes($productData->datetime),
                    "image" => $image_url,
                  "inventory" => stripslashes($productData->inventory),
		);
 
		
		
	   

	  



                }
            }
           
            $data['Ack'] = 1;
            $app->response->setStatus(200);
            $data['msg'] = 'Records Found';
        } else {
           
            $data['Ack'] = 0;
             $app->response->setStatus(200);
            $data['msg'] = 'No Records Found';
        }
    
   $app->response->write(json_encode($data));
}






function listGroup() {
   // echo "aa"; exit;
   $data = array();
   
    $allcategories = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
   $user_id = isset($body->user_id) ? $body->user_id : '';

    
   $sql = "select * FROM barter_grpcategory";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $getgroupCategory = $stmt->fetchAll(PDO::FETCH_OBJ);
  // print_r($getgroupCategory);
  
    if (!empty($getgroupCategory)) {
    foreach ($getgroupCategory as $groupcat) {


       /*    if ($groupcat->image != '') {
                $image = SITE_URL . 'upload/groupcategoryimage/' . $cuisine->image;
            } else {
            $image =  SITE_URL . 'webservice/noimg.png';
            }  */
 


         $groupDetails = "select * FROM barter_group WHERE (find_in_set('" . $groupcat->id . "',group_cat))";
            $stmtgroupDetails = $db->query($groupDetails);
             $groupDataa = $stmtgroupDetails->fetchAll(PDO::FETCH_OBJ);
             
              
                   unset($listgroup);
	           $listgroup=array();
                   
                    foreach ($groupDataa as $groupData)
                    {
             if ($groupData->img != '') {
                $image = SITE_URL . 'upload/group/' . $groupData->img;
            } else {
            $image =  SITE_URL . 'upload/no.png';
            }
               $description=substr($groupData->description,0,60);
            
            $listgroup[] = array(
           
            "groupid" => stripslashes($groupData->id),
            "groupimage" => stripslashes($image),
            "groupname" => stripslashes($groupData->name),
             "description" => stripslashes($description));
            
            
            
                    }   // if (!empty($groupData)) {
             //print_r($groupData); 
          //echo $aa= $groupData->name;
               
            $allcategories[] = array(
            "catid" => stripslashes($groupcat->id),
            "catname" => stripslashes($groupcat->name),
             "allgroup" => $listgroup,
            );

               
               }
               
              
    /*if($groupcat->feature_group =='Yes') {
                      $data['FeatureGroup'][] = array(
            "id" => stripslashes($groupcat->id),
            //"image" => stripslashes($image),
            "name" => stripslashes($groupcat->name),
             "description" => stripslashes($groupcat->description));

                }*/

               /* if($groupcat->discover_group =='Yes') {
                      $data['DiscoverGroup'][] = array(
            "id" => stripslashes($groupcat->id),
            //"image" => stripslashes($image),
            "name" => stripslashes($groupcat->name),
             "description" => stripslashes($groupcat->description));

                }*/

         
         
  
   
    
      $data['allcategories'] = $allcategories;
    $data['Ack'] = 1;
    $app->response->setStatus(200);
    } else {
    $data = array();
    $data['Ack'] = 0;
    $app->response->setStatus(200);
    }
    
    $app->response->write(json_encode($data));


}





	function groupJoin() {
	$data = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$user_id = isset($body->user_id) ? $body->user_id : '';
	$group_id = isset($body->group_id) ? $body->group_id : '';
	$date = date("Y-m-d");
	$status = 1;
	
	
	$sql = "SELECT * FROM barter_group_members WHERE user_id=:user_id AND group_id=:group_id";
	
	try {
	$db = getConnection();
	$stmt = $db->prepare($sql);
	$stmt->bindParam("user_id", $user_id);
	$stmt->bindParam("group_id", $group_id);
	$stmt->execute();
	$isExistsUsergroup = $stmt->fetchObject();
	$count = $stmt->rowCount();
	if ($count == 0) {
            
                	// SENDING NOTIFICATIONS ===========================================
            
                $sqll = "SELECT * FROM barter_group_members WHERE group_id=:group_id ORDER BY `id` DESC";
		$stmtt = $db->prepare($sqll);
		$stmtt->bindParam("group_id", $group_id);
		//$stmt->bindParam("type", $type);
		$stmtt->execute();
		$getmembers = $stmtt->fetchAll(PDO::FETCH_OBJ);	
		
		if (!empty($getmembers)) {
		foreach ($getmembers as $members) {
                    
                    $to=$members->user_id;
                    $from=$user_id;
                    $sqlSenderDetails = "SELECT * FROM barter_user  WHERE id='" . $from . "'";
		    $stmtSenderDetails = $db->prepare($sqlSenderDetails);
		    $stmtSenderDetails->execute();
		    $getSenderDetails = $stmtSenderDetails->fetchObject();
                    $from_name= $getSenderDetails->fname;
                    $groupid=$group_id;
                    $sqlGroupDetails = "SELECT * FROM barter_group  WHERE id='" . $group_id . "'";
		    $stmtGroupDetails = $db->prepare($sqlGroupDetails);
		    $stmtGroupDetails->execute();
		    $getGroupDetails = $stmtGroupDetails->fetchObject();
                    $group_name=$getGroupDetails->name;
                    $type=2;
                    $message=$from_name." Has Joined ".$group_name;
                    $date=date('Y-m-d H:i:s');
                    $last_id=0;
                    $sql = "INSERT INTO barter_notification (product_or_group,from_id, to_id, type, msg, date, last_id) VALUES (:product_or_group, :from_id, :to_id, :type, :msg, :date, :last_id)";
		//$db = getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("product_or_group", $groupid);
		    $stmt->bindParam("from_id", $from);
		    $stmt->bindParam("to_id", $to);
		    $stmt->bindParam("type", $type);
		    $stmt->bindParam("msg", $message);
		    $stmt->bindParam("date", $date);
		    $stmt->bindParam("last_id", $last_id);
		    $stmt->execute();		
                    
                    
                    
                }
                
                }
                // END OF NOTIFICATIONS =================================================		
	
            
            
            
            
	$sql = "INSERT INTO barter_group_members (user_id, group_id, date, status) VALUES (:user_id, :group_id, :date, :status)";
	
	$stmt = $db->prepare($sql);
	$stmt->bindParam("user_id", $user_id);
	$stmt->bindParam("group_id", $group_id);
	$stmt->bindParam("date", $date);
	$stmt->bindParam("status", $status);
	$stmt->execute();
	$lastID = $db->lastInsertId();
	$data['id'] = $lastID;
	
	
	
	
	// SENDING NOTIFICATIONS ===========================================


	//$sqlProductDetails = "SELECT * FROM barter_group where id = '".$group_id."'";
	//$stmtProductDetails = $db->query($sqlProductDetails);
	//$getstmtProductDetails = $stmtProductDetails->fetchObject();
	
			
		//$to_id=$getstmtProductDetails->user_id;
		//$type='2';
		//$msg='';
		//$date=date('Y-m-d H:i:s');
		//$product_or_group=$group_id;
		//$sql = "INSERT INTO barter_notification (product_or_group,from_id, to_id, type, msg, date, last_id) VALUES (:product_or_group, :from_id, :to_id, :type, :msg, :date, :last_id)";
		//$db = getConnection();
		//$stmt = $db->prepare($sql);
		//$stmt->bindParam("product_or_group", $product_or_group);
		//$stmt->bindParam("from_id", $user_id);
		//$stmt->bindParam("to_id", $to_id);
		//$stmt->bindParam("type", $type);
		//$stmt->bindParam("msg", $msg);
		//$stmt->bindParam("date", $date);
		//$stmt->bindParam("last_id", $lastID);
		//$stmt->execute();			
		
// END OF NOTIFICATIONS =================================================		
	
	
	
	
	
	$data['ACK'] = '1';
	$app->response->setStatus(200);
	$data['msg'] = 'Added In the Group';
	$db = null;
	} else {
	
	$data['last_id'] = '';
	$data['Ack'] = '2';
	$app->response->setStatus(200);
	$data['msg'] = 'Already Joined Group';
	$db = null;
	}
	//echo json_encode($user);
	} catch (PDOException $e) {
	
	$data['last_id'] = '';
	$data['msg'] = 'Error!!';
	$data['Ack'] = '0';
	$app->response->setStatus(401);
	}
	$app->response->write(json_encode($data));
}




		function myNotifications() {
		
		$resturantsList = array();
		$proImg = array();
		$data = array();
		
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$to_id = $body->user_id;
		//$type = $body->type;

		
		
		try {
		$db = getConnection();
		
		
	//	$sql = "SELECT * FROM barter_notification WHERE to_id=:to_id AND type=:type ORDER BY `id` DESC";
		$sql = "SELECT * FROM barter_notification WHERE to_id=:to_id ORDER BY `id` DESC";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("to_id", $to_id);
		//$stmt->bindParam("type", $type);
		$stmt->execute();
		$getNotifications = $stmt->fetchAll(PDO::FETCH_OBJ);	
		
		if (!empty($getNotifications)) {
		foreach ($getNotifications as $notifications) {
		
		
		
		$sqlSenderDetails = "SELECT * FROM barter_user  WHERE id='" . $notifications->from_id . "'";
		$stmtSenderDetails = $db->prepare($sqlSenderDetails);
		$stmtSenderDetails->execute();
		$getSenderDetails = $stmtSenderDetails->fetchObject();
		
		if ($getSenderDetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getSenderDetails->image;
		} else {
		$profile_image =  SITE_URL . 'webservice/no-user.png';
		}
		
		//if($type=='1'){
		
		
		$sqlProductDetails = "SELECT * FROM barter_product where id = '".$notifications->product_or_group."'";
		$stmtProductDetails = $db->query($sqlProductDetails);
		$getstmtProductDetails = $stmtProductDetails->fetchObject();
		
		$sqlUserDetails = "SELECT * FROM barter_user where id = '".$notifications->from_id."'";
		$stmtUserDetails = $db->query($sqlUserDetails);
		$getUserDetails = $stmtUserDetails->fetchObject();		
				
		$message = $getSenderDetails->fname.' '.$getSenderDetails->lname . ' Has Placed an Offer.';
		
		
		$resturantsList[] = array(
		'id' => stripslashes($notifications->id),
		'user_id' => stripslashes($notifications->from_id),
		'profile_image' => $profile_image,
		'user_name' => stripslashes($getUserDetails->fname.' '.$getUserDetails->lname),
		'date' => stripslashes($notifications->date),
		'message' => stripslashes($notifications->msg),

		
		);
		}
		
		
/*		$is_notified=1;
		$sql = "UPDATE oladg_notification SET is_notified=:is_notified WHERE notifing_id=:notifing_id";
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("notifing_id", $user_id);
		$stmt->bindParam("is_notified", $is_notified);
		//$stmt->bindParam("quantity", $quantity);
		$stmt->execute();	*/		
		
		
		
		//print_r($resturantsList);
		// die;
		$data['notificationList'] = $resturantsList;
		$app->response->setStatus(200);
		$data['Ack'] = 1;
		} else {
		
		$data['notificationList'] = '';
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$db = null;
		} catch (PDOException $e) {
		$data['id'] = '';
		$data['Ack'] = '0';
		$data['msg'] = $e->getMessage();
		$app->response->setStatus(401);
		}
		$app->response->write(json_encode($data));
}





function groupDetails() {
		$data = array();
                $GroupDetails = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$group_id = isset($body->group_id) ? $body->group_id : '';

		$db = getConnection();
		
		
		
		
		$sql = "SELECT * from barter_group WHERE id='" . $group_id . "'";
		
		
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
	//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$groupList = $stmt->fetchObject();

       // exit;
		
		if (!empty($groupList)) {
		
                    
                     $description=$groupList->description;
                     $group_owner='Admin';
                     $owner_image = SITE_URL . 'upload/user_images/allpro_2.png';
                     if ($groupList->img != '') {
                $image = SITE_URL . 'upload/group/' . $groupList->img;
            } else {
            $image =  SITE_URL . 'upload/no.png';
            }
                     
                     $product_cat=$groupList->product_cat;
                      $grpids = explode(',', $product_cat);
                       $final_groupids = "'" . implode("','", $grpids) . "'";
                       
                            $sqlpro = "SELECT * from barter_product WHERE cat_id IN($final_groupids)";
                            
                            $stmt7 = $db->query($sqlpro);
                         
                            $productList = $stmt7->fetchAll(PDO::FETCH_OBJ);
                            $productsDetails=array();
                            foreach ($productList as $products) {
                       
                     $sql1 = "SELECT * FROM barter_user WHERE id=:id ";
                     $stmt1 = $db->prepare($sql1);
                     $stmt1->bindParam("id", $products->user_id);
                     $stmt1->execute();
                     $getUserdetails = $stmt1->fetchObject();
                     
                     if(!empty($getUserdetails))
                     {
                    
                         $user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
                          $email=$getUserdetails->email;
                    
                    if ($getUserdetails->image != '') {
            $profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
        } else {
          $profile_image =  SITE_URL . 'webservice/no-user.png';
        }
                     }
                     else
                     {
                       $profile_image='';  
                     }
                     
                

                     $sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
                     $stmt2 = $db->prepare($sql2);
                     $stmt2->bindParam("id", $products->cat_id);
                     $stmt2->execute();
                     $getcategory = $stmt2->fetchObject();
                     if(!empty($getcategory))
                     {
                         $categoryname=$getcategory->name;
                     }
                     
                     
                     $productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
            $stmtProductImage = $db->query($productImage);
            $productImages = $stmtProductImage->fetchAll(PDO::FETCH_OBJ);

//print_r($productImages); exit;
            $imdarray = array();
            foreach ($productImages as $myproductImage) {

                $image_url = '';
                if ($myproductImage->image != '') {
                    $imdarray[] = array("images" => SITE_URL . 'upload/product/' . $myproductImage->image);
                } else {
                    $imdarray[] = array("images" => SITE_URL . 'upload/no.png');
                }
//$imdarray[] = array("images" => $image_url);
            }
            
            
             
		
			$productsDetails[] = array(
			"id" => stripslashes($products->id),
			"name" => stripslashes($products->name),
			
			"lat" => stripslashes($products->lat),
			
			"lang" => stripslashes($products->lang),
			
			"description" => stripslashes($description),
			"regular_price" => stripslashes($products->regular_price),
			
			"user_id" => stripslashes($products->user_id),
			"user_image" => stripslashes($profile_image),
			"user_name" => stripslashes($user_namee),
			"user_email" => stripslashes($email),
			"cat_id" => stripslashes($products->cat_id),
			"categoryname" => stripslashes($categoryname),
			"offer_type" => stripslashes($products->offer_type),
			"datetime" => stripslashes($products->datetime),
			"image" => $imdarray,
			"inventory" => stripslashes($products->inventory),
			);
		
		
                            }
                            
                            $memberImage = "select * FROM barter_group_members WHERE `group_id`='" . $group_id . "'";
            $stmtmemberImage = $db->query($memberImage);
            $memberImages = $stmtmemberImage->fetchAll(PDO::FETCH_OBJ);
            $memberCount = $stmtmemberImage->rowCount();
            $memberDetails=array();
             
             
                
            foreach ($memberImages as $mymemberImage) {
                
                $sql11 = "SELECT * FROM barter_user WHERE id=:id ";
                     $stmt11 = $db->prepare($sql11);
                     $stmt11->bindParam("id", $mymemberImage->user_id);
                     $stmt11->execute();
                     $getUserdetailss = $stmt11->fetchObject();
                     
                     if(!empty($getUserdetailss))
                     {
                    
                         $user_nameee=$getUserdetailss->fname.' '.$getUserdetailss->lname;
                          $emaill=$getUserdetailss->email;
                    
                    if ($getUserdetailss->image != '') {
            $profile_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailss->image;
        } else {
          $profile_imagee =  SITE_URL . 'webservice/no-user.png';
        }
                     }
                     else
                     {
                       $profile_imagee='';  
                     }
                
                

			$memberDetails[] = array(
			"id" => stripslashes($mymemberImage->user_id),
			"username" => stripslashes($user_nameee),
			"email" => stripslashes($emaill),
			"user_image" => stripslashes($profile_imagee),
			);
                

            }
         
            
                            
		$GroupDetails = array(
		"group_id" => stripslashes($groupList->id),
		"group_name" => stripslashes($groupList->name),
		"group_owner" => stripslashes($group_owner),
		"owner_image" => stripslashes($owner_image),
		"group_image" => $image,
		"no_member"=>$memberCount,
		"allproducts" => $productsDetails,
		"memberdetails" => (!empty($memberDetails)?$memberDetails:''),
           
            );
                  $data['GroupDetails'] = $GroupDetails;
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}

		function groupeventDetails() {
		$data = array();
		$GroupDetails = array();
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$group_id = isset($body->group_id) ? $body->group_id : '';
		
		$db = getConnection();
		
		$sql = "SELECT * from barter_group WHERE id='" . $group_id . "'";
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$groupList = $stmt->fetchObject();		
		// exit;		
		if (!empty($groupList)) {
		
		
		$description=$groupList->description;
		$group_owner='Admin';
		$owner_image = SITE_URL . 'upload/user_images/allpro_2.png';
		if ($groupList->img != '') {
		$image = SITE_URL . 'upload/group/' . $groupList->img;
		} else {
		$image =  SITE_URL . 'upload/no.png';
		}
		
		$product_cat=$groupList->product_cat;
		$grpids = explode(',', $product_cat);
		$final_groupids = "'" . implode("','", $grpids) . "'";
		
		$sqlpro = "SELECT * from barter_event WHERE `group_id`='".$group_id."'";
		
		$stmt7 = $db->query($sqlpro);
		
		$productList = $stmt7->fetchAll(PDO::FETCH_OBJ);
		
		foreach ($productList as $products) {
		
		
		if ($products->img != '') {
		$imageevent = SITE_URL . 'upload/group/' . $products->img;
		} else {
		$imageevent =  SITE_URL . 'upload/no.png';
		}
		
		
		
		
		$eventDetails[] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		"description" => stripslashes(substr(strip_tags($description),0,50)),
		"image" => $imageevent,
		"time" => stripslashes($products->time),
		"date" => stripslashes($products->date),
		);
		
		
		}
		
		$memberImage = "select * FROM barter_group_members WHERE `group_id`='" . $group_id . "'";
		$stmtmemberImage = $db->query($memberImage);
		$memberImages = $stmtmemberImage->fetchAll(PDO::FETCH_OBJ);
		$nomember = $stmtmemberImage->rowCount();
		
		$memberDetails=array();
		
		foreach ($memberImages as $mymemberImage) {
		
		$sql11 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt11 = $db->prepare($sql11);
		$stmt11->bindParam("id", $mymemberImage->user_id);
		$stmt11->execute();
		$getUserdetailss = $stmt11->fetchObject();
		
		if(!empty($getUserdetailss))
		{		
		$user_nameee=$getUserdetailss->fname.' '.$getUserdetailss->lname;
		$emaill=$getUserdetailss->email;
		
		if ($getUserdetailss->image != '') {
		$profile_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailss->image;
		} else {
		$profile_imagee =  SITE_URL . 'webservice/no-user.png';
		}
		}
		else
		{
		$profile_imagee='';  
		}
		
		
		
		$memberDetails[] = array(
		"id" => stripslashes($mymemberImage->user_id),
		"username" => stripslashes($user_nameee),
		"email" => stripslashes($emaill),
		"user_image" => stripslashes($profile_imagee),
		);
		
		
		}
		
		
		$GroupDetails = array(
		"group_id" => stripslashes($groupList->id),
		"group_name" => stripslashes($groupList->name),
		"group_owner" => stripslashes($group_owner),
		"owner_image" => stripslashes($owner_image),
		"group_image" => $image,
		"no_member" => $nomember,
		"allevents" => (!empty($eventDetails)?$eventDetails:''),
		"memberdetails" => (!empty($memberDetails)?$memberDetails:'')
		
		);
		$data['GroupDetails'] = $GroupDetails;
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}


	function eventDetails() {
	$data = array();
	$GroupDetails = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$event_id = isset($body->event_id) ? $body->event_id : '';	
	$db = getConnection();	
	$sql = "SELECT * from barter_event WHERE id='" . $event_id . "'";
	
	$db = getConnection();
	$stmt = $db->prepare($sql);
	//	$stmt->bindParam("user_id", $user_id);
	$stmt->execute();
	$eventDetails = $stmt->fetchObject();
	
	// exit;
	
	if (!empty($eventDetails)) {
	
	$memberImage = "select * FROM barter_join_event WHERE `event_id`='" . $event_id . "'";
	$stmtmemberImage = $db->query($memberImage);
	$memberImages = $stmtmemberImage->fetchAll(PDO::FETCH_OBJ);
	$nomember = $stmtmemberImage->rowCount();
	
	$memberDetails=array();
	
	foreach ($memberImages as $mymemberImage) {
	
	$sql11 = "SELECT * FROM barter_user WHERE id=:id ";
	$stmt11 = $db->prepare($sql11);
	$stmt11->bindParam("id", $mymemberImage->user_id);
	$stmt11->execute();
	$getUserdetailss = $stmt11->fetchObject();
	
	if(!empty($getUserdetailss))
	{		
	$user_nameee=$getUserdetailss->fname.' '.$getUserdetailss->lname;
	$emaill=$getUserdetailss->email;
	
	if ($getUserdetailss->image != '') {
	$profile_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailss->image;
	} else {
	$profile_imagee =  SITE_URL . 'webservice/no-user.png';
	}
	}
	else
	{
	$profile_imagee='';  
	}
	
	
	
	$memberDetails[] = array(
	"id" => stripslashes($mymemberImage->user_id),
	"username" => stripslashes($user_nameee),
	"email" => stripslashes($emaill),
	"user_image" => stripslashes($profile_imagee),
	);
	
	
	}
	

	if ($eventDetails->img != '') {
	$event_imagee = SITE_URL . 'upload/group/' . $eventDetails->img;
	} else {
	$event_imagee =  SITE_URL . 'webservice/no-user.png';
	}


	$eventDetailsData = array(
	"id" => stripslashes($eventDetails->id),
	"name" => stripslashes($eventDetails->name),
	"description" => strip_tags($eventDetails->description),
	"event_imagee" => $event_imagee,
	"members" => $memberDetails,
	"address" => stripslashes($eventDetails->fulladdress),
	"time" => stripslashes($eventDetails->time),
	"date" => stripslashes($eventDetails->date),
	);

	
	$data['eventDetailsData'] = $eventDetailsData;
	$data['Ack'] = 1;
	$app->response->setStatus(200);
	} else {
	$data = array();
	$data['Ack'] = 0;
	$app->response->setStatus(200);
	}
	
	$app->response->write(json_encode($data));
}


	function eventJoin() {
	$data = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$user_id = isset($body->user_id) ? $body->user_id : '';
	$event_id = isset($body->event_id) ? $body->event_id : '';
	$date = date("Y-m-d");
	$status = 1;
	
	
	$sql = "SELECT * FROM barter_join_event WHERE user_id=:user_id AND event_id=:event_id";
	
	try {
	$db = getConnection();
	$stmt = $db->prepare($sql);
	$stmt->bindParam("user_id", $user_id);
	$stmt->bindParam("event_id", $event_id);
	$stmt->execute();
	$isExistsUsergroup = $stmt->fetchObject();
	$count = $stmt->rowCount();
	if ($count == 0) {
            
            
                $sqll = "SELECT * FROM barter_join_event WHERE event_id=:event_id ORDER BY `id` DESC";
		$stmtt = $db->prepare($sqll);
		$stmtt->bindParam("event_id", $event_id);
		//$stmt->bindParam("type", $type);
		$stmtt->execute();
		$getmembers = $stmtt->fetchAll(PDO::FETCH_OBJ);	
		
		if (!empty($getmembers)) {
		foreach ($getmembers as $members) {
                    
                    $to=$members->user_id;
                    $from=$user_id;
                    $sqlSenderDetails = "SELECT * FROM barter_user  WHERE id='" . $from . "'";
		    $stmtSenderDetails = $db->prepare($sqlSenderDetails);
		    $stmtSenderDetails->execute();
		    $getSenderDetails = $stmtSenderDetails->fetchObject();
                    $from_name= $getSenderDetails->fname;
                      $eventid=$event_id;
                    $sqlEventDetails = "SELECT * FROM barter_event  WHERE id='" . $event_id . "'";
		    $stmtEventDetails = $db->prepare($sqlEventDetails);
		    $stmtEventDetails->execute();
		    $getEventDetails = $stmtEventDetails->fetchObject();
                    $event_name=$getEventDetails->name;
                 
                    $type=3;
                   $message=$from_name." Has Joined ".$event_name." Event";
                    $date=date('Y-m-d H:i:s');
                    $last_id=0;
                    $sql = "INSERT INTO barter_notification (product_or_group,from_id, to_id, type, msg, date, last_id) VALUES (:product_or_group, :from_id, :to_id, :type, :msg, :date, :last_id)";
		//$db = getConnection();
		    $stmt = $db->prepare($sql);
		    $stmt->bindParam("product_or_group", $eventid);
		    $stmt->bindParam("from_id", $from);
		    $stmt->bindParam("to_id", $to);
		    $stmt->bindParam("type", $type);
		    $stmt->bindParam("msg", $message);
		    $stmt->bindParam("date", $date);
		    $stmt->bindParam("last_id", $last_id);
		    $stmt->execute();		
                    
                    
                    
                }
                
                }
                // END OF NOTIFICATIONS =================================================		
	
            
            
            
            
	$sql = "INSERT INTO barter_join_event (user_id, event_id, date) VALUES (:user_id, :event_id, :date)";
	
	$stmt = $db->prepare($sql);
	$stmt->bindParam("user_id", $user_id);
	$stmt->bindParam("event_id", $event_id);
	$stmt->bindParam("date", $date);
	$stmt->execute();
	$lastID = $db->lastInsertId();
	$data['id'] = $lastID;
	$data['ACK'] = '1';
	$app->response->setStatus(200);
	$data['msg'] = 'Added In the Event';
	$db = null;
	} else {
	
	$data['last_id'] = '';
	$data['Ack'] = '1';
	$app->response->setStatus(200);
	$data['msg'] = 'Already Joined Event';
	$db = null;
	}
	//echo json_encode($user);
	} catch (PDOException $e) {
            
            print_r($e);
            exit;
	
	$data['last_id'] = '';
	$data['msg'] = 'Error!!';
	$data['Ack'] = '0';
	$app->response->setStatus(401);
	}
	$app->response->write(json_encode($data));
}


function listFreeProducts() {
		$data = array();            
		$app = \Slim\Slim::getInstance();
		$request = $app->request();
		$body2 = $app->request->getBody();
		$body = json_decode($body2);
		
		$user_id = isset($body->user_id) ? $body->user_id : '';
		$key_txt = isset($body->key_txt) ? $body->key_txt : '';
		
		$db = getConnection();
		
		$sql = "SELECT * FROM barter_user WHERE id=:id";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$userDetails = $stmt->fetchObject();
		
		$lat = isset($userDetails->my_latitude) ? $userDetails->my_latitude : '';
		$lang = isset($userDetails->my_longitude) ? $userDetails->my_longitude : '';
		
		/*		$sqlGetDistance = "SELECT  * FROM velo_distance_radious WHERE `id`='1'";
		$stmtGetDistance = $db->prepare($sqlGetDistance);
		$stmtGetDistance->execute();
		$getDistance = $stmtGetDistance->fetchObject();
		$admin_distance_radious=$getDistance->distance_radious;*/
		
		$admin_distance_radious=1000;
		
		
		$distanceToCheck = $admin_distance_radious;
		//$user_id = 0;
		if ($lat != '' && $lang != '') {
		
		/* $sql = "SELECT
		*, (
		3959 * acos (
		cos ( radians('" . $lat . "') )
		* cos( radians( lat ) )
		* cos( radians( lang ) - radians('" . $lang . "') )
		+ sin ( radians('" . $lat . "') )
		* sin( radians( lat ) )
		)
		) AS distance
		FROM barter_product
		HAVING distance < $distanceToCheck and `user_id`!='" . $user_id . "'"; */
		$sql = "SELECT * FROM barter_product WHERE `user_id`!='".$user_id."' and `regular_price`='0.00' AND `is_sold`='0' LIMIT 1";
		//exit;
		
		/* if ($type != '') {
		
		$sql .=" AND find_in_set('" . $type . "',car_types)";
		} */
		
		//$sql .=" ORDER BY distance";
		} else {
		
	//	$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' LIMIT 1";
		
		
		
		if($key_txt==''){
		$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' and `regular_price`='0.00' AND `is_sold`='0'  LIMIT 1";
		}
		else{
		$sql = "SELECT * from barter_product WHERE user_id<>'" . $user_id . "' AND `is_sold`='0' AND `name` LIKE '%".$key_txt."%' and `regular_price`='0.00'  LIMIT 1";
		}
		
		
		}
		//echo $sql;
		
		$db = getConnection();
		$stmt = $db->prepare($sql);
		//	$stmt->bindParam("user_id", $user_id);
		$stmt->execute();
		$productList = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		// exit;
		
		if (!empty($productList)) {
		foreach ($productList as $products) {
		
		$description=substr($products->description,0,100);
		$sql1 = "SELECT * FROM barter_user WHERE id=:id ";
		$stmt1 = $db->prepare($sql1);
		$stmt1->bindParam("id", $products->user_id);
		$stmt1->execute();
		$getUserdetails = $stmt1->fetchObject();
		
		if(!empty($getUserdetails))
		{
		
		$user_namee=$getUserdetails->fname.' '.$getUserdetails->lname;
		$email=$getUserdetails->email;
		
		if ($getUserdetails->image != '') {
		$profile_image = SITE_URL . 'upload/user_images/' . $getUserdetails->image;
		} else {
		$profile_image =  SITE_URL . 'webservice/no-user.png';
		}
		}
		else
		{
		$profile_image='';  
		}
		
		$sql2 = "SELECT * FROM  barter_category WHERE id=:id ";
		$stmt2 = $db->prepare($sql2);
		$stmt2->bindParam("id", $products->cat_id);
		$stmt2->execute();
		$getcategory = $stmt2->fetchObject();
		if(!empty($getcategory))
		{
		$categoryname=$getcategory->name;
		}
		
		
		$productImage = "select * FROM barter_moreimage WHERE `pro_id`='" . $products->id . "'";
            $stmtProductImage = $db->query($productImage);
            $productImages = $stmtProductImage->fetchAll(PDO::FETCH_OBJ);

//print_r($productImages); exit;
            $imdarray = array();
            foreach ($productImages as $myproductImage) {

                $image_url = '';
                if ($myproductImage->image != '') {
                    $imdarray[] = array("images" => SITE_URL . 'upload/product/' . $myproductImage->image);
                } else {
                    $imdarray[] = array("images" => SITE_URL . 'upload/no.png');
                }
//$imdarray[] = array("images" => $image_url);
            }
		
		// print_r($products);
		
		/* if ($resturants->image != '') {
		$resturants_image = SITE_URL . 'upload/product/' . $resturants->image;
		} else {
		$resturants_image = '';
		} */
		
		/*		if(isset($resturants->distance)){
		$finalDiostance=$resturants->distance;  
		}
		else{
		$finalDiostance=10;  
		}*/
		
		/*		$sqlRating = "SELECT AVG(`rating`) as rating FROM `velo_feedback` WHERE `rest_id`='" . $resturants->id . "'";
		$stmtRating = $db->prepare($sqlRating);
		$stmtRating->execute();
		$getRating = $stmtRating->fetchObject();*/
		
		
		
		$data['productsList'][] = array(
		"id" => stripslashes($products->id),
		"name" => stripslashes($products->name),
		
		"lat" => stripslashes($products->lat),
		
		"lang" => stripslashes($products->lang),
		
		"description" => stripslashes($description),
		"regular_price" => stripslashes($products->regular_price),
		
		"user_id" => stripslashes($products->user_id),
		"user_image" => stripslashes($profile_image),
		"user_name" => stripslashes($user_namee),
		"user_email" => stripslashes($email),
		"cat_id" => stripslashes($products->cat_id),
		"categoryname" => stripslashes($categoryname),
		"offer_type" => stripslashes($products->offer_type),
		"datetime" => stripslashes($products->datetime),
		"image" => $imdarray,
		"inventory" => stripslashes($products->inventory),
		);
		}
		
		
		$data['Ack'] = 1;
		$app->response->setStatus(200);
		} else {
		$data = array();
		$data['Ack'] = 0;
		$app->response->setStatus(200);
		}
		
		$app->response->write(json_encode($data));
}



function getCms() {
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    $id = $body->id;
    
    $sql = "SELECT * from barter_cms WHERE id=:id";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam("id", $id);
    $stmt->execute();
    $cmdetails = $stmt->fetchAll(PDO::FETCH_OBJ);
    
    if (!empty($cmdetails)) {
    foreach ($cmdetails as $cms) {
    
    $data['cmsdetails']= array(
    "id" => stripslashes($cms->id),
    "pagename" => stripslashes($cms->pagename),
     "title" => stripslashes($cms->title),
    "pagedetails" => stripslashes($cms->pagedetail));
    }
    
    
    $data['Ack'] = 1;
    $app->response->setStatus(200);
    } else {
    $data = array();
    $data['Ack'] = 0;
    $app->response->setStatus(200);
    }
    
    $app->response->write(json_encode($data));
}





function myGroup() {
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
	$allgroup=array();
    
    $user_id = isset($body->user_id) ? $body->user_id : '';
    //$id = $body->id;
    
    $sql = "SELECT * from barter_group_members WHERE user_id='" . $user_id . "'";
    
    $db = getConnection();
    $stmt = $db->prepare($sql);
    //$stmt->bindParam("user_id", $user_id);
    $stmt->execute();
    $myGroupList = $stmt->fetchAll(PDO::FETCH_OBJ);
    
    if (!empty($myGroupList)) {
    foreach ($myGroupList as $mgrplist) {
        
       $sql2 = "SELECT * FROM  barter_group WHERE id=:id";
       $stmt2 = $db->prepare($sql2);
       $stmt2->bindParam("id", $mgrplist->group_id);
       $stmt2->execute();
       $getgroup = $stmt2->fetchObject();
       if(!empty($getgroup))
       {
       $groupname=$getgroup->name;
       $groupdesc=$getgroup->description;
	   
	   
		if ($getgroup->img != '') {
		$image = SITE_URL . 'upload/group/' . $getgroup->img;
		} else {
		$image =  SITE_URL . 'upload/no.png';
		}
		$description=substr($getgroup->description,0,60);
	   
	   
	   
   
	
	$allgroup[]= array(
    "id" => stripslashes($getgroup->id),
    "name" => stripslashes($getgroup->name),
     "groupimage" => stripslashes($image),
    "description" => stripslashes($description));	   
	   
       }
        
        
    

    
    }
    
     $data['myGroupList']=$allgroup;
    $data['Ack'] = 1;
    $app->response->setStatus(200);
    } else {
    $data = array();
    $data['Ack'] = 0;
	 $data['myGroupList']=array();
    $app->response->setStatus(200);
    }
    
    $app->response->write(json_encode($data));
}







function listingComplete() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);

    $id = $body->id;
    $is_sold = '1';
    //$user_id = isset($body->user_id) ? $body->user_id: '';
     //$is_del=1;
    
    $sql = "UPDATE barter_product SET is_sold=:is_sold WHERE id=:id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->bindParam("is_sold", $is_sold);
        //$stmt->bindParam("is_del", $is_del);
        $stmt->execute();

        
        $data['Ack'] = '1';
        $data['msg'] = 'Product Completed';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}

// Cancel offer 
function cancelOffer() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);

    $id = $body->id;
    //$user_id = isset($body->user_id) ? $body->user_id: '';
     //$is_del=1;

    $sql = "UPDATE baybarter_product_offers SET status=1 WHERE id=:id";    

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        //$stmt->bindParam("is_del", $is_del);
        $stmt->execute();

        
        $data['Ack'] = '1';
        $data['msg'] = 'Offer Canceled';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}

function listSocial() {
	$data = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	//$user_id = isset($body->user_id) ? $body->user_id : '';
	
	$sql = "SELECT * from barter_social";
        
	$db = getConnection();
	$stmt = $db->prepare($sql);
	//$stmt->bindParam("user_id", $user_id);
	$stmt->execute();
	$getAllCards = $stmt->fetchAll(PDO::FETCH_OBJ);
	
	if (!empty($getAllCards)) {
	foreach ($getAllCards as $cards) {
	
	$data['socialDetails'][]= array(
	"id" => stripslashes($cards->id),
	"name" => stripslashes($cards->name),
        "link" => stripslashes($cards->link));
        //"status" => stripslashes($cards->status));
	}
	
	
	$data['Ack'] = 1;
	$app->response->setStatus(200);
	} else {
	$data = array();
	$data['Ack'] = 0;
	$app->response->setStatus(200);
	}
	
	$app->response->write(json_encode($data));
} 
 






	function sendChat() {
	
	date_default_timezone_set('GMT');
	
	$data = array();
    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
	
	$sender_id = $body->user_id;
	$receiver_id = $body->to_id;
	$product_id = $body->product_id;
	$message = $body->message;
	$date_time = gmdate("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")));
	$status = 0;
	$parent_id = 0;
        $is_read = 1;
	
	$sql = "INSERT INTO barter_chat (sender_id, receiver_id, product_id, message,date_time, status, is_read) VALUES (:sender_id, :receiver_id, :product_id, :message, :date_time, :status, :is_read)";
	try {
	 $db = getConnection();
	$stmt = $db->prepare($sql);
	$stmt->bindParam("sender_id", $sender_id);
	$stmt->bindParam("receiver_id", $receiver_id);
	$stmt->bindParam("product_id", $product_id);
	$stmt->bindParam("message", $message);
	$stmt->bindParam("date_time", $date_time);
	$stmt->bindParam("status", $status);
        $stmt->bindParam("is_read", $is_read);
	$stmt->execute();
	
	//$data['id'] = $db->lastInsertId();
	$lastID = $db->lastInsertId();
	
	
	//$data['chat_list'] = $chatlist;
	$data['ACK'] = '1';
	$data['MSG'] = 'Message Sent.';
	   $app->response->setStatus(200);
	//print_r($data);die;
	$db = null;
	} catch (PDOException $e) {
	
	//print_r($e);
	   $app->response->setStatus(401);
	$data['id'] = '';
	$data['ACK'] = '0';
	$data['MSG'] = 'Message Not Sent. Please Try Again.';
	}
$app->response->write(json_encode($data));
}





function countChat() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    
    //$product_id = $body->product_id;    
    $reciver_id = $body->reciver_id;
    $is_read = 1;
        
    //$user_id = isset($body->user_id) ? $body->user_id: '';
     //$is_del=1;
    //$sql = "SELECT count(*) from barter_chat WHERE receiver_id='$reciver_id' AND is_read='1'"; 
                        
    
$sql = "SELECT * from barter_chat WHERE receiver_id=:reciver_id AND is_read=:is_read";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
       
       //$stmt->bindParam("product_id", $product_id);
       $stmt->bindParam("reciver_id", $reciver_id);
       $stmt->bindParam("is_read", $is_read);
        $stmt->execute();
        
        $getChats = $stmt->fetchAll(PDO::FETCH_OBJ);	                
	$count = $stmt->rowCount();

        
        $data['Ack'] = '1';
        $data['msg'] = $count;

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        //$data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}










function unreadChat() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    $reciver_id = $body->reciver_id;
    $product_id = $body->product_id;
    $sender_id = $body->sender_id;
    
    $is_read = '0';
    //$user_id = isset($body->user_id) ? $body->user_id: '';
     //$is_del=1;
    
    $sql = "UPDATE barter_chat SET is_read=:is_read WHERE receiver_id=:reciver_id AND product_id=:product_id AND sender_id=:sender_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("reciver_id", $reciver_id);
        $stmt->bindParam("product_id", $product_id);
         $stmt->bindParam("sender_id", $sender_id);
        $stmt->bindParam("is_read", $is_read);
        //$stmt->bindParam("is_del", $is_del);
        $stmt->execute();

        
        $data['Ack'] = '1';
        $data['msg'] = 'Message Read';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        //$data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}











	function getallConvesssion() {
	
	$data = array();
	$allproducts = array();
	$reciver = array();
	$allmessagearray = array();
	
    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
	
	$sender_id = $body->sender_id;
	$reciver_id = $body->reciver_id;
	$product_id = $body->product_id;
//	$last_id = $body->last_id;
	
	try {
	
	
	$sql = "SELECT * FROM barter_chat WHERE ((sender_id='".$sender_id."' and receiver_id='".$reciver_id."') or (sender_id='".$reciver_id."' and receiver_id='".$sender_id."')) and product_id='".$product_id."' order by date_time ASC";
	
	$db = getConnection();
	$stmt = $db->prepare($sql);
/*	$stmt->bindParam("sender_id", $sender_id);
	$stmt->bindParam("reciver_id", $reciver_id);
	$stmt->bindParam("product_id", $product_id);*/
	$stmt->execute();
	$getChats = $stmt->fetchAll(PDO::FETCH_OBJ);
	$count = $stmt->rowCount();	
	
	if ($count > 0) {
	foreach ($getChats as $chats) {
	
	
	
	
	
	$productDetails = "select * FROM barter_product WHERE `id`='" . $chats->product_id . "'";
	$stmtProductDetails = $db->query($productDetails);
	$productData = $stmtProductDetails->fetchObject();	
	
	/*$productImage1 = "select * FROM barter_moreimage WHERE `pro_id`='" . $productData->id . "'";
	$stmtProductImage1 = $db->query($productImage1);
	$productImages = $stmtProductImage1->fetchObject();
	
	$isImageExists = $stmtProductImage1->rowCount();
	if ($isImageExists > 0) {
	$image_url = SITE_URL . 'upload/product/' . $productImages->image;
	} else {
	$image_url = SITE_URL . 'upload/no.png';
	}*/
	
	$reciverd = "select * FROM barter_user WHERE `id`='" . $chats->receiver_id . "'";
	$stmtUsertImage1 = $db->query($reciverd);
	$receiverDetails = $stmtUsertImage1->fetchObject();
	
	if ($receiverDetails->image != '') {
	$image_url1 = SITE_URL . 'upload/user_images/' . $receiverDetails->image;
	} else {
	$image_url1 = SITE_URL . 'upload/no.png';
	}
	
	$send = "select * FROM barter_user WHERE `id`='" . $chats->sender_id . "'";
	$stmtUsertImage2 = $db->query($send);
	$senderDetails = $stmtUsertImage2->fetchObject();
	
	if ($senderDetails->image != '') {
	$image_url2 = SITE_URL . 'upload/user_images/' . $senderDetails->image;
	} else {
	$image_url2 = SITE_URL . 'upload/no.png';
	}
	
	
	
/*	$allproducts = array(
	"id" => stripslashes($productData->id),
	"name" => stripslashes($productData->name),
	"desc" => stripslashes(strip_tags($productData->description)),
	"price" => stripslashes($productData->regular_price),
	"image" => stripslashes($image_url));*/
	
	
	$reciver = array(
	"id" => stripslashes($receiverDetails->id),
	"fname" => stripslashes($receiverDetails->fname),
	"lname" => stripslashes($receiverDetails->lname),
	"gender" => stripslashes($receiverDetails->gender),
	"image" => stripslashes($image_url1));
	
	
	$sender = array(
	"id" => stripslashes($senderDetails->id),
	"fname" => stripslashes($senderDetails->fname),
	"lname" => stripslashes($senderDetails->lname),
	"gender" => stripslashes($senderDetails->gender),
	"image" => stripslashes($image_url2));
	
	
	
	
	
	
	$allmessagearray[] = array(
	"id" => stripslashes($chats->id),
	"sender_id" => stripslashes($chats->sender_id),
	"receiver_id" => stripslashes($chats->receiver_id),
	"sender_image" => stripslashes($image_url2),
//	"receiver_image" => stripslashes($chats->receiver_id),
	"sender" => $sender,
	"reciver" => $reciver,
	"message" => stripslashes($chats->message),
	"date" => gmdate("Y-m-d H:i:s", strtotime($chats->date_time)));
	
	}
	

	//$data['chat_list'] = array_reverse($allmessagearray);
	$data['chat_list'] = $allmessagearray;

	$data['ACK'] = 1;
	$data['msg'] = 'Records Found';
   $app->response->setStatus(200);
	
	} else {
	$data['chat_list'] = array();
	$data['ACK'] = 2;
	$data['msg'] = 'No Records Found';
		$app->response->setStatus(200);
	}
	
	} catch (PDOException $e) {
	//print_r($e);
	$data['product_details'] = array();
	$data['reciver_user'] = array();
	$data['chat_list'] = array();
	$data['ACK'] = 0;
	$data['msg'] = 'Data Error';
	$app->response->setStatus(401);
	}
	$app->response->write(json_encode($data));
}

	function blockUser() {
	
	date_default_timezone_set('GMT');
	
	$data = array();
    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
	
	$user_id = $body->user_id;
	$block_to_userid = $body->block_to_userid;
	$date = gmdate("Y-m-d H:i:s");
	$status = 1;
	
	
	
	try {
	
	$sql_exists = "SELECT * FROM `barter_block` WHERE  `user_id`='" . $user_id . "' and `block_to_userid`='" . $block_to_userid . "'";
	$db = getConnection();
	$stmtimgexists = $db->prepare($sql_exists);
	$stmtimgexists->execute();
	$num_exists = $stmtimgexists->rowCount();
	
	if ($num_exists == 0) {
	$sql = "INSERT INTO `barter_block` (user_id, block_to_userid, date, status) VALUES (:user_id, :block_to_userid, :date, :status)";
	$stmt = $db->prepare($sql);
	$stmt->bindParam("user_id", $user_id);
	$stmt->bindParam("block_to_userid", $block_to_userid);
	$stmt->bindParam("date", $date);
	$stmt->bindParam("status", $status);
	$stmt->execute();
	
	$data['ACK'] = '1';
	$data['MSG'] = 'Blocked';
		$app->response->setStatus(200);
	} else {
	
	$data['ACK'] = '1';
	$data['MSG'] = 'Already Blocked';
		$app->response->setStatus(200);
	}
	
	$db = null;
	} catch (PDOException $e) {
//	print_r($e);
	$data['ACK'] = '0';
	$data['MSG'] = 'Data Error!!';
	}
	$app->response->write(json_encode($data));
}


	function unblockUser() {
	
	date_default_timezone_set('GMT');
	
	$data = array();
    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
	
	$user_id = $body->user_id;
	$block_to_userid = $body->block_to_userid;
	$date = gmdate("Y-m-d H:i:s");
	$status = 1;
	
	try {
	
	$sql_exists = "DELETE FROM `barter_block` WHERE  `user_id`='" . $user_id . "' and `block_to_userid`='" . $block_to_userid . "'";
	$db = getConnection();
	$stmtimgexists = $db->prepare($sql_exists);
	$stmtimgexists->execute();
	$num_exists = $stmtimgexists->rowCount();
	
	$data['ACK'] = '1';
	$data['MSG'] = 'unblocked Successfully..';
		$app->response->setStatus(200);
	$db = null;
	} catch (PDOException $e) {
	//print_r($e);
	$data['ACK'] = '0';
	$data['MSG'] = 'Data Error!!';
		$app->response->setStatus(401);
	}
	$app->response->write(json_encode($data));
}



	function getBlockedUsers() {
	$data = array();
	$GroupDetails = array();
	$app = \Slim\Slim::getInstance();
	$request = $app->request();
	$body2 = $app->request->getBody();
	$body = json_decode($body2);
	
	$user_id = isset($body->user_id) ? $body->user_id : '';
	
	$db = getConnection();
	
	$sql = "SELECT * from barter_block WHERE user_id='" . $user_id . "'";
	
	//$db = getConnection();
	$stmt = $db->prepare($sql);
	//	$stmt->bindParam("user_id", $user_id);
	$stmt->execute();
	$userList = $stmt->fetchAll(PDO::FETCH_OBJ);
	
	// exit;
	
	if (!empty($userList)) {
	
	
	foreach ($userList as $mymemberImage) {
	
	//print_r($mymemberImage);
	
	$sql11 = "SELECT * FROM barter_user WHERE id='".$mymemberImage->block_to_userid."' ";
	$stmt11 = $db->prepare($sql11);
	//$stmt11->bindParam("id", $mymemberImage->block_to_userid);
	$stmt11->execute();
	$getUserdetailss = $stmt11->fetchObject();
	
	if(!empty($getUserdetailss))
	{
	
	$user_nameee=$getUserdetailss->fname.' '.$getUserdetailss->lname;
	$emaill=$getUserdetailss->email;
	
	if ($getUserdetailss->image != '') {
	$profile_imagee = SITE_URL . 'upload/user_images/' . $getUserdetailss->image;
	} else {
	$profile_imagee =  SITE_URL . 'webservice/no-user.png';
	}
	}
	else
	{
	$profile_imagee='';  
	}
	
	
	
	$memberDetails[] = array(
	"id" => stripslashes($mymemberImage->id),
	"user_id" => stripslashes($mymemberImage->block_to_userid),
	"username" => stripslashes($user_nameee),
	"email" => stripslashes($emaill),
	"user_image" => stripslashes($profile_imagee),
   // "blockedstatus" ==> stripslashes($mymemberImage->block_to_userid),
	);
	
	
	}	
	
	$data['blockedUsers'] = $memberDetails;
		$data['Ack'] = 1;
	$data['msg'] = 'Records Found...';
	$app->response->setStatus(200);
	} else {
	$data = array();
	$data['blockedUsers'] = array();
	$data['Ack'] = 0;
	$data['msg'] = 'No Records Found...';
	$app->response->setStatus(200);
	}
	
	$app->response->write(json_encode($data));
}


function chatBlockuser() {
    $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
     $user_id = $body->user_id;
     $block_to_userid = $body->block_to_userid;
 
    
   $sql = "SELECT * from barter_block WHERE user_id=:user_id AND block_to_userid=:block_to_userid";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam("user_id", $user_id);
    $stmt->bindParam("block_to_userid", $block_to_userid);
    $stmt->execute();
    $blockeduserdetails = $stmt->fetchAll(PDO::FETCH_OBJ);
    
    if (!empty($blockeduserdetails)) {
    foreach ($blockeduserdetails as $blockeduser) {
    
    $data['blockeduserstatus']= array(
    "status" => stripslashes($blockeduser->status)
	);
    } 
    
    
    $data['Ack'] = 1;
    $app->response->setStatus(200);
    } else {
    $data = array();
	
	 $data['blockeduserstatus']= array(
    "status" => 0
	);
    $data['Ack'] = 0;
    $app->response->setStatus(200);
    }
    
    $app->response->write(json_encode($data)); 
}













function groupEventSetting() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    //$offer_received_status= isset($body->offer_received_status) ? $body->offer_received_status: '';
    $user_id = $body->user_id;        
    $group_event_status = $body->group_event_status;

    $sql = "UPDATE barter_user SET group_event_status=:group_event_status WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("group_event_status", $group_event_status);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Group Event Status Update';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}




function offerReceiveStatus() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    //$offer_received_status= isset($body->offer_received_status) ? $body->offer_received_status: '';
    $user_id = $body->user_id;        
    $offer_received_status = $body->offer_received_status;

    $sql = "UPDATE barter_user SET offer_received_status=:offer_received_status WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("offer_received_status", $offer_received_status);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Offer Received Status Update';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}





function messageStatus() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    //$offer_received_status= isset($body->offer_received_status) ? $body->offer_received_status: '';
    $user_id = $body->user_id;        
    $message_status = $body->message_status;

    $sql = "UPDATE barter_user SET message_status=:message_status WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("message_status", $message_status);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Message Status Update';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}





function forum_reply_status() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    //$offer_received_status= isset($body->offer_received_status) ? $body->offer_received_status: '';
    $user_id = $body->user_id;        
    $forum_reply_status = $body->forum_reply_status;

    $sql = "UPDATE barter_user SET forum_reply_status=:forum_reply_status WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("forum_reply_status", $forum_reply_status);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Forum Reply Update';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}


function watchlist_complete_status() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    //$offer_received_status= isset($body->offer_received_status) ? $body->offer_received_status: '';
    $user_id = $body->user_id;        
    $watchlist_complete_status = $body->watchlist_complete_status;

    $sql = "UPDATE barter_user SET watchlist_complete_status=:watchlist_complete_status WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("watchlist_complete_status", $watchlist_complete_status);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Watchlist Status Update';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}



function other_notification_status() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    //$offer_received_status= isset($body->offer_received_status) ? $body->offer_received_status: '';
    $user_id = $body->user_id;        
    $other_notification_status = $body->other_notification_status;

    $sql = "UPDATE barter_user SET other_notification_status=:other_notification_status WHERE id=:user_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("user_id", $user_id);
        $stmt->bindParam("other_notification_status", $other_notification_status);
        $stmt->execute();

        $data['user_id'] = $user_id;
        $data['Ack'] = '1';
        $data['msg'] = 'Other Notification Status Update';

        $app->response->setStatus(200);
        $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        $data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = $e->getMessage();;

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}



function notificationsettingDetails() {
   
      $data = array();
    $app = \Slim\Slim::getInstance();
    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
  $user_id = $body->user_id;

    
    $sql = "SELECT * from barter_user WHERE id=:id";
    $db = getConnection();
    $stmt = $db->prepare($sql);
    $stmt->bindParam("id", $user_id);
    $stmt->execute();
    $settingdetails = $stmt->fetchAll(PDO::FETCH_OBJ);
    
    if (!empty($settingdetails)) {
    foreach ($settingdetails as $sdetails) {
    
    $data['settingdetails']= array(
    "id" => stripslashes($sdetails->id),
    "offer_received_status" => stripslashes($sdetails->offer_received_status),
     "message_status" => stripslashes($sdetails->message_status),
      "watchlist_complete_status" => stripslashes($sdetails->watchlist_complete_status),
       "other_notification_status" => stripslashes($sdetails->other_notification_status),
        "group_event_status" => stripslashes($sdetails->group_event_status),
    "forum_reply_status" => stripslashes($sdetails->forum_reply_status));
    }
    
    
    $data['Ack'] = 1;
    $app->response->setStatus(200);
    } else {
    $data = array();
    $data['Ack'] = 0;
    $app->response->setStatus(200);
    }
    
    $app->response->write(json_encode($data)); 
}




function listunreadchat() {
    $data = array();

    $app = \Slim\Slim::getInstance();

    $request = $app->request();
    $body2 = $app->request->getBody();
    $body = json_decode($body2);
    
    
    //$product_id = $body->product_id;    
    $reciver_id = $body->reciver_id;
    $is_read = 1;
        
    //$user_id = isset($body->user_id) ? $body->user_id: '';
     //$is_del=1;
    //echo $sql = "SELECT  from barter_chat WHERE receiver_id='$reciver_id' AND is_read='1'";  exit;

    //echo $sql = "SELECT * from barter_chat WHERE receiver_id='$reciver_id' AND is_read='1' GROUP BY sender_id";

   // exit;
                        
    
$sql = "SELECT * from barter_chat WHERE receiver_id=:reciver_id AND is_read=:is_read GROUP BY sender_id,receiver_id,product_id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
       
       //$stmt->bindParam("product_id", $product_id);
       $stmt->bindParam("reciver_id", $reciver_id);
       $stmt->bindParam("is_read", $is_read);
        $stmt->execute();
        
        $getChats = $stmt->fetchAll(PDO::FETCH_OBJ);                    
    //$count = $stmt->rowCount();

        if(!empty($getChats)) {
        foreach ($getChats as $chat) {

            //echo $chat->sender_id;

            $sqlSenderDetails = "SELECT * FROM barter_user  WHERE id='" . $chat->sender_id . "'";
        $stmtSenderDetails = $db->prepare($sqlSenderDetails);
        $stmtSenderDetails->execute();
        $getSenderDetails = $stmtSenderDetails->fetchObject();
        
        if ($getSenderDetails->image != '') {
        $profile_image = SITE_URL . 'upload/user_images/' . $getSenderDetails->image;
        } else {
        $profile_image =  SITE_URL . 'webservice/no-user.png';
        }

       
        $listunreadchat[] = array(
        'id' => stripslashes($chat->id),
        'sender_id' => stripslashes($chat->sender_id),
        'product_id' => stripslashes($chat->product_id),
        'profile_image' => $profile_image,
        'user_name' => stripslashes($getSenderDetails->fname.' '.$getSenderDetails->lname),
        'date' => stripslashes($chat->date_time),
        'message' => stripslashes($chat->message),

        
        );
 }
        

        
        $data['Ack'] = '1';
       // $data['msg'] = $count;
        $data['listunreadchat'] = $listunreadchat;

        $app->response->setStatus(200);

    }
    else {
        $data['listunreadchat'] = '';
        $data['Ack'] = 0;
        $app->response->setStatus(200);
    }
    $db = null;
//    print_r($user);
    } catch (PDOException $e) {
//print_r($e);
        //$data['user_id'] = '';
        $data['Ack'] = 0;
        $data['msg'] = 'Error!!!';

        $app->response->setStatus(401);
    }

    $app->response->write(json_encode($data));
}



$app->run();
?>